let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css')
mix.styles([
   		'resources/assets/admincp/css/icons/icomoon/styles.css',
   		'resources/assets/admincp/css/bootstrap.css',
   		'resources/assets/admincp/css/core.css',
   		'resources/assets/admincp/css/components.css',
   		'resources/assets/admincp/css/colors.css',
   		'resources/assets/admincp/font-awesome-4.6.3/css/font-awesome.min.css',
   		'resources/assets/admincp/sweetalert/sweetalert2.min.css',
   		'resources/assets/admincp/css/style.css',
   	], 'public/css/vendor.css')
   .scripts([
   		'resources/assets/admincp/js/plugins/loaders/pace.min.js',
   		'resources/assets/admincp/js/core/libraries/jquery.min.js',
   		'resources/assets/admincp/js/core/libraries/bootstrap.min.js',
   		'resources/assets/admincp/js/plugins/loaders/blockui.min.js',
   		'resources/assets/admincp/js/plugins/sliders/ion_rangeslider.min.js',
   		'resources/assets/admincp/js/plugins/visualization/d3/d3.min.js',
	      'resources/assets/admincp/js/plugins/visualization/d3/d3_tooltip.js',
	      'resources/assets/admincp/js/plugins/forms/styling/switchery.min.js',
	      'resources/assets/admincp/js/plugins/forms/styling/uniform.min.js',
	      'resources/assets/admincp/js/plugins/forms/selects/bootstrap_multiselect.js',
	      'resources/assets/admincp/js/plugins/ui/moment/moment.min.js',
	      'resources/assets/admincp/js/plugins/pickers/daterangepicker.js',
	      'resources/assets/admincp/js/plugins/tables/datatables/datatables.min.js',
	      'resources/assets/admincp/js/plugins/tables/datatables/extensions/select.min.js',
	      'resources/assets/admincp/js/plugins/forms/selects/select2.min.js',
	      'resources/assets/admincp/js/plugins/forms/styling/switch.min.js',
	      'resources/assets/admincp/js/plugins/forms/tags/tagsinput.min.js',
	      'resources/assets/admincp/js/plugins/visualization/c3/c3.min.js',
	      'resources/assets/admincp/js/core/app.js',
	      'resources/assets/admincp/js/plugins/ui/ripple.min.js',
	      'resources/assets/admincp/js/plugins/notifications/jgrowl.min.js',
	      'resources/assets/admincp/sweetalert/sweetalert2.min.js',
         'resources/assets/admincp/js/apps.js',
   	], 'public/js/vendor.js')
   // .styles([
   //    'resources/assets/admincp/ckeditor/skins/bootstrapck/editor.css',
   //    ], 'public/css/ckeditor.css')
   // .scripts([
   //       'resources/assets/admincp/ckeditor/ckeditor.js',
   //       'resources/assets/admincp/ckeditor/config.js',
   //       'resources/assets/admincp/ckeditor/styles.js',
   //       'resources/assets/admincp/ckeditor/lang/en.js',
   // ], 'public/js/ckeditor.js')
   ;

