@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - add page </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">add page</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">เพิ่มข้อมูล</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <fieldset class="content-group">
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">ทัวร์นาเมนท์ <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-list"></i></span>
                                    <input type="text" id="txt_title" name="txt_title" class="form-control" required="required" value="" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">หมวดของทัวร์นาเมนท์ <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-list"></i></span>
                                    <select id="txt_category" name="txt_category" class="form-control">
                                        <option value="">>> หมวดทัวร์นาเมนท์ <<</option>
                                        <option value="Thai">Thai</option>
                                        <option value="Sea">Sea</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">ประเภทของทัวร์นาเมนท์ <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-list"></i></span>
                                    <select id="txt_tournament_type" name="txt_tournament_type" class="form-control">
                                        <option value="">>> ประเภททัวร์นาเมนท์ <<</option>
                                        <option value="Single Elimination">Single Elimination</option>
                                        <option value="Round Robin">Round Robin</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">จำนวนที่แข่งขัน <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-list"></i></span>
                                    <select id="txt_best_of" name="txt_best_of" class="form-control">
                                        <option value="">>> Best of? <<</option>
                                        <option value="1">Best of 1</option>
                                        <option value="3">Best of 3</option>
                                        <option value="5">Best of 5</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">จำกัดจำนวนทีม <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <input type="text" id="txt_limit" name="txt_limit" class="form-control" required="required" value="" maxlength="5" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">จำนวนกลุ่มที่ต้องการแบ่ง <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-list"></i></span>
                                    <select id="txt_group" name="txt_group" class="form-control">
                                        <option value="">>> แบ่งกลุ่ม <<</option>
                                        <option value="1">1 กลุ่ม</option>
                                        <option value="2">2 กลุ่ม</option>
                                        <option value="4">4 กลุ่ม</option>
                                        <option value="8">8 กลุ่ม</option>
                                        <option value="16">16 กลุ่ม</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">วันเวลาแสดงผล <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                    <input type="text" id="txt_show_datetime" name="txt_show_datetime" class="form-control" required="required" value="" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">ลงทะเบียนในช่วง <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                    <input type="text" id="txt_register_period" name="txt_register_period" class="form-control" required="required" value="" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">เริ่มแข่งขันในช่วง <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                    <input type="text" id="txt_tournament_period" name="txt_tournament_period" class="form-control" required="required" value="" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">ไลฟ์สตีม <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="txt_live_steam" name="txt_live_steam" class="form-control" required="required" value="" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">ภาพแบนเนอร์</label>
                            <div class="col-lg-9">
                                <input type="file" id="txt_image" name="txt_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 522(W) x 150(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </div>
            </div>
        </div>
        


    </div>


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">รายละเอียด</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <textarea name="txt_details" class="summernote-height"></textarea>
                    </div>
                </div>
            </div>

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">กฎ-กติกา</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <textarea name="txt_rules" class="summernote-height"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">

            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        <button type="button" id="btn-save" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('script')
<script>
function detailsAndRulesUploadImage(file,editor,welEditable) {

    var formData = new FormData();
    formData.append("file", file);
    $.ajax({
        type: 'POST',
        {{-- url: "{{ route('hon.tournament.detailsAndRulesUploadImage') }}", --}}
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
            $('.summernote-height').summernote(
                'insertImage',
                data.message
            );
        },
        error: function (xhr, type) {
            status_submit = false;
        }
    });

}

$(document).ready(function() {
    App.initSwitch(".switch");

    $('.summernote-height').summernote({
        height: 500,
        toolbar: [
            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
            ['font', ['color']],
            ['insert', ['picture', 'link']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['misc', ['fullscreen', 'codeview']]
        ],
        callbacks: {
            onImageUpload: function(files,editor,welEditable) {
                detailsAndRulesUploadImage(files[0], editor, welEditable);
            }
        }
    });

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('input[name="txt_register_period"], input[name="txt_tournament_period"]').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('click', '#btn-save', function(event) {
        var msg = '';

        if($('#txt_title').val().trim() == '')
            msg = 'กรุณากรอก หัวข้อ';

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{
            saveCreate();
        }
        return false;
    });

    $('body').on('change', 'input[name="txt_image"]', function() {

        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });

    function saveCreate() {

        var formData = new FormData($('#form-data')[0]);
        $.ajax({
            type: 'POST',
            {{-- url: "{{ route('hon.tournament.saveEdit') }}", --}}
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        {{-- window.location.href = "{{ route('hon.tournament.index') }}"; --}}
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });

    }
});
</script>
@endsection