@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - edit page - {{$slug}} </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">edit page</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <input type="hidden" name="txt_slug" id="txt_slug" value="{{ $slug }}">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">แก้ไขข้อมูล</h5>
                    
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <a href="#modal_add_new_section" data-toggle="modal" class="btn btn-primary hide"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Section </a>
                    <a href="#modal_add_new_element" data-toggle="modal" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Element </a>
                </div>
            </div>
        </div>

        @foreach($count_section as $section)
        
               
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Section {{ $section }}</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                
                <div class="panel-body">
                    <fieldset class="content-group">

                        @foreach($datas as $data)

                            @if($section == $data->section_id)

                                @if($data->types == 'txt')

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">ข้อความ <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="{{ $data->id.'--content' }}" name="{{ $data->id.'--content' }}" class="form-control" required="required" value="{{ $data->content  }}" >
                                            </div>
                                            <div class="help-block">
                                                <span class="label label-success arrowed-right arrowed-in main__color">
                                                    ให้แก้ไขได้เฉพาะข้อความเท่านั้น
                                                </span>
                                                <span class="label label-danger arrowed-right arrowed-in">
                                                    ห้ามแก้ไข tag หรือสัญลักษณ์อื่นๆ
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'content')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>
                                    <hr>

                                @endif()

                                @if($data->types == 'bg')

                                    @if(!empty($data->src))
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">Background src <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-link"></i></span>
                                                <input type="text" id="{{ $data->id.'--src' }}" name="{{ $data->id.'--src' }}" class="form-control" required="required" value="{{ $data->src  }}" >
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'src')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>

                                    @else
                                    
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Background src</label>
                                        <div class="col-lg-8">
                                            <input type="file" id="txt_image" name="txt_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                            <br>
                                            <div class="help-block">
                                                <span class="label label-success arrowed-right arrowed-in main__color">
                                                    รูปควรมีขนาด 522(W) x 150(H) PX
                                                </span>
                                                <span class="label label-danger arrowed-right arrowed-in">
                                                    รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveImage({{ $data->id }})"> <i class="icon-checkmark"></i></button>
                                        </div>
                                    </div>
                                    @endif
                                    
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">Background alt <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="{{ $data->id.'--alt' }}" name="{{ $data->id.'--alt' }}" class="form-control" required="required" value="{{ $data->alt  }}" >
                                            </div>
                                            <div class="input-group">
                                                <img src="{{ $data->src }}" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'alt')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>
                                    <hr>

                                @endif()

                                @if($data->types == 'img')

                                    @if(!empty($data->src))

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">Image src<span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-link"></i></span>
                                                <input type="text" id="{{ $data->id.'--src' }}" name="{{ $data->id.'--src' }}" class="form-control" required="required" value="{{ $data->src  }}" >
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'src')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>

                                    @else

                                    <div class="form-group">
                                        <label class="control-label col-lg-2">Image src</label>
                                        <div class="col-lg-8">
                                            <input type="file" id="txt_image" name="txt_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                            <br>
                                            <div class="help-block">
                                                <span class="label label-success arrowed-right arrowed-in main__color">
                                                    รูปควรมีขนาด 522(W) x 150(H) PX
                                                </span>
                                                <span class="label label-danger arrowed-right arrowed-in">
                                                    รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveImage({{ $data->id }})"> <i class="icon-checkmark"></i></button>
                                        </div>
                                    </div>

                                    @endif

                                    

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">Image alt<span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="{{ $data->id.'--alt' }}" name="{{ $data->id.'--alt' }}" class="form-control" required="required" value="{{ $data->alt  }}" >
                                            </div>
                                            <div class="input-group">
                                                <img src="{{ $data->src }}" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'alt')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>
                                    <hr>

                                @endif()

                                @if($data->types == 'btn')

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">Button <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="{{ $data->id.'--content' }}" name="{{ $data->id.'--content' }}" class="form-control" required="required" value="{{ $data->content  }}" >
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'content')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2"> Link </label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-link"></i></span>
                                                <input type="text" id="{{ $data->id.'--url' }}" name="{{ $data->id.'--url' }}" class="form-control" required="required" value="{{ $data->url  }}" >
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'url')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>
                                    <hr>

                                @endif()

                                @if($data->types == 'vdo')

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">Video <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-link"></i></span>
                                                <input type="text" id="{{ $data->id.'--url' }}" name="{{ $data->id.'--url' }}" class="form-control" required="required" value="{{ $data->url  }}" >
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'url')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>
                                    <hr>

                                @endif()

                                @if($data->types == 'editor')

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">Editor <span class="text-danger">*</span></label>
                                        <div class="col-lg-8 fo4_page_summernote">
                                            <textarea id="{{ $data->id.'--content' }}" name="{{ $data->id.'--content' }}" class="summernote-height">{{ $data->content }}</textarea>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'content')"> <i class="icon-checkmark"></i></button>
                                            <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                                        </div>
                                    </div>
                                    <hr>

                                @endif()

                            @endif

                        @endforeach()

                    </fieldset>
                    <div class="text-right">
                        <button type="button" class="btn btn-danger hide" title="Delete This Section" onclick="deleteSection({{ $section }})"> <i class="icon-bin"></i></button>
                    </div>
                </div>

            </div>

        </div>
        
        @endforeach()

    </div>


    <div class="row">
        
        <div class="col-md-12">
            
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        {{-- <button type="button" id="btn-save" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

<!-- Basic modal -->
    <div id="modal_add_new_section" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Add New section</h5>
                </div>

                <div class="modal-body">

                    <form class="form-horizontal" action="#" id="form_add_new_section">
                        <fieldset class="content-group">

                            <div class="form-group">
                                <label class="control-label col-lg-2">Section :</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" placeholder="Name" name="modal_add_new_section_id" id="modal_add_new_section_id" value="{{ $max_section+1 }}" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Element :</label>
                                <div class="col-lg-10">
                                    <select name="modal_add_new_section_element_type" id="modal_add_new_section_element_type" class="form-control">
                                        <option value="txt">Text</option>
                                        <option value="bg">Background</option>
                                        <option value="img">Image</option>
                                        <option value="btn">Button</option>
                                        <option value="vdo">Video</option>
                                        <option value="editor">Editor</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addNewSection()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal_add_new_element" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Add New element</h5>
                </div>

                <div class="modal-body">

                    <form class="form-horizontal" action="#" id="form_add_new_section">
                        <fieldset class="content-group">

                            <div class="form-group">
                                <label class="control-label col-lg-2">Section :</label>
                                <div class="col-lg-10">
                                    <select name="modal_add_new_element_section_id" id="modal_add_new_element_section_id" class="form-control">
                                        @foreach($count_section as $section)
                                        <option value="{{$section}}">{{$section}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Element :</label>
                                <div class="col-lg-10">
                                    <select name="modal_add_new_element_element_type" id="modal_add_new_element_element_type" class="form-control">
                                        <option value="txt">Text</option>
                                        <option value="bg">Background</option>
                                        <option value="img">Image</option>
                                        <option value="btn">Button</option>
                                        <option value="vdo">Video</option>
                                        <option value="editor">Editor</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addNewElement()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<!-- /basic modal -->
@endsection

@section('script')
<script>

    function addNewSection() {
        var section_id = $('#modal_add_new_section_id').val();
        var element_type = $('#modal_add_new_section_element_type').val();

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.page.addSection') }}",
            data: { 
                'slug': slug,
                'section_id': section_id,
                'element_type': element_type
                },
            dataType: 'json',
            // processData: false,  // tell jQuery not to process the data
            // contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

    function addNewElement() {
        var section_id = $('#modal_add_new_element_section_id').val();
        var element_type = $('#modal_add_new_element_element_type').val();
        console.log(section_id);
        console.log(element_type);

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.page.addElement') }}",
            data: { 
                'slug': slug,
                'section_id': section_id,
                'element_type': element_type
                },
            dataType: 'json',
            // processData: false,  // tell jQuery not to process the data
            // contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

    function deleteSection(id) {

        console.log(id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function () {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.page.deleteSection') }}",
                data: { 
                    'id': id
                    },
                dataType: 'json',
                // processData: false,  // tell jQuery not to process the data
                // contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                   status_submit = false;
                }
            });
        }, function (dismiss) {});
        
    }

    function deleteElement(id) {

        console.log(id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function () {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.page.deleteElement') }}",
                data: { 
                    'id': id
                    },
                dataType: 'json',
                // processData: false,  // tell jQuery not to process the data
                // contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                   status_submit = false;
                }
            });
        }, function (dismiss) {});
        
    }

    function saveElement(id, field) {

        var value;
        if (field == 'editor') {
            value = $('#'+id+'--'+field).html();
        }else{
            value = $('#'+id+'--'+field).val();
        }
        

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.page.saveElement') }}",
            data: { 
                'id': id,
                'field': field,
                'value': value
                },
            dataType: 'json',
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

    function saveImage(id) {
        var formData = new FormData($('#form-data')[0]);
        formData.append('id',id);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.page.saveImage') }}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.msg,
                        confirmButtonColor: "#4caf50",
                        type: "error"
                    })
                }
            },
            error: function (xhr, type) {
              // console.log(xhr);
            }
        });
    }

    function detailsAndRulesUploadImage(file,editor) {

        var formData = new FormData();
        formData.append("file", file);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.page.saveImageEditor') }}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                $(editor).summernote(
                    'insertImage',
                    data.message
                );
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }

$(document).ready(function() {
    App.initSwitch(".switch");
    slug = $('#txt_slug').val();

    $('.summernote-height').summernote({
        height: 500,
        toolbar: [
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],

            ['misc', ['fullscreen', 'codeview', 'help']]
        ],
        styleTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        imageAttributes:{
            icon:'<i class="note-icon-pencil"/>',
            removeEmpty: false, // true = remove attributes | false = leave empty if present
            disableUpload: true // true = don't display Upload Options | Display Upload Options
        },
        callbacks: {
            onImageUpload: function(files) {
                detailsAndRulesUploadImage(files[0], this);
            }
        }
    });

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('input[name="txt_register_period"], input[name="txt_tournament_period"]').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('click', '#btn-save', function(event) {
        var msg = '';

        if($('#txt_title').val().trim() == '')
            msg = 'กรุณากรอก หัวข้อ';

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{
            saveCreate();
        }
        return false;
    });

    $('body').on('change', 'input[name="txt_image"]', function() {

        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });

    function saveCreate() {

        var formData = new FormData($('#form-data')[0]);
        $.ajax({
            type: 'POST',
            {{-- // url: "{{ route('hon.tournament.saveEdit') }}", --}}
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        {{-- window.location.href = "{{ route('hon.tournament.index') }}"; --}}
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });

    }

});
</script>
@endsection