@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Class </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Class</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <div class="row">
            <div class="col-md-9">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">

                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">จัดการ Class นักเตะ</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">
                        <fieldset class="content-group">

                            <input type="hidden" name="txt_id" value="{{ $id }}">
                            <input type="hidden" name="old_manage_class" value="{{ $data->id or ''}}">
                            {{--       Background Logo Class       --}}
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background Class นักเตะ<span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="txt_background_class_player" name="txt_background_class_player" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 1920 x 436 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 2MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->bg_logo_class))
                                        <div class="input-group">
                                            <img src="{{ $data->bg_logo_class or '' }}" class="img-responsive" alt="" id="preview_class_player">
                                        </div>
                                    @else
                                        <img id="preview_class_player" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            {{--       Logo Class       --}}
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Logo class นักเตะ <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="txt_logo_class_player" name="txt_logo_class_player" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 680 x 270 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 2MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->logo_class))
                                        <div class="input-group">
                                            <img src="{{ $data->logo_class or '' }}" class="img-responsive" alt="" id="preview_logo_class_player">
                                        </div>
                                    @else
                                        <img id="preview_logo_class_player" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>


                            {{--       Background list class player       --}}
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background list class นักเตะ </label>
                                <div class="col-lg-5">
                                    <input type="file" id="txt_bg_list_class_player" name="txt_bg_list_class_player" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 1920 x 388 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 2MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->bg_list_class))
                                        <div class="input-group">
                                            <img src="{{ $data->bg_list_class or '' }}" class="img-responsive" alt="" id="preview_bg_list_class_player">
                                        </div>
                                    @else
                                        <img id="preview_bg_list_class_player" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>



                            {{--       list class player       --}}
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">List class นักเตะ <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="txt_list_class_player" name="txt_list_class_player" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
{{--                                    <span class="label label-success arrowed-right arrowed-in main__color">--}}
{{--                                        รูปควรมีขนาด 240 × 76 px--}}
{{--                                    </span>--}}
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 2MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->list_class))
                                        <div class="input-group">
                                            <img src="{{ $data->list_class or '' }}" class="img-responsive" alt="" id="preview_list_class_player">
                                        </div>
                                    @else
                                        <img id="preview_list_class_player" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>


                            {{--       Background detail class player       --}}
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background Detail class นักเตะ <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="txt_bg_detail" name="txt_bg_detail" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 1920 x 280 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 2MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->detail_class))
                                        <div class="input-group">
                                            <img src="{{ $data->detail_class or '' }}" class="img-responsive" alt="" id="preview_bg_detail">
                                        </div>
                                    @else
                                        <img id="preview_bg_detail" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col-md-3">

                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                            <button type="button" id="btn-save" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            <h3 id="wait" style="display: none">Waiting . . .</h3>
                        </div>
                    </div>
                </div>
            </div>


        </div>


        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">รายละเอียด</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>

                    <div class="panel-body rov_summernote">
                        <div class="form-group">
                            <textarea id="txt_details" name="txt_details" class="summernote-height">{{ $data->txt_detail or '' }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection

@section('script')
    <script type="text/javascript">
        function detailsAndRulesUploadImage(file,editor,welEditable) {

            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.content.saveImage') }}",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.summernote-height').summernote(
                        'insertImage',
                        data.message
                    );
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }


        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });


            $('.summernote-height').summernote({
                height: 500,
                popover: {
                    image: [
                        ['custom', ['imageAttributes']],
                        ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                },
                imageAttributes:{
                    icon:'<i class="note-icon-pencil"/>',
                    removeEmpty: false, // true = remove attributes | false = leave empty if present
                    disableUpload: true // true = don't display Upload Options | Display Upload Options
                },
                fontNames: [
                    'Kanit','Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande',
                    'Lucida Sans', 'Tahoma', 'Times', 'Times New Roman', 'Verdana'
                ],
                callbacks: {
                    onImageUpload: function(files) {
                        detailsAndRulesUploadImage(files[0], this);
                    }
                }
            });

            // var init = "<div class='rov_summernote_custom'>test</div>";
            // $('#txt_details').summernote('code', init);
            // $('#txt_rules').summernote('code', init);


            $('body').on('click', '#btn-save', function(event) {

                $('#btn-save').hide();
                $('#reset').hide();
                $('#wait').show();

                var msg = '';
                const status = '{!! isset($data->id) !!}';

                if($('#txt_background_class_player').val().trim() == '' && status != 1)
                    msg = 'กรุณาเพิ่มภาพ Background logo class นักเตะ';

                if($('#txt_logo_class_player').val().trim() == '' && status != 1)
                    msg = 'กรุณาเพิ่มภาพ Logo class นักเตะ';

                if($('#txt_list_class_player').val().trim() == '' && status != 1)
                    msg = 'กรุณาเพิ่มภาพ List class นักเตะ';

                if($('#txt_bg_detail').val().trim() == '' && status != 1)
                    msg = 'กรุณาเพิ่มภาพ Background detail class นักเตะ';

                if(msg != ''){
                    $('#btn-save').show();
                    $('#reset').show();
                    $('#wait').hide();
                    swal({
                        title: msg,
                        confirmButtonColor: "#EF5350",
                        allowOutsideClick: false,
                        type: "info"
                    });
                }else{
                    saveCreate();
                }
                return false;
            });

            $('body').on('change', 'input[name="txt_background_class_player"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_logo_class_player"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_bg_list_class_player"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });


            $('body').on('change', 'input[name="txt_list_class_player"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });


            $('body').on('change', 'input[name="txt_bg_detail"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#txt_background_class_player").change(function() {
                readURL(this,'preview_class_player');
            });

            $("#txt_logo_class_player").change(function() {
                readURL(this,'preview_logo_class_player');
            });

            $("#txt_bg_list_class_player").change(function() {
                readURL(this,'preview_bg_list_class_player');
            });

            $("#txt_list_class_player").change(function() {
                readURL(this,'preview_list_class_player');
            });

            $("#txt_bg_detail").change(function() {
                readURL(this,'preview_bg_detail');
            });


            function saveCreate() {

                var formData = new FormData($('#form-data')[0]);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.fo4.class.save.manage') }}",
                    data: formData,
                    dataType: 'json',
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                window.location.href = "{{ route('admin.fo4.class.index') }}";
                                // console.log('pass');
                            }, function (dismiss) {});
                        }else{
                            $('#btn-save').show();
                            $('#reset').show();
                            $('#wait').hide();


                            if (data.message === 'The txt background class player failed to upload.') {
                                data.message = 'ขนาดรูป Background Logo ต้องไม่เกิน 2 mb';
                            }

                            if (data.message === 'The txt logo class player failed to upload.') {
                                data.message = 'ขนาดรูป Logo ต้องไม่เกิน 2 mb';
                            }

                            if (data.message === 'The txt bg list class player failed to upload.') {
                                data.message = 'ขนาดรูป Background List ต้องไม่เกิน 2 mb';
                            }
                            if (data.message === 'The txt list class player failed to upload.') {
                                data.message = 'ขนาดรูป List ต้องไม่เกิน 2 mb';
                            }
                            if (data.message === 'The txt bg detail failed to upload.') {
                                data.message = 'ขนาดรูป Background Detail ต้องไม่เกิน 2 mb';
                            }

                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                        status_submit = false;
                    }
                });

            }
        });
    </script>
@endsection
