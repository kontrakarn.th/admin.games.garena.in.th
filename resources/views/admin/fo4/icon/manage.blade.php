
@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Manage Room </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active"><a href="{{ route('admin.fo4.icon.index') }}">Manage</a></li>
                <li class="active">Manage Room</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <h4>ตั้งค่า Icon หน้าเว็บ</h4>
                    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
                        <div class="row">
                            <label class="control-label col-lg-2">โชว์ Icon ห้อง {{ $room->name }} <span class="text-danger">(13 Icon)</span></label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <input type="hidden" name="room" value="{{ $room->id }}">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <select id="icon_show" name="icon_show[]" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                        @foreach ($icons as $icon)
                                            <option value="{{$icon->id}}" data-image="{{$icon->profile_small_image}}">{{$icon->middle_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveCreate()">Save <i class="icon-arrow-right14 position-right"></i></button>
                    <a href="{{ route('admin.fo4.icon.order',[ 'room' => $room->id ]) }}" class="btn btn-info" ><i class="icon-menu position-left"></i> จัดการลำดับการแสดงผล</a>


                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Icon ลงในห้อง {{ $room->name }}</button>
            @include('admin.fo4.icon.modals.addicon' , array('all_icons' => $all_icons))
            <p></p>
            <div class="table-header main__color main__background">ข้อมูล Icon ทั้งหมด</div>
            <div class="table-responsive">
                <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Nation</th>
                        <th>Feet</th>
                        <th>FP</th>
                        <th>OVR</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('script-head')
@endpush

@section('script')
    <script type="text/javascript">

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.icon.setting') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.fo4.icon.manage', ['id' => $room->id]) }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }


        $(function(){

            function formatData (data) {
                if (!data.id) { return data.text; }
                var image = $(data.element).data('image');
                var $result= $(
                    '<span><img src="'+image+'" height="30px" style="margin-right: 10px"/> ' + data.text + '</span>'
                );
                return $result;
            };

            $(".js-example-responsive").select2({
                // width: 'resolve',
                multiple: true,
                maximumSelectionLength: 13,
                templateResult: formatData,
                templateSelection: formatData,
                formatSelectionTooBig: function (limit) {
                    return 'Too many selected items';
                }
            });

            $(".js-example-responsive-your-icon").select2({
                // width: 'resolve',
                multiple: true,
                // maximumSelectionLength: 1,
                templateResult: formatData,
                templateSelection: formatData,
                formatSelectionTooBig: function (limit) {
                    return 'Too many selected items';
                }
            });
            $(".js-example-responsive").val({!! json_encode($icon_rooms) !!}).change();
            $(".js-example-responsive-your-icon").val({!! json_encode($all_icons_in_room) !!}).change();


            $('#datatable').DataTable({
                // $('#').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.fo4.icon.datatable',['room' => $room->id ])  }}',
                method: 'post',
                columns: [
                    {   data: 'name',
                        name: 'name'
                    },
                    {   data: 'image',
                        name: 'image'
                    },
                    {   data: 'nation',
                        name: 'nation'
                    },
                    {   data: 'feet',
                        name: 'feet'
                    },
                    {   data: 'fp',
                        name: 'fp'
                    },
                    {   data: 'ovr',
                        name: 'ovr'
                    },
                    {   data: 'action',
                        name: 'action'
                    }
                ],
                order: [[ 0, "ASC" ]],
            });
        });



        function addIcon() {
            var formData = new FormData($('#form-data-add-icon')[0]);


            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.icon.addIcon') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.fo4.icon.manage', ['id' => $room->id]) }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });

        }



        function leaveIcon(id) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel pls!",
                closeOnConfirm: false,
                closeOnCancel: true
            }).then(function () {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.fo4.icon.leave') }}",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    // processData: false,  // tell jQuery not to process the data
                    // contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                location.reload();
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                        status_submit = false;
                    }
                });
            }, function (dismiss) {});
        }
    </script>
@endsection
