@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{isset($data->id) ? "Edit":"Add"}} Icon Profile {{ $data->alt or '' }} </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">{{isset($data->id) ? "Edit":"Add"}} Icon Profile</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="txt_id" id="txt_id" value="{{ $data->id or '' }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">@if(isset($data->id))แก้ไข Icon Profile @else Icon Profile @endif</h5>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">


                            {{--     First name , Middle name , Last name     --}}
                            <div style="padding-bottom: 20px; border-bottom: 1px solid #c4c4c4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label col-md-3">ชื่อ <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="txt_first_name" name="txt_first_name" class="form-control" required="required" value="{{ $data->first_name or '' }}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label col-md-3">ชื่อกลาง <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="txt_middle_name" name="txt_middle_name" class="form-control" required="required"  value="{{ $data->middle_name or '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label col-md-3">นามสกุล <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="txt_last_name" name="txt_last_name" class="form-control" required="required" value="{{ $data->last_name or '' }}">
                                            </div>
                                        </div>
                                    </div>


                                    {{--     ฉายา น้ำหนัก ส่วนสูง     --}}
                                    <div class="col-md-4">
                                        <label class="control-label col-md-3">ฉายา <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="text" id="txt_nick_name" name="txt_nick_name" class="form-control" required="required" value="{{ $data->nick_name or '' }}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label col-md-3">น้ำหนัก <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="number" min="0" id="txt_weight" name="txt_weight" class="form-control" required="required" value="{{ $data->weight or '' }}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="control-label col-md-3">ส่วนสูง <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="number" min="0" id="txt_height" name="txt_height" class="form-control" required="required" value="{{ $data->height or '' }}">
                                            </div>
                                        </div>
                                    </div>



                                    {{--     stat     --}}
                                    <div class="col-md-3">
                                        <label class="control-label col-md-3">เท้าซ้าย <span class="text-danger">*</span></label>
                                        <div class="col-md-9">
                                            <select name="foot_left" id="foot_left" class="form-control">
                                                @if (!empty($data->foot_left))
                                                    <option disabled selected>เลือกคะแนนเท้าซ้าย</option>
                                                    <option value="1" {{ $data->foot_left == "1" ? 'selected' : '' }}>1</option>
                                                    <option value="2" {{ $data->foot_left == "2" ? 'selected' : '' }}>2</option>
                                                    <option value="3" {{ $data->foot_left == "3" ? 'selected' : '' }}>3</option>
                                                    <option value="4" {{ $data->foot_left == "4" ? 'selected' : '' }}>4</option>
                                                    <option value="5" {{ $data->foot_left == "5" ? 'selected' : '' }}>5</option>
                                                @else
                                                    <option disabled selected>เลือกคะแนนเท้าซ้าย</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                @endif

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label col-md-3">เท้าขวา <span class="text-danger">*</span></label>
                                        <div class="col-md-9">
                                            <select name="foot_right" id="foot_right" class="form-control">

                                                @if (!empty($data->foot_right))
                                                    <option disabled selected>เลือกคะแนนเท้าขวา</option>
                                                    <option value="1" {{ $data->foot_right == "1" ? 'selected' : '' }}>1</option>
                                                    <option value="2" {{ $data->foot_right == "2" ? 'selected' : '' }}>2</option>
                                                    <option value="3" {{ $data->foot_right == "3" ? 'selected' : '' }}>3</option>
                                                    <option value="4" {{ $data->foot_right == "4" ? 'selected' : '' }}>4</option>
                                                    <option value="5" {{ $data->foot_right == "5" ? 'selected' : '' }}>5</option>
                                                @else
                                                    <option disabled selected>เลือกคะแนนเท้าขวา</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                @endif

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label col-md-3">FP <span class="text-danger">*</span></label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="number" min="0" id="txt_fp" name="txt_fp" class="form-control" required="required"  value="{{ $data->FP or '' }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label col-md-3">OVR <span class="text-danger">*</span></label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                                <input type="number" min="0" id="txt_ovr" name="txt_ovr" class="form-control" required="required" value="{{ $data->OVR or '' }}" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label col-md-3">รูปร่าง <span class="text-danger">*</span></label>
                                        <div class="col-md-9">
                                            <select name="body" id="body" class="form-control">

                                                @if (!empty($data->body))
                                                    <option disabled selected>เลือกรูปร่าง</option>
                                                    <option value="ผอม" {{ $data->body == "ผอม" ? 'selected' : '' }}>ผอม</option>
                                                    <option value="มาตราฐาน" {{ $data->body == "มาตราฐาน" ? 'selected' : '' }}>มาตราฐาน</option>
                                                @else
                                                    <option disabled selected>เลือกรูปร่าง</option>
                                                    <option value="ผอม" >ผอม</option>
                                                    <option value="มาตราฐาน" >มาตราฐาน</option>
                                                @endif

                                            </select>
                                        </div>
                                    </div>

                                    <div class="mt-20 col-md-12" style="border: 1px solid black">
                                        <label class="control-label col-md-12">Position <span class="text-danger">* (เลือกได้สูงสุด 3 ตำแหน่ง)</span> </label>
                                        <div class="col-md-12">
                                            <div class="row">
                                                @if (!empty($data->position))
                                                    @php $positions = explode(",",$data->position) @endphp
                                                    @foreach($expertise as $ex)
                                                        <div class="col-md-1">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">{{ $ex->position }}</span>
                                                                @if (in_array($ex->id,$positions))
                                                                    <input type="checkbox" name="expertise[]" value="{{ $ex->id }}" checked class="mt-10 clickCheck">
                                                                @else
                                                                    <input type="checkbox" name="expertise[]" value="{{ $ex->id }}"  class="mt-10 clickCheck">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    @foreach($expertise as $ex)
                                                        <div class="col-md-1">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">{{ $ex->position }}</span>
                                                                <input type="checkbox" name="expertise[]" value="{{ $ex->id }}" class="mt-10 clickCheck">
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif


                                            </div>
                                        </div>
                                    </div>


                                </div>


                            </div>


{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-lg-2" >วันเวลาแสดงผล <span class="text-danger">*</span></label>--}}
{{--                                <div class="col-lg-9" >--}}
{{--                                    <div class="input-group">--}}
{{--                                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>--}}
{{--                                        <input type="text" id="txt_show_datetime" name="txt_show_datetime" class="form-control" required="required" value="{{ $data->show_datetime or '' }}" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-lg-2" >วันเริ่มแสดง <span class="text-danger">*</span></label>--}}
{{--                                <div class="col-lg-9" >--}}
{{--                                    <div class="input-group">--}}
{{--                                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>--}}
{{--                                        <input type="text" id="txt_start_datetime" name="txt_start_datetime" class="form-control" required="required" value="{{ $data->start_date or ''  }}" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-lg-2" >วันสิ้นสุดแสดง <span class="text-danger">*</span></label>--}}
{{--                                <div class="col-lg-9" >--}}
{{--                                    <div class="input-group">--}}
{{--                                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>--}}
{{--                                        <input type="text" id="txt_end_datetime" name="txt_end_datetime" class="form-control" required="required" value="{{ $data->end_date or '' }}" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            {{--    image profile large     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">Profile Image (Large)<span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_large" name="txt_banner_image_large" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 660 × 1000 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->profile_big_image))
                                        <div class="input-group">
                                            <img src="{{ $data->profile_big_image or '' }}" class="img-responsive" alt="" id="preview_large" style="width: 50%">
                                        </div>
                                    @else
                                        <img id="preview_large" src="" style="width: 50%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>


                            {{--    image profile small     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">Profile Small (Large)<span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_small" name="txt_banner_image_small" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 200 × 200 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20" >
                                    @if(!empty($data->profile_small_image))
                                        <div class="input-group">
                                            <img src="{{ $data->profile_small_image or '' }}" class="img-responsive" alt="" id="preview_small" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_small" src="" style="width: 50%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>


                            {{--    image profile icon     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">Profile Small (Icon)<span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_icon" name="txt_banner_image_icon" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 80 × 80 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->profile_image_icon))
                                        <div class="input-group">
                                            <img src="{{ $data->profile_image_icon or '' }}" class="img-responsive" alt="" id="preview_icon" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_icon" src="" style="width: 50%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            {{--    image profile history     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">History <span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_history" name="txt_banner_image_history" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 670 × 175 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->history_image))
                                        <div class="input-group">
                                            <img src="{{ $data->history_image or '' }}" class="img-responsive" alt="" id="preview_history" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_history" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>


                            {{--    image profile award    --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">Award <span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_award" name="txt_banner_image_award" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 670 × 175 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->award_image))
                                        <div class="input-group">
                                            <img src="{{ $data->award_image or '' }}" class="img-responsive" alt="" id="preview_award" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_award" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            {{--    image flag     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">Flag <span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_flag" name="txt_banner_image_flag" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 50 × 30 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->flag_image))
                                        <div class="input-group">
                                            <img src="{{ $data->flag_image or '' }}" class="img-responsive" alt="" id="preview_flag" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_flag" src="" style="width: 50%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            {{--    image all stat     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">All Stat <span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_all_stat" name="txt_banner_image_all_stat" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 1285 × 800 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->profile_all_stat))
                                        <div class="input-group">
                                            <img src="{{ $data->profile_all_stat or '' }}" class="img-responsive" alt="" id="preview_all_stat" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_all_stat" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>



                            {{--    image logo team     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">Logo Team <span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_logo_team" name="txt_banner_image_logo_team" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 110 × 140 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->logo_team))
                                        <div class="input-group">
                                            <img src="{{ $data->logo_team or '' }}" class="img-responsive" alt="" id="preview_logo_team" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_logo_team" src="" style="width: 50%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            {{--    image shirt     --}}
                            <div class="form-group has-feedback " style="border-bottom: 1px solid #cdcdcd ; padding-bottom: 20px">
                                <label class="control-label col-lg-2 mt-20">Shirt <span class="text-danger">*</span></label>
                                <div class="col-lg-5 mt-20">
                                    <input type="file" id="txt_banner_image_shirt" name="txt_banner_image_shirt" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 120 × 170 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-20">
                                    @if(!empty($data->shirt_image))
                                        <div class="input-group">
                                            <img src="{{ $data->shirt_image or '' }}" class="img-responsive" alt="" id="preview_shirt" style="width: 100%">
                                        </div>
                                    @else
                                        <img id="preview_shirt" src="" style="width: 50%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2" style="margin-top: 3%">สถานะเปิดใช้งาน <span class="text-danger">*</span></label>
                                <div class="col-lg-9" style="margin-top:3%">
                                    <div class="input-group">
                                        @if (!empty($data->status))
                                            <input type="checkbox" name="txt_status" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" {{ $data->status  == 'active' ? 'checked=checked' : '' }}>
                                        @else
                                            <input type="checkbox" name="txt_status" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" >

                                        @endif

                                    </div>
                                </div>
                            </div>



                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                            <h3 id="loading" style="display: none">Waiting . . .</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>
        function detailsAndRulesUploadImage(file,editor,welEditable) {

            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.content.saveImage') }}",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.summernote-height').summernote(
                        'insertImage',
                        data.message
                    );
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function save() {
            var msg = '';
            const status = '{!! isset($data->id) !!}';

            if($('input[name="expertise[]"]:checked').length === 0)
                msg = 'กรุณาเลือกตำแหน่งอย่างน้อย 1 ตำแหน่ง';

            if($('#txt_first_name').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก ชื่อ';

            if($('#txt_middle_name').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก ชื่อกลาง';

            if($('#txt_last_name').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก นามสกุล';

            if($('#txt_nick_name').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก ฉายา';

            if($('#txt_weight').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก น้ำหนัก';

            if($('#txt_height').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก ส่วนสูง';

            if($('#txt_fp').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก FP';

            if($('#txt_ovr').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก OVR';

            if($('#txt_banner_image_large').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป Icon ขนาดใหญ่';

            if($('#txt_banner_image_small').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป Profile Small (Large)';

            if($('#txt_banner_image_icon').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป Icon Profile Small (Icon)';

            if($('#txt_banner_image_history').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป History';

            if($('#txt_banner_image_award').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป Award';

            if($('#txt_banner_image_flag').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป Flag';

            if($('#txt_banner_image_all_stat').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป All Stat';

            if($('#txt_banner_image_logo_team').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป โลโกทีม';

            if($('#txt_banner_image_logo_team').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป เสื้อทีม';


            if(msg != ''){
                swal({
                    title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
            }else{
                saveCreate();
            }
            return false;
        }

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $('#btn-save-draft').hide();
            $('#reset').hide();
            $('#loading').show();


            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.icon.profile.save') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    $('#btn-save-draft').show();
                    $('#reset').show();
                    $('#loading').hide();
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.fo4.icon.profile.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });

        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            // check เมื่อ run edit ครั้งแรก
            var checkedFirstTime = $('input[name="expertise[]"]:checked').length ;
            if (checkedFirstTime >= 3) {
                $('input[name="expertise[]"]').attr("disabled",true);
                $('input[name="expertise[]"]:checked').attr("disabled",false);
            }
            else {
                $('input[name="expertise[]"]').attr("disabled",false);
            }


            $('.clickCheck').on('click',function () {
                var atLeastOneIsChecked = $('input[name="expertise[]"]:checked').length ;

                if (atLeastOneIsChecked >= 3) {
                    $('input[name="expertise[]"]').attr("disabled",true);
                    $('input[name="expertise[]"]:checked').attr("disabled",false);
                }

                else {
                    $('input[name="expertise[]"]').attr("disabled",false);
                }

            });

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="txt_show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('input[name="txt_start_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('input[name="txt_end_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('body').on('change', 'input[name="txt_banner_image_large"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_small"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_icon"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_history"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_award"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_flag"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_all_stat"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_logo_team"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $('body').on('change', 'input[name="txt_banner_image_shirt"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#txt_banner_image_large").change(function() {
                readURL(this,'preview_large');
            });

            $("#txt_banner_image_small").change(function() {
                readURL(this,'preview_small');
            });

            $("#txt_banner_image_icon").change(function() {
                readURL(this,'preview_icon');
            });

            $("#txt_banner_image_history").change(function() {
                readURL(this,'preview_history');
            });

            $("#txt_banner_image_award").change(function() {
                readURL(this,'preview_award');
            });

            $("#txt_banner_image_flag").change(function() {
                readURL(this,'preview_flag');
            });

            $("#txt_banner_image_all_stat").change(function() {
                readURL(this,'preview_all_stat');
            });

            $("#txt_banner_image_logo_team").change(function() {
                readURL(this,'preview_logo_team');
            });

            $("#txt_banner_image_shirt").change(function() {
                readURL(this,'preview_shirt');
            });

            $("#txt_banner_image_mobile").change(function() {
                readURL(this,'preview_mobile');
            });

            {{--$(".switch").on('switchChange.bootstrapSwitch', function(event, state) {--}}
            {{--    event.preventDefault();--}}
            {{--    $.post(--}}
            {{--        "{{ route('admin.rov.banner.updateStatus') }}",--}}
            {{--        { id: $(this).attr('data-id'), status: state },--}}
            {{--        function( data ) {--}}
            {{--            if(data.status){--}}
            {{--                swal({--}}
            {{--                    title: data.message,--}}
            {{--                    confirmButtonColor: "#4caf50",--}}
            {{--                    type: "success"--}}
            {{--                }).then(function () {--}}

            {{--                });--}}
            {{--            }else{--}}
            {{--                swal({--}}
            {{--                    title: data.message,--}}
            {{--                    confirmButtonColor: "#EF5350",--}}
            {{--                    type: "info"--}}
            {{--                }).then(function () {--}}

            {{--                });--}}
            {{--            }--}}
            {{--        }--}}
            {{--    );--}}
            {{--});--}}

        });
    </script>
@endsection