@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{isset($data->id) ? "Edit":"Add"}} Icon Room {{ $data->alt or '' }} </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">{{isset($data->id) ? "Edit":"Add"}} Icon Room</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="txt_id" id="txt_id" value="{{ $data->id or '' }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">@if(isset($data->id))แก้ไข Icon Room @else เพิ่ม Icon Room @endif</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Room Name <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="txt_title" name="txt_title" class="form-control" required="required" value="{{ $data->name or '' }}" >
                                    </div>
                                </div>
                            </div>


                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Banner Image <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="txt_banner_image" name="txt_banner_image" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->banner_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 1920 × 1080 px
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png  และขนาดไม่เกิน 2MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->banner_image))
                                        <div class="input-group">
                                            <img src="{{ $data->banner_image or '' }}" class="img-responsive" alt="" id="preview">
                                        </div>
                                    @else
                                        <img id="preview" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2" style="margin-top: 3%">สถานะเปิดใช้งาน <span class="text-danger">*</span></label>
                                <div class="col-lg-9" style="margin-top:3%">
                                    <div class="input-group">
                                        <input type="checkbox" name="txt_status" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" {{ $data->status  == 'active' ? 'checked=checked' : '' }}>


                                    </div>
                                </div>
                            </div>


                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>
        function detailsAndRulesUploadImage(file,editor,welEditable) {

            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.content.saveImage') }}",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.summernote-height').summernote(
                        'insertImage',
                        data.message
                    );
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function save() {
            var msg = '';
            const status = '{!! isset($data->id) !!}';

            if($('#txt_title').val().trim() == '' && status != 1)
                msg = 'กรุณากรอก Title';

            // if($('#txt_link').val().trim() == '' && status != 1)
            //     msg = 'กรุณากรอก Link';
            //
            // if($('#txt_banner_image').val().trim() == '' && status != 1)
            //     msg = 'กรุณาอัพโหลดรูป';


            if(msg != ''){
                swal({
                    title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
            }else{
                saveCreate();
            }
            return false;
        }

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.icon.room.save') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.fo4.icon.room.index') }}";
                        }, function (dismiss) {});
                    }else{
                        if (data.message === 'The txt banner image failed to upload.') {
                            data.message = 'รูปต้องมีขนาดไม่เกิน 2 mb'
                        }
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="txt_show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('input[name="txt_start_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('input[name="txt_end_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('body').on('change', 'input[name="txt_banner_image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#txt_banner_image").change(function() {
                readURL(this,'preview');
            });

            $("#txt_banner_image_mobile").change(function() {
                readURL(this,'preview_mobile');
            });

            {{--$(".switch").on('switchChange.bootstrapSwitch', function(event, state) {--}}
            {{--    event.preventDefault();--}}
            {{--    $.post(--}}
            {{--        "{{ route('admin.rov.banner.updateStatus') }}",--}}
            {{--        { id: $(this).attr('data-id'), status: state },--}}
            {{--        function( data ) {--}}
            {{--            if(data.status){--}}
            {{--                swal({--}}
            {{--                    title: data.message,--}}
            {{--                    confirmButtonColor: "#4caf50",--}}
            {{--                    type: "success"--}}
            {{--                }).then(function () {--}}

            {{--                });--}}
            {{--            }else{--}}
            {{--                swal({--}}
            {{--                    title: data.message,--}}
            {{--                    confirmButtonColor: "#EF5350",--}}
            {{--                    type: "info"--}}
            {{--                }).then(function () {--}}

            {{--                });--}}
            {{--            }--}}
            {{--        }--}}
            {{--    );--}}
            {{--});--}}

        });
    </script>
@endsection