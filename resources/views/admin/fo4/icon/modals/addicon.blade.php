<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">เลือกนักเตะใส่ห้อง {{ $room->name }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-data-add-icon" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
                    <div class="row">
                        <label class="control-label col-lg-2">ห้อง {{ $room->name }}</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <input type="hidden" name="room" value="{{ $room->id }}">
                                <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                <select id="icon_show_in_room" name="icon_show_in_room[]" class="form-control js-example-responsive-your-icon" required="required" style="width: 100%;">
                                    @foreach ($all_icons as $icon)
                                        <option value="{{$icon->id}}" data-image="{{$icon->profile_small_image}}">{{$icon->middle_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addIcon()">Save changes</button>
            </div>
        </div>
    </div>
</div>