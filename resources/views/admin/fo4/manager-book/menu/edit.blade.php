
@extends('layout.layout')
@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Edit Manager Book Menu </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Edit Manager Book Menu</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">

        <form action="#" id="saveData">
            @CSRF
            <div class="col-md-9">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">

                <input type="hidden" name="txt_id" value="{{ $menu_manager->id }}">

                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">สร้าง Menu</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">
                        <fieldset class="content-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-3">ชื่อ Menu: <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-address-book"></i></span>
                                        <input type="text" id="txt_menu" name="txt_menu" class="form-control" required="required" value="{{ $menu_manager->menu_name }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-3" style="margin-top: 3%">Url : <span class="text-danger">*</span></label>
                                <div class="col-lg-9" style="margin-top: 3%">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-link"></i></span>
                                        <input type="text" id="txt_url" name="txt_url" class="form-control" required="required" value="{{ $menu_manager->url }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3" style="margin-top: 3%">วันเวลาแสดงผล <span class="text-danger">*</span></label>
                                <div class="col-lg-9" style="margin-top: 3%">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                        <input type="text" id="txt_show_datetime" name="txt_show_datetime" class="form-control" required="required" value="{{ $menu_manager->show_datetime }}" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-3" style="margin-top: 3%">สถานะเปิดใช้งาน <span class="text-danger">*</span></label>
                                <div class="col-lg-9" style="margin-top:3%">
                                    <div class="input-group">
                                        <input type="checkbox" name="txt_status"  data-id="{{ $menu_manager->id or '' }}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" {{ $menu_manager->status == 'active' ? 'checked=checked' : '' }}>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div class="col-md-3">

                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                            <button type="button" id="btn-save" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>

        </form>

    </div>


@endsection

@push('script-head')
@endpush

@section('script')
    <script type="text/javascript">

        $(document).ready(function () {
            App.initSwitch(".switch");


            $('input[name="txt_show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });


            $('body').on('click', '#btn-save', function(event) {
                var msg = '';

                if($('#txt_menu').val().trim() == '')
                    msg = 'กรุณากรอก ชื่อเมนู';
                else if ($('#txt_menu').val().length > 100) {
                    msg = 'ชื่อเมนู ไม่ควรเกิน 100 ตัวอักษร';
                }
                else if ($('#txt_url').val().trim() == '') {
                    msg = 'กรุณากรอก URL';
                }


                if(msg != ''){
                    swal({
                        title: msg,
                        confirmButtonColor: "#EF5350",
                        allowOutsideClick: false,
                        type: "info"
                    });
                }else{
                    saveCreate();
                }
                return false;
            });


            function saveCreate() {
                console.log('saveeee');
                var formData = new FormData($('#saveData')[0]);

                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.fo4.manager-book.save.edit') }}",
                    data: formData,
                    dataType: 'json',
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                        console.log(data);
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                window.location.href = "{{ route('admin.fo4.manager-book.index') }}" ;
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                });
            }


        })



    </script>
@endsection
