
@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - manage Event </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">manage Event</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat ">
            <div class="panel-heading">
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="panel-body">
                <a href="{{ route('admin.fo4.event.add') }}" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Event</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header main__color main__background">ข้อมูล</div>
        <div class="table-responsive">
            <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Alt</th>
                        <th>Position</th>
                        <th>Index</th>
                        <th>URL</th>
                        <th>Show Datetime</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<style>
    .table tr td img {
        max-height: 200px;
    }
</style>

@endsection

@push('script-head')
@endpush

@section('script')
<script type="text/javascript">


function deleteEvent(id) {


    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#EF5350",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel pls!",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then(function () {
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.event.deleteEvent') }}",
            data: { 
                'id': id
                },
            dataType: 'json',
            // processData: false,  // tell jQuery not to process the data
            // contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }, function (dismiss) {});
    
}

$(function(){
    
    $('#datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin.fo4.event.datatable') }}',
        method: 'post',
        columns: [
            {   data: 'id',
                name: 'id' 
            },
            {   data: 'alt',
                name: 'alt' 
            },
            {   data: 'position',
                name: 'position' 
            },
            {   data: 'index',
                name: 'index' 
            },
            {   data: 'url',
                name: 'url' 
            },
            {   data: 'show_datetime',
                name: 'show_datetime' 
            },
            {   data: 'image',
                name: 'image' 
            },
            {   data: 'status',
                name: 'status' 
            },
            {   data: 'action',
                name: 'action' 
            }
        ],
        order: [[ 0, "desc" ]],
        drawCallback: function( settings ) {
            var api = this.api();
            // console.log('DataTables has redrawn the table');
            // console.log( api.rows( {page:'current'} ).data() );

            var _ObjTable = $('#datatable');
            App.initSwitch(_ObjTable.find(".switch"));
            _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                console.log('test');
                console.log($(this).attr('data-id'));
                console.log(state);
            });
        },
        drawCallback : function(settings) {
            var _ObjTable = $('#datatable');
             App.initSwitch(_ObjTable.find(".switch"));
             _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.fo4.event.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                         if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                         });
                         }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                         });
                         }
                    }
                );
            });
        }
    });

    

});
</script>
@endsection
