@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - edit event {{ $data->alt or '' }} </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">edit event</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <input type="hidden" name="txt_id" id="txt_id" value="{{ $data->id or '' }}">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">@if(isset($data->id))แก้ไขข้อมูล@else เพิ่มข้อมูล @endif</h5>
                    
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    

                    <fieldset class="content-group">
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Alt <span class="text-danger">ข้อความคาดบนรูป</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_alt" name="txt_alt" class="form-control" value="{{ $data->alt or '' }}" >
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">URL <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="txt_url" name="txt_url" class="form-control" required="required" value="{{ $data->url or '' }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Position <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>

                                    <select id="txt_position" name="txt_position" class="form-control" required="required">
                                            <option 
                                                value="slide" 
                                                @if(isset($data->position)) {{ $data->position == 'slide' ? 'selected' : '' }} @endif
                                            >slide</option>
                                            <option 
                                                value="banner" 
                                                @if(isset($data->position)) {{ $data->position == 'banner' ? 'selected' : '' }} @endif
                                            >banner</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Index (Order) <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-link"></i></span>
                                        <input type="number" id="txt_index" name="txt_index" class="form-control" required="required" value="{{ $data->index or 0 }}" >
                                    </div>
                                </div>
                            </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Show Date Time <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_show_datetime" name="txt_show_datetime" class="form-control" required="required" value="{{ $data->show_datetime or '' }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Image <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                            @if(!empty($data->image))
                                <div class="input-group">
                                    <img src="{{ $data->image or '' }}" class="img-responsive" alt="">
                                </div>
                            @endif

                                <input type="file" id="txt_image" name="txt_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                            </div>
                        </div>

                    </fieldset>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    function detailsAndRulesUploadImage(file,editor,welEditable) {

        var formData = new FormData();
        formData.append("file", file);
        formData.append("file", file);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.content.saveImage') }}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                $('.summernote-height').summernote(
                    'insertImage',
                    data.message
                );
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }

    function save() {
        // var msg = '';

        // if($('#txt_alt').val().trim() == '')
        //     msg = 'กรุณากรอก Title';

        // if(msg != ''){
        //     swal({
        //         title: msg,
        //         confirmButtonColor: "#EF5350",
        //         allowOutsideClick: false,
        //         type: "info"
        //     });
        // }else{
            saveCreate();
        // }
        // return false;
    }

    function saveCreate() {

        var formData = new FormData($('#form-data')[0]);

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.event.saveEvent') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        window.location.href = "{{ route('admin.fo4.event.index') }}";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

$(document).ready(function() {
    App.initSwitch(".switch");
    slug = $('#txt_slug').val();
    id = $('#txt_id').val();

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('change', 'input[name="txt_image"]', function() {

        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });
});
</script>
@endsection