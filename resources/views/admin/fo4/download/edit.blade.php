@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - edit download </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">edit download</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">แก้ไขข้อมูล</h5>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    {{-- <a href="#modal_add_new_section" data-toggle="modal" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Section </a> --}}
                    <a href="#modal_add_new_element" data-toggle="modal" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Element </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Spec</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                
                <div class="panel-body">
                    <fieldset class="content-group">

                        @foreach($specData as $data)
                        <div class="form-group has-feedback">
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" placeholder="Item (e.g. CPU)" id="txt_item--{{ $data->id }}" name="txt_item--{{ $data->id }}" class="form-control" required="required" value="{{ $data->item }}" >
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-arrow-down8"></i></span>
                                    <input type="text" placeholder="Minimum (e.g. 4GB)" id="txt_min--{{ $data->id }}" name="txt_min--{{ $data->id }}" class="form-control" required="required" value="{{ $data->minimum }}" >
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-star-full2"></i></span>
                                    <input type="text" placeholder="Recommended (e.g. 8GB)" id="txt_rec--{{ $data->id }}" name="txt_rec--{{ $data->id }}" class="form-control" required="required" value="{{ $data->recommended }}" >
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'spec')"> <i class="icon-checkmark"></i></button>
                                <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElementSpec({{ $data->id }})"> <i class="icon-bin"></i></button>
                            </div>
                        </div>
                        @endforeach
                        
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Download</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                
                <div class="panel-body">
                    <fieldset class="content-group">

                        @foreach($downloadData as $data)
                        <div class="form-group has-feedback">
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" placeholder="Text (e.g. AMD)" id="txt_text--{{ $data->id }}" name="txt_text--{{ $data->id }}" class="form-control" required="required" value="{{ $data->txt }}" >
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-price-tags"></i></span>
                                    <select name="txt_type--{{ $data->id }}" id="txt_type--{{ $data->id }}" class="form-control">
                                        <option value="driver" {{ $data->types == 'driver' ? 'selected' : '' }}>Driver</option>
                                        <option value="download" {{ $data->types == 'download' ? 'selected' : '' }}>Download</option>
                                        <option value="patch" {{ $data->types == 'patch' ? 'selected' : '' }}>Patch</option>
                                        <option value="gpc" {{ $data->types == 'gpc' ? 'selected' : '' }}>GPC</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" placeholder="Download Link " id="txt_url--{{ $data->id }}" name="txt_url--{{ $data->id }}" class="form-control" required="required" value="{{ $data->url }}" >
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-success" onclick="saveElement({{ $data->id }}, 'download')"> <i class="icon-checkmark"></i></button>
                                <button type="button" class="btn btn-danger" title="Delete This Element" onclick="deleteElement({{ $data->id }})"> <i class="icon-bin"></i></button>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" placeholder="Title (e.g. Manual Patch)" id="txt_title--{{ $data->id }}" name="txt_title--{{ $data->id }}" class="form-control" required="required" value="{{ $data->title }}" >
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" placeholder="Description (e.g. Last Update)" id="txt_description--{{ $data->id }}" name="txt_description--{{ $data->id }}" class="form-control" required="required" value="{{ $data->description }}" >

                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" placeholder="Detail (e.g. File Size) " id="txt_detail--{{ $data->id }}" name="txt_detail--{{ $data->id }}" class="form-control" required="required" value="{{ $data->detail }}" >
                                </div>
                            </div>
                            
                        </div>
                        <hr>
                        @endforeach
                        
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- basic modal -->
<div id="modal_add_new_element" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Add New element</h5>
                </div>

                <div class="modal-body">

                    <form class="form-horizontal" action="#" id="form_add_new_section">
                        <fieldset class="content-group">

                            <div class="form-group">
                                <label class="control-label col-lg-2">Element :</label>
                                <div class="col-lg-10">
                                    <select name="modal_add_new_element_element_type" id="modal_add_new_element_element_type" class="form-control">
                                        <option value="spec">Specification</option>
                                        <option value="driver">Driver</option>
                                        <option value="download">Download</option>
                                        <option value="patch">Patch</option>
                                        <option value="gpc">GPC</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addNewElement()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<!-- /basic modal -->
@endsection

@section('script')
<script>
    function addNewElement() {
        var element_type = $('#modal_add_new_element_element_type').val();
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.download.addElement') }}",
            data: { 
                'types': element_type
                },
            dataType: 'json',
            // processData: false,  // tell jQuery not to process the data
            // contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

    function saveElement(id,type) {

        var txt ='' ;
        var types ='' ;
        var url ='' ;
        var item ='' ;
        var minimum ='' ;
        var recommended ='' ;
        var title ='' ;
        var description ='' ;
        var detail ='' ;

        if (type == 'spec') {
            item = $('#txt_item--'+id).val();
            minimum = $('#txt_min--'+id).val();
            recommended = $('#txt_rec--'+id).val();
        }else{
            txt = $('#txt_text--'+id).val();
            types = $('#txt_type--'+id).val();
            url = $('#txt_url--'+id).val();
            title = $('#txt_title--'+id).val();
            description = $('#txt_description--'+id).val();
            detail = $('#txt_detail--'+id).val();
        }

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.fo4.download.saveElement') }}",
            data: { 
                'id': id,
                'txt': txt,
                'types': types,
                'url': url,
                'item': item,
                'minimum': minimum,
                'recommended': recommended,
                'title': title,
                'description': description,
                'detail': detail,
                },
            dataType: 'json',
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }
    function deleteElement(id) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function () {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.download.deleteElement') }}",
                data: { 
                    'id': id
                    },
                dataType: 'json',
                // processData: false,  // tell jQuery not to process the data
                // contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                   status_submit = false;
                }
            });
        }, function (dismiss) {});
    }


    function deleteElementSpec(id) {

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function () {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.fo4.download.deleteElementSpec') }}",
                data: {
                    'id': id
                    },
                dataType: 'json',
                // processData: false,  // tell jQuery not to process the data
                // contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                   status_submit = false;
                }
            });
        }, function (dismiss) {});
    }

</script>
@endsection