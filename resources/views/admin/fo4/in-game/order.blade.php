
@extends('layout.layout')

@section('header')
    <style>
        #items{
            display: flex;
            width: 100%;
            overflow-x: scroll;
            list-style-type: none;
        }
        #items li{
            margin: 10px 5px;
            /* padding: 10px; */
            /* background: #ffffff; */
            text-align: center;
            /* border-radius: 10px; */
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>

    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Ordering Banner In Game </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Ordering Banner In Game</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <h4>จัดลำดับการแสดงผล Banners</h4>
                    <ul id="items">
                        @foreach ($banners as $banner)
                            <li data-id="{{$banner->id}}">
                                @if ($banner->file_format == "mp4" || $banner->file_format == "webm")
                                    <img src="https://static2.garena.in.th/data/rov/content/content/66b2d780fe8be1b8a5098103caf03d97.png" height="100px">
                                @else
                                    <img src="{{$banner->banner_image}}" height="100px">
                                @endif
                                <br><h6>{{$banner->title}}</h6>
                            </li>
                        @endforeach
                    </ul>
                    <div class="text-center">
                        <button type="button" id="btn-save" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script-head')
@endpush

@section('script')
    <script type="text/javascript">

        $(document).ready(function () {
            var el = document.getElementById('items');
            var sortable = new Sortable(el, {
                animation: 150,
                easing: "cubic-bezier(1, 0, 0, 1)",
                dataIdAttr: 'data-id',
                onUpdate: function (evt) {
                    // console.log("Updated : "+sortable.toArray());
                },
            });

            $('#btn-save').on('click',function () {
                var formData = new FormData();
                formData.append("newSort", sortable.toArray());

                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.fo4.in_game.save.order') }}",
                    data: formData,
                    dataType: 'json',
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                window.location.href = "{{ route('admin.fo4.in_game.index') }}";
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                        status_submit = false;
                    }
                });
            });

        });

    </script>
@endsection
