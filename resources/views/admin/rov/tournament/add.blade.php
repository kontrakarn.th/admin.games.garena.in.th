@extends('layout.layout')

@section('header')
    <style>
        .tour_card{
            text-align: center;
            position: relative;
            margin-bottom: 30px;

        }
        .tour_logo{
            position: absolute;
            /* margin: 0 auto; */
            bottom: 20px;
            width: 50%;
            left: 25%;
            z-index: 1;
        }
        .tour_bg{
            width: 100%;
            height: auto;
        }
        img.tour_bg{
            -webkit-filter: brightness(50%);
        }
        .tour_title{
            position: absolute;
            color: #ffe400;
            background: #0000008c;
            text-align: left;
            padding: 10px 20px;
            font-weight: bold;
            width: 92%;
            padding: 10px 20px;
            z-index: 1;
        }
    </style>
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{isset($data->id) ? "Edit":"Add"}} Tour</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">{{isset($data->id) ? "Edit":"Add"}} Tour</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ $data->id or '' }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">รายละเอียดการแข่งขัน</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">
                            @if (isset($data->id))
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Active</label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            @if ($data->status === 'active')
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">
                                            @else
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Title <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="title" name="title" class="form-control" required="required" value="{{ $data->title or '' }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Status Tour <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <select name="status_tour" id="status_tour" class="form-control">
                                            <option value="" disabled selected>Please select status.</option>
                                            <option value="comingsoon" {{isset($data->id) ? $data->status_tour == "comingsoon" ? 'selected': null : null}}>Coming soon</option>
                                            <option value="registration" {{isset($data->id) ? $data->status_tour == "registration" ? 'selected': null : null}}>Registration</option>
                                            <option value="expired" {{isset($data->id) ? $data->status_tour == "expired" ? 'selected': null : null}}>Expired / Live</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Link</label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="link" name="link" class="form-control" required="required" value="{{ $data->link or '' }}" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-3">Logo Tour <span class="text-danger">*</span></label>
                                        <div class="col-lg-9">
                                            <input type="file" id="logo_image" name="logo_image" class="file-styled" data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required" value="{{ $data->logo_image or '' }}">
                                            <br>
                                            <div class="help-block">
                                                <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-3">Background<span class="text-danger">*</span></label>
                                        <div class="col-lg-9">
                                            <input type="file" id="tour_image" name="tour_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required" value="{{ $data->tour_image or '' }}">
                                            <br>
                                            <div class="help-block">
                                                <span class="label label-success arrowed-right arrowed-in main__color">
                                                    รูปควรมีขนาด 280 × 410 px
                                                </span>
                                                <p></p>
                                                <span class="label label-danger arrowed-right arrowed-in">
                                                    รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2 tour_card">
                                    <div class="tour_title">
                                        <h4>{{ $data->title or '' }}</h4>
                                    </div>
                                    <img src="{{ $data->logo_image or '' }}" alt="" id="preview" class="tour_logo">
                                    <img src="{{ $data->tour_image or '' }}" alt="" id="preview_tour_image" class="tour_bg">
                                </div>
                            </div>
                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveCreate()">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.rov.tournament.saveTour') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.rov.tournament.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('body').on('change', 'input[name="logo_image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#logo_image").change(function() {
                readURL(this,'preview');
            });

            $("#tour_image").change(function() {
                readURL(this,'preview_tour_image');
            });

            $(".switch").on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.rov.tournament.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                            });
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                            });
                        }
                    }
                );
            });

        });
    </script>
@endsection