@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Users Registration Detail</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Users Registration Detail</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="txt_id" id="txt_id" value="{{ $user->open_id or '' }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">ข้อมูลผู้ใช้</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">

                            <div class="text-center">
                                <img src="{{$user->profile_image}}" class="img-circle" width="250px">
                                <h3>{{$user->game_name_decode}}</h3>
                                @if ($user->verified)
                                    <p class='text-success'><i class='icon-check'></i> Verified Email</p>
                                @else
                                    <p class='text-danger'>Not Verify Email</p>
                                @endif
                                @if (!$user->email)

                                    <p class='text-danger'>Not Register</p>
                                @endif
                                <hr>
                            </div>

                            <h4>Personal Information</h4>

                            <div class="form-group">
                                <label class="control-label col-lg-3">UID :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="UID" name="uid" id="uid" value="{{ $user->uid or '' }}" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Open ID :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Open ID" name="open_id" id="open_id" value="{{ $user->open_id or '' }}" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Tencent ID :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Tencent ID" name="t_open_id" id="t_open_id" value="{{ $user->t_open_id or '' }}" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Game Name :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Game Name" name="game_name" id="game_name" value="{{ $user->game_name_decode or '' }}" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Firstname :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Firstname" name="firstname" id="firstname" value="{{ $user->firstname or '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Lastname :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Lastname" name="lastname" id="lastname" value="{{ $user->lastname or '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">E-mail :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="E-mail" name="email" id="email" value="{{ $user->email or '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Birth Date :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Birth Date" name="birth_date" id="birth_date" value="{{ $user->birth_date or '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">ID Card / Passport:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="ID Card" name="id_card" id="id_card" value="{{ $user->id_card or '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Province :</label>
                                <div class="col-lg-9">
                                    <select name="province" class="form-control">
                                        <option value="" disabled selected>จังหวัด</option>
                                        <option value="1" {{ $user->province == 1 ? 'selected':null }}>กรุงเทพมหานคร</option>
                                        <option value="2" {{ $user->province == 2 ? 'selected':null }}>สมุทรปราการ</option>
                                        <option value="3" {{ $user->province == 3 ? 'selected':null }}>นนทบุรี</option>
                                        <option value="4" {{ $user->province == 4 ? 'selected':null }}>ปทุมธานี</option>
                                        <option value="5" {{ $user->province == 5 ? 'selected':null }}>พระนครศรีอยุธยา</option>
                                        <option value="6" {{ $user->province == 6 ? 'selected':null }}>อ่างทอง</option>
                                        <option value="7" {{ $user->province == 7 ? 'selected':null }}>ลพบุรี</option>
                                        <option value="8" {{ $user->province == 8 ? 'selected':null }}>สิงห์บุรี</option>
                                        <option value="9" {{ $user->province == 9 ? 'selected':null }}>ชัยนาท</option>
                                        <option value="10" {{ $user->province == 10 ? 'selected':null }}>สระบุรี</option>
                                        <option value="11" {{ $user->province == 11 ? 'selected':null }}>ชลบุรี</option>
                                        <option value="12" {{ $user->province == 12 ? 'selected':null }}>ระยอง</option>
                                        <option value="13" {{ $user->province == 13 ? 'selected':null }}>จันทบุรี</option>
                                        <option value="14" {{ $user->province == 14 ? 'selected':null }}>ตราด</option>
                                        <option value="15" {{ $user->province == 15 ? 'selected':null }}>ฉะเชิงเทรา</option>
                                        <option value="16" {{ $user->province == 16 ? 'selected':null }}>ปราจีนบุรี</option>
                                        <option value="17" {{ $user->province == 17 ? 'selected':null }}>นครนายก</option>
                                        <option value="18" {{ $user->province == 18 ? 'selected':null }}>สระแก้ว</option>
                                        <option value="19" {{ $user->province == 19 ? 'selected':null }}>นครราชสีมา</option>
                                        <option value="20" {{ $user->province == 20 ? 'selected':null }}>บุรีรัมย์</option>
                                        <option value="21" {{ $user->province == 21 ? 'selected':null }}>สุรินทร์</option>
                                        <option value="22" {{ $user->province == 22 ? 'selected':null }}>ศรีสะเกษ</option>
                                        <option value="23" {{ $user->province == 23 ? 'selected':null }}>อุบลราชธานี</option>
                                        <option value="24" {{ $user->province == 24 ? 'selected':null }}>ยโสธร</option>
                                        <option value="25" {{ $user->province == 25 ? 'selected':null }}>ชัยภูมิ</option>
                                        <option value="26" {{ $user->province == 26 ? 'selected':null }}>อำนาจเจริญ</option>
                                        <option value="27" {{ $user->province == 27 ? 'selected':null }}>หนองบัวลำภู</option>
                                        <option value="28" {{ $user->province == 28 ? 'selected':null }}>ขอนแก่น</option>
                                        <option value="29" {{ $user->province == 29 ? 'selected':null }}>อุดรธานี</option>
                                        <option value="30" {{ $user->province == 30 ? 'selected':null }}>เลย</option>
                                        <option value="31" {{ $user->province == 31 ? 'selected':null }}>หนองคาย</option>
                                        <option value="32" {{ $user->province == 32 ? 'selected':null }}>มหาสารคาม</option>
                                        <option value="33" {{ $user->province == 33 ? 'selected':null }}>ร้อยเอ็ด</option>
                                        <option value="34" {{ $user->province == 34 ? 'selected':null }}>กาฬสินธุ์</option>
                                        <option value="35" {{ $user->province == 35 ? 'selected':null }}>สกลนคร</option>
                                        <option value="36" {{ $user->province == 36 ? 'selected':null }}>นครพนม</option>
                                        <option value="37" {{ $user->province == 37 ? 'selected':null }}>มุกดาหาร</option>
                                        <option value="38" {{ $user->province == 38 ? 'selected':null }}>เชียงใหม่</option>
                                        <option value="39" {{ $user->province == 39 ? 'selected':null }}>ลำพูน</option>
                                        <option value="40" {{ $user->province == 40 ? 'selected':null }}>ลำปาง</option>
                                        <option value="41" {{ $user->province == 41 ? 'selected':null }}>อุตรดิตถ์</option>
                                        <option value="42" {{ $user->province == 42 ? 'selected':null }}>แพร่</option>
                                        <option value="43" {{ $user->province == 43 ? 'selected':null }}>น่าน</option>
                                        <option value="44" {{ $user->province == 44 ? 'selected':null }}>พะเยา</option>
                                        <option value="45" {{ $user->province == 45 ? 'selected':null }}>เชียงราย</option>
                                        <option value="46" {{ $user->province == 46 ? 'selected':null }}>แม่ฮ่องสอน</option>
                                        <option value="47" {{ $user->province == 47 ? 'selected':null }}>นครสวรรค์</option>
                                        <option value="48" {{ $user->province == 48 ? 'selected':null }}>อุทัยธานี</option>
                                        <option value="49" {{ $user->province == 49 ? 'selected':null }}>กำแพงเพชร</option>
                                        <option value="50" {{ $user->province == 50 ? 'selected':null }}>ตาก</option>
                                        <option value="51" {{ $user->province == 51 ? 'selected':null }}>สุโขทัย</option>
                                        <option value="52" {{ $user->province == 52 ? 'selected':null }}>พิษณุโลก</option>
                                        <option value="53" {{ $user->province == 53 ? 'selected':null }}>พิจิตร</option>
                                        <option value="54" {{ $user->province == 54 ? 'selected':null }}>เพชรบูรณ์</option>
                                        <option value="55" {{ $user->province == 55 ? 'selected':null }}>ราชบุรี</option>
                                        <option value="56" {{ $user->province == 56 ? 'selected':null }}>กาญจนบุรี</option>
                                        <option value="57" {{ $user->province == 57 ? 'selected':null }}>สุพรรณบุรี</option>
                                        <option value="58" {{ $user->province == 58 ? 'selected':null }}>นครปฐม</option>
                                        <option value="59" {{ $user->province == 59 ? 'selected':null }}>สมุทรสาคร</option>
                                        <option value="60" {{ $user->province == 60 ? 'selected':null }}>สมุทรสงคราม</option>
                                        <option value="61" {{ $user->province == 61 ? 'selected':null }}>เพชรบุรี</option>
                                        <option value="62" {{ $user->province == 62 ? 'selected':null }}>ประจวบคีรีขันธ์</option>
                                        <option value="63" {{ $user->province == 63 ? 'selected':null }}>นครศรีธรรมราช</option>
                                        <option value="64" {{ $user->province == 64 ? 'selected':null }}>กระบี่</option>
                                        <option value="65" {{ $user->province == 65 ? 'selected':null }}>พังงา</option>
                                        <option value="66" {{ $user->province == 66 ? 'selected':null }}>ภูเก็ต</option>
                                        <option value="67" {{ $user->province == 67 ? 'selected':null }}>สุราษฎร์ธานี</option>
                                        <option value="68" {{ $user->province == 68 ? 'selected':null }}>ระนอง</option>
                                        <option value="69" {{ $user->province == 69 ? 'selected':null }}>ชุมพร</option>
                                        <option value="70" {{ $user->province == 70 ? 'selected':null }}>สงขลา</option>
                                        <option value="71" {{ $user->province == 71 ? 'selected':null }}>สตูล</option>
                                        <option value="72" {{ $user->province == 72 ? 'selected':null }}>ตรัง</option>
                                        <option value="73" {{ $user->province == 73 ? 'selected':null }}>พัทลุง</option>
                                        <option value="74" {{ $user->province == 74 ? 'selected':null }}>ปัตตานี</option>
                                        <option value="75" {{ $user->province == 75 ? 'selected':null }}>ยะลา</option>
                                        <option value="76" {{ $user->province == 76 ? 'selected':null }}>นราธิวาส</option>
                                        <option value="77" {{ $user->province == 77 ? 'selected':null }}>บึงกาฬ</option>
                                    </select>
                                </div>
                            </div>

                            <hr>

                            <h4>Game Information</h4>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Mobile Brand :</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" placeholder="Mobile Brand" name="mobile_brand" id="mobile_brand" value="{{ $user->mobile_brand or '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Mobile Model :</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" placeholder="Mobile Model" name="mobile_model" id="mobile_model" value="{{ $user->mobile_model or '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Network :</label>
                                        <div class="col-lg-9">
                                            <select name="mobile_network" id="mobile_network" class="form-control">
                                                <option value="" disabled selected>Please select network.</option>
                                                <option value="ais" {{$user->mobile_network == "ais" ? 'selected' : null}}>AIS</option>
                                                <option value="dtac" {{$user->mobile_network == "dtac" ? 'selected' : null}}>Dtac</option>
                                                <option value="true" {{$user->mobile_network == "true" ? 'selected' : null}}>True</option>
                                                <option value="other" {{$user->mobile_network == "other" ? 'selected' : null}}>อื่นๆ</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Role :</label>
                                        <div class="col-lg-9">
                                            <select name="game_role" id="game_role" class="form-control">
                                                <option value="" disabled selected>Please select Role.</option>
                                                <option value="Mage" {{$user->game_role == "Mage" ? 'selected' : null}}>Mage</option>
                                                <option value="Support" {{$user->game_role == "Support" ? 'selected' : null}}>Support</option>
                                                <option value="Tank" {{$user->game_role == "Tank" ? 'selected' : null}}>Tank</option>
                                                <option value="Fighter" {{$user->game_role == "Fighter" ? 'selected' : null}}>Fighter</option>
                                                <option value="Assassin" {{$user->game_role == "Assassin" ? 'selected' : null}}>Assassin</option>
                                                <option value="Carry" {{$user->game_role == "Carry" ? 'selected' : null}}>Carry</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Hero :</label>
                                        <div class="col-lg-9">
                                            <select name="game_hero" id="game_hero" class="form-control">
                                                <option value="" disabled selected>Please select Hero.</option>
                                                @foreach (\App\Models\rov\DBHero::orderBy('name','asc')->get() as $hero)
                                                    <option value="{{$hero->slug}}" {{$user->game_hero == $hero->slug ? 'selected' : null}}>{{$hero->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Time :</label>
                                        <div class="col-lg-9">
                                            <select name="game_time" id="game_time" class="form-control">
                                                <option value="" disabled="" selected="">Please select time.</option>
                                                <option value="01:00 - 05:00" {{$user->game_time == "01:00 - 05:00" ? 'selected' : null}}>01:00 - 05:00</option>
                                                <option value="05:00 - 07:00" {{$user->game_time == "05:00 - 07:00" ? 'selected' : null}}>05:00 - 07:00</option>
                                                <option value="07:00 - 09:00" {{$user->game_time == "07:00 - 09:00" ? 'selected' : null}}>07:00 - 09:00</option>
                                                <option value="09:00 - 11:00" {{$user->game_time == "09:00 - 11:00" ? 'selected' : null}}>09:00 - 11:00</option>
                                                <option value="11:00 - 13:00" {{$user->game_time == "11:00 - 13:00" ? 'selected' : null}}>11:00 - 13:00</option>
                                                <option value="13:00 - 15:00" {{$user->game_time == "13:00 - 15:00" ? 'selected' : null}}>13:00 - 15:00</option>
                                                <option value="15:00 - 17:00" {{$user->game_time == "15:00 - 17:00" ? 'selected' : null}}>15:00 - 17:00</option>
                                                <option value="17:00 - 19:00" {{$user->game_time == "17:00 - 19:00" ? 'selected' : null}}>17:00 - 19:00</option>
                                                <option value="19:00 - 21:00" {{$user->game_time == "19:00 - 21:00" ? 'selected' : null}}>19:00 - 21:00</option>
                                                <option value="21:00 - 23:00" {{$user->game_time == "21:00 - 23:00" ? 'selected' : null}}>21:00 - 23:00</option>
                                                <option value="23:00 - 01:00" {{$user->game_time == "23:00 - 01:00" ? 'selected' : null}}>23:00 - 01:00</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-lg-3">Email :</label>--}}
{{--                                <div class="col-lg-9">--}}
{{--                                    <input type="text" class="form-control" placeholder="Email" name="txt_email" id="txt_email" value="{{ $data->email or '' }}" >--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-lg-3">Type :</label>--}}
{{--                                <div class="col-lg-9">--}}
{{--                                    <select name="txt_type" id="txt_type" class="form-control">--}}
{{--                                        <option value="agent" @if(isset($data) && $data->access_level == 'agent') selected="selected" @endif >Agent</option>--}}
{{--                                        <option value="admin" @if(isset($data) && $data->access_level == 'admin') selected="selected" @endif>Admin</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

{{--        <div class="row">--}}
{{--            <div class="col-md-12">--}}
{{--                <div class="panel panel-flat">--}}
{{--                    <div class="panel-body">--}}
{{--                        <div class="text-center">--}}
{{--                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>--}}
{{--                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </form>

@endsection

@section('script')
    <script>

        // function save() {
        //     var msg = '';
        //
        //     if($('#txt_name').val().trim() == '')
        //         msg = 'กรุณากรอก Title';
        //
        //     if(msg != ''){
        //         swal({
        //             title: msg,
        //             confirmButtonColor: "#EF5350",
        //             allowOutsideClick: false,
        //             type: "info"
        //         });
        //     }else{
        //         saveCreate();
        //     }
        //     return false;
        // }

        {{--function saveCreate() {--}}

        {{--    var formData = new FormData($('#form-data')[0]);--}}

        {{--    $.ajax({--}}
        {{--        type: 'POST',--}}
        {{--        url: "{{ route('admin.main.user.addUser') }}",--}}
        {{--        data: formData,--}}
        {{--        dataType: 'json',--}}
        {{--        processData: false,  // tell jQuery not to process the data--}}
        {{--        contentType: false,   // tell jQuery not to set contentType--}}
        {{--        success: function (data) {--}}
        {{--            status_submit = false;--}}
        {{--            if(data.status){--}}
        {{--                swal({--}}
        {{--                    title: data.message,--}}
        {{--                    confirmButtonColor: "#66BB6A",--}}
        {{--                    type: "success",--}}
        {{--                    confirmButtonText: "ตกลง",--}}
        {{--                }).then(function () {--}}
        {{--                    window.location.href = "{{ route('admin.main.user.index') }}";--}}
        {{--                }, function (dismiss) {});--}}
        {{--            }else{--}}
        {{--                swal({--}}
        {{--                    title: data.message,--}}
        {{--                    confirmButtonColor: "#EF5350",--}}
        {{--                    type: "error"--}}
        {{--                }).then(function (dismiss) {});--}}
        {{--            }--}}
        {{--        },--}}
        {{--        error: function (xhr, type) {--}}
        {{--            status_submit = false;--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}

        $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="birth_date"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('body').on('change', 'input[name="txt_banner_image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });
        });
    </script>
@endsection