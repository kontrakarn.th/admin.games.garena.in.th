@extends('layout.layout')

@section('style')
    <style>
        .bigfont{
            font-size: 6em;
            height: 100px;
        }
        .mediumfont{
            font-size: 2em;
            height: 60px;
        }
    </style>
@stop

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Game Info</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Hero</li>
                <li class="active">Game Info</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ isset($data->id) ? $data->id : null  }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">Items</h5>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($items as $item)
                                <div class="col-xs-4 col-md-2 text-center">
                                    <img src="{{$item->image}}" height="80px">
                                    <h6>{{$item->name}}
                                    <br>
                                    <small class="text-muted">
                                        {{$item->type}}
                                    </small></h6>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">Runes</h5>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($runes as $rune)
                                <div class="col-xs-4 col-md-2 text-center">
                                    <img src="{{$rune->image}}" height="80px">
                                    <h6>{{$rune->name}}</h6>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">Spells</h5>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($spells as $spell)
                                <div class="col-xs-4 col-md-2 text-center">
                                    <img src="{{$spell->image}}" height="80px">
                                    <h6>{{$spell->name}}</h6>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">Roles</h5>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($roles as $role)
                                <div class="col-xs-4 col-md-2 text-center">
                                    <img src="{{$role->icon}}" height="80px">
                                    <h6>{{$role->name}}</h6>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">Enchantments</h5>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @foreach ($enchantments as $enchantment)
                                <div class="col-xs-4 col-md-2 text-center">
                                    <img src="{{$enchantment->image}}" height="80px">
                                    <h6>{{$enchantment->name}}
                                        <br>
                                    <small>{{$enchantment->type}} LV. {{$enchantment->level}}</small>
                                    </h6>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

@endsection

@section('script')
    <script>

    </script>
@endsection