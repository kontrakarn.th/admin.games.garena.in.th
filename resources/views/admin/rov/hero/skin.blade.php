@extends('layout.layout')

@section('style')
    <style>
        .select2-selection--single{
            height: 60px;
        }
    </style>
@stop

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span></h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Add new skin for {{$hero->name}}</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ isset($skin) ? $skin->id : null  }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">Add new skin for {{$hero->name}}</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Skin name <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="name" name="name" class="form-control" required="required" value="{{ $skin->name ?? null  }}" placeholder="Default">
                                    <input type="hidden" id="hero_id" name="hero_id" class="form-control" required="required" value="{{ $hero->id ?? null }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Skin image <span class="text-danger">*</span></label>
                            <div class="col-lg-5">

                                <input type="file" id="image" name="image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*">
                                <br>
                                <div class="help-block">

                                </div>
                            </div>
                            <div class="col-lg-3">
                                @if(!empty($skin->image))
                                    <div class="input-group">
                                        <img src="{{ $skin->image ?? null }}" class="img-responsive" alt="" id="preview_image">
                                    </div>
                                @else
                                    <img id="preview_image" src="" style="width: 100%;display: none" class="img-responsive"/>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Skin banner image <span class="text-danger">*</span></label>
                            <div class="col-lg-5">

                                <input type="file" id="image_banner" name="image_banner" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*">
                                <br>
                                <div class="help-block">

                                </div>
                            </div>
                            <div class="col-lg-3">
                                @if(!empty($skin->image_banner))
                                    <div class="input-group">
                                        <img src="{{ $skin->image_banner ?? null }}" class="img-responsive" alt="" id="preview_image_banner">
                                    </div>
                                @else
                                    <img id="preview_image_banner" src="" style="width: 100%;display: none" class="img-responsive"/>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <a href="{{route('admin.rov.hero.edit',[$hero])}}">
                                <span type="reset" class="btn btn-default" id="reset">Back</span>
                            </a>
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveCreate()">Save <i class="icon-arrow-right14 position-right"></i></button>
                            @if (isset($skin))
                                <a href="{{route('admin.rov.hero.deleteSkin',[$hero,$skin])}}">
                                    <span type="reset" class="btn btn-danger" id="reset">Delete</span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.rov.hero.saveSkin') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.rov.hero.edit',[$hero]) }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $(id).show();
                    $(id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready( () => {

            function formatData (data) {
                if (!data.id) { return data.text; }
                var image = $(data.element).data('image');
                var $result= $(
                    '<span><img src="'+image+'" height="50px" style="margin-right: 10px"/> ' + data.text + '</span>'
                );
                return $result;
            };

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $(".js-example-responsive").select2({
                // width: 'resolve',
                templateResult: formatData,
                templateSelection: formatData
            });

            $("#image").change(function() {
                readURL(this,'#preview_image');
            });

            $("#image_banner").change(function() {
                readURL(this,'#preview_image_banner');
            });

        });

    </script>
@endsection