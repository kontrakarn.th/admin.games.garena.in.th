@extends('layout.layout')

@section('style')
    <style>
        .bigfont{
            font-size: 6em;
            height: 100px;
        }
        .mediumfont{
            font-size: 2em;
            height: 60px;
        }
    </style>
@stop

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{isset($data->id) ? "Edit":"Add"}}  {{ $data->name ?? null }} </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">{{isset($data->id) ? "Edit":"Add"}} Hero</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ isset($data->id) ? $data->id : null  }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">@if(isset($data->id))แก้ไข Hero @else เพิ่ม Hero @endif</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">
                            <div class="col-xs-12">
                                <div class="text-center">
                                    @if(!empty($data->image))
                                        <img src="{{ $data->image ?? null }}" alt="" id="preview">
                                    @else
                                        <img id="preview" src="" style="width: 100%;display: none"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <div class="col-xs-12">
                                    <input type="text" id="name" name="name" class="form-control text-center bigfont" required="required" value="{{ $data->name ?? null }}" placeholder="ชื่อ Hero">
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <div class="col-lg-12">
                                    <input type="text" id="slogan" name="slogan" class="form-control text-center mediumfont" required="required" value="{{ $data->slogan ?? null }}" placeholder="Slogan">
                                </div>
                            </div>

                            @if (isset($data->id))
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Active</label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            @if ($data->status === 'active')
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">
                                            @else
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Icon Image</label>
                                <div class="col-lg-5">
                                    <input type="file" id="image" name="image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->image ?? null }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมี Ratio : 1 ต่อ 1
                                    </span>
                                        <p></p>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->banner_image))
                                        <div class="input-group">
                                            <img src="{{ $data->banner_image or '' }}" class="img-responsive" alt="" id="preview">
                                        </div>
                                    @else
                                        <img id="preview" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

{{--                            <div class="form-group has-feedback">--}}
{{--                                <label class="control-label col-lg-2">Banner Hero<span class="text-danger">*</span></label>--}}
{{--                                <div class="col-lg-5">--}}
{{--                                    <input type="file" id="image_bg" name="image_bg" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required" value="{{ $data->image_bg ?? null }}">--}}
{{--                                    <br>--}}
{{--                                    <div class="help-block">--}}
{{--                                    <span class="label label-success arrowed-right arrowed-in main__color">--}}
{{--                                        รูปควรมีขนาด 640 x 1137 px--}}
{{--                                    </span>--}}
{{--                                        <p></p>--}}
{{--                                    <span class="label label-danger arrowed-right arrowed-in">--}}
{{--                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB--}}
{{--                                    </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    @if(!empty($data->image_bg))--}}
{{--                                        <div class="input-group">--}}
{{--                                            <img src="{{ $data->image_bg or '' }}"  class="img-responsive" id="preview_mobile">--}}
{{--                                        </div>--}}
{{--                                    @else--}}
{{--                                        <img id="preview_mobile" src="" style="width: 100%;display: none" class="img-responsive"/>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Flag <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>

                                        <select id="flag" name="flag" class="form-control" required="required">
                                            <option value="" disabled selected>กรุณาเลือกสถานะ</option>
                                            <option value="none" {{isset($data) ? $data->flag == "none" ? "selected" : null : null}}>None</option>
                                            <option value="new" {{isset($data) ? $data->flag == "new" ? "selected" : null : null}}>New</option>
                                            <option value="free" {{isset($data) ? $data->flag == "free" ? "selected" : null : null}}>Free</option>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Story <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <textarea type="text" id="description" name="description" class="form-control" required="required" style="height: 120px">{{ $data->description ?? null }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">คำแนะนำ</label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <textarea type="text" id="suggestion" name="suggestion" class="form-control" required="required" style="height: 120px">{{ $data->suggestion ?? null }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Hero Spotlight <span class="text-danger">(Youtube)</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="spotlight" name="spotlight" class="form-control" required="required" value="{{ $data->spotlight ?? null }}">
                                        <span class="label label-default">
                                            อยู่ในรูปแบบ https://www.youtube.com/watch?v=xxxxxxxxxxx เท่านั้น
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Role <span class="text-danger">*</span></label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <select id="role_id" name="role_id" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                        <option value="" disabled selected>กรุณาเลือก Primary Role</option>
                                        @foreach ($roles as $role)
                                            <option value="{{$role->id}}" {{isset($data) ? $data->role_id == $role->id ? "selected" : null : null}} data-image="{{$role->icon}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <label class="control-label col-lg-2">Second Role</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <select id="role_second_id" name="role_second_id" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                        <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                        @foreach ($roles as $role)
                                            <option value="{{$role->id}}" {{isset($data) ? $data->role_second_id == $role->id ? "selected" : null : null}} data-image="{{$role->icon}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Spell <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>

                                    <select id="spell" name="spell" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                        <option value="" disabled selected>กรุณาเลือก Spell</option>
                                        @foreach ($spells as $spell)
                                            <option value="{{$spell->id}}" {{isset($data) ? $data->spell == $spell->id ? "selected" : null : null}} data-image="{{$spell->image}}">{{$spell->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @isset($data)
            <div class="row">
                <div class="col-md-12">
                    <h3>Skills</h3>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            @if ($data->skills()->count() > 0)
                                <p>You have {{$data->skills()->count()}} skills,
                                    <a href="{{route('admin.rov.hero.addSkill',[$data])}}">
                                        Click here
                                    </a>
                                    to add new skill for {{$data->name}}
                                </p>

                                <div class="row">
                                    @foreach ($data->skills()->orderBy('type')->get() as $skill)
                                        <div class="col-xs-2 text-center">
                                            <a href="{{route('admin.rov.hero.editSkill',[$data,$skill])}}">
                                                <img src="{{$skill->image}}" height="100px">
                                                <p style="margin-top: 20px">
                                                    <b>{{$skill->name}}</b><br>
                                                    @if ($skill->type == 0)
                                                        <small>Passive</small>
                                                    @elseif($skill->type == 3 )
                                                        <small>Ultimate</small>
                                                    @else
                                                        <small>Skill {{$skill->type}}</small>
                                                    @endif
                                                </p>
                                            </a>
                                            @if ($skill->isEx == "1")
                                                <small class="text-warning">Extra</small>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>

                            @else
                                <div class="text-center">
                                    <p>You have 0 skills. Click below button to add new skill for {{$data->name}}</p>
                                    <a href="{{route('admin.rov.hero.addSkill',[$data])}}">
                                        <span class="btn btn-success">Add new skill</span>
                                    </a>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3>Items</h3>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            @if ($data->itemsets()->count() > 0)
                                <p>You have {{$data->itemsets()->count()}} set items,
                                    <a href="{{route('admin.rov.hero.addItemSet',[$data])}}">
                                        Click here
                                    </a>
                                    to add new set item for {{$data->name}}
                                </p>

                                @foreach ($data->itemsets as $itemset)
                                    <a href="{{route('admin.rov.hero.editItemSet',[$data,$itemset])}}">
                                    <span class="btn btn-info">
                                        {!! $itemset->preview !!}
                                    </span>
                                    </a>
                                @endforeach
                            @else
                                <div class="text-center">
                                    <p>You have 0 set items. Click below button to add new set for {{$data->name}}</p>
                                    <a href="{{route('admin.rov.hero.addItemSet',[$data])}}">
                                        <span class="btn btn-success">Add new Set item</span>
                                    </a>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3>Skins</h3>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            @if ($data->skins()->count() > 0)
                                <p>You have {{$data->skins()->count()}} skins,
                                    <a href="{{route('admin.rov.hero.addSkin',[$data])}}">
                                        Click here
                                    </a>
                                    to add new skin for {{$data->name}}
                                </p>

                                <div class="row">
                                    @foreach ($data->skins()->get() as $skin)
                                        <div class="col-xs-3 text-center">
                                            <a href="{{route('admin.rov.hero.editSkin',[$data,$skin])}}">
                                                <img src="{{$skin->image}}" height="200px">
                                                <p style="margin-top: 20px">
                                                    <b>{{$skin->name}}</b>
                                                </p>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>

                            @else
                                <div class="text-center">
                                    <p>You have 0 skin. Click below button to add new skin for {{$data->name}}</p>
                                    <a href="{{route('admin.rov.hero.addSkin',[$data])}}">
                                        <span class="btn btn-success">Add new skin</span>
                                    </a>
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endisset



        <div class="row">
            <div class="col-md-12">
                <h3>Runes</h3>
                <div class="panel panel-flat">
                    <div class="panel-body">
                        @for ($i = 1; $i <= 6; $i++)
                            @php
                                $num            = "rune".$i;
                                $user_rune      = $userRunes[$i-1];
                                $amount_rune    = $runesAmount[$i-1];

                            @endphp
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Rune {{$i % 2 == 1 ? $i : $i-1 .' (เสริม)'}}</label>
                                    <div class="col-lg-10">
                                        <select name="{{$num}}" id="{{$num}}" class="form-control js-example-responsive">
                                            <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">ไม่มี</option>
                                            @foreach ($runes as $rune)
                                                <option value="{{$rune->id}}" data-image="{{$rune->image}}" {{$user_rune == $rune->id ? "selected" : null}}>{{$rune->name}}</option>
                                            @endforeach
                                        </select>
                                        <input type="number" min="0" max="0" placeholder="จำนวน" class="form-control" name="amount_{{$num}}" value="{{$amount_rune ?? 0}}">
                                    </div>
                                </div>
                            </div>
                        @endfor

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h3>Enchantments</h3>
                <div class="panel panel-flat">
                    <div class="panel-body">
                        @for ($i = 1; $i <= 5; $i++)
                            @php
                                $num            = "enchantment".$i;
                                $userEnc        = $userEnchantment[$i-1];
                            @endphp
                            <div class="col-xs-6 col-md-4" style="{{$i<4?"background: #f0f0f0":null}}">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Enc {{$i}}</label>
                                    <div class="col-lg-10">
                                        <select name="{{$num}}" id="{{$num}}" class="form-control js-example-responsive">
                                            <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                            @foreach ($enchantments as $enchantment)
                                                <option value="{{$enchantment->id}}" data-image="{{$enchantment->image}}" {{$userEnc == $enchantment->id ? "selected":null}}>[ {{$enchantment->type}} lv.{{$enchantment->level}} ] {{$enchantment->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>


        function uploadImage(file,id,preivew) {
            var formData = new FormData();
            formData.append("file", file.files[0]);
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.rov.saveImage') }}",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    $(id).val(data.message)
                    $(preivew).attr('src',data.message)
                },
                error: function (xhr, type) {
                    swal({
                        title: 'Cant Upload image',
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            });
        }


        function save() {
            saveCreate();
        }

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.rov.hero.saveData') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.rov.hero.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURLSkin(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    const id = "#preview_"+input.id;
                    $(id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            id = $('#id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="txt_show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('body').on('change', 'input[name="txt_banner_image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#image").change(function() {
                readURL(this,'preview');
            });

            $("#image_bg").change(function() {
                readURL(this,'preview_mobile');
            });

            $(".switch").on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.rov.hero.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                            });
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                            });
                        }
                    }
                );
            });

            function formatData (data) {
                if (!data.id) { return data.text; }
                var image = $(data.element).data('image');
                var $result= $(
                    '<span><img src="'+image+'" height="30px" style="margin-right: 10px"/> ' + data.text + '</span>'
                );
                return $result;
            };

            $(".js-example-responsive").select2({
                // width: 'resolve',
                templateResult: formatData,
                templateSelection: formatData
            });


            $('.btnAddnewSkin').on('click',function () {
               window.$JSC && $JSC.log("Add new");
               var str = Math.random().toString(36).substring(7);
               html = "<div class=\"panel panel-flat parentSkin\">\n" +
                   "            <div style=\"display: none\">\n" +
                   "                <input type=\"text\" name=\"skin_id[]\">\n" +
                   "                <input type=\"text\" name=\"skin_images[]\" id=\"skin_images_"+str+"\">\n" +
                   "                <input type=\"text\" name=\"skin_banner_images[]\" id=\"skin_banner_images_"+str+"\">\n" +
                   "            </div>\n" +
                   "\n" +
                   "            <div class=\"panel-body\">\n" +
                   "                <div class=\"form-group has-feedback\">\n" +
                   "                    <label class=\"control-label col-lg-2\">Skin Name <span class=\"text-danger\">*</span></label>\n" +
                   "                    <div class=\"col-lg-8\">\n" +
                   "                        <div class=\"input-group\">\n" +
                   "                            <span class=\"input-group-addon\"><i class=\"icon-pencil\"></i></span>\n" +
                   "                            <input type=\"text\" id=\"skin_name\" name=\"skin_name[]\" class=\"form-control\" required=\"required\">\n" +
                   "                        </div>\n" +
                   "                    </div>\n" +
                   "                </div>\n" +
                   "\n" +
                   "                <div class=\"form-group has-feedback\">\n" +
                   "                    <label class=\"control-label col-lg-2\">Skin Image<span class=\"text-danger\">*</span></label>\n" +
                   "                    <div class=\"col-lg-5\">\n" +
                   "                        <input type=\"file\" class=\"file-styled skin_banner_image\" data-type=\"jpg,jpeg,png\" data-show-caption=\"false\" data-show-upload=\"false\" data-browse-class=\"btn btn-primary btn-sm\" data-remove-class=\"btn btn-default btn-sm\" accept=\"image/*\" required=\"required\" onchange=\"uploadImage(this,'#skin_images_"+str+"','#preview_skin_images_"+str+"')\">\n" +
                   "                        <br>\n" +
                   "                        <div class=\"help-block\">\n" +
                   "                                    <span class=\"label label-success arrowed-right arrowed-in main__color\">\n" +
                   "                                        รูปควรมีขนาด 800 x 704 px\n" +
                   "                                    </span>\n" +
                   "                            <p></p>\n" +
                   "                            <span class=\"label label-danger arrowed-right arrowed-in\">\n" +
                   "                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB\n" +
                   "                                    </span>\n" +
                   "                        </div>\n" +
                   "                    </div>\n" +
                   "                    <div class=\"col-lg-3\">\n" +
                   "                        <img id=\"preview_skin_images_"+str+"\" src=\"\" style=\"width: 100%;\" class=\"img-responsive\"/>\n" +
                   "                    </div>\n" +
                   "                </div>\n" +
                   "\n" +
                   "                <div class=\"form-group has-feedback\">\n" +
                   "                    <label class=\"control-label col-lg-2\">Skin Banner</label>\n" +
                   "                    <div class=\"col-lg-5\">\n" +
                   "                        <input type=\"file\" class=\"file-styled skin_image\" data-type=\"jpg,jpeg,png\" data-show-caption=\"false\" data-show-upload=\"false\" data-browse-class=\"btn btn-primary btn-sm\" data-remove-class=\"btn btn-default btn-sm\" accept=\"image/*\" required=\"required\" onchange=\"uploadImage(this,'#skin_banner_images_"+str+"','#preview_skin_banner_images_"+str+"')\">\n" +
                   "                        <br>\n" +
                   "                        <div class=\"help-block\">\n" +
                   "                                    <span class=\"label label-success arrowed-right arrowed-in main__color\">\n" +
                   "                                        รูปควรมีขนาด 1921 x 1189 px\n" +
                   "                                    </span>\n" +
                   "                            <p></p>\n" +
                   "                            <span class=\"label label-danger arrowed-right arrowed-in\">\n" +
                   "                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB\n" +
                   "                                    </span>\n" +
                   "                        </div>\n" +
                   "                    </div>\n" +
                   "                    <div class=\"col-lg-3\">\n" +
                   "                        <img id=\"preview_skin_banner_images_"+str+"\" src=\"\" style=\"width: 100%;\" class=\"img-responsive\"/>\n" +
                   "                    </div>\n" +
                   "                </div>\n" +
                   "            </div>\n" +
                   "        </div>"
               $('#skinZone').append(html);
            });


            $('.btnDeleteSkin').on('click',function () {
                const id = $(this).parents('.parentSkin').attr('data-id');
                if (id > 0){
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#EF5350",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }).then(function () {
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('admin.rov.hero.deletedSkin') }}",
                            data: {
                                'id': id
                            },
                            dataType: 'json',
                            // processData: false,  // tell jQuery not to process the data
                            // contentType: false,   // tell jQuery not to set contentType
                            success: function (data) {
                                status_submit = false;
                                if(data.status){
                                    swal({
                                        title: data.message,
                                        confirmButtonColor: "#66BB6A",
                                        type: "success",
                                        confirmButtonText: "ตกลง",
                                    }).then(function () {
                                        location.reload();
                                    }, function (dismiss) {});
                                }else{
                                    swal({
                                        title: data.message,
                                        confirmButtonColor: "#EF5350",
                                        type: "error"
                                    }).then(function (dismiss) {});
                                }
                            },
                            error: function (xhr, type) {
                                status_submit = false;
                            }
                        });
                    }, function (dismiss) {});
                } else{
                    swal({
                        title: 'Something went wrong',
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function () {

                    });
                }

            });

        });
    </script>
@endsection