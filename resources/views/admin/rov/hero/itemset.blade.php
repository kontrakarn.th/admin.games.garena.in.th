@extends('layout.layout')

@section('style')
    <style>
        .select2-selection--single{
            height: 60px;
        }
    </style>
@stop

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span></h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Add new item set for {{$hero->name}}</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ isset($itemset) ? $itemset->id : null  }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">Add new item set for {{$hero->name}}</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Set name <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="name" name="name" class="form-control" required="required" value="{{ $itemset->name ?? null  }}" placeholder="เซตเข้าป่า เอาตัวรอด">
                                    <input type="hidden" id="hero_id" name="hero_id" class="form-control" required="required" value="{{ $hero->id }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Items <span class="text-danger">*</span></label>
                            <div class="col-lg-8">

                                <div class="col-xs-6 col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">1</label>
                                        <div class="col-lg-10">
                                            <select name="item1" id="item1" class="form-control js-example-responsive">
                                                <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                                @foreach ($items as $item)
                                                    <option value="{{$item->id}}" data-image="{{$item->image}}" {{isset($itemset) ? $itemset->item1 == $item->id ? "selected" : null : null}}>[ {{$item->type}} ] {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">2</label>
                                        <div class="col-lg-10">
                                            <select name="item2" id="item2" class="form-control js-example-responsive">
                                                <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                                @foreach ($items as $item)
                                                    <option value="{{$item->id}}" data-image="{{$item->image}}" {{isset($itemset) ? $itemset->item2 == $item->id ? "selected" : null : null}}>[ {{$item->type}} ] {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">3</label>
                                        <div class="col-lg-10">
                                            <select name="item3" id="item1" class="form-control js-example-responsive">
                                                <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                                @foreach ($items as $item)
                                                    <option value="{{$item->id}}" data-image="{{$item->image}}" {{isset($itemset) ? $itemset->item3 == $item->id ? "selected" : null : null}}>[ {{$item->type}} ] {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">4</label>
                                        <div class="col-lg-10">
                                            <select name="item4" id="item1" class="form-control js-example-responsive">
                                                <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                                @foreach ($items as $item)
                                                    <option value="{{$item->id}}" data-image="{{$item->image}}" {{isset($itemset) ? $itemset->item4 == $item->id ? "selected" : null : null}}>[ {{$item->type}} ] {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">5</label>
                                        <div class="col-lg-10">
                                            <select name="item5" id="item1" class="form-control js-example-responsive">
                                                <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                                @foreach ($items as $item)
                                                    <option value="{{$item->id}}" data-image="{{$item->image}}" {{isset($itemset) ? $itemset->item5 == $item->id ? "selected" : null : null}}>[ {{$item->type}} ] {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-6">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">6</label>
                                        <div class="col-lg-10">
                                            <select name="item6" id="item1" class="form-control js-example-responsive">
                                                <option value="0" selected data-image="https://static2.garena.in.th/data/rov/content/content/d2f3fe47f965ab6dc78221c7fe26cc74.png">-</option>
                                                @foreach ($items as $item)
                                                    <option value="{{$item->id}}" data-image="{{$item->image}}" {{isset($itemset) ? $itemset->item6 == $item->id ? "selected" : null : null}}>[ {{$item->type}} ] {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <a href="{{route('admin.rov.hero.edit',[$hero])}}">
                                <span type="reset" class="btn btn-default" id="reset">Back</span>
                            </a>
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveCreate()">Save <i class="icon-arrow-right14 position-right"></i></button>
                            @if (isset($itemset))
                                <a href="{{route('admin.rov.hero.deleteItemSet',[$hero,$itemset])}}">
                                    <span type="reset" class="btn btn-danger" id="reset">Delete</span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.rov.hero.saveItemSet') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.rov.hero.edit',[$hero]) }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        $(document).ready( () => {

            function formatData (data) {
                if (!data.id) { return data.text; }
                var image = $(data.element).data('image');
                var $result= $(
                    '<span><img src="'+image+'" height="50px" style="margin-right: 10px"/> ' + data.text + '</span>'
                );
                return $result;
            };

            $(".js-example-responsive").select2({
                // width: 'resolve',
                templateResult: formatData,
                templateSelection: formatData
            });

        });

    </script>
@endsection