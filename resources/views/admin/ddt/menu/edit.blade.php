@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - edit Menu </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">edit Menu</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <input type="hidden" name="id" id="id" value="{{ $id }}">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">แก้ไขข้อมูล</h5>
                    
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    

                    <fieldset class="content-group">
                        @if($data->parent_id != 0)
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Parent <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <select name="txt_parent_id" id="txt_parent_id" class="form-control" required="required">
                                        @foreach($parentMenuData as $menu)
                                            <option value="{{ $menu->id }}" {{ $menu->id == $data->parent_id ? 'selected' : '' }}>{{ $menu->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif
                      
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_name" name="txt_name" class="form-control" required="required" value="{{ $data->name }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Slug <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_slug" name="txt_slug" class="form-control" required="required" value="{{ $data->slug }}" >
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">URL</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="txt_url" name="txt_url" class="form-control" required="required" value="{{ $data->url }}" >
                                </div>
                            </div>
                        </div>

                        <!-- <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Show <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="checkbox" id="txt_show" name="txt_show" data-id="{{ $data->id }}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" {{ $data->show == true ? 'checked="checked"' : '' }} >
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Share title <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_share_title" name="txt_share_title" class="form-control" required="required" value="{{ $data->share_title }}" >
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Share description <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_share_description" name="txt_share_description" class="form-control" required="required" value="{{ $data->share_description }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Keyword <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_keyword" name="txt_keyword" class="form-control" required="required" value="{{ $data->keyword }}" >
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Share Image</label>
                            <div class="col-lg-8">
                            @if(!empty($data->share_image))
                                <div class="input-group">
                                    <img src="{{ $data->share_image or '' }}" class="img-responsive" alt="">
                                </div>
                            @endif
                                <input type="file" id="txt_image" name="txt_image" class="file-styled" data-type="jpg,jpeg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 522(W) x 150(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        <button type="button" id="btn-save" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    function detailsAndRulesUploadImage(file,editor,welEditable) {

        var formData = new FormData();
        formData.append("file", file);
        formData.append("file", file);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.ddt.menu.saveImage') }}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                $('.summernote-height').summernote(
                    'insertImage',
                    data.message
                );
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }

    function save(status) {
        var msg = '';
        console.log(status);

        if($('#txt_name').val().trim() == '')
            msg = 'กรุณากรอก Title';

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{

       
            saveCreate(status);
        }
        return false;
    }

    function saveCreate(status) {
       
        var formData = new FormData($('#form-data')[0]);
        // formData.append("status", status);
        console.log(formData);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.ddt.menu.saveMenu') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                console.log(data);
                
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

$(document).ready(function() {
    App.initSwitch(".switch");
    slug = $('#txt_slug').val();

    $('.summernote-height').summernote({
        height: 500,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        imageAttributes:{
            icon:'<i class="note-icon-pencil"/>',
            removeEmpty: false, // true = remove attributes | false = leave empty if present
            disableUpload: true // true = don't display Upload Options | Display Upload Options
        },
        fontNames: [
            'Kanit','Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande',
            'Lucida Sans', 'Tahoma', 'Times', 'Times New Roman', 'Verdana'
        ],
        callbacks: {
            onImageUpload: function(files,editor,welEditable) {
                detailsAndRulesUploadImage(files[0], editor, welEditable);
            }
        }
    });

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('change', 'input[name="txt_image"]', function() {

        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });
});
</script>
@endsection