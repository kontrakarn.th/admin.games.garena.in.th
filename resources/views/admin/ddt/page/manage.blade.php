
@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - manage Page </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">manage Page</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="table-header main__color main__background">ข้อมูล</div>
        <div class="table-responsive">
            <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>URL</th>
                        <th>Status</th>
                        <th>Parent</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@push('script-head')
@endpush

@section('script')
<script type="text/javascript">
$(function(){
    
    $('#datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin.ddt.page.datatable') }}',
        method: 'post',
        columns: [
            {   data: 'id',
                name: 'id' 
            },
            {   data: 'name',
                name: 'name' 
            },
            {   data: 'slug',
                name: 'slug' 
            },
            {   data: 'url',
                name: 'url' 
            },
            {   data: 'status',
                name: 'status' 
            },
            {   data: 'parent_id',
                name: 'parent_id' ,
                visible: false
            },
        ],
        order: [[ 0, "asc" ]],
        drawCallback: function( settings ) {
            var api = this.api();
            console.log('DataTables has redrawn the table');
            // console.log( api.rows( {page:'current'} ).data() );

            var _ObjTable = $('#datatable');
            App.initSwitch(_ObjTable.find(".switch"));
            _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                console.log('test');
                console.log($(this).attr('data-id'));
                console.log(state);
            });
        },
        // drawCallback : function(settings) {
        //      App.initSwitch(_ObjTable.find(".switch"));
        //      _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                // event.preventDefault();
           //      $.post(
        {{-- //             "{{ route('admin.users.changeStatus') }}", --}}
        //             { id: $(this).attr('data-id'), status: state },
        //             function( data ) {
                    //      if(data.status){
        //                     swal({
                    //             title: data.msg,
                    //             confirmButtonColor: "#4caf50",
                    //             type: "success"
                    //         }).then(function () {

        //                  });
                    //      }else{
        //                     swal({
                    //             title: data.msg,
                    //             confirmButtonColor: "#EF5350",
                    //             type: "info"
                    //         }).then(function () {

        //                  });
                    //      }
                    // }
        //         );
        //     });
        // }
    });

    

});
</script>
@endsection
