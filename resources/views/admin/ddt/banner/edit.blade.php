@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{$page}} banner {{ $data->alt or '' }} </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">{{$page}} banner</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <input type="hidden" name="txt_id" id="txt_id" value="{{ $data->id or '' }}">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">@if(isset($data->id))แก้ไขข้อมูล@else เพิ่มข้อมูล @endif</h5>
                    
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    

                    <fieldset class="content-group">
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Title <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_alt" name="txt_alt" class="form-control" required="required" value="{{ $data->alt or '' }}" >
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">URL <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="txt_url" name="txt_url" class="form-control" required="required" value="{{ $data->url or '' }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Menu <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>

                                    <select id="txt_menu_id" name="txt_menu_id" class="form-control" required="required">
                                        @foreach($menues as $menu)
                                            <option value="{{ $menu->id }}" @if(isset($data->menu_id)) {{ $data->menu_id == $menu->id? 'selected' : '' }} @endif>{{ $menu->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Show Date Time <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_show_datetime" name="txt_show_datetime" class="form-control" required="required" value="{{ $data->show_datetime or '' }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Banner Image <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                            @if(!empty($data->image))
                                <div class="input-group">
                                    <img src="{{ $data->image or '' }}" class="img-responsive" alt="">
                                </div>
                            @endif

                                <input type="file" id="txt_banner_image" name="txt_banner_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        RANK รูปควรมีขนาด 400(W) x 213(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        NEWS รูปควรมีขนาด 1175(W) x 330(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        SCREENSHOT รูปควรมีขนาด 758(W) x 330(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    function detailsAndRulesUploadImage(file,editor,welEditable) {

        var formData = new FormData();
        formData.append("file", file);
        formData.append("file", file);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.ddt.content.saveImage') }}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                $('.summernote-height').summernote(
                    'insertImage',
                    data.message
                );
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }

    function save() {
        var msg = '';

        if($('#txt_alt').val().trim() == '')
            msg = 'กรุณากรอก Title';

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{
            saveCreate();
        }
        return false;
    }

    function saveCreate() {

        var formData = new FormData($('#form-data')[0]);

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.ddt.banner.saveBanner') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        window.location.href = "{{ route('admin.ddt.banner.index') }}";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

$(document).ready(function() {
    App.initSwitch(".switch");
    slug = $('#txt_slug').val();
    id = $('#txt_id').val();

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('change', 'input[name="txt_banner_image"]', function() {

        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });
});
</script>
@endsection