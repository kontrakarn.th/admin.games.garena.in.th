@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - edit user {{ $data->alt or '' }} </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">edit user</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <input type="hidden" name="txt_id" id="txt_id" value="{{ $data->id or '' }}">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">@if(isset($data->id))แก้ไขข้อมูล@else เพิ่มข้อมูล @endif</h5>
                    
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    

                    <fieldset class="content-group">

                            <div class="form-group">
                                <label class="control-label col-lg-3">Name :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Name" name="txt_name" id="txt_name" value="{{ $data->name or '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Email :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Email" name="txt_email" id="txt_email" value="{{ $data->email or '' }}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Type :</label>
                                <div class="col-lg-9">
                                    <select name="txt_type" id="txt_type" class="form-control">
                                        <option value="agent" @if(isset($data) && $data->access_level == 'agent') selected="selected" @endif >Agent</option>
                                        <option value="admin" @if(isset($data) && $data->access_level == 'admin') selected="selected" @endif>Admin</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group pt-15">
                                <label class="control-label col-lg-3">Game :</label>
                                <div class="col-lg-9">

                                    @foreach ($games as $game)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="txt_game[]" value="{{ $game->id }}" class="checkbox-styled" @if(isset($game) && isset($userGameRelation) && in_array($game->id, $userGameRelation->toArray()) == 'agent') checked="checked" @endif>
                                            <span class="btn">{{ $game->name }}</span>
                                        </label>
                                    </div>
                                    @endforeach

                                    {{-- @foreach($menues as $menu)
                                            <option value="{{ $menu->id }}" @if(isset($data->menu_id)) {{ $data->menu_id == $menu->id? 'selected' : '' }} @endif>{{ $menu->name }}</option>
                                        @endforeach --}}

                                </div>
                            </div>

                        </fieldset>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>

    function save() {
        var msg = '';

        if($('#txt_name').val().trim() == '')
            msg = 'กรุณากรอก Title';

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{
            saveCreate();
        }
        return false;
    }

    function saveCreate() {

        var formData = new FormData($('#form-data')[0]);

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.main.user.addUser') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        window.location.href = "{{ route('admin.main.user.index') }}";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

$(document).ready(function() {
    App.initSwitch(".switch");
    slug = $('#txt_slug').val();
    id = $('#txt_id').val();

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('change', 'input[name="txt_banner_image"]', function() {

        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });
});
</script>
@endsection