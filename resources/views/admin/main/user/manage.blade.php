
@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - manage User </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">manage User</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat ">
            <div class="panel-heading">
                <h5 class="panel-title">User Manager</h5>
                
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="panel-body">
                <a href="{{ route('admin.main.user.add') }}" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม User </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header main__color main__background">ข้อมูล</div>
        <div class="table-responsive">
            <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>


<!-- Basic modal -->
    <div id="modal_add_new_user" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Add New User</h5>
                </div>

                <div class="modal-body">

                    <form class="form-horizontal" action="#" id="form_add_new_user">
                        <fieldset class="content-group">

                            <div class="form-group">
                                <label class="control-label col-lg-3">Name :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Name" name="txt_name" id="txt_name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Email :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Email" name="txt_email" id="txt_email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Type :</label>
                                <div class="col-lg-9">
                                    <select name="txt_type" id="txt_type" class="form-control">

                                        <option value="agent">Agent</option>
                                        <option value="admin">Admin</option>
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="form-group pt-15">
                                <label class="control-label col-lg-3">Game :</label>
                                <div class="col-lg-9">
                                    <div class="checkbox">
                                        <label>
                                            <div class="checker"><span class="checked"><input type="checkbox" class="styled" checked="checked" ></span></div>
                                            Checked styled
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <div class="checker"><span><input type="checkbox" class="styled"></span></div>
                                            Unchecked styled
                                        </label>
                                    </div>

                                    <div class="checkbox disabled">
                                        <label>
                                            <div class="checker disabled"><span><input type="checkbox" class="styled" disabled="disabled"></span></div>
                                            Disabled styled
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addNewUser()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<!-- /Basic modal -->
@endsection

@push('script-head')
@endpush

@section('script')
<script type="text/javascript">

    function addNewUser() {
        var formData = new FormData($('#form_add_new_user')[0]);

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.main.user.addUser') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

$(function(){
    
    $('#datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin.main.user.userdatatable') }}',
        method: 'post',
        columns: [
            {   data: 'id',
                name: 'id' 
            },
            {   data: 'name',
                name: 'name' 
            },
            {   data: 'email',
                name: 'email' 
            },
            {   data: 'access_level',
                name: 'access_level' 
            },
            {   data: 'status',
                name: 'status' 
            },
            {   data: 'action',
                name: 'action' 
            },
        ],
        order: [[ 0, "asc" ]],
        drawCallback: function( settings ) {
            var api = this.api();
            console.log('DataTables has redrawn the table');
            // console.log( api.rows( {page:'current'} ).data() );

            var _ObjTable = $('#datatable');
            App.initSwitch(_ObjTable.find(".switch"));
            _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                console.log('test');
                console.log($(this).attr('data-id'));
                console.log(state);
            });
        },
        drawCallback : function(settings) {
            var _ObjTable = $('#datatable');
             App.initSwitch(_ObjTable.find(".switch"));
             _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.main.user.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                         if(data.status){
                            swal({
                                title: data.msg,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                         });
                         }else{
                            swal({
                                title: data.msg,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                         });
                         }
                    }
                );
            });
        }
    });

    

});
</script>
@endsection
