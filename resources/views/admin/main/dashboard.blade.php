
@extends('layout.layout')

@section('content')

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h6 class="panel-title">Welcome</h6>

                <div class="panel-body">
                    Hi, {{ Auth::user()->name }}. <br/>
                    Please select the tool you need from the menu on your left.
                </div>
            </div>
        </div>
    </div>
</div>
                    
@endsection
