@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - edit content - {{$slug or ''}} </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">edit content</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
<input type="hidden" id="txt_id" name="txt_id" value="{{ $data->id or '' }}" >
<input type="hidden" id="txt_category" name="txt_category" value="{{ 'news' }}" >
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">แก้ไขข้อมูล</h5>
                    
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    

                    <fieldset class="content-group">
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Title <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_title" name="txt_title" class="form-control" required="required" value="{{ $data->title or '' }}" >
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Slug <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_slug" name="txt_slug" class="form-control" required="required" value="{{ $data->slug or '' }}" >
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_description" name="txt_description" class="form-control" required="required" value="{{ $data->description or '' }}" >
                                </div>
                            </div>
                        </div>

                        {{-- <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Keyword <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_keyword" name="txt_keyword" class="form-control" required="required" value="{{ $data->keyword or '' }}" >
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Category <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>

                                    <select id="txt_category" name="txt_category" class="form-control" required="required">
                                        <option value="news" {{ $data->category == 'news'? 'selected' : '' }}>news</option>
                                        <option value="events" {{ $data->category == 'events'? 'selected' : '' }}>events</option>
                                        <option value="promotion" {{ $data->category == 'promotion'? 'selected' : '' }}>promotion</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}

                        

                        {{-- <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Pin <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="checkbox" id="txt_pin" name="txt_pin" data-id="{{ $data->id }}" class="switch" data-on-text="Pin" data-off-text="Not pin" data-on-color="success" {{ $data->pin == 1 ? 'checked="checked"' : '' }} >
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Show Date Time <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_show_datetime" name="txt_show_datetime" class="form-control" required="required" value="{{ $data->show_datetime or '' }}" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Cover Image <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                            @if(!empty($data->cover_image))
                                <div class="input-group">
                                    <img src="{{ $data->cover_image or '' }}" class="img-responsive" alt="">
                                </div>
                            @endif

                                <input type="file" id="txt_cover_image" name="txt_cover_image" class="file-styled" data-type="jpg,jpeg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 438(W) x 249(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Share Image</label>
                            <div class="col-lg-8">
                            @if(!empty($data->share_image))
                                <div class="input-group">
                                    <img src="{{ $data->share_image or '' }}" class="img-responsive" alt="">
                                </div>
                            @endif

                                <input type="file" id="txt_share_image" name="txt_share_image" class="file-styled" data-type="jpg,jpeg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 1200(W) x 630(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">รายละเอียด</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body contra_content_summernote">
                    <div class="form-group">
                        <textarea name="txt_detail" class="summernote-height">{{ $data->detail or '' }}</textarea>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save('draft')">Save draft <i class="icon-arrow-right14 position-right"></i></button>
                        <button type="button" id="btn-save" class="btn btn-primary" onclick="save('active')">Save and Public <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    function detailsAndRulesUploadImage(file,editor,welEditable) {

        var formData = new FormData();
        formData.append("file", file);
        formData.append("file", file);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.contra.content.saveImage') }}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                $('.summernote-height').summernote(
                    'insertImage',
                    data.message
                );
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }

    function save(status) {
        var msg = '';
        console.log(status);

        if($('#txt_slug').val().trim() == '')
            msg = 'กรุณากรอก Slug';

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{
            saveCreate(status);
        }
        return false;
    }

    function saveCreate(status) {

        var formData = new FormData($('#form-data')[0]);
        formData.append("status", status);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.contra.content.saveContent') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

$(document).ready(function() {
    App.initSwitch(".switch");
    slug = $('#txt_slug').val();
    id = $('#txt_id').val();

    $('.summernote-height').summernote({
        height: 500,
        toolbar: [
            ['headline', ['style']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],

            ['misc', ['fullscreen', 'codeview', 'help']]
        ],
        styleTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
        popover: {
            image: [
                ['custom', ['imageAttributes']],
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
        },
        imageAttributes:{
            icon:'<i class="note-icon-pencil"/>',
            removeEmpty: false, // true = remove attributes | false = leave empty if present
            disableUpload: true // true = don't display Upload Options | Display Upload Options
        },
        callbacks: {
            onImageUpload: function(files,editor,welEditable) {
                detailsAndRulesUploadImage(files[0], editor, welEditable);
            }
        }
    });

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('change', 'input[name="txt_image"]', function() {

        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });
});
</script>
@endsection