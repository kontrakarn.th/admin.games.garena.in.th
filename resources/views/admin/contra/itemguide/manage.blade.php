
@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Item Guide </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">manage Item Guide</li>
        </ul>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat ">
            <div class="panel-heading">
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="panel-body">
                <a href="{{ route('admin.contra.itemguide.add') }}" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Banner</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header main__color main__background">ข้อมูล</div>
        <div class="table-responsive">
            <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Category</th>
                        <th>Level</th>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<!-- Basic modal -->
    <div id="modal_add_new_content" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Add New Content</h5>
                </div>

                <div class="modal-body">

                    <form class="form-horizontal" action="#" id="form_add_new_content">
                        <fieldset class="content-group">

                            <div class="form-group">
                                <label class="control-label col-lg-3">Parent Menu :</label>
                                <div class="col-lg-9">
                                    <select name="txt_menu_id" id="txt_menu_id" class="form-control">

                                        @foreach($parentMenu as $menu)
                                            <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Name :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Name" name="txt_name" id="txt_name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Slug :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Slug" name="txt_slug" id="txt_slug">
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addNewContent()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<!-- /Basic modal -->

@endsection

@push('script-head')
@endpush

@section('script')
<script type="text/javascript">



$(function(){
    
    $('#datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin.contra.itemguide.datatable') }}',
        method: 'post',
        columns: [
            {   data: 'id',
                name: 'id' 
            },
            {   data: 'category',
                name: 'category' 
            },
            {   data: 'level',
                name: 'level' 
            },
            {   data: 'name',
                name: 'name' 
            },
            {   data: 'title',
                name: 'title' 
            },
            {   data: 'image',
                name: 'image' 
            },
            {   data: 'status',
                name: 'status' 
            },
            {   data: 'action',
                name: 'action' 
            }
        ],
        order: [[ 0, "asc" ]],
        drawCallback: function( settings ) {
            var api = this.api();
            console.log('DataTables has redrawn the table');
            // console.log( api.rows( {page:'current'} ).data() );

            var _ObjTable = $('#datatable');
            App.initSwitch(_ObjTable.find(".switch"));
            _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                console.log('test');
                console.log($(this).attr('data-id'));
                console.log(state);
            });
        },
        drawCallback : function(settings) {
            var _ObjTable = $('#datatable');
             App.initSwitch(_ObjTable.find(".switch"));
             _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.contra.banner.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                         if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                         });
                         }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                         });
                         }
                    }
                );
            });
        }
    });

    

});
</script>
@endsection
