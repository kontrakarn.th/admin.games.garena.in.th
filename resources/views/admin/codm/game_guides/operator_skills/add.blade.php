@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Operator Skills</span> - CODM Main Site </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Operator Skills</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" value="{{ $data->id ?? null }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">@if(isset($data->id))แก้ไข Operator Skill @else เพิ่ม Operator Skills ใหม่@endif</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">
                            @if (isset($data->id))
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Active</label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            @if ($data->status === 'active')
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">
                                            @else
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Operator Skill Name <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="name" name="name" class="form-control" required="required" value="{{ $data->name ?? null }}" >
                                        <span class="label label-info">ขนาดไม่เกิน 255 ตัวอักษร</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Operator Skill Description<span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="description" name="description" class="form-control" required="required" value="{{ $data->description ?? null }}" >
                                        <span class="label label-info">ขนาดไม่เกิน 255 ตัวอักษร</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Operator Skill Image <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="image" name="image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->image ?? null }}">
                                    <br>
                                    <div class="help-block">
{{--                                    <span class="label label-success arrowed-right arrowed-in main__color">--}}
{{--                                        รูปควรมีขนาด 1920 × 970 px--}}
{{--                                    </span>--}}
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->image))
                                        <div class="input-group">
                                            <img src="{{ $data->image ?? null }}" class="img-responsive" alt="" id="preview">
                                        </div>
                                    @else
                                        <img id="preview" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Image for Preview Video</label>
                                <div class="col-lg-5">
                                    <input type="file" id="preview_video" name="preview_video" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required" value="{{ $data->preview_video ?? null }}">
                                    <br>
                                    <div class="help-block">
{{--                                    <span class="label label-success arrowed-right arrowed-in main__color">--}}
{{--                                        รูปควรมีขนาด 640 × 700 px--}}
{{--                                    </span>--}}
                                        <p></p>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->preview_video))
                                        <div class="input-group">
                                            <img src="{{ $data->preview_video ?? null }}" class="img-responsive" alt="" id="preview_mobile">
                                        </div>
                                    @else
                                        <img id="preview_mobile" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Youtube Video</label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="yt" name="yt" class="form-control" required="required" value="{{ $data->yt ?? null }}" >
                                        <span class="label label-default">หากไม่มีให้เว้นว่าง</span>
                                    </div>
                                </div>
                            </div>


                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveCreate()">Save <i class="icon-arrow-right14 position-right"></i></button>
                            @if (isset($data))
                                <a href="{{route('admin.codm.gameguides.operator_skills.delete',[$data])}}">
                                    <span type="reset" class="btn btn-danger" id="reset">Delete</span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>


        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.codm.gameguides.operator_skills.saveData') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.codm.gameguides.operator_skills.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="txt_show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('body').on('change', 'input[name="image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#image").change(function() {
                readURL(this,'preview');
            });

            $("#preview_video").change(function() {
                readURL(this,'preview_mobile');
            });

            $(".switch").on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.codm.gameguides.operator_skills.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                            });
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                            });
                        }
                    }
                );
            });

            $('#link').on('keyup',function () {
                if (  ($(this).val().length >=5) && ($(this).val().substr(0, 5) != 'http:') && ($(this).val().substr(0, 5) != 'https') ) {
                    $(this).val('https://' + $(this).val());
                }
            });


        });
    </script>
@endsection