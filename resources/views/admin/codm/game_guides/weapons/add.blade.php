@extends('layout.layout')

@section('style')
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <style>
        .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 15px;
            border-radius: 5px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }

        .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: #4CAF50;
            cursor: pointer;
        }

        .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: #4CAF50;
            cursor: pointer;
        }
    </style>
@endsection

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Weapons</span> - CODM Main Site </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Weapons</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ $data->id ?? null }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">@if(isset($data->id))แก้ไข Weapon @else เพิ่ม Weapon @endif</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">
                            @if (isset($data->id))
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Active</label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            @if ($data->status === 'active')
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">
                                            @else
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            @endif

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Weapon Series <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="series" name="series" placeholder="M4" class="form-control" required="required" value="{{ $data->series ?? null }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Weapon Name <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="name" name="name" placeholder="M4 - Colour Burst" class="form-control" required="required" value="{{ $data->name ?? null }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Weapon Description <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="description" name="description" class="form-control" required="required" value="{{ $data->description ?? null }}" >
                                        <span class="label label-info arrowed-right arrowed-in">สูงสุด 255 ตัวอักษร</span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Weapon Class <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <select name="class" class="form-control">
{{--                                            'sr','ar','lmg','smg','shotgun'--}}
                                            <option value="" disabled selected>Please select class</option>
                                            <option value="primary" {{isset($data) ? $data->class == "primary" ? "selected" : null: null}}>Primary</option>
                                            <option value="secondary" {{isset($data) ? $data->class == "secondary" ? "selected" : null: null}}>Secondary</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Weapon Type <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <select name="type" class="form-control">
{{--                                            'sr','ar','lmg','smg','shotgun'--}}
                                            <optgroup label="Primary Weapons">
                                                <option value="sr" {{isset($data) ? $data->type == "sr" ? "selected" : null: null}}>SR</option>
                                                <option value="ar" {{isset($data) ? $data->type == "ar" ? "selected" : null: null}}>AR</option>
                                                <option value="lmg" {{isset($data) ? $data->type == "lmg" ? "selected" : null: null}}>LMG</option>
                                                <option value="smg" {{isset($data) ? $data->type == "smg" ? "selected" : null: null}}>SMG</option>
                                                <option value="shotgun" {{isset($data) ? $data->type == "shotgun" ? "selected" : null: null}}>Shotgun</option>
                                            </optgroup>
                                            <optgroup label="Secondary Weapons">
                                                <option value="pistol" {{isset($data) ? $data->type == "pistol" ? "selected" : null: null}}>Pistol</option>
                                                <option value="melee" {{isset($data) ? $data->type == "melee" ? "selected" : null: null}}>Melee</option>
                                                <option value="launcher" {{isset($data) ? $data->type == "launcher" ? "selected" : null: null}}>Launcher</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Weapon Image <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="image" name="image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->image ?? null }}">
                                    <br>
                                    <div class="help-block">

                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->image))
                                        <div class="input-group">
                                            <img src="{{ $data->image ?? null }}" class="img-responsive" alt="" id="preview">
                                        </div>
                                    @else
                                        <img id="preview" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>


                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <h4>Percentage</h4>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Damage : <span class="text-danger" id="percent_damage_text">{{$data->percent_damage ?? 0}}</span></label>
                            <div class="col-lg-4">
                                <input type="range" min="1" max="100" value="{{$data->percent_damage ?? 0}}" class="slider" id="percent_damage" name="percent_damage" onchange="sliderPercent('percent_damage')">
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Rate of fire : <span class="text-danger" id="percent_rof_text">{{$data->percent_rof ?? 0}}</span></label>
                            <div class="col-lg-4">
                                <input type="range" min="1" max="100" value="{{$data->percent_rof ?? 0}}" class="slider" id="percent_rof" name="percent_rof" onchange="sliderPercent('percent_rof')">
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Accuracy : <span class="text-danger" id="percent_accuracy_text">{{$data->percent_accuracy ?? 0}}</span></label>
                            <div class="col-lg-4">
                                <input type="range" min="1" max="100" value="{{$data->percent_accuracy ?? 0}}" class="slider" id="percent_accuracy" name="percent_accuracy" onchange="sliderPercent('percent_accuracy')">
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Mobility : <span class="text-danger" id="percent_mobility_text">{{$data->percent_mobility ?? 0}}</span></label>
                            <div class="col-lg-4">
                                <input type="range" min="1" max="100" value="{{$data->percent_mobility ?? 0}}" class="slider" id="percent_mobility" name="percent_mobility" onchange="sliderPercent('percent_mobility')">
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Range : <span class="text-danger" id="percent_range_text">{{$data->percent_range ?? 0}}</span></label>
                            <div class="col-lg-4">
                                <input type="range" min="1" max="100" value="{{$data->percent_range ?? 0}}" class="slider" id="percent_range" name="percent_range" onchange="sliderPercent('percent_range')">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <h4>Attachments</h4>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Scope</label>
                            <div class="col-lg-8">
                                <select id="scope" name="scope" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                    <option value="" disabled selected>กรุณาเลือก Scope</option>
                                    @foreach ($scopes as $scope)
                                        <option value="{{$scope->id}}" {{isset($data) ? $data->scope == $scope->id ? "selected" : null : null}} data-image="{{$scope->image}}">{{$scope->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Attachment 1</label>
                            <div class="col-lg-8">
                                <select id="attachment1" name="attachment1" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                    <option value="" disabled selected>กรุณาเลือก Attachment 1</option>
                                    @foreach ($attachments as $attachment)
                                        <option value="{{$attachment->id}}" {{isset($data) ? $data->attachment1 == $attachment->id ? "selected" : null : null}} data-image="{{$attachment->image}}">{{$attachment->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Attachment 2</label>
                            <div class="col-lg-8">
                                <select id="attachment2" name="attachment2" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                    <option value="" disabled selected>กรุณาเลือก Attachment 2</option>
                                    @foreach ($attachments as $attachment)
                                        <option value="{{$attachment->id}}" {{isset($data) ? $data->attachment2 == $attachment->id ? "selected" : null : null}} data-image="{{$attachment->image}}">{{$attachment->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Attachment 3</label>
                            <div class="col-lg-8">
                                <select id="attachment3" name="attachment3" class="form-control js-example-responsive" required="required" style="width: 100%;">
                                    <option value="" disabled selected>กรุณาเลือก Attachment 3</option>
                                    @foreach ($attachments as $attachment)
                                        <option value="{{$attachment->id}}" {{isset($data) ? $data->attachment3 == $attachment->id ? "selected" : null : null}} data-image="{{$attachment->image}}">{{$attachment->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveCreate()">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>

        function sliderPercent(id) {
            const id_text = id+"_text";
            $('#'+id_text).text($('#'+id).val())
        }


        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.codm.gameguides.weapons.saveData') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.codm.gameguides.weapons.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function formatData (data) {
            if (!data.id) { return data.text; }
            var image = $(data.element).data('image');
            var $result= $(
                '<span><img src="'+image+'" height="30px" style="margin-right: 10px"/> ' + data.text + '</span>'
            );
            return $result;
        };


        $(document).ready(function() {
            App.initSwitch(".switch");
            App.initSwitch(".switch-highlight");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            $(".js-example-responsive").select2({
                // width: 'resolve',
                templateResult: formatData,
                templateSelection: formatData
            });

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });


            $('body').on('change', 'input[name="image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#image").change(function() {
                readURL(this,'preview');
            });

            $("#image_share").change(function() {
                readURL(this,'preview_mobile');
            });

            $(".switch").on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.codm.gameguides.weapons.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                            });
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                            });
                        }
                    }
                );
            });
        });
    </script>
@endsection