@extends('layout.layout')

@section('style')
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <style>
        .ql-editor{
            min-height:500px;
            background-image: url('https://static2.garena.in.th/data/codmth/main/6a065d9d90248806e0119e65cada22fe.jpg');
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            color: white;
        }
    </style>
@endsection

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Beginner Guides</span> - CODM Main Site </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Beginner Guides</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ $data->id ?? null }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">@if(isset($data->id))แก้ไข Beginner Guides @else เพิ่ม Beginner Guides @endif</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">


                        <fieldset class="content-group">
                            @if (isset($data->id))
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Active</label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            @if ($data->status === 'active')
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">
                                            @else
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">URL</label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="slug" name="slug" class="form-control" required="required" value="https://codm.garena.in.th/gameguides/{{ $data->slug ?? null }}" readonly>
                                        </div>
                                    </div>
                                </div>

                            @endif

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Title <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="title" name="title" class="form-control" required="required" value="{{ $data->title ?? null }}" >
                                        <span class="label label-info arrowed-right arrowed-in">สูงสุด 255 ตัวอักษร</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Short Description <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="short_desc" name="short_desc" class="form-control" required="required" value="{{ $data->short_desc ?? null }}" >
                                        <span class="label label-info arrowed-right arrowed-in">สูงสุด 255 ตัวอักษร</span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Beginner Guides Image <span class="text-danger">*</span></label>
                                <div class="col-lg-5">
                                    <input type="file" id="image" name="image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*,video/*" required="required" value="{{ $data->image ?? null }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 1200 × 630 px
                                    </span>
                                        <p></p>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                        <p></p>
                                    <span class="label label-info arrowed-right arrowed-in">
                                        รูปภาพจะไม่ปรากฏบนเนื้อหาข่าว
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->image))
                                        <div class="input-group">
                                            <img src="{{ $data->image ?? null }}" class="img-responsive" alt="" id="preview">
                                        </div>
                                    @else
                                        <img id="preview" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Image Share</label>
                                <div class="col-lg-5">
                                    <input type="file" id="image_share" name="image_share" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required" value="{{ $data->image_share ?? null }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 1200 x 630 px เท่านั้น ถึงจะแสดงผลอย่างถูกต้อง
                                    </span>
                                        <p></p>
                                        <span class="label label-danger arrowed-right arrowed-in">
                                        หากไม่ใส่จะเป็นรูป <a href="https://static2.garena.in.th/data/rov/content/content/4d9f4761275c83ab15df75eca81d1765.png" target="_blank">Default</a>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->image_share))
                                        <div class="input-group">
                                            <img src="{{ $data->image_share ?? null }}" class="img-responsive" alt="" id="preview_mobile">
                                        </div>
                                    @else
                                        <img id="preview_mobile" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">News Detail <span class="text-danger">*</span></h5>
                        <span class="label label-info arrowed-right arrowed-in">สูงสุด 8000 ตัวอักษร</span>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <input name="description" type="hidden" value="{{isset($data->description) ? $data->description : null}}">
                            <div id="editor-container">
                                {!! isset($data->description) ? $data->description : null !!}
                            </div>
                            {{--                            <textarea name="detail" class="summernote-height">{{ isset($data->description) ? $data->description : null }}</textarea>--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveCreate()">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>


        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.codm.gameguides.beginners.saveData') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.codm.gameguides.beginners.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");
            App.initSwitch(".switch-highlight");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });


            $('body').on('change', 'input[name="image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#image").change(function() {
                readURL(this,'preview');
            });

            $("#image_share").change(function() {
                readURL(this,'preview_mobile');
            });

            $(".switch").on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.codm.gameguides.beginners.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                            });
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                            });
                        }
                    }
                );
            });

            $(".switch-highlight").on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.codm.news.updateHighlight') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                            });
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                            });
                        }
                    }
                );
            });

            var toolbarOptions = [
                ['bold', 'italic', 'underline'],

                [{ 'list': 'ordered'}, { 'list': 'bullet' }],                    // text direction

                [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                [{ 'color': [] }],          // dropdown with defaults from theme
                // [{ 'font': [] }],
                [{ 'align': [] }],
                ['link', 'image'],

                ['clean']                                         // remove formatting button
            ];

            var quill = new Quill('#editor-container', {
                theme: 'snow',
                syntax: true,
                modules: {
                    toolbar: toolbarOptions
                },
            });

            quill.on('text-change', function(delta, source) {
                var detail = $('input[name="description"]');
                detail.val(quill.root.innerHTML);

            });

            quill.getModule("toolbar").addHandler("image", imageHandler);


            function imageHandler(image, callback) {
                const input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.click();
                input.onchange = () => {
                    const file = input.files[0];

                    // file type is only image.
                    if (/^image\//.test(file.type)) {
                        var formData = new FormData();
                        formData.append("file", file);
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('admin.codm.news.saveImage') }}",
                            data: formData,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                const range = quill.getSelection();
                                quill.insertEmbed(range.index, 'image', data.message);
                            },
                            error: function (xhr, type) {
                                alert('Error');
                            }
                        });
                    } else {
                        alert('You could only upload images.');
                    }
                };
            }


        });
    </script>
@endsection