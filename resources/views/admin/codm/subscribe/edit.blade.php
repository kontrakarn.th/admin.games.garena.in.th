
@extends('layout.layout')

@section('style')
    <style>
        .bg_black{
            background: black;
        }
    </style>
@stop
@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Setting</span> - CODM Main Site </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Setting</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5>Main Youtube</h5>
                </div>
                <form id="form-header" name="form-header" method="post" class="form-horizontal form-validate-jquery" >
                <input type="hidden" name="id_subscribe" value="{{ $id }}">
                <div class="panel-body ">
                    <fieldset class="content-group">
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Main Youtube<b> </b></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="main_title" class="form-control" value="{{$main_title}}" placeholder="Title" maxlength="255">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="main_description" class="form-control" value="{{$main_description}}" placeholder="Description" maxlength="500">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" name="main_link_youtube" class="form-control" value="{{$main_link_youtube}}" placeholder="Link Youtube" maxlength="255" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" style="color: #ff0000;">Youtube Code Ex. youtu.be/abc123 ให้ใส่ abc123</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="content-group">
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Slug<b> </b></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" name="slug" class="form-control" value="{{ $slug }}" placeholder="slug" maxlength="255">
                                </div>
                            </div>
                        </div>
                    </fieldset>


                    <fieldset class="content-group">
                        <h5>Video 1</h5>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Thumbnail <span class="text-danger">*</span></label>
                            <div class="col-lg-5">
                                <input type="file" id="logo_image_mobile1" name="logo_image_mobile1" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB size แนะนำ คือ 1280 x 720
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-3 bg_black">
                                @if(!empty($video1_image))
                                    <div class="input-group">
                                        <img src="{{ $video1_image ?? '' }}" class="img-responsive" id="video1_image">
                                    </div>
                                @else
                                    <img id="video1_image" src="" style="width: 100%;display: none" class="img-responsive"/>
                                @endif
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Description<b> </b></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video1_promote" class="form-control" value="{{$video1_promote}}" placeholder="Promote Text" maxlength="100" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video1_title" class="form-control" value="{{$video1_title}}" placeholder="Title" maxlength="100" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video1_description" class="form-control" value="{{$video1_description}}" placeholder="Description" maxlength="255" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" name="video1_youtube" class="form-control" value="{{$video1_youtube}}" placeholder="Link Youtube" maxlength="255" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" style="color: #ff0000;">Youtube Code Ex. youtu.be/abc123 ให้ใส่ abc123</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="content-group">
                        <h5>Video 2</h5>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Thumbnail <span class="text-danger">*</span></label>
                            <div class="col-lg-5">
                                <input type="file" id="logo_image_mobile2" name="logo_image_mobile2" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB size แนะนำ คือ 1280 x 720
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-3 bg_black">
                                @if(!empty($video2_image))
                                    <div class="input-group">
                                        <img src="{{ $video2_image ?? '' }}" class="img-responsive" id="video2_image">
                                    </div>
                                @else
                                    <img id="video2_image" src="" style="width: 100%;display: none" class="img-responsive"/>
                                @endif
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Description<b> </b></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video2_promote" class="form-control" value="{{$video2_promote}}" placeholder="Promote Text" maxlength="100" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video2_title" class="form-control" value="{{$video2_title}}" placeholder="Title" maxlength="100" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video2_description" class="form-control" value="{{$video2_description}}" placeholder="Description" maxlength="255" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" name="video2_youtube" class="form-control" value="{{$video2_youtube}}" placeholder="Link Youtube" maxlength="255" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" style="color: #ff0000;">Youtube Code Ex. youtu.be/abc123 ให้ใส่ abc123</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="content-group">
                        <h5>Video 3</h5>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Thumbnail <span class="text-danger">*</span></label>
                            <div class="col-lg-5">
                                <input type="file" id="logo_image_mobile3" name="logo_image_mobile3" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB size แนะนำ คือ 1280 x 720
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-3 bg_black">
                                @if(!empty($video3_image))
                                    <div class="input-group">
                                        <img src="{{ $video3_image ?? '' }}" class="img-responsive" id="video3_image">
                                    </div>
                                @else
                                    <img id="video3_image" src="" style="width: 100%;display: none" class="img-responsive"/>
                                @endif
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Description<b> </b></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video3_promote" class="form-control" value="{{$video3_promote}}" placeholder="Promote Text" maxlength="100" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video3_title" class="form-control" value="{{$video3_title}}" placeholder="Title" maxlength="100" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-display"></i></span>
                                    <input type="text" name="video3_description" class="form-control" value="{{$video3_description}}" placeholder="Description" maxlength="255" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" name="video3_youtube" class="form-control" value="{{$video3_youtube}}" placeholder="Link Youtube" maxlength="255" >
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" style="color: #ff0000;">Youtube Code Ex. youtu.be/abc123 ให้ใส่ abc123</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div class="text-center">
                        <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save('{{ route("admin.codm.subscribe.saveHeader") }}','#form-header')">Save Subscribe<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script-head')
@endpush

@section('script')
    <script type="text/javascript">
        function save(url,id) {
            var formData = new FormData($(id)[0]);

            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    console.log(data);
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.codm.subscribe.edit',['id' => $id]) }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });

        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).show();
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function () {
            $("#logo_image_mobile1").change(function() {
                readURL(this,'video1_image');
            });
            $("#logo_image_mobile2").change(function() {
                readURL(this,'video2_image');
            });

            $("#logo_image_mobile3").change(function() {
                readURL(this,'video3_image');
            });
        });


    </script>
@endsection
