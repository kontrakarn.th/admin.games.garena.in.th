@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Man</span> - CODM TV </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active"><a href="{{ route('admin.codm.tv.index') }}">TV</a></li>
                <li class="active"><a href="{{ route('admin.codm.tv.show', ['id' => $vdo->slug_id]) }}">{{ $vdo->slug->slug }}</a></li>
                <li class="active">Edit</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="panel panel-flat">
                    <div class="panel-body">         
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif 
            <div class="panel panel-flat">
                <div class="panel-body vdo-section">
                    <form  action="{{ route('admin.codm.tv.vdo.update') }}" method="POST" enctype="multipart/form-data">  
                        @csrf
                        <input type="hidden" value="{{ $vdo->id }}" name="id">
                        <div class="form-group">
                            <label for="youtube-title"><h4>Youtube Title</h4></label>
                            <input type="text" class="form-control" id="youtube-title" placeholder="Enter Youtube title" name="vdo_title" value="{{ $vdo->title }}">
                        </div>
                        <div class="form-group">
                            <label for="youtube-url"><h4>Youtube URL</h4></label>
                            <input type="text" class="form-control" id="youtube-url" placeholder="Paste url here" name="vdo_url" value="{{ $vdo->url }}">
                            <medium id="emailHelp" class="form-text text-danger">* Youtube Code Ex. youtu.be/abc123 ให้ใส่ abc123 *</medium>
                        </div>
                        <div class="form-group">
                            <label for="thumbnail"><h4>Thumbnail Youtube</h4></label>
                            <div class="form-group">
                                <img src="{{ $vdo->thumbnail }}" width="200px">
                            </div>
                            <input type="file" class="form-control-file" id="youtube-thumbnail" name="vdo_thumbnail" value="{{ $vdo->thumbnail }}">
                            <medium id="emailHelp" class="form-text text-danger">* Thumbnail size 400x225 *</medium>
                           
                        </div>
                        <div class="form-group form-check">
                            <label class="form-check-label" for="youtube-status"><h4>Active</h4></label>
                            <select class="form-control" id="youtube-status" name="vdo_status">
                                @if($vdo->status == 1)
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                @else
                                    <option value="1">Active</option>
                                    <option value="0" selected>Inactive</option>
                                @endif
                            </select>                   
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" >Save</button>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        $(document).ready(function(){
            $("#add-vdo-btn").click(function(){       
                $(".template-youtube-wrap").first().clone().appendTo(".vdo-section").show();
            });

            $("#add-playlist-btn").click(function(){
                $(".template-playlist-wrap").first().clone().appendTo(".playlist-section").show();
            });
            
           $("form").submit(function(){
                $(".template-youtube-wrap").first().remove();
                $(".template-playlist-wrap").first().remove();
            });
        });

        function deleteVdo(id) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel pls!",
                closeOnConfirm: false,
                closeOnCancel: true
            }).then(function () {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.codm.tv.deleteVdo') }}",
                    data: { 
                        'id': id
                        },
                    dataType: 'json',
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                location.reload();
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
                });
            }, function (dismiss) {});
        }

        function deletePlaylist(id) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel pls!",
                closeOnConfirm: false,
                closeOnCancel: true
            }).then(function () {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.codm.tv.deletePlaylist') }}",
                    data: { 
                        'id': id
                        },
                    dataType: 'json',
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                location.reload();
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
                });
            }, function (dismiss) {});
        }
    </script>
@endsection