@extends('layout.layout')

@section('header')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Man</span> - CODM TV </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active"><a href="{{ route('admin.codm.tv.index') }}">TV</a></li>
                <li class="active">{{ $slug->slug }}</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-body">           
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @csrf
                    <div class="form-group">
                        <input type="hidden" value="{{ $slug->id }}" name="slug_id">
                        <label for="slug-name">Slug Name</label>
                        <input type="text" class="form-control" id="slug-name" aria-describedby="slugHelp" placeholder="Enter slug" name="slug" value="{{ $slug->slug ?? '-'}}">
                        <small id="slugHelp" class="form-text text-muted">slug should be uniqe.</small>
                    </div>
                    <button type="submit" class="btn btn-primary" onclick="save()" >Save</button>
            </div>
        </div>
        <div class="panel panel-flat">
            <div class="panel-body">
                <form action="{{ route('admin.codm.tv.saveMainVdo') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="youtube-url"><h4>Main VDO Url</h4></label>
                        <input type="hidden" value="{{ $slug->id }}" name="slug_id">
                        <input type="hidden" value="{{ $main->id ?? '' }}" name="main_id">
                        <input type="text" class="form-control" id="main-url" placeholder="Paste url here" name="main_url" value="{{ $main->url ?? '' }}">
                        <medium id="emailHelp" class="form-text text-danger">*Don't paste url that upload by Youtube  the autoplay will not working, use url that upload from other site.*</medium>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>   
            </div>
        </div>
        <div class="panel panel-flat">
            <div class="panel-heading text-right">
                <a href="{{ route('admin.codm.tv.edit', ['id' => $slug->id, 'type' => 'vdo']) }}" class="btn btn-info"><i class="icon-plus-circle2 position-left"></i>Add VDO</a>
            </div>
            <div class="panel-body">     
                <div class="table-header main__color main__background">Videos</div>
                <div class="table-responsive">
                    <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                        <thead>
                            <tr>
                                <th>Slug</th>
                                <th>Title</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>  
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading text-right">
                <a href="{{ route('admin.codm.tv.edit', ['id' => $slug->id, 'type' => 'playlist']) }}" class="btn btn-info"><i class="icon-plus-circle2 position-left"></i>Add Playlist</a>
            </div>
            <div class="panel-body">     
                <div class="table-header main__color main__background">Playlists</div>
                <div class="table-responsive">
                    <table id="datatable2" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                        <thead>
                            <tr>
                                <th>Slug</th>
                                <th>Title</th>
                                <th  class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>  
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script') 


    <script type="text/javascript">
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    $(function(){
    
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url :'{{ route('admin.codm.tv.datatableVdo')}}',
                data : {
                    slug_id : '{{ $slug->id }}'
                },
                type : 'POST'
            },
            method: 'post',
            columns: [
                {   data: 'slug',
                    name: 'slug'
                },
                {   data: 'title',
                    name: 'title'
                },
                {   data: 'status',
                    className: 'text-center',
                    name: 'status'
                },
                {   data: 'action',
                    className: 'text-center',
                    name: 'action'
                },
            ],
            order: [[ 0, "desc" ]],
            drawCallback: function( settings ) {
                var api = this.api();
    
                var _ObjTable = $('#datatable');
                App.initSwitch(_ObjTable.find(".switch"));
                _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                    event.preventDefault();
                    console.log('test');
                    console.log($(this).attr('data-id'));
                    console.log(state);
                });
            },
            drawCallback : function(settings) {
                var _ObjTable = $('#datatable');
                App.initSwitch(_ObjTable.find(".switch"));
                _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                    event.preventDefault();
                    $.post(
                        "{{ route('admin.codm.tv.activeVdo') }}",
                        { id: $(this).attr('data-id'), status: state },
                        function( data ) {
                            if(data.status){
                                swal({
                                    title: data.message,
                                    confirmButtonColor: "#4caf50",
                                    type: "success"
                                }).then(function () {

                            });
                            }else{
                                swal({
                                    title: data.message,
                                    confirmButtonColor: "#EF5350",
                                    type: "error"
                                }).then(function () {

                                });
                            }
                        }
                    );
                });
            }
        });

        var slug_id = 1;
        $('#datatable2').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url :'{{ route('admin.codm.tv.datatablePlaylist')}}',
                data : {
                    slug_id : '{{ $slug->id }}'
                },
                type : 'POST'
            },
            method: 'post',
            columns: [
                {   data: 'slug',
                    name: 'slug'
                },
                {   data: 'title',
                    name: 'title'
                },
               
                {   data: 'status',
                    className: 'text-center',
                    name: 'status'
                },
                {   data: 'action',
                    className: 'text-center',
                    name: 'action'
                },
            ],
            order: [[ 0, "desc" ]],
            drawCallback: function( settings ) {
                var api = this.api();
    
                var _ObjTable = $('#datatable2');
                App.initSwitch(_ObjTable.find(".switch"));
                _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                    event.preventDefault();
                    console.log('test');
                    console.log($(this).attr('data-id'));
                    console.log(state);
                });
            },
            drawCallback : function(settings) {
                var _ObjTable = $('#datatable2');
                App.initSwitch(_ObjTable.find(".switch"));
                _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                    event.preventDefault();
                    $.post(
                        "{{ route('admin.codm.tv.activePlaylist') }}",
                        { id: $(this).attr('data-id'), status: state },
                        function( data ) {
                            if(data.status){
                                swal({
                                    title: data.message,
                                    confirmButtonColor: "#4caf50",
                                    type: "success"
                                }).then(function () {

                            });
                            }else{
                                swal({
                                    title: data.message,
                                    confirmButtonColor: "#EF5350",
                                    type: "error"
                                }).then(function () {

                                });
                            }
                        }
                    );
                });
            }
        });
    });



    function setDefault(slug_id) {
        swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#EF5350",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then(function () {
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.codm.tv.setDefault') }}",
            data: {
                'id': slug_id
                },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }, function (dismiss) {});
    }
    
    //save slug
    function save() {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Yes, save it!",
                cancelButtonText: "No, cancel pls!",
                closeOnConfirm: false,
                closeOnCancel: true
            }).then(function () {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.codm.tv.update') }}",
                    data: { 
                        'slug_id': '{{ $slug->id }}',
                        'slug': $('#slug-name').val()
                        },
                    dataType: 'json',
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                location.reload();
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
                });
            }, function (dismiss) {});
        }
        

    // set main vdo 
    function saveMain(slug_id) {
      
        var main_title = $('input[name="main_title"]').val();
        var main_url = $('input[name="main_url"]').val();
        var token =  '{{ csrf_token() }}';
        var thumbnail =  $('input[type=file]')[0].files[0];
        var data =  { _token: token, slug_id: slug_id, main_title: main_title, main_url: main_url, main_thumbnail: thumbnail}
        swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#EF5350",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then(function () {
       console.log(main_url);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.codm.tv.saveMainVdo') }}",
            data: data,
            dataType: 'json',
            success: function (data) {
                console.log(data);
              /*   status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                } */
            },
            error: function (xhr, type) {
               status_submit = false;
               console.log(xhr);
            }
        }); 
    }, function (dismiss) {});
    }

    //set main playlist
    function setMainPlaylist(slug_id) {
        swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#EF5350",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then(function () {
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.codm.tv.setMainPlaylist') }}",
            data: {
                'id': slug_id
                },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }, function (dismiss) {});
    }

    


    function deleteVdo(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function () {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.codm.tv.deleteVdo') }}",
                data: { 
                    'id': id
                    },
                dataType: 'json',
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                status_submit = false;
                }
            });
        }, function (dismiss) {});

    }


    function deletePlaylist(id) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function () {
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.codm.tv.deletePlaylist') }}",
                data: { 
                    'id': id
                    },
                dataType: 'json',
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                status_submit = false;
                }
            });
        }, function (dismiss) {});

    }
   

    </script>
@endsection 