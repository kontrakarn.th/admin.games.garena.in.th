@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Man</span> - CODM TV </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Setting</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <div class="row">
        <form action="{{ route('admin.codm.tv.store') }}" method="POST" enctype="multipart/form-data">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">           
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @csrf
                            <div class="form-group">
                                <label for="slug-name">Slug Name</label>
                                <input type="text" class="form-control" id="slug-name" aria-describedby="slugHelp" placeholder="Enter slug" name="slug" value="{{ old('slug') ?? '' }}">
                                <small id="slugHelp" class="form-text text-muted">slug should be uniqe.</small>
                            </div>
                            <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var count = 1;
        $(document).ready(function(){
            $("#add-vdo-btn").click(function(){        
                $(".template-youtube-wrap").first().clone().appendTo(".vdo-section").show();
            });

            $("#add-playlist-btn").click(function(){
                $(".template-playlist-wrap").first().clone().appendTo(".playlist-section").show();
            });
            
            $("form").submit(function(){
                $(".template-youtube-wrap").first().remove();
                $(".template-playlist-wrap").first().remove();
            });
        });
    </script>
@endsection