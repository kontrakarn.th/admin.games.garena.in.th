@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - edit page - {{$slug}} </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">edit page</li>
        </ul>
    </div>
</div>
@endsection
 <?php 
         function browImg($section_name,$types,$name,$column,$id,$txt,$img,$width=500,$height=500){
         return  '<form id="form-'.$id.'" name="form-'.$id.'" method="post" class="form-horizontal form-validate-jquery" >
                          <div class="form-group">
                                <label class="control-label col-lg-2">'.$txt.'</label>
                                <div class="col-lg-8">
                                
                                <div class="input-group">
                                    <img src="'.$img.'" class="img-responsive" alt="" style="height:200px">
                                </div>
                                    <input type="file" id="'.$id.'" name="txt_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                    <br>
                                    <div class="help-block">
                                        <span class="label label-success arrowed-right arrowed-in main__color">
                                            รูปควรมีขนาด '.$width.'(W) x '.$height.'(H) PX
                                        </span> 
                                        <span class="label label-danger arrowed-right arrowed-in">
                                            รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                        </span>
                                    </div>
                                </div> <div class="col-lg-2">
                                        <button type="button" class="btn btn-success" onclick="save(\''.$section_name.'\',\''.$types.'\',\''.$name.'\',\''.$column.'\',\''.$id.'\')"> <i class="icon-checkmark"></i></button>
                                    </div>
                            </div>
                           
                     </form>';
             }
             
       ?>
@section('content')
<form id="form-" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <!--<input type="hidden" name="txt_slug" id="txt_slug" value="{{ $slug }}">-->
    <div class="row">

        <!-- ================================================= Popup ==================================================== !-->
       
       <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Popup</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <form id="form-" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
                    </form>

                    <fieldset class="content-group">
                        <?php echo browImg('popup','img','image','src','popup_img_image_src','Image src',$data['popup_img_image_src'], 1920, 944) ?>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Popup Link </label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="popup_link_goto_url" name="popup_link_goto_url" class="form-control" required="required" value="{{$data['popup_link_goto_url']}}" >
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-success"  onclick="update('popup','link','goto','url','popup_link_goto_url')"> <i class="icon-checkmark"></i></button>

                            </div>
                        </div>
                </div>
            </div>
        </div>

        <!-- ================================================= Download ==================================================== !-->

        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Download</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <fieldset class="content-group">
                        <?php echo browImg('download','bg','bg','src','download_bg_src','Background src',$data['download_bg_src'], 1950, 1058) ?>
                          
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background alt<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="download_bg_alt" name="download_bg_alt" class="form-control" required="required" value="{{$data['download_bg_alt']}}" >
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success" onclick="update('download','bg','bg','alt','download_bg_alt')"> <i class="icon-checkmark"></i></button>
                                </div>
                            </div>
                            <hr>
                              <?php echo browImg('download','img','logo','src','download_img_logo_src','Logo src',$data['download_img_logo_src'], 600, 362) ?>
                            <div class="form-group">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Logo alt<span class="text-danger">*</span></label>
                                    <div class="col-lg-8">

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="download_img_logo_alt" name="download_img_logo_alt" class="form-control" required="required" value="{{$data['download_img_logo_alt']}}" >
                                        </div>
                                        <div class="input-group">
                                            <img src="" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success" onclick="update('download','img','logo','alt','download_img_logo_alt')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                                <hr>
                                    <?php echo browImg('download','img','qr_code','src','download_img_qrcode_src','QR Code src',$data['download_img_qrcode_src'], 316, 615) ?>
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">QR Code alt<span class="text-danger">*</span></label>
                                    <div class="col-lg-8">

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="download_img_qrcode_alt" name="download_img_qrcode_alt" class="form-control" required="required" value="{{$data['download_img_qrcode_alt']}}" >
                                        </div>
                                        <div class="input-group">
                                            <img src="" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('download','img','qr_code','alt','download_img_qrcode_alt')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                                <hr>

                                <div class="form-group has-feedback">
                                    <div class="form-group has-feedback">
                                        <label class="control-label col-lg-2">App Store Link </label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-link"></i></span>
                                                <input type="text" id="download_btn_app_store_url" name="download_btn_app_store_url" class="form-control" required="required" value="{{$data['download_btn_app_store_url']}}" >
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="button" class="btn btn-success"  onclick="update('download','btn','app_store','url','download_btn_app_store_url')"> <i class="icon-checkmark"></i></button>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Gpogle plaly Link </label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-link"></i></span>
                                            <input type="text" id="download_btn_google_play_url" name="download_btn_google_play_url" class="form-control" required="required" value="{{$data['download_btn_google_play_url']}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('download','btn','google_play','url','download_btn_google_play_url')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">APK File Link </label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-link"></i></span>
                                        <input type="text" id="download_btn_apk_url" name="download_btn_apk_url" class="form-control" required="required" value="{{$data['download_btn_apk_url']}}" >
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('download','btn','apk','url','download_btn_apk_url')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>


        <!-- ================================================= Rank ==================================================== !-->

        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Rank</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body">
                    <fieldset class="content-group">
                            <?php echo browImg('rank','bg','bg rank','src','rank_bg_src','Background src',$data['rank_bg_src'], 1950, 1611) ?>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background alt<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="rank_bg_alt" name="rank_bg_alt" class="form-control" required="required" value="{{$data['rank_bg_alt']}}">
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('rank','bg','bg rank','alt','rank_bg_alt')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                            <hr>
                            
                        <?php echo browImg('rank','img','header','src','rank_img_header_src','Image src',$data['rank_img_header_src'], 1950, 38) ?>
                            <div class="form-group">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Image alt<span class="text-danger">*</span></label>
                                    <div class="col-lg-8">

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="rank_img_header_alt" name="rank_img_header_alt" class="form-control" required="required" value="{{$data['rank_img_header_alt']}}">
                                        </div>
                                        <div class="input-group">
                                            <img src="" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('rank','img','header','alt','rank_img_header_alt')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                                <hr>
                            <div class="form-group">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Btn Beginner </label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-link"></i></span>
                                            <input type="text" id="rank_btn_beginner_url" name="rank_btn_beginner_url" class="form-control" required="required" value="{{$data['rank_btn_beginner_url']}}" >
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('rank','btn','beginner','url','rank_btn_beginner_url')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Add ticket Link </label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-link"></i></span>
                                        <input type="text" id="rank_btn_add_ticket_url" name="rank_btn_add_ticket_url" class="form-control" required="required" value="{{$data['rank_btn_add_ticket_url']}}" >
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('rank','btn','add_ticket','url','rank_btn_add_ticket_url')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Add ticket Content<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="rank_btn_add_ticket_content" name="rank_btn_add_ticket_content" class="form-control" required="required" value="{{$data['rank_btn_add_ticket_content']}}">
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('rank','btn','add_ticket','content','rank_btn_add_ticket_content')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Community Link </label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="rank_btn_community_url" name="rank_btn_community_url" class="form-control" required="required" value="{{$data['rank_btn_community_url']}}">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-success"  onclick="update('rank','btn','community','url','rank_btn_community_url')"> <i class="icon-checkmark"></i></button>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Community Content<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="rank_btn_community_content" name="rank_btn_community_content" class="form-control" required="required" value="{{$data['rank_btn_community_content']}}" >
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('rank','btn','community','content','rank_btn_community_content')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Gallery Link </label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="rank_btn_gallery_url" name="rank_btn_gallery_url" class="form-control" required="required" value="{{$data['rank_btn_gallery_url']}}">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-success"  onclick="update('rank','btn','gallery','url','rank_btn_gallery_url')"> <i class="icon-checkmark"></i></button>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Gallery Content<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="rank_btn_gallery_content" name="rank_btn_gallery_content" class="form-control" required="required" value="{{$data['rank_btn_gallery_content']}}">
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('rank','btn','gallery','content','rank_btn_gallery_content')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Support Link </label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="rank_btn_support_url" name="rank_btn_support_url" class="form-control" required="required" value="{{$data['rank_btn_support_url']}}" >
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-success"  onclick="update('rank','btn','support','url','rank_btn_support_url')"> <i class="icon-checkmark"></i></button>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Support Content<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="rank_btn_support_content" name="rank_btn_support_content" class="form-control" required="required" value="{{$data['rank_btn_support_content']}}" >
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('rank','btn','support','content','rank_btn_support_content')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Facebook Link </label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="rank_btn_facebook_url" name="rank_btn_facebook_url" class="form-control" required="required" value="{{$data['rank_btn_facebook_url']}}" >
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-success" o onclick="update('rank','link','facebook','url','rank_btn_facebook_url')"> <i class="icon-checkmark"></i></button>

                            </div>
                        </div>
                </div>
            </div>
        </div>

        <!-- ================================================ news ============================================= -->



        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">News</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body">
                    <fieldset class="content-group">
                        <?php echo browImg('news','bg','bg','src','news_bg_src','Background src',$data['news_bg_src'], 1950, 1563) ?>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background alt<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="news_bg_alt" name="news_bg_alt" class="form-control" required="required" value="{{$data['news_bg_alt']}}" >
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('news','bg','bg','alt','news_bg_alt')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                            <hr>
                                <?php echo browImg('news','img','header','src','news_img_src','Image src',$data['news_img_src'], 1950, 422) ?>
                            <div class="form-group">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Image alt<span class="text-danger">*</span></label>
                                    <div class="col-lg-8">

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="news_img_alt" name="news_img_alt" class="form-control" required="required" value="{{$data['news_img_alt']}}">
                                        </div>
                                        <div class="input-group">
                                            <img src="" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('news','img','header','alt','news_img_alt')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>


        <!-- ===================================================== item guide ================================================== -->



        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Item Guide</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body">
                    <fieldset class="content-group">
                            <?php echo browImg('item_guide','bg','bg','src','item_guide_bg_src','Background src',$data['item_guide_bg_src'],1950, 1563) ?>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background alt<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="item_guide_bg_alt" name="item_guide_bg_alt" class="form-control" required="required" value="{{$data['item_guide_bg_alt']}}">
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('item_guide','bg','bg','alt','item_guide_bg_alt')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                            <hr>
                               <?php echo browImg('item_guide','img','header','src','item_guide_img_src','Image src',$data['item_guide_img_src'], 1950, 867) ?>
                            <div class="form-group">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Image alt<span class="text-danger">*</span></label>
                                    <div class="col-lg-8">

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="item_guide_img_alt" name="item_guide_img_alt" class="form-control" required="required" value="{{$data['item_guide_img_alt']}}" >
                                        </div>
                                        <div class="input-group">
                                            <img src="" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('item_guide','img','header','alt','item_guide_img_alt')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>


        <!-- ===================================================== Banner ================================================== -->



        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Banner</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body">
                    <fieldset class="content-group">
                           <?php echo browImg('banner','bg','bg','src','banner_bg_src','Background src',$data['banner_bg_src'], 1950, 517) ?>
                        <div class="form-group">
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Background alt<span class="text-danger">*</span></label>
                                <div class="col-lg-8">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="banner_bg_alt" name="banner_bg_alt" class="form-control" required="required" value="{{$data['banner_bg_alt']}}">
                                    </div>
                                    <div class="input-group">
                                        <img src="" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-success"  onclick="update('banner','bg','bg','alt','banner_bg_alt')"> <i class="icon-checkmark"></i></button>

                                </div>
                            </div>
                            <hr>
                                <?php echo browImg('banner','img','header','src','banner_img_src','Image src',$data['banner_img_src'], 737, 637) ?>
                            <div class="form-group">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Image alt<span class="text-danger">*</span></label>
                                    <div class="col-lg-8">

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="banner_img_alt" name="banner_img_alt" class="form-control" required="required" value="{{$data['banner_img_alt']}}">
                                        </div>
                                        <div class="input-group">
                                            <img src="" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('banner','img','header','alt','banner_img_alt')"> <i class="icon-checkmark"></i></button>

                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>


        <!-- ===================================================== Footer ================================================== -->


        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">Footer</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>

                <div class="panel-body">
                    <fieldset class="content-group">
                               <?php echo browImg('footer','img','bg','src','footer_img_src','Image src',$data['footer_img_src'], 1950, 945) ?>
                            <div class="form-group">
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Image alt<span class="text-danger">*</span></label>
                                    <div class="col-lg-8">

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                            <input type="text" id="footer_img_alt" name="footer_img_alt" class="form-control" required="required" value="{{$data['footer_img_alt']}}">
                                        </div>
                                        <div class="input-group">
                                            <img src="" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success"  onclick="update('footer','img','bg','alt','footer_img_alt')"> <i class="icon-checkmark"></i></button>
                                     </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    
</form>
   
    
        <!-- /basic modal -->
        @endsection

        @section('script')
        <script>

            function save(section_name,types,name,column,id){
                console.log(section_name,types,name,column,id);
                  var formData = new FormData($('#form-'+id)[0]);
                    formData.append("section_name", section_name);
                    formData.append("types", types);
                    formData.append("name", name);
                    formData.append("column", column);
                 console.log(formData);
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('admin.pubglite.page.saveImg') }}",
                        data: formData,
                        dataType: 'json',
                        processData: false,  // tell jQuery not to process the data
                        contentType: false,   // tell jQuery not to set contentType
                        success: function (data) {
                            status_submit = false;
                            if(data.status){
                                swal({
                                    title: data.message,
                                    confirmButtonColor: "#66BB6A",
                                    type: "success",
                                    confirmButtonText: "ตกลง",
                                }).then(function () {
                                    location.reload();
                                }, function (dismiss) {});
                            }else{
                                swal({
                                    title: data.message,
                                    confirmButtonColor: "#EF5350",
                                    type: "error"
                                }).then(function (dismiss) {});
                            }
                        },
                        error: function (xhr, type) {
                           status_submit = false;
                        }
                    });
            }

            function update(section_name , types , name , column , id){
                var value = $('#'+id).val();
                console.log(section_name , types , name , column , value);
                
                
                 $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.pubglite.page.updatePage') }}",
                        data: {
                                'section_name': section_name,
                                'types' : types,
                                'name'  : name,
                                'column': column,
                                'value' : value,
                        },
                        dataType: 'json',
                        // processData: false,  // tell jQuery not to process the data
                        // contentType: false,   // tell jQuery not to set contentType
                        success: function (data) {
                            console.log(data);
                        status_submit = false;
                        if (data.status){
                        swal({
                        title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                        }).then(function () {
                        location.reload();
                        }, function (dismiss) {});
                        } else{
                        swal({
                        title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                        }).then(function (dismiss) {});
                        }
                        },
                        error: function (xhr, type) {
                        status_submit = false;
                        }
                 });
                
            }
            function addNewSection() {
            var section_id = $('#modal_add_new_section_id').val();
            var element_type = $('#modal_add_new_section_element_type').val();
            $.ajax({
            type: 'POST',
                    url: "{{ route('admin.pubglite.page.addSection') }}",
                    data: {
                            'slug': slug,
                            'section_id': section_id,
                            'element_type': element_type
                    },
                    dataType: 'json',
                    // processData: false,  // tell jQuery not to process the data
                    // contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                    status_submit = false;
                    if (data.status){
                    swal({
                    title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                    }).then(function () {
                    location.reload();
                    }, function (dismiss) {});
                    } else{
                    swal({
                    title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                    }).then(function (dismiss) {});
                    }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
            });
            }

            function addNewElement() {
            var section_id = $('#modal_add_new_element_section_id').val();
            var element_type = $('#modal_add_new_element_element_type').val();
            console.log(section_id);
            console.log(element_type);
            $.ajax({
            type: 'POST',
                    url: "{{ route('admin.fo4.page.addElement') }}",
                    data: {
                    'slug': slug,
                            'section_id': section_id,
                            'element_type': element_type
                    },
                    dataType: 'json',
                    // processData: false,  // tell jQuery not to process the data
                    // contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                    status_submit = false;
                    if (data.status){
                    swal({
                    title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                    }).then(function () {
                    location.reload();
                    }, function (dismiss) {});
                    } else{
                    swal({
                    title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                    }).then(function (dismiss) {});
                    }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
            });
            }

            function deleteSection(id) {

            console.log(id);
            swal({
            title: "Are you sure?",
                    text: "You will not be able to recover this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel pls!",
                    closeOnConfirm: false,
                    closeOnCancel: true
            }).then(function () {
            $.ajax({
            type: 'POST',
                    url: "{{ route('admin.fo4.page.deleteSection') }}",
                    data: {
                    'id': id
                    },
                    dataType: 'json',
                    // processData: false,  // tell jQuery not to process the data
                    // contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                    status_submit = false;
                    if (data.status){
                    swal({
                    title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                    }).then(function () {
                    location.reload();
                    }, function (dismiss) {});
                    } else{
                    swal({
                    title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                    }).then(function (dismiss) {});
                    }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
            });
            }, function (dismiss) {});
            }

            function deleteElement(id) {

            console.log(id);
            swal({
            title: "Are you sure?",
                    text: "You will not be able to recover this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel pls!",
                    closeOnConfirm: false,
                    closeOnCancel: true
            }).then(function () {
            $.ajax({
            type: 'POST',
                    url: "{{ route('admin.fo4.page.deleteElement') }}",
                    data: {
                    'id': id
                    },
                    dataType: 'json',
                    // processData: false,  // tell jQuery not to process the data
                    // contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                    status_submit = false;
                    if (data.status){
                    swal({
                    title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                    }).then(function () {
                    location.reload();
                    }, function (dismiss) {});
                    } else{
                    swal({
                    title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                    }).then(function (dismiss) {});
                    }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
            });
            }, function (dismiss) {});
            }

            function saveElement(id, field) {

            var value;
            if (field == 'editor') {
            value = $('#' + id + '--' + field).html();
            } else{
            value = $('#' + id + '--' + field).val();
            }


            $.ajax({
            type: 'POST',
                    url: "{{ route('admin.fo4.page.saveElement') }}",
                    data: {
                    'id': id,
                            'field': field,
                            'value': value
                    },
                    dataType: 'json',
                    success: function (data) {
                    status_submit = false;
                    if (data.status){
                    swal({
                    title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                    }).then(function () {
                    location.reload();
                    }, function (dismiss) {});
                    } else{
                    swal({
                    title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                    }).then(function (dismiss) {});
                    }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
            });
            }

            function saveImage(id) {
            var formData = new FormData($('#form-data')[0]);
            formData.append('id', id);
            $.ajax({
            type: 'POST',
                    url: "{{ route('admin.fo4.page.saveImage') }}",
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                    console.log(data);
                    if (data.status){
                    swal({
                    title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                    }).then(function () {
                    location.reload();
                    }, function (dismiss) {});
                    } else{
                    swal({
                    title: data.msg,
                            confirmButtonColor: "#4caf50",
                            type: "error"
                    })
                    }
                    },
                    error: function (xhr, type) {
                    // console.log(xhr);
                    }
            });
            }

            function detailsAndRulesUploadImage(file, editor) {

            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
            type: 'POST',
                    url: "{{ route('admin.fo4.page.saveImageEditor') }}",
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                    $(editor).summernote(
                            'insertImage',
                            data.message
                            );
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
            });
            }

            $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            $('.summernote-height').summernote({
            height: 500,
                    toolbar: [
                    ['headline', ['style']],
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['misc', ['fullscreen', 'codeview', 'help']]
                    ],
                    styleTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
                    popover: {
                    image: [
                    ['custom', ['imageAttributes']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                    ],
                    },
                    imageAttributes:{
                    icon:'<i class="note-icon-pencil"/>',
                            removeEmpty: false, // true = remove attributes | false = leave empty if present
                            disableUpload: true // true = don't display Upload Options | Display Upload Options
                    },
                    callbacks: {
                    onImageUpload: function(files) {
                    detailsAndRulesUploadImage(files[0], this);
                    }
                    }
            });
            $('input[type="file"]').uniform({
            fileButtonClass: 'action btn bg-pink-400'
            });
            $('input[name="txt_show_datetime"]').daterangepicker({
            showDropdowns: true,
                    singleDatePicker: true,
                    timePicker: true,
                    timePicker24Hour: true,
                    locale: {
                    format: 'YYYY-MM-DD HH:mm'
                    }
            });
            $('input[name="txt_register_period"], input[name="txt_tournament_period"]').daterangepicker({
            showDropdowns: true,
                    timePicker: true,
                    timePicker24Hour: true,
                    locale: {
                    format: 'YYYY-MM-DD HH:mm'
                    }
            });
            $('body').on('click', '#btn-save', function(event) {
            var msg = '';
            if ($('#txt_title').val().trim() == '')
                    msg = 'กรุณากรอก หัวข้อ';
            if (msg != ''){
            swal({
            title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
            });
            } else{
            saveCreate();
            }
            return false;
            });
            
            $('body').on('change', 'input[name="txt_image"]', function() {
                var id = $(this).attr('id');
console.log(id);

                    if (this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                    var ext = this.value.match(/\.(.+)$/)[1];
                    var _type = $(this).attr('data-type');
                    if (_type.search(ext) < 0){
                        setTimeout(function(){
                        swal({
                        title: 'กรุณาเลือกรูปนามสกุล ' + _type,
                                confirmButtonColor: "#EF5350",
                                allowOutsideClick: false,
                                type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                        }, 300);
                    return false;
                    }
                    
            });
            function saveCreate() {

            var formData = new FormData($('#form-data')[0]);
            $.ajax({
            type: 'POST',
            {{-- // url: "{{ route('hon.tournament.saveEdit') }}", --}}
                    data: formData,
                    dataType: 'json',
                    processData: false, // tell jQuery not to process the data
                    contentType: false, // tell jQuery not to set contentType
                    success: function (data) {
                    status_submit = false;
                    if (data.status){
                    swal({
                    title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                    }).then(function () {
                    {{-- window.location.href = "{{ route('hon.tournament.index') }}"; --}}
                    }, function (dismiss) {});
                    } else{
                    swal({
                    title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                    }).then(function (dismiss) {});
                    }
                    },
                    error: function (xhr, type) {
                    status_submit = false;
                    }
            });
            }

            });
        </script>
        @endsection