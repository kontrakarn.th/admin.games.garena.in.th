@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{$page}} Item Guide {{ $data->alt or '' }} </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">{{$page}} Item Guide</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
    <input type="hidden" name="txt_id" id="txt_id" value="{{ $data->id or '' }}">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">@if(isset($data->id))แก้ไขข้อมูล@else เพิ่มข้อมูล @endif</h5>
                    
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                      <fieldset class="content-group">
                          <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Category <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>

                                    <select id="txt_category" name="txt_category" class="form-control" required="required">
                                       
                                            <option value="pet" @if(isset($data->category)) {{ $data->category == 'pet' ? 'selected' : '' }} @endif>pet</option>
                                            <option value="weapon" @if(isset($data->category)) {{ $data->category == 'weapon' ? 'selected' : '' }} @endif>weapon</option>
                                            
                                    </select>
                                </div>
                            </div>
                        </div>
                    <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Name <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_name" name="txt_name" class="form-control" required="required" value="{{ $data->name or '' }}" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Title <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="text" id="txt_title" name="txt_title" class="form-control" required="required" value="{{ $data->title or '' }}" >
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Level <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <input type="number" id="txt_level" name="txt_level" class="form-control" min="0" max="60" required="required" value="{{ $data->level or '' }}" >
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Item Guide Image <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                            @if(!empty($data->image))
                                <div class="input-group">
                                    <img src="{{ $data->image or '' }}" class="img-responsive" alt="">
                                </div>
                            @endif

                                <input type="file" id="txt_image" name="txt_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 394(W) x 552(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Detail <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                    <textarea name="txt_detail" class="form-control" rows="5">{{$data->detail or ''}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-2">Thumbnail <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                            @if(!empty($data->thumbnail))
                                <div class="input-group">
                                    <img src="{{ $data->thumbnail or '' }}" class="img-responsive" alt="">
                                </div>
                            @endif

                                <input type="file" id="txt_thumbnail" name="txt_thumbnail" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required">
                                <br>
                                <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 203(W) x 204(H) PX
                                    </span>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
<?php //dd("test"); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="text-center">
                        <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                        <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script>
    function detailsAndRulesUploadImage(file,editor,welEditable) {

        var formData = new FormData();
        formData.append("file", file);
        formData.append("file", file);
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.pubglite.content.saveImage') }}",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                $('.summernote-height').summernote(
                    'insertImage',
                    data.message
                );
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }

    function save() {
        var msg = '';

        if($('#txt_title').val().trim() == '')
            msg = 'กรุณากรอก Title';

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{
            saveCreate();
        }
        return false;
    }

    function saveCreate() {

        var formData = new FormData($('#form-data')[0]);

        $.ajax({
            type: 'POST',
            url: "{{ route('admin.pubglite.itemguide.saveItemguide') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        window.location.href = "{{ route('admin.pubglite.itemguide.index') }}";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }

$(document).ready(function() {
    App.initSwitch(".switch");
    slug = $('#txt_slug').val();
    id = $('#txt_id').val();

    $('input[type="file"]').uniform({
        fileButtonClass: 'action btn bg-pink-400'
    });

    $('input[name="txt_show_datetime"]').daterangepicker({
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    });

    $('body').on('change', 'input[name="txt_image"]', function() {
        console.log("txt_image");
        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });
    
     $('body').on('change', 'input[name="txt_thumbnail"]', function() {
  console.log("txt_thumbnail");
        if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
        var ext = this.value.match(/\.(.+)$/)[1];
        var _type = $(this).attr('data-type');
        if(_type.search(ext) < 0){
            setTimeout(function(){
                swal({
                    title: 'กรุณาเลือกรูปนามสกุล '+_type,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
                    $('input[type="file"]').val('');
                    $('.uploader .filename').text('No file selected');
            }, 300);

            return false;
        }

    });
});
</script>
@endsection