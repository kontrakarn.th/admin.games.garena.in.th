
@extends('layout.layout')

@section('header')
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - manage Content </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">manage Content</li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat ">
            <div class="panel-heading">
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
            <div class="panel-body">
                <a href="#modal_add_new_content" data-toggle="modal" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Content</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-header main__color main__background">ข้อมูล</div>
        <div class="table-responsive">
            <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Show Datetime</th>
                        <th>Category</th>
                        <th>view</th>
                        <th>Draft</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<!-- Basic modal -->
    <div id="modal_add_new_content" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Add New Content</h5>
                </div>

                <div class="modal-body">

                    <form class="form-horizontal" action="#" id="form_add_new_content">
                        <fieldset class="content-group">

                            <div class="form-group">
                                <label class="control-label col-lg-3">Title :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Name" name="txt_title" id="txt_title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-3">Slug :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" placeholder="Slug" name="txt_slug" id="txt_slug">
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="addNewContent()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<!-- /Basic modal -->

@endsection

@push('script-head')
@endpush

@section('script')
<script type="text/javascript">

function addNewContent() {
    var formData = new FormData($('#form_add_new_content')[0]);

    $.ajax({
        type: 'POST',
        url: "{{ route('admin.speed.content.addContent') }}",
        data: formData,
        dataType: 'json',
        processData: false,  // tell jQuery not to process the data
        contentType: false,   // tell jQuery not to set contentType
        success: function (data) {
            status_submit = false;
            if(data.status){
                swal({
                    title: data.message,
                    confirmButtonColor: "#66BB6A",
                    type: "success",
                    confirmButtonText: "ตกลง",
                }).then(function () {
                    location.reload();
                }, function (dismiss) {});
            }else{
                swal({
                    title: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error"
                }).then(function (dismiss) {});
            }
        },
        error: function (xhr, type) {
           status_submit = false;
        }
    });
}

function deleteContent(id) {


    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#EF5350",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel pls!",
        closeOnConfirm: false,
        closeOnCancel: true
    }).then(function () {
        $.ajax({
            type: 'POST',
            url: "{{ route('admin.speed.content.deleteContent') }}",
            data: { 
                'id': id
                },
            dataType: 'json',
            // processData: false,  // tell jQuery not to process the data
            // contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                    }).then(function () {
                        location.reload();
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
               status_submit = false;
            }
        });
    }, function (dismiss) {});
    
}

$(function(){
    
    $('#datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin.speed.content.datatable') }}',
        method: 'post',
        columns: [
            {   data: 'id',
                name: 'id' 
            },
            {   data: 'name',
                name: 'name' 
            },
            {   data: 'slug',
                name: 'slug' 
            },
            {   data: 'show_datetime',
                name: 'show_datetime' 
            },
            {   data: 'category',
                name: 'category' 
            },
            {   data: 'view',
                name: 'view' 
            },
            {   data: 'draft',
                name: 'draft' 
            },
            {   data: 'status',
                name: 'status' 
            },
            {   data: 'action',
                name: 'action' 
            }
        ],
        order: [[ 0, "asc" ]],
        drawCallback: function( settings ) {
            
            var api = this.api();
            var _ObjTable = $('#datatable');
            App.initSwitch(_ObjTable.find(".switch"));
            _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
            });
        },
        drawCallback : function(settings) {
            var _ObjTable = $('#datatable');
             App.initSwitch(_ObjTable.find(".switch"));
             _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.speed.content.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                         if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                         });
                         }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                         });
                         }
                    }
                );
            });
        }
    });

});
</script>
@endsection
