@extends('layout.layout')


@section('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - {{isset($data->id) ? "Edit":"Add"}} Content/News {{ $data->alt or '' }} </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">{{isset($data->id) ? "Edit":"Add"}} Content/News</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
        <input type="hidden" name="id" id="id" value="{{ $data->id or '' }}">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat ">
                    <div class="panel-heading">
                        <h5 class="panel-title">@if(isset($data->id))แก้ไขข่าวสาร@else เพิ่มข่าวสาร @endif</h5>

                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>
                    <div class="panel-body">
                        <fieldset class="content-group">
                            @if (isset($data->id))
                                <div class="form-group has-feedback">
                                    <label class="control-label col-lg-2">Active</label>
                                    <div class="col-lg-8">
                                        <div class="input-group">
                                            @if ($data->status === 'active')
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">
                                            @else
                                                <input type="checkbox" data-id="{{$data->id}}" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Title <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="title" name="title" class="form-control" required="required" value="{{ $data->title or '' }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Category <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>

                                        <select id="category_id" name="category_id" class="form-control" required="required">
                                            <option value="" disabled selected>กรุณาเลือกหมวดหมู่</option>
                                            @foreach ($categories as $category)
                                                @if (isset($data))
                                                    <option value="{{$category->id}}" data-slug="{{$category->slug}}" {{$data->category_id == $category->id ? "selected" : null}}>{{$category->name}}</option>
                                                @else
                                                    <option value="{{$category->id}}" data-slug="{{$category->slug}}">{{$category->name}}</option>

                                                @endif
                                            @endforeach
                                        </select>
                                        <span class="label label-info arrowed-right arrowed-in main__color" id="textInfomation" style="display: none">หมวดหมู่ "ข้อมูล" จะไม่ปรากฏอยู่ในหน้าข่าวสาร</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Slug <span class="text-danger">*</span> <small class="text-muted"> จะปรากฏหลัง URL (เช่น awc-announcment) สูงสุด 16 ตัวอักษร</small></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="slug" name="slug" class="form-control" required="required" value="{{ $data->slug or '' }}" maxlength="16" >

                                        @if (empty($data->id))
                                            <p id="urlShow" class="text-primary" style="margin-top: 50px;display: none;">URL ที่จะได้ : https://af.in.th/<span id="catUrl"></span>/<span id="slugUrl"></span></p>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Description <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="description" name="description" class="form-control" required="required" value="{{ $data->description or '' }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Show Date Time <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>
                                        <input type="text" id="show_datetime" name="show_datetime" class="form-control" required="required" value="{{ $data->show_datetime or '' }}" >
                                    </div>
                                </div>
                            </div>

{{--                            <div class="form-group has-feedback">--}}
{{--                                <label class="control-label col-lg-2">Tags</label>--}}
{{--                                <div class="col-lg-8">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>--}}
{{--                                        <select class="js-example-responsive" multiple="multiple" style="width: 100%;" name="tags[]">--}}
{{--                                            @if (isset($data->id))--}}
{{--                                                @foreach($data->content_tags as $tag)--}}
{{--                                                    <option value="{{$tag->tag_name}}" selected>{{$tag->tag_name}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            @else--}}
{{--                                                @foreach($tags as $tag)--}}
{{--                                                    <option value="{{$tag->name}}">{{$tag->name}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            @endif--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Pin/Hilight <span class="text-danger">*</span></label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-pin"></i></span>

                                        <select id="hilight" name="hilight" class="form-control" required="required">
                                            @if (!isset($data))
                                                <option value="no" selected>No</option>
                                                <option value="yes">Yes</option>
                                            @else
                                                <option value="no" {{$data->hilight == "no" ? "selected" : null}}>No</option>
                                                <option value="yes" {{$data->hilight == "yes" ? "selected" : null}}>Yes</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
{{--                            <div class="form-group has-feedback">--}}
{{--                                <label class="control-label col-lg-2">Pin/Hilight <span class="text-danger">*</span></label>--}}
{{--                                <div class="col-lg-8">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <span class="input-group-addon"><i class="icon-pencil"></i></span>--}}
{{--                                        <input type="checkbox" id="txt_pin" name="txt_pin" data-id="{{ $data->id }}" class="switch" data-on-text="Pin" data-off-text="Not pin" data-on-color="success" >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Thumbnail Image <span class="text-danger">*</span></label>
                                <div class="col-lg-5">

                                    <input type="file" id="content_image" name="content_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" required="required" value="{{ $data->content_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 350 × 350 px
                                    </span>
                                        <p></p>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->content_image))
                                        <div class="input-group">
                                            <img src="{{ $data->content_image or '' }}" class="img-responsive" alt="" id="preview">
                                        </div>
                                    @else
                                        <img id="preview" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="control-label col-lg-2">Share Image (facebook)</label>
                                <div class="col-lg-5">

                                    <input type="file" id="share_image" name="share_image" class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm" accept="image/*" value="{{ $data->share_image or '' }}">
                                    <br>
                                    <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปต้องมีขนาด 1200 x 630 px เท่านั้น ถึงจะแสดงผลอย่างถูกต้อง
                                    </span>
                                        <p></p>
                                    <span class="label label-danger arrowed-right arrowed-in">
                                        หากไม่ใส่จะเป็นรูป <a href="https://static2.garena.in.th/data/rov/content/content/4d9f4761275c83ab15df75eca81d1765.png" target="_blank">Default</a>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    @if(!empty($data->share_image))
                                        <div class="input-group">
                                            <img src="{{ $data->share_image or '' }}" class="img-responsive" alt="" id="preview_share_image">
                                        </div>
                                    @else
                                        <img id="preview_share_image" src="" style="width: 100%;display: none" class="img-responsive"/>
                                    @endif
                                </div>
                            </div>

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">News Detail <span class="text-danger">*</span></h5>
                        {{-- <span class="label label-info arrowed-right arrowed-in">สูงสุด 8000 ตัวอักษร</span> --}}
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                            </ul>
                        </div>
                        <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            {{--<input name="detail" type="hidden" value="{{isset($data->description) ? $data->description : null}}">
                                <div id="editor-container">
                                    {!! isset($data->description) ? $data->description : null !!}
                                </div>--}}
                            <textarea name="detail" class="summernote-height">{{ isset($data->detail) ? $data->detail : null }}</textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="text-center">
                            <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                            <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="save()">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('script')
    <script>
        function detailsAndRulesUploadImage(file,editor,welEditable) {

            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.aceforce.saveImage') }}",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.summernote-height').summernote(
                        'insertImage',
                        data.message
                    );
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function save() {
            var msg = '';

            if($('#description').val().trim() == '')
                msg = 'กรุณากรอก Description';

            if($('#slug').val().trim() == '')
                msg = 'กรุณากรอก Slug';

            if($('#title').val().trim() == '')
                msg = 'กรุณากรอก Title';

            if(msg != ''){
                swal({
                    title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
            }else{
                saveCreate();
            }
            return false;
        }

        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.aceforce.content.saveContent') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.aceforce.content.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $(id).show();
                    $(id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            App.initSwitch(".switch");
            slug = $('#slug').val();
            id = $('#id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('body').on('change', 'input[name="content_image"]', function() {

                if(this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if(_type.search(ext) < 0){
                    setTimeout(function(){
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล '+_type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#content_image").change(function() {
                readURL(this,'#preview');
            });

            $("#share_image").change(function() {
                readURL(this,'#preview_share_image');
            });

            $('#slug').on('change keyup',function () {
                $('#slug').val($(this).val()
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'')
                    .replace(/ -/g,''));

                $('#slugUrl').text($(this).val())
                $('#urlShow').fadeIn();
            });

            $('.summernote-height').summernote({
                height: 500,
                toolbar: [
                    ['headline', ['style']],
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],

                    ['misc', ['fullscreen', 'codeview', 'help']]
                ],
                styleTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
                popover: {
                    image: [
                        ['custom', ['imageAttributes']],
                        ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                },
                imageAttributes:{
                    icon:'<i class="note-icon-pencil"/>',
                    removeEmpty: false, // true = remove attributes | false = leave empty if present
                    disableUpload: true // true = don't display Upload Options | Display Upload Options
                },
                callbacks: {
                    onImageUpload: function(files,editor,welEditable) {
                        detailsAndRulesUploadImage(files[0], editor, welEditable);
                    }
                }
            });

            $(".switch").on('switchChange.bootstrapSwitch', function(event, state) {
                event.preventDefault();
                $.post(
                    "{{ route('admin.aceforce.content.updateStatus') }}",
                    { id: $(this).attr('data-id'), status: state },
                    function( data ) {
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#4caf50",
                                type: "success"
                            }).then(function () {

                            });
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "info"
                            }).then(function () {

                            });
                        }
                    }
                );
            });

            $(".js-example-responsive").select2({
                // width: 'resolve',
                tags: true
            });

            $('#category_id').on('change',function () {
                const slug = $( "#category_id option:selected" ).attr('data-slug');
               if (slug == 'informations'){
                   $('#textInfomation').fadeIn();
               } else{
                   $('#textInfomation').fadeOut();
               }

               $('#catUrl').text(slug)
                $('#urlShow').fadeIn();

            });
        });
    </script>
@endsection