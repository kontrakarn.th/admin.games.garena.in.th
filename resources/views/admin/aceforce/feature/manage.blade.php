@extends('layout.layout')

@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - manage
                    Feature </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">manage Feature</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <a href="{{ route('admin.aceforce.feature.add') }}" class="btn btn-primary"><i
                                class="icon-plus-circle2 position-left"></i> เพิ่ม Feature</a>
                    <a href="{{ route('admin.aceforce.feature.ordering') }}" class="btn btn-info"><i
                                class="icon-menu position-left"></i> จัดการลำดับการแสดงผล</a>
                </div>
            </div>
        </div>

    </div>

    <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery">
    <div class="cold-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </div>
                <div class="panel-body">
                    <div class="form-group has-feedback col-md-12">
                        <label class="control-label col-lg-2">Background Image <span
                                    class="text-danger">*</span></label>
                        <div class="col-lg-5">
                            <input type="file" id="txt_banner_image" name="txt_banner_image" class="file-styled"
                                   data-type="jpg,jpeg,png,mp4,webm" data-show-caption="false" data-show-upload="false"
                                   data-browse-class="btn btn-primary btn-sm" data-remove-class="btn btn-default btn-sm"
                                   accept="image/*,video/*" required="required" value="{{ $content->bg_image or '' }}">
                            <br>
                            <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 1920 × 700 px
                                    </span>
                                <p></p>
                                <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            @if(!empty($content->bg_image))
                                <div class="input-group">
                                    <img src="{{ $content->bg_image or '' }}" class="img-responsive" alt=""
                                         id="preview">
                                </div>
                            @else
                                <img id="preview" src="" style="width: 100%;display: none" class="img-responsive"/>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="form-group has-feedback col-md-12">
                        <label class="control-label col-lg-2">Image Header<span class="text-danger">*</span></label>
                        <div class="col-lg-5">
                            <input type="file" id="txt_image_header" name="txt_image_header"
                                   class="file-styled" data-type="jpg,jpeg,png" data-show-caption="false"
                                   data-show-upload="false" data-browse-class="btn btn-primary btn-sm"
                                   data-remove-class="btn btn-default btn-sm" accept="image/*" required="required"
                                   value="{{ $content->image_header or '' }}">
                            <br>
                            <div class="help-block">
                                    <span class="label label-success arrowed-right arrowed-in main__color">
                                        รูปควรมีขนาด 500 x 130 px
                                    </span>
                                <p></p>
                                <span class="label label-danger arrowed-right arrowed-in">
                                        รูปภาพประกอบต้องเป็น .jpg, .png และขนาดไม่เกิน 1MB
                                    </span>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            @if(!empty($content->image_header))
                                <div class="input-group">
                                    <img src="{{ $content->image_header or '' }}" class="img-responsive" alt=""
                                         id="preview_image_header">
                                </div>
                            @else
                                <img id="preview_image_header" src="" style="width: 100%;display: none"
                                     class="img-responsive"/>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2">
                            <button  type="button" class="btn btn-primary" style="width: 100%" onclick="save()">save</button>
                        </div>
                        <div class="col-md-5"></div>
                    </div>
                </div>

        </div>
    </div>

    </form>

    <div class="row">
        <div class="col-xs-12">
            <div class="table-header main__color main__background">ข้อมูล</div>
            <div class="table-responsive">
                <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25"
                       width="100%">
                    <thead>
                    <tr>
                        {{--                        <th>ID</th>--}}
                        <th></th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Order</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('script-head')
@endpush

@section('script')
    <script>

        function detailsAndRulesUploadImage(file, editor, welEditable) {

            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                type: 'POST',
                url: "{{ route('admin.aceforce.saveImage') }}",
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.summernote-height').summernote(
                        'insertImage',
                        data.message
                    );
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }

        function save() {
            var msg = '';
            const status = '{!! isset($content->id) !!}';


            if ($('#txt_banner_image').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูป Background';

            if ($('#txt_image_header').val().trim() == '' && status != 1)
                msg = 'กรุณาอัพโหลดรูปสำหรับ Header';

            if (msg != '') {
                swal({
                    title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
            } else {
                saveCreate();
            }
            return false;
        }


        function saveCreate() {

            var formData = new FormData($('#form-data')[0]);

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.aceforce.feature.saveBgFeature') }}",
                data: formData,
                dataType: 'json',
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    status_submit = false;
                    if(data.status){
                        swal({
                            title: data.message,
                            confirmButtonColor: "#66BB6A",
                            type: "success",
                            confirmButtonText: "ตกลง",
                        }).then(function () {
                            window.location.href = "{{ route('admin.aceforce.feature.index') }}";
                        }, function (dismiss) {});
                    }else{
                        swal({
                            title: data.message,
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        }).then(function (dismiss) {});
                    }
                },
                error: function (xhr, type) {
                    status_submit = false;
                }
            });
        }


        function deleteBanner(id) {


            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel pls!",
                closeOnConfirm: false,
                closeOnCancel: true
            }).then(function () {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.aceforce.feature.deleteBanner') }}",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    // processData: false,  // tell jQuery not to process the data
                    // contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                location.reload();
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                        status_submit = false;
                    }
                });
            }, function (dismiss) {});

        }

        $(document).ready(function () {
            App.initSwitch(".switch");
            slug = $('#txt_slug').val();
            id = $('#txt_id').val();

            $('input[type="file"]').uniform({
                fileButtonClass: 'action btn bg-pink-400'
            });

            $('input[name="txt_show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });

            $('body').on('change', 'input[name="txt_banner_image"]', function () {

                if (this.value.match(/\.(.+)$/) == '' || this.value.match(/\.(.+)$/) == null) return false;
                var ext = this.value.match(/\.(.+)$/)[1];
                var _type = $(this).attr('data-type');
                if (_type.search(ext) < 0) {
                    setTimeout(function () {
                        swal({
                            title: 'กรุณาเลือกรูปนามสกุล ' + _type,
                            confirmButtonColor: "#EF5350",
                            allowOutsideClick: false,
                            type: "info"
                        });
                        $('input[type="file"]').val('');
                        $('.uploader .filename').text('No file selected');
                    }, 300);

                    return false;
                }

            });

            $("#txt_banner_image").change(function () {
                readURL(this, 'preview');
            });
            $("#txt_image_header").change(function () {
                readURL(this, 'preview_image_header');
            });


            function readURL(input, id) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#' + id).show();
                        $('#' + id).attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(function () {

                $('#datatable').DataTable({
                    // $('#').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{{ route('admin.aceforce.feature.datatable') }}',
                    method: 'post',
                    columns: [
                        {
                            data: 'image',
                            name: 'image'
                        },
                        {
                            data: 'title',
                            name: 'title'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'order',
                            name: 'order'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        }
                    ],
                    order: [[0, "desc"]],
                    drawCallback: function (settings) {
                        var api = this.api();
                        // console.log('DataTables has redrawn the table');
                        // console.log( api.rows( {page:'current'} ).data() );

                        var _ObjTable = $('#datatable');
                        App.initSwitch(_ObjTable.find(".switch"));
                        _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function (event, state) {
                            event.preventDefault();
                            console.log('test');
                            console.log($(this).attr('data-id'));
                            console.log(state);
                        });
                    },
                    drawCallback: function (settings) {
                        var _ObjTable = $('#datatable');
                        App.initSwitch(_ObjTable.find(".switch"));
                        _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function (event, state) {
                            event.preventDefault();
                            $.post(
                                "{{ route('admin.aceforce.feature.updateStatus') }}",
                                {id: $(this).attr('data-id'), status: state},
                                function (data) {
                                    if (data.status) {
                                        swal({
                                            title: data.message,
                                            confirmButtonColor: "#4caf50",
                                            type: "success"
                                        }).then(function () {

                                        });
                                    } else {
                                        swal({
                                            title: data.message,
                                            confirmButtonColor: "#EF5350",
                                            type: "info"
                                        }).then(function () {

                                        });
                                    }
                                }
                            );
                        });
                    }
                });
            });
        });
    </script>
@endsection
