
@extends('layout.layout')
@section('header')
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Sub Menu Bar </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
                <li><a href="{{ route('admin.aceforce.menubar.index') }}"> Menubar</a></li>
                <li class="active"> Sub Menu Bar</li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat ">
                <div class="panel-heading">
                    <h5 class="panel-title">จัดการ Sub Menu Bar ของ <span class="text-danger">{{ $name_parent->name }}</span> </h5>

                    <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                </div>
                <div class="panel-body">
                    <a href="{{ route('admin.aceforce.menubar.createSubmenu',['parent' => $parent]) }}" data-toggle="modal" class="btn btn-primary"><i class="icon-plus-circle2 position-left"></i> เพิ่ม Sub Menu bar </a>
                    {{--                    <a href="{{ route('admin.fo4.class.ordering') }}" class="btn btn-info"><i class="icon-menu position-left"></i> จัดการลำดับการแสดงผล</a>--}}

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-header main__color main__background">ข้อมูล</div>
            <div class="table-responsive">
                <table id="datatable" class="table datatable-select-checkbox pjax-container" data-page-length="25" width="100%"   >
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Parent</th>
                        <th>Name</th>
                        <th>Url</th>
                        <th>Order</th>
                        <th>Show</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Basic modal -->

    <!-- /Basic modal -->
@endsection

@push('script-head')
@endpush

@section('script')
    <script type="text/javascript">

        $(document).ready(function () {
            App.initSwitch(".switch");


            $('input[name="txt_show_datetime"]').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm'
                }
            });
        })


        function deleteBanner(id) {


            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel pls!",
                closeOnConfirm: false,
                closeOnCancel: true
            }).then(function () {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('admin.aceforce.menubar.sub.deleteEvent') }}",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    // processData: false,  // tell jQuery not to process the data
                    // contentType: false,   // tell jQuery not to set contentType
                    success: function (data) {
                        status_submit = false;
                        if(data.status){
                            swal({
                                title: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                confirmButtonText: "ตกลง",
                            }).then(function () {
                                location.reload();
                            }, function (dismiss) {});
                        }else{
                            swal({
                                title: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error"
                            }).then(function (dismiss) {});
                        }
                    },
                    error: function (xhr, type) {
                        status_submit = false;
                    }
                });
            }, function (dismiss) {});

        }



        $(function(){

            $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.aceforce.menubar.submenu.dataTable', ['parent' => $parent]) }}',
                method: 'post',
                columns: [
                    {   data: 'id',
                        name: 'id'
                    },
                    {   data: 'parent',
                        name: 'parent'
                    },
                    {   data: 'name',
                        name: 'name'
                    },
                    {   data: 'url',
                        name: 'url'
                    },
                    {   data: 'order',
                        name: 'order'
                    },

                    {   data: 'status',
                        name: 'status'
                    },
                    {   data: 'action',
                        name: 'action'
                    },
                ],
                order: [[ 0, "asc" ]],
                drawCallback: function( settings ) {
                    var api = this.api();
                    console.log('DataTables has redrawn the table');
                    // console.log( api.rows( {page:'current'} ).data() );

                    var _ObjTable = $('#datatable');
                    App.initSwitch(_ObjTable.find(".switch"));
                    _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                        event.preventDefault();
                        console.log('test');
                        console.log($(this).attr('data-id'));
                        console.log(state);
                    });
                },
                drawCallback : function(settings) {
                    var _ObjTable = $('#datatable');
                    App.initSwitch(_ObjTable.find(".switch"));
                    _ObjTable.find('.switch').on('switchChange.bootstrapSwitch', function(event, state) {
                        event.preventDefault();
                        $.post(
                            "{{ route('admin.aceforce.menubar.updateStatusSubmenu') }}",
                            { id: $(this).attr('data-id'), status: state },
                            function(data) {
                                if(data.status === true){
                                    swal({
                                        title: data.message,
                                        confirmButtonColor: "#4caf50",
                                        type: "success"
                                    }).then(function () {
                                        window.location.reload();
                                    });
                                }else{
                                    swal({
                                        title: data.message,
                                        confirmButtonColor: "#EF5350",
                                        type: "info"
                                    }).then(function () {
                                        window.location.reload();
                                    });
                                }
                            }
                        );
                    });
                }
            });



        });
    </script>
@endsection
