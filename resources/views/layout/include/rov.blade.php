
<!-- ROV -->
<li class="">

    <a href="#"><i class="icon-list"></i> <span>ROV</span></a>

    <ul>
        <li class="{{ Menu::active(['admin.rov.banner.index']) }}"><a href="{{ route('admin.rov.banner.index') }}">Banners</a></li>
        <li class="{{ Menu::active(['admin.rov.content.index']) }}"><a href="{{ route('admin.rov.content.index') }}">Content/News</a></li>
        <li class="{{ Menu::active(['admin.rov.hero.index']) }}"><a href="{{ route('admin.rov.hero.index') }}">Heros</a></li>
        <li class="{{ Menu::active(['admin.rov.hero.gameinfo']) }}"><a href="{{ route('admin.rov.hero.gameinfo') }}">Game Info</a></li>
        <li class="{{ Menu::active(['admin.rov.tournament.index']) }}"><a href="{{ route('admin.rov.tournament.index') }}">Tournaments</a></li>
        <li class="{{ Menu::active(['admin.rov.setting.index']) }}"><a href="{{ route('admin.rov.setting.index') }}">Settings</a></li>
        <li class="{{ Menu::active(['admin.rov.user.index']) }}"><a href="{{ route('admin.rov.user.index') }}">Users</a></li>

    </ul>
</li>