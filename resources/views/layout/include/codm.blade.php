<!-- CODM -->
<li class="">

    <a href="#"><i class="icon-list"></i> <span>CODM</span></a>

    <ul>
        <li class="{{ Request::is('codm/news*') ? 'active' : '' }}"><a href="{{ route('admin.codm.news.index') }}">News</a></li>
        <li class="{{ Request::is('codm/banner*') ? 'active' : '' }}"><a href="{{ route('admin.codm.banner.index') }}">Banners</a></li>
        <li class="{{ Request::is('codm/setting*') ? 'active' : '' }}"><a href="{{ route('admin.codm.setting.index') }}">Setting</a></li>

        <li class="">
            <a href="#"><i class="icon-contrast"></i> <span>Subscribe</span></a>
            <ul>
                <li  class="{{ Menu::active(['admin.codm.subscribe.create']) }}"><a href="{{ route('admin.codm.subscribe.create') }}">manage</a></li>
{{--                <li  class="{{ Menu::active(['admin.codm.subscribe.index']) }}"><a href="{{ route('admin.codm.subscribe.index') }}">contents</a></li>--}}
            </ul>
        </li>

        <li class="">
            <a href="#"><i class="icon-contrast"></i> <span>TV</span></a>
            <ul>
                <li  class="{{ Menu::active(['admin.codm.tv.index']) }}"><a href="{{ route('admin.codm.tv.index') }}">manage</a></li>
{{--                <li  class="{{ Menu::active(['admin.codm.subscribe.index']) }}"><a href="{{ route('admin.codm.subscribe.index') }}">contents</a></li>--}}
            </ul>
        </li>


        <hr>
        <li class="{{ Request::is('codm/gameguides/beginners*') ? 'active' : '' }}"><a href="{{ route('admin.codm.gameguides.beginners.index') }}">Beginner Guides</a></li>
        <li class="{{ Request::is('codm/gameguides/modes*') ? 'active' : '' }}"><a href="{{ route('admin.codm.gameguides.modes.index') }}">Modes</a></li>
        <li class="{{ Request::is('codm/gameguides/perks*') ? 'active' : '' }}"><a href="{{ route('admin.codm.gameguides.perks.index') }}">Perks</a></li>
        <li class="{{ Request::is('codm/gameguides/weapons*') ? 'active' : '' }}"><a href="{{ route('admin.codm.gameguides.weapons.index') }}">Weapons</a></li>
        <li class="{{ Request::is('codm/gameguides/attachments*') ? 'active' : '' }}"><a href="{{ route('admin.codm.gameguides.attachments.index') }}">Attachments</a></li>
        <li class="{{ Request::is('codm/gameguides/operator_skills*') ? 'active' : '' }}"><a href="{{ route('admin.codm.gameguides.operator_skills.index') }}">Operator Skills</a></li>
        <li class="{{ Request::is('codm/gameguides/characters*') ? 'active' : '' }}"><a href="{{ route('admin.codm.gameguides.characters.index') }}">Characters</a></li>
    </ul>
</li>
