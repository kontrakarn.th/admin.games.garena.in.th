<!-- Menu -->
<li class="">

    <a href="#"><i class="icon-list"></i> <span>FIFA Online 4</span></a>

    <ul>


        {{--                    <li class="{{ Menu::active(['admin.fo4.menu.index']) }}"><a href="{{ route('admin.fo4.menu.index') }}">Menu</a></li>--}}
        <li class="{{ Menu::active(['admin.fo4.menubar.index']) }}"><a href="{{ route('admin.fo4.menubar.index') }}">Menu</a></li>
        <li class="{{ Menu::active(['admin.fo4.page.index']) }}"><a href="{{ route('admin.fo4.page.index') }}">Page</a></li>
        <li class="{{ Menu::active(['admin.fo4.content.index']) }}"><a href="{{ route('admin.fo4.content.index') }}">Content</a></li>
        <li class="{{ Menu::active(['admin.fo4.banner.index']) }}"><a href="{{ route('admin.fo4.banner.index') }}">Banner</a></li>
        <li class="{{ Menu::active(['admin.fo4.event.index']) }}"><a href="{{ route('admin.fo4.event.index') }}">Event</a></li>
        <li class="{{ Menu::active(['admin.fo4.download.edit']) }}"><a href="{{ route('admin.fo4.download.edit') }}"><span>Download</span></a></li>


        {{-- <li class=""><a href="{{ route('admin.main.index') }}">ข่าวสาร</a></li> --}}
        {{-- <li class="{{ Menu::active(['admin.pb.sponsor.history']) }}"><a href="{{ route('admin.pb.sponsor.history') }}">Search History</a></li>
        <li class="{{ Menu::active(['admin.pb.sponsor.report']) }}"><a href="{{ route('admin.pb.sponsor.report') }}">Report</a></li> --}}
        {{-- <li class="">
            <a href="#"><i class="icon-list"></i> <span>เกมส์</span></a>
            <ul>
                <li class=""><a href="{{ route('admin.main.index') }}">จุดเด่น</a></li>
                <li class=""><a href="{{ route('admin.main.index') }}">คู่มือการเล่น</a></li>
            </ul>
        </li>

        <li class="">
            <a href="#"><i class="icon-list"></i> <span>ข่าวสาร</span></a>
            <ul>
                <li class=""><a href="{{ route('admin.main.index') }}">ข่าวสาร</a></li>
                <li class=""><a href="{{ route('admin.main.index') }}">ไอเทม</a></li>
            </ul>
        </li>

        <li class="{{ Menu::active(['admin.fo4.download.edit']) }}"><a href="{{ route('admin.fo4.download.edit') }}"><span>Download</span></a></li>

    </ul>
</li>

<!-- DDT -->
<li class="">

    <a href="#"><i class="icon-list"></i> <span>DD Tank</span></a>

    <ul>
        <li class="{{ Menu::active(['admin.ddt.menu.index']) }}"><a href="{{ route('admin.ddt.menu.index') }}">Menu</a></li>
        <li class="{{ Menu::active(['admin.ddt.page.index']) }}"><a href="{{ route('admin.ddt.page.index') }}">Page</a></li>
         <li class="{{ Menu::active(['admin.ddt.content.index']) }}"><a href="{{ route('admin.ddt.content.index') }}">Content</a></li>
         <li class="{{ Menu::active(['admin.ddt.banner.index']) }}"><a href="{{ route('admin.ddt.banner.index') }}">Banner</a></li>
         <li class="{{ Menu::active(['admin.ddt.itemguide.index']) }}"><a href="{{ route('admin.ddt.itemguide.index') }}">Item Guide</a></li>


    </ul>
</li>

<!-- Contra -->
<li class="">

    <a href="#"><i class="icon-list"></i> <span>Contra</span></a>

    <ul>
        {{-- <li class="{{ Menu::active(['admin.contra.menu.index']) }}"><a href="{{ route('admin.contra.menu.index') }}">Menu</a></li>
        <li class="{{ Menu::active(['admin.contra.page.index']) }}"><a href="{{ route('admin.contra.page.index') }}">Page</a></li> --}}
        <li class="{{ Menu::active(['admin.contra.content.index']) }}"><a href="{{ route('admin.contra.content.index') }}">Content</a></li>
        {{-- <li class="{{ Menu::active(['admin.contra.banner.index']) }}"><a href="{{ route('admin.contra.banner.index') }}">Banner</a></li>
        <li class="{{ Menu::active(['admin.contra.itemguide.index']) }}"><a href="{{ route('admin.contra.itemguide.index') }}">Item Guide</a></li>  --}}

        <li class="">
            <a href="#"><i class="icon-book"></i> <span>Manager Book</span></a>
            <ul>
                <li  class="{{ Menu::active(['admin.fo4.manager-book.index']) }}"><a href="{{ route('admin.fo4.manager-book.index') }}">Menu</a></li>
                <li  class="{{ Menu::active(['admin.fo4.manager-book.event.index']) }}"><a href="{{ route('admin.fo4.manager-book.event.index') }}">Event</a></li>
            </ul>
        </li>

        <li class="">
            <a href="#"><i class="icon-ticket"></i> <span>In Game</span></a>
            <ul>
                <li class="{{ Menu::active(['admin.fo4.in_game.header']) }}"><a href="{{ route('admin.fo4.in_game.header') }}">Header</a></li>
                <li class="{{ Menu::active(['admin.fo4.in_game.index']) }}"><a href="{{ route('admin.fo4.in_game.index') }}">List</a></li>
            </ul>
        </li>

        <li class="">
            <a href="#"><i class="icon-book-play"></i> <span>Icon</span></a>
            <ul>
                <li class="{{ Menu::active(['admin.fo4.icon.room.index']) }}"><a href="{{ route('admin.fo4.icon.room.index') }}">Manage Room</a></li>
                <li class="{{ Menu::active(['admin.fo4.icon.profile.index']) }}"><a href="{{ route('admin.fo4.icon.profile.index') }}">Manage Profile</a></li>
                <li class="{{ Menu::active(['admin.fo4.icon.index']) }}"><a href="{{ route('admin.fo4.icon.index') }}">Manage</a></li>
            </ul>
        </li>

        <li class="">
            <a href="#"><i class="icon-people"></i> <span>Class</span></a>
            <ul>
                <li class="{{ Menu::active(['admin.fo4.class.index']) }}"><a href="{{ route('admin.fo4.class.index') }}">Manage</a></li>
            </ul>
        </li>





    </ul>
</li>
