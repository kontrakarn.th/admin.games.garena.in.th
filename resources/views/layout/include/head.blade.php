<head>
	<title>{{ config('app.name') }}</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  @include('layout.include.style')

</head>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Garena Website - Admin Control Panel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">

    @include('layout.include.style')

    <!-- /global stylesheets -->

    <!-- Local stylesheets -->
    @if(!empty($css) && count($css))
        @foreach ($css as $key => $value)
            <link rel="stylesheet" href="/{{$value}}?={{time('YmdHi')}}">
        @endforeach
    @endif
    <!-- /local stylesheets -->

    <!-- Core JS files -->
    @include('layout.include.script')
    
    <!-- /core JS files -->

    <!-- Theme JS files -->
    
    <!-- /theme JS files -->

    <!-- Local JS files -->
    @if(!empty($js) && count($js))
        @foreach ($js as $key => $value)
            <script src="/{{$value}}?={{time('YmdHi')}}"></script>
        @endforeach
    @endif
    <!-- /local JS files -->

    @stack('script-head')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

</head>
