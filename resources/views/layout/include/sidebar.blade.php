<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    <a target="_blank" href="{{ Auth::user()->profile_picture }}" class="legitRipple"><img src="{{ Auth::user()->profile_picture }}" class="img-circle img-responsive" alt=""></a>
                    <h6>{{Auth::user()->name}}</h6>
                    <span class="text-size-small">{{Auth::user()->email}}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    {{-- <li><a target="_blank" href="{{ $userinfo['link'] }}"><i class="icon-user-plus"></i> <span>My profile</span></a></li> --}}
                    <li class="divider"></li>
                    <li><a target="_blank" href="https://myaccount.google.com/"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                    <li><a href="/auth/logout"><i class="icon-switch2"></i> <span>Logout</span></a></li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        @include('layout.include.menu')
        <!-- /main navigation -->

    </div>
</div>