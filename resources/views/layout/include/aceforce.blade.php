<!-- ACE FORCE -->
<li class="">

    <a href="#"><i class="icon-list"></i> <span>ACE FORCE</span></a>

    <ul>
        <li class="{{ Menu::active(['admin.aceforce.menubar.index']) }}"><a href="{{ route('admin.aceforce.menubar.index') }}">Menu</a></li>
        <li class="{{ Menu::active(['admin.aceforce.banner.index']) }}"><a href="{{ route('admin.aceforce.banner.index') }}">Banner</a></li>
        <li class="{{ Menu::active(['admin.aceforce.setting.index']) }}"><a href="{{ route('admin.aceforce.setting.index') }}">Setting</a></li>
        <li class="{{ Menu::active(['admin.aceforce.feature.index']) }}"><a href="{{ route('admin.aceforce.feature.index') }}">Feature</a></li>
        <li class="{{ Menu::active(['admin.aceforce.content.index']) }}"><a href="{{ route('admin.aceforce.content.index') }}">Content/News</a></li>
        <li class="{{ Menu::active(['admin.aceforce.highlight.index']) }}"><a href="{{ route('admin.aceforce.highlight.index') }}">Highlight</a></li>
    </ul>
</li>