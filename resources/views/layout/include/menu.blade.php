<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->

            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main contents"></i></li>
            <li class="{{ Menu::active(['admin.main.index']) }}"><a href="{{ route('admin.main.index') }}"><i class="icon-home4"></i> <span>Home</span></a></li>

            @if(Auth::user()->access_level=='admin')
                <li class=""><a href="{{ route('admin.main.user.index') }}"><i class="icon-user"></i> <span>User</span></a></li>
                <li class=""><a href="#"><i class="icon-puzzle3"></i> <span>Game</span></a></li>
            @endif
            <!-- Menu Header -->
            <li class="navigation-header"><span>Games</span> <i class="icon-menu" title="PointBlank"></i></li>


            
            @if (Auth::user()->access_level == "admin")
                @php $games = \App\Models\Games::where('status','active')->select('slug')->get() @endphp
                @foreach($games as $game)
                    @include('layout.include.'.$game->slug)
                @endforeach
            @else
                @php $games = \App\Models\UserGame::where('user_id',Auth::user()->id)->where('status','active')->select('game_id')->get() @endphp

                @foreach($games as $game)
                    @include('layout.include.'.$game->getGameName->slug)
                @endforeach
            @endif




        </ul>
    </div>
</div>
