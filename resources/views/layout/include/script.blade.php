<script type="text/javascript" src="{{ asset('/js/vendor.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('/js/ckeditor.js') }}"></script> --}}
<script type="text/javascript" src="/assets/admincp/summernote/summernote.min.js"></script>
<script type="text/javascript" src="/assets/admincp/summernote/summernote-image-attributes.js"></script>
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('/assets/admincp/ckeditor/ckeditor.js') }}"> --}}
@yield('script')
