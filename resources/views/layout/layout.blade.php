<!DOCTYPE html>
<html lang="en">
@include('layout.include.head')

<body>

<!-- Main navbar -->
@include('layout.include.navbar')
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
    {{-- @if (isset($userinfo)) --}}
        @include('layout.include.sidebar')
    {{-- @endif --}}
    <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            @yield('header')
            <!-- /page header -->
			
            <!-- Content area -->
            <div class="content">
	 		
            @yield('content')

            <!-- Footer -->
            @include('layout.include.footer')
            <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
