@extends('layout.layout-auth')

@section('content')
<!-- Advanced login -->
<form action="index.html">
    <div class="panel panel-body login-form">
        <div class="text-center">
            <img style="width: 128px; height: 128px;" src="/images/GARENA_LOGO.png" title="Garena">
            <h5 class="content-group">Login to your account</h5>
        </div>
        <div class="content-divider text-muted form-group"><span>sign in with</span></div>
        <ul class="list-inline form-group list-inline-condensed text-center">
            <li><a href="{{route('auth.login.google')}}" class="btn border-danger text-danger btn-flat btn-icon btn-rounded"><i class="icon-google"></i></a></li>
        </ul>
    </div>
</form>
<!-- /advanced login -->
@endsection