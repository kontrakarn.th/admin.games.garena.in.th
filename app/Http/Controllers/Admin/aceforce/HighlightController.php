<?php

namespace App\Http\Controllers\Admin\aceforce;

use App\Http\Controllers\ApiController;
use App\Models\aceforce\DBHighlight;
use Carbon\Carbon;
use Cache;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class HighlightController extends ApiController
{

    private $pagepath = 'admin.aceforce.highlight.';
    private $uploadPath = 'aceforce/highlight/';
    private $default_share = 'https://static2.garena.in.th/data/aceforce/highlight/highlight/4d9f4761275c83ab15df75eca81d1765.png';


    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return view($this->pagepath . 'manage');
    }

    public function add()
    {
        return view($this->pagepath . 'add');
    }

    public function edit($banner)
    {
        $this->viewData['id'] = $id = $banner;
        $this->viewData['data'] = $data = DBHighlight::findOrFail($id);
        return view($this->pagepath . 'add', $this->viewData);
    }

    public function getDatatable()
    {
        $banners = DBHighlight::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            if ($_model->file_format == "mp4" || $_model->file_format == "webm" || $_model->video_path) {
                return '<a href="' . route('admin.aceforce.highlight.edit', ['id' => $_model->id]) . '"><img src="https://static2.garena.in.th/data/rov/content/content/66b2d780fe8be1b8a5098103caf03d97.png" width="150px">
</a>';
            }
            return '<a href="' . route('admin.aceforce.highlight.edit', ['id' => $_model->id]) . '"><img src="' . $_model->banner_image . '" class="img-responsive" width="150px"></a>';
        });
        $datatables->editColumn('title', function ($_model) {
            return '<small>' . $_model->head_title . '</small><h6><b>' . $_model->title . '</b></h6>';
        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.aceforce.highlight.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image', 'title', 'status', 'order', 'action'])->make(true);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $updated = DBHighlight::updateStatus($id, $status);

        $this->clearCache();

        if ($updated) {
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Status Not Updated'
        ]);
    }

    public function saveBanner(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'txt_title' => 'required',
            'txt_banner_image' => 'mimes:jpeg,jpg,gif,png|dimensions:width=750,height=370',

        ], [
            'txt_banner_image.dimensions' => 'ขนาดรูปต้องเป็น 750 × 370 เท่านั้น',

        ]);



        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()->first()
            ]);
        }

        if ($request->hasFile('txt_banner_image')) {
            try {
                $banner_image = $this->uploadImage(
                    $request->file('txt_banner_image'),
                    $this->uploadPath . 'banner',
                    'upload',
                    null
                );
            } catch (\Exception $x) {
                return response()->json([
                    'status' => false,
                    'message' => $x->getMessage(),
                ]);
            }
        } else {
            if (isset($request->txt_id)) {
                $banner_image = DBHighlight::where('id', $request->txt_id)->value('banner_image');
            } else {
                $banner_image = "-";
            }
        }

        try {
            $saved = DBHighlight::saveBanner($request, $banner_image);

        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
        if ($saved) {
            $this->clearCache();

            return response()->json([
                'status' => true,
                'message' => 'Saved',
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Not Save',
        ]);
    }

    public function ordering()
    {
        $banners = DBHighlight::where('status', 'active')->orderBy('order', 'asc')->get();
        return view('admin.aceforce.highlight.ordering', [
            'banners' => $banners
        ]);
    }

    public function saveOrder(Request $request)
    {

        if (!$request->newSort) {
            return response()->json([
                'status' => false,
                'message' => "Something Wrong.",
            ]);
        }
        $news = explode(',', $request->newSort);
        try {
            DB::connection('rov')->table('mainsite_banners')->update(array('order' => null));
            for ($i = 0; $i < sizeof($news); $i++) {
                $banner = DBHighlight::where('id', $news[$i])->first();
                if ($banner) {
                    $banner->order = $i + 1;
                    $banner->updated_at = Carbon::now();
                    $banner->updated_by = Auth::user()->id;
                    $banner->save();
                }
            }
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        $this->clearCache();

        return response()->json([
            'status' => true,
            'message' => "Sorted",
        ]);
    }

    public function deleteBanner(Request $request)
    {
        $id = $request->id;
        if (!$id) {
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = DBHighlight::find($id);
        if (!$check) {
            return response()->json([
                'status' => false,
                'message' => "Not found banner.",
            ]);
        }

        try {
            $check->delete();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        $this->clearCache();

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }

    public function clearCache()
    {
        Cache::setPrefix("");
        Cache::forget(config("cache.af_prefix").":getHomeInfo");
    }
}
