<?php

namespace App\Http\Controllers\Admin\aceforce;

use App\Http\Controllers\ApiController;
use App\Models\aceforce\DBFeature;
use App\Models\aceforce\DBFeatureBg;
use Carbon\Carbon;
use Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class FeatureController extends ApiController
{
    private $pagepath = 'admin.aceforce.feature.';
    private $uploadPath = 'aceforce/mainsite/';

    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $content = DBFeatureBg::where("key","features")->first();

        return view('admin.aceforce.feature.manage',['content'=>$content]);

    }


    public function add()
    {
        return view('admin.aceforce.feature.add');
    }

    public function edit($banner)
    {
        $this->viewData['id'] = $id = $banner;
        $this->viewData['data'] = $data = DBFeature::findOrFail($id);
        return view($this->pagepath . 'add', $this->viewData);
    }

    public function ordering()
    {
        $banners = DBFeature::where('status', 'active')->orderBy('order', 'asc')->get();
        return view('admin.aceforce.feature.ordering', [
            'banners' => $banners
        ]);
    }

    public function saveOrder(Request $request)
    {

        if (!$request->newSort) {
            return response()->json([
                'status' => false,
                'message' => "Something Wrong.",
            ]);
        }
        $news = explode(',', $request->newSort);
        try {
            for ($i = 0; $i < sizeof($news); $i++) {
                $banner = DBFeature::where('id', $news[$i])->first();
                if ($banner) {
                    $banner->order = $i + 1;
                    $banner->updated_at = Carbon::now();
                    $banner->updated_by = Auth::user()->id;
                    $banner->save();
                }
            }
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        $this->clearCache();

        return response()->json([
            'status' => true,
            'message' => "Sorted",
        ]);
    }


    public function getDatatable()
    {
        $banners = DBFeature::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            if ($_model->file_format == "mp4" || $_model->file_format == "webm" || $_model->video_path) {
                return '<a href="' . route('admin.aceforce.feature.edit', ['id' => $_model->id]) . '"><img src="https://static2.garena.in.th/data/rov/content/content/66b2d780fe8be1b8a5098103caf03d97.png" width="150px">
</a>';
            }
            return '<a href="' . route('admin.aceforce.feature.edit', ['id' => $_model->id]) . '"><img src="' . $_model->banner_image . '" class="img-responsive" width="150px"></a>';
        });
        $datatables->editColumn('title', function ($_model) {
            return '<small>' . $_model->head_title . '</small><h6><b>' . $_model->title . '</b></h6>';
        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.aceforce.feature.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image', 'title', 'status', 'order', 'action'])->make(true);
    }


    public function saveBanner(Request $request)
    {

//        dd($request->all());
//        $validator = Validator::make($request->all(), [
//            'txt_title'                 => 'required',
//            'txt_link'                 => 'required',
//            'txt_banner_image'          => 'mimes:jpeg,jpg,gif,png|dimensions:width=1920,height=1142',

//        ],[
//            'txt_banner_image.dimensions'           => 'ขนาดรูปต้องเป็น 1920 × 1142 เท่านั้น',
//            'txt_link.required'           => 'กรุณากรอก URL',

//        ]);
//
//        if ($validator->fails()) {
//            return response()->json([
//                'status'    => false,
//                'message'   => $validator->messages()->first()
//            ]);
//        }

        if ($request->hasFile('txt_banner_image')) {
            try {
                $banner_image = $this->uploadImage(
                    $request->file('txt_banner_image'),
                    $this->uploadPath . 'banner',
                    'upload',
                    null
                );
            } catch (\Exception $x) {
                return response()->json([
                    'status' => false,
                    'message' => $x->getMessage(),
                ]);
            }
        } else {
            if (isset($request->txt_id)) {
                $banner_image = DBFeature::where('id', $request->txt_id)->value('banner_image');
            } else {
                $banner_image = "-";
            }
        }

        try {
            $saved = DBFeature::saveFeature($request, $banner_image);

        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
        if ($saved) {
            $this->clearCache();

            return response()->json([
                'status' => true,
                'message' => 'Saved',
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Not Save',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $updated = DBFeature::updateStatus($id, $status);

        $this->clearCache();

        if ($updated) {
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Status Not Updated'
        ]);
    }


    public function saveBgFeature(Request $request)
    {
//        dd($request->all());
        //        $validator = Validator::make($request->all(), [
//            'txt_banner_image'          => 'mimes:jpeg,jpg,gif,png|dimensions:width=1920,height=1142',
//            'txt_image_header'          => 'mimes:jpeg,jpg,gif,png|dimensions:width=1920,height=1142',

//        ],[
//            'txt_banner_image.dimensions'           => 'ขนาดรูปต้องเป็น 1920 × 1142 เท่านั้น',
//            'txt_image_header.dimensions'           => 'ขนาดรูปต้องเป็น 1920 × 1142 เท่านั้น',

//        ]);
//
//        if ($validator->fails()) {
//            return response()->json([
//                'status'    => false,
//                'message'   => $validator->messages()->first()
//            ]);
//        }

        if ($request->hasFile('txt_banner_image')) {
            try {
                $bg_image = $this->uploadImage(
                    $request->file('txt_banner_image'),
                    $this->uploadPath . 'banner',
                    'upload',
                    null
                );
            } catch (\Exception $x) {
                return response()->json([
                    'status' => false,
                    'message' => $x->getMessage(),
                ]);
            }
        } else {
            $bg_image = DBFeatureBg::where('key', 'features')->value('bg_image');
        }

        if ($request->hasFile('txt_image_header')) {
            try {
                $image_header = $this->uploadImage(
                    $request->file('txt_image_header'),
                    $this->uploadPath . 'banner',
                    'upload',
                    null
                );
            } catch (\Exception $x) {
                return response()->json([
                    'status' => false,
                    'message' => $x->getMessage(),
                ]);
            }
        } else {
            $image_header = DBFeatureBg::where('key', 'features')->value('image_header');
        }

        $key = 'features';
        try {
            $saved = DBFeatureBg::saveBg($request, $bg_image,$image_header,$key);

        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
        if ($saved) {
            $this->clearCache();

            return response()->json([
                'status' => true,
                'message' => 'Saved',
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Not Save',
        ]);
    }


    public function deleteBanner(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = DBFeature::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found banner.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        $this->clearCache();

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }

    public function clearCache()
    {
        Cache::setPrefix("");
        Cache::forget(config("cache.af_prefix").":getHomeInfo");
    }


}
