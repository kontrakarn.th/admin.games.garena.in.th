<?php

namespace App\Http\Controllers\Admin\aceforce;

use App\Http\Controllers\ApiController;
use App\Models\aceforce\DBHighlight;
use App\Models\aceforce\DBMenubar;
use Carbon\Carbon;
use Cache;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class MenuController extends ApiController
{

    private $pagepath = 'admin.aceforce.menubar.';
    private $uploadPath = 'aceforce/menubar/';
    private $default_share = 'https://static2.garena.in.th/data/aceforce/menu/menu/4d9f4761275c83ab15df75eca81d1765.png';


    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {

        return view('admin.aceforce.menubar.index');
    }


    public function dataTable()
    {
        $banners = DBMenubar::where('parent_id', '-')->get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('name', function ($_model) {
            return '<a href="' . route('admin.aceforce.menubar.submenu', ['id' => $_model->id]) . '">' . $_model->name . '</a>';
        });
        $datatables->editColumn('url', function ($_model) {
            return $_model->url ;
        });
        $datatables->editColumn('order', function ($_model) {
            return $_model->order;
        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.aceforce.menubar.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['name', 'url', 'order', 'status', 'action'])->make(true);
    }


    public function ordering()
    {
        $parents = DBMenubar::where('status', 'active')
            ->where('parent_id', '-')
            ->orderBy('order', 'asc')
            ->get();

        return view('admin.aceforce.menubar.orderparent', [
            'parents' => $parents
        ]);

    }

    public function saveOrder(Request $request)
    {
        if (!$request->newSort) {
            return response()->json([
                'status' => false,
                'message' => "Something Wrong.",
            ]);
        }
        $news = explode(',', $request->newSort);


        try {
            for ($i = 0; $i < sizeof($news); $i++) {

                $event = DBMenubar::where('id', $news[$i])->first();
                if ($event) {
                    $event->order = $i + 1;
                    $event->updated_at = Carbon::now();
                    $event->updated_by = Auth::user()->id;
                    $event->save();
                }


            }

            $this->clearCache();

        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'จัดเรียงเรียบร้อย',
        ]);

    }


    public function deleteEvent(Request $request)
    {

        try {
            $parent = DBMenubar::where('id', $request->id)->delete();
            $sub = DBMenubar::where('parent_id', $request->id)->delete();
            $this->clearCache();
            return response()->json([
                'status' => true,
                'message' => 'ลบเรียบร้อยแล้ว'
            ]);

        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }


    }


    public function createParent()
    {
        return view('admin.aceforce.menubar.createparent');
    }

    public function save(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'txt_title' => 'required|max:20',
            'txt_title_en' => 'required|max:20',
//            'txt_slug' => 'required|max:100',
//            'txt_url' => 'required|max:100',
        ], [
            'txt_title.required' => 'กรุณากรอก ชื่อ',
            'txt_title.max' => 'ชื่อต้องไม่เกิน 20 ตัวอักษร',
            'txt_title_en.required' => 'กรุณากรอก ชื่อ (EN)',
            'txt_title_en.max' => 'ชื่อ (EN) ต้องไม่เกิน 20 ตัวอักษร',
//            'txt_url.required' => 'กรุณากรอก url',
//            'txt_url.max' => 'url ต้องไม่เกิน 100 ตัวอักษร',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()->first()
            ]);
        }


        if ($request->txt_id) {
            $parent_menu = DBMenubar::where('id', $request->txt_id)->first();
        } else {
            $parent_menu = new DBMenubar();
            $find_last_order = DBMenubar::where('parent_id', '-')->orderBy('order', 'desc')->first();

            if ($find_last_order) {
                $parent_menu->order = $find_last_order->order + 1;
            } else {
                $parent_menu->order = 1;
            }
        }

        if ($request->txt_status) {
            $request->txt_status = 'active';
        } else {
            $request->txt_status = 'inactive';
        }


        $parent_menu->parent_id = '-';
        $parent_menu->name = $request->txt_title;
        $parent_menu->name_en = $request->txt_title_en;
        $parent_menu->url = $request->txt_url;
        $parent_menu->status = $request->txt_status;

        try {
            $parent_menu->save();
            $this->clearCache();

            return response()->json([
                'status' => true,
                'message' => 'บันทึกเรียบร้อยแล้ว'
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }
    }

    public function updateStatus(Request $request)
    {
        $parent = DBMenubar::where('id', $request->id)->first();

        if ($request->status == 'false') {
            $parent->status = 'inactive';
        } else {
            $parent->status = 'active';
        }


        try {
            $parent->save();
            $this->clearCache();
            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียบร้อยแล้ว'
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function edit($id)
    {
        $data = DBMenubar::where('id', $id)->first();

        return view('admin.aceforce.menubar.createparent', ['data' => $data]);
    }

    public function submenu($id)
    {
        $name_parent = DBMenubar::where('id', $id)->first();
        return view('admin.aceforce.menubar.submenubar.submenu', [
            'parent' => $id,
            'name_parent' => $name_parent
        ]);
    }

    public function submenuDataTable($parent)
    {
        $banners = DBMenubar::where('parent_id', $parent)->get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('parent', function ($_model) {
            $find_parent = DBMenubar::where('id', $_model->parent_id)->first();
            return $find_parent->name;
        });
        $datatables->editColumn('name', function ($_model) {
            return '<a href="' . route('admin.aceforce.menubar.edit.submenu', ['id' => $_model->id]) . '">' . $_model->name . '</a>';
        });
        $datatables->editColumn('url', function ($_model) {
            return $_model->url ;
        });
        $datatables->editColumn('order', function ($_model) {
            return $_model->order;
        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.aceforce.menubar.edit.submenu', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['parent', 'name', 'url', 'order', 'status', 'action'])->make(true);
    }


    public function createSubmenu($parent)
    {
        $txt_parent = DBMenubar::where('id', $parent)->first();
        return view('admin.aceforce.menubar.submenubar.createsubmenu', [
            'parent' => $parent,
            'txt_parent' => $txt_parent
        ]);
    }


    public function saveSubmenu(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'txt_title' => 'required|max:20',
//            'txt_slug' => 'required|max:100',
            'txt_url' => 'required|max:100',
        ], [
            'txt_title.required' => 'กรุณากรอก ชื่อ',
            'txt_title.max' => 'ชื่อต้องไม่เกิน 20 ตัวอักษร',
            'txt_url.required' => 'กรุณากรอก url',
            'txt_url.max' => 'url ต้องไม่เกิน 100 ตัวอักษร',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()->first()
            ]);
        }


        if ($request->txt_id) {
            $parent_menu = DBMenubar::where('id', $request->txt_id)->first();
        } else {
            $parent_menu = new DBMenubar();
        }

        if ($request->txt_status) {
            $request->txt_status = 'active';
        } else {
            $request->txt_status = 'inactive';
        }

        $parent_menu->parent_id = '-';
        $parent_menu->parent_id = $request->txt_parent_id;
        $parent_menu->name = $request->txt_title;
        $parent_menu->url = $request->txt_url;
        $parent_menu->status = $request->txt_status;

        try {

            $parent_menu->save();
            $this->clearCache();

            return response()->json([
                'status' => true,
                'message' => 'บันทึกเรียบร้อยแล้ว'
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }
    }


    public function updateStatusSubmenu(Request $request)
    {
        $sub_menu = DBMenubar::where('id', $request->id)->first();

        if ($request->status == 'false') {
            $sub_menu->status = 'inactive';
        } else {
            $sub_menu->status = 'active';
        }


        try {
            $sub_menu->save();
            $this->clearCache();

            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียบร้อยแล้ว'
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
    }


    public function editSubmenu($id)
    {
        $data = DBMenubar::where('id', $id)->first();
        $txt_parent = DBMenubar::where('id', $data->parent_id)->first();

        return view('admin.aceforce.menubar.submenubar.createsubmenu', [
            'data' => $data,
            'txt_parent' => $txt_parent
        ]);
    }

    public function subDeleteEvent(Request $request)
    {

        try {
            $sub_parent = DBMenubar::where('id', $request->id)->delete();
            $this->clearCache();
            return response()->json([
                'status' => true,
                'message' => 'ลบเรียบร้อยแล้ว'
            ]);

        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }
    }


    public function clearCache()
    {
        Cache::setPrefix("");
        Cache::forget(config("cache.af_prefix").":getHomeInfo");
    }
}
