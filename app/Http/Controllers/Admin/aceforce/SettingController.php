<?php

namespace App\Http\Controllers\Admin\aceforce;

use App\Http\Controllers\ApiController;
use App\Models\aceforce\DBSetting;
use Carbon\Carbon;
use Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class SettingController extends ApiController
{

    private $uploadPath = 'aceforce/mainsite/';

    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $setting = new DBSetting();
        $header_logo = $setting::where('option_name','header_logo')->first()->option_value;
        $ios_download_link = $setting::where('option_name','ios_download_link')->first()->option_value;
        $android_download_link = $setting::where('option_name','android_download_link')->first()->option_value;
        $qr_code = $setting::where('option_name','qr_code')->first()->option_value;
        $facebook_link = $setting::where('option_name','facebook_link')->first()->option_value;
        $instagram_link = $setting::where('option_name','instagram_link')->first()->option_value;
        $twitter_link = $setting::where('option_name','twitter_link')->first()->option_value;
        $youtube_link = $setting::where('option_name','youtube_link')->first()->option_value;
        $data = [
            'header_logo'           => $header_logo,
            'ios_download_link'     => $ios_download_link,
            'android_download_link' => $android_download_link,
            'qr_code'               => $qr_code,
            'facebook_link'         => $facebook_link,
            'instagram_link'        => $instagram_link,
            'twitter_link'          => $twitter_link,
            'youtube_link'          => $youtube_link,
        ];


        return view('admin.aceforce.setting.index',$data);
    }

    public function saveHeader(Request $request)
    {

        $mesg = null;

        try{

            if($request->hasFile('header_logo')) {
                $header_logo = $this->uploadImage(
                    $request->file('header_logo'),
                    $this->uploadPath.'header',
                    'upload',
                    null
                );
                $setting = DBSetting::where('option_name','header_logo')->first();
                $setting->option_value = $header_logo;
                $setting->updated_at = Carbon::now();
                $setting->updated_by = Auth::user()->id;
                $setting->save();
            }
            $this->clearCache();

            $status = true;
            $mesg = "Updated Header Settings";
        }catch (\Exception $x){
            $status = false;
            $mesg = $x->getMessage();
        }


        return response()->json([
            'status'=>$status,
            'message'=>$mesg,
        ]);
    }

    public function saveDownload(Request $request)
    {

        $status = false;
        $mesg = null;

        $validator = Validator::make($request->all(), [
            'ios_download_link' => 'required',
            'android_download_link' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first() ?? "Validate",
            ]);
        }

        $ios_download_link = DBSetting::where('option_name','ios_download_link')->first();
        $ios_download_link->option_value = $request->ios_download_link;
        $ios_download_link->updated_at = Carbon::now();
        $ios_download_link->updated_by = Auth::user()->id;
        $android_download_link = DBSetting::where('option_name','android_download_link')->first();
        $android_download_link->option_value = $request->android_download_link;
        $android_download_link->updated_at = Carbon::now();
        $android_download_link->updated_by = Auth::user()->id;

        if($request->hasFile('qr_code')) {
            $qr_code = $this->uploadImage(
                $request->file('qr_code'),
                $this->uploadPath.'qr_code',
                'upload',
                null
            );
            $setting = DBSetting::where('option_name','qr_code')->first();
            $setting->option_value = $qr_code;
            $setting->updated_at = Carbon::now();
            $setting->updated_by = Auth::user()->id;
            $setting->save();
        }

        try{
            $ios_download_link->save();
            $android_download_link->save();
            $status = true;
            $this->clearCache();


            $mesg = "Updated Download Settings";
        }catch (\Exception $x){
            $status = false;
            $mesg = $x->getMessage();
        }

        return response()->json([
            'status'=>$status,
            'message'=>$mesg,
        ]);
    }

    public function saveFooter(Request $request)
    {

        $status = false;
        $mesg = null;

        $validator = Validator::make($request->all(), [
            'facebook_link' => 'required',
            'instagram_link' => 'required',
            'twitter_link' => 'required',
            'youtube_link' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first() ?? "Validate",
            ]);
        }

        $facebook_link = DBSetting::where('option_name','facebook_link')->first();
        $facebook_link->option_value = $request->facebook_link;
        $facebook_link->updated_at = Carbon::now();
        $facebook_link->updated_by = Auth::user()->id;

        $instagram_link = DBSetting::where('option_name','instagram_link')->first();
        $instagram_link->option_value = $request->instagram_link;
        $instagram_link->updated_at = Carbon::now();
        $instagram_link->updated_by = Auth::user()->id;

        $twitter_link = DBSetting::where('option_name','twitter_link')->first();
        $twitter_link->option_value = $request->twitter_link;
        $twitter_link->updated_at = Carbon::now();
        $twitter_link->updated_by = Auth::user()->id;

        $youtube_link = DBSetting::where('option_name','youtube_link')->first();
        $youtube_link->option_value = $request->youtube_link;
        $youtube_link->updated_at = Carbon::now();
        $youtube_link->updated_by = Auth::user()->id;

        try{
            $facebook_link->save();
            $instagram_link->save();
            $twitter_link->save();
            $youtube_link->save();

            $this->clearCache();


            $status = true;
            $mesg = "Updated Social Settings";
        }catch (\Exception $x){
            $status = false;
            $mesg = $x->getMessage();
        }

        return response()->json([
            'status'=>$status,
            'message'=>$mesg,
        ]);
    }

    public function clearCache()
    {
        Cache::setPrefix("");
        Cache::forget(config("cache.af_prefix").":getHomeInfo");
    }
}
