<?php

namespace App\Http\Controllers\Admin\fo4;

use App\Http\Controllers\ApiController;
use App\Models\fo4\DBIngame;
use App\Models\fo4\DBIngameHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use function GuzzleHttp\Promise\all;

use Validator;
use Cache;


class MenuIngameController extends ApiController
{

    private $pagepath='admin.fo4.in_game.';
    private $pageheader='In Game';
    private $uploadPath = 'fo4/mainsite/';


    function __construct()
    {

    }

    public function getIndex()
    {

        return view('admin.fo4.in-game.index');
    }


    public function create()
    {

        return view('admin.fo4.in-game.create');
    }


    public function save(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'txt_title'                 => 'required',
            'txt_link'                 => 'required',
            'txt_banner_image'          => 'mimes:jpeg,jpg,gif,png|dimensions:width=240,height=76',
        ],[
            'txt_title.required' => 'กรุณากรอก หัวข้อ',
            'txt_link.required' => 'กรุณากรอก Link',
            'txt_banner_image.dimensions'           => 'ขนาดรูปต้องเป็น 240 × 76 เท่านั้น',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first()
            ]);
        }


        if($request->hasFile('txt_banner_image')) {
            $banner_image = $this->uploadImage(
                $request->file('txt_banner_image'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image = DBIngame::where('id',$request->txt_id)->value('banner_image');

            }else{
                $banner_image = null;
            }
        }

        $saved = DBIngame::saveEventManagerBook($request, $banner_image);


        if ($saved) {
            $this->clearCache();
            return response()->json([
                'status'=>true,
                'message'=>'บันทึกเรียบร้อย',
            ]);
        }

        return response()->json([
            'status'=>false,
            'message'=>'เกิดข้อปิดพลาด โปรดลองใหม่อีกครั้ง',
        ]);

    }


    public function dataTable(Request $request)
    {

        $banners = DBIngame::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            return '<a href="' . route('admin.fo4.in_game.edit', ['id' => $_model->id]) . '"><img src="' . $_model->banner_image . '" class="img-responsive" width="150px"></a>';
        });
        $datatables->editColumn('url', function ($_model) {
            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';
        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.fo4.in_game.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image','url', 'title', 'status', 'order', 'action'])->make(true);
    }


    public function updateStatus(Request $request)
    {


        $event = DBIngame::where('id',$request->id)->first();

        if ($request->status == 'false') {
            $event->status = 'inactive';
            $event->order = null;
        }
        else {
            $event->status = 'active';

            $find_last_order = DBIngame::orderBy('order','desc')->first();
            $event->order = ($find_last_order->order)+1 ;
        }


        try {
            $event->save();
            $this->clearCache();

            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียนร้อยแล้ว'
            ]);
        } catch (\Exception $x ) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง'
            ]);
        }
    }


    public function edit($id)
    {
        $data = DBIngame::where('id',$id)->first();

        return view('admin.fo4.in-game.create',['data' => $data]);
    }

    public function ordering()
    {
        $banners = DBIngame::where('status', 'active')->orderBy('order', 'asc')->get();
        return view('admin.fo4.in-game.order', [
            'banners' => $banners
        ]);
    }


    public function saveOrder(Request $request)
    {

        if (!$request->newSort) {
            return response()->json([
                'status' => false,
                'message' => "Something Wrong.",
            ]);
        }
        $news = explode(',', $request->newSort);


        try {
            for ($i=0 ; $i<sizeof($news) ; $i++) {

                $event = DBIngame::where('id',$news[$i])->first();
                if ($event) {
                    $event->order = $i + 1;
                    $event->updated_at = Carbon::now();
                    $event->updated_by = Auth::user()->id;
                    $event->save();
                }


            }


        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด',
            ]);
        }

        $this->clearCache();
        return response()->json([
            'status' => true,
            'message' => 'จัดเรียงเรียบร้อย',
        ]);



    }


    public function deleteEvent(Request $request)
    {

        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = DBIngame::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found event.",
            ]);
        }

        try{
            $check->delete();
            $this->clearCache();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }


        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }


    public function header()
    {
        return view('admin.fo4.in-game.header.index');
    }


    public function headerCreate()
    {
        return view('admin.fo4.in-game.header.create');
    }

    public function headerDataTable(Request $request)
    {
        $banners = DBIngameHeader::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            return '<a href="' . route('admin.fo4.in_game.header.edit', ['id' => $_model->id]) . '"><img src="' . $_model->banner_image . '" class="img-responsive" width="150px"></a>';
        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.fo4.in_game.header.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image', 'title', 'status', 'action'])->make(true);
    }


    public function headerSave(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_title'                 => 'required',
            'txt_banner_image'          => 'mimes:jpeg,jpg,gif,png|max:1024',
        ],[
            'txt_title.required' => 'กรุณากรอก หัวข้อ',
//            'txt_banner_image.dimensions'           => 'ขนาดรูปต้องเป็น 240 × 76 เท่านั้น',
            'txt_banner_image.max'           => 'ขนาดรูปต้องไม่เกิน 1 MB',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first()
            ]);
        }


        if($request->hasFile('txt_banner_image')) {
            $banner_image = $this->uploadImage(
                $request->file('txt_banner_image'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image = DBIngameHeader::where('id',$request->txt_id)->value('banner_image');

            }else{
                $banner_image = null;
            }
        }

        $saved = DBIngameHeader::saveEventManagerBook($request, $banner_image);


        if ($saved) {
            return response()->json([
                'status'=>true,
                'message'=>'บันทึกเรียบร้อย',
            ]);
        }

        return response()->json([
            'status'=>false,
            'message'=>'เกิดข้อปิดพลาด โปรดลองใหม่อีกครั้ง',
        ]);

    }


    public function headerEdit($id)
    {
        $data = DBIngameHeader::where('id',$id)->first();

        return view('admin.fo4.in-game.header.create',['data' => $data]);
    }



    public function headerUpdateStatus(Request $request)
    {


        $event = DBIngameHeader::where('id',$request->id)->first();

        if ($request->status == 'false') {
            $event->status = 'inactive';
        }
        else {
            $event->status = 'active';
        }


        try {
            $event->save();

            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียนร้อยแล้ว'
            ]);
        } catch (\Exception $x ) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง'
            ]);
        }
    }


    public function headerdDleteEvent(Request $request)
    {

        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = DBIngameHeader::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found event.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }


        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }

    public function clearCache()
    {
        Cache::tags('in_game')->flush();
    }
}
