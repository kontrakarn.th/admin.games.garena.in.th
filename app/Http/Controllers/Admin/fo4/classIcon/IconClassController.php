<?php

namespace App\Http\Controllers\Admin\fo4\classIcon;

use App\Http\Controllers\ApiController;
use App\Models\fo4\PlayerClass\ManagePlayerClass;
use App\Models\fo4\PlayerClass\PlayerClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Validator;
use Yajra\DataTables\DataTables;
use Response;

class IconClassController extends ApiController
{
    private $pagepath='admin.fo4.class.';
    private $pageheader='Class';
    private $uploadPath = 'fo4/mainsite/';

    function __construct()
    {

    }


    public function index()
    {
        return view('admin.fo4.class.index');
    }

    public function create()
    {

        return view('admin.fo4.class.create');
    }


    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_title'                 => 'required|max:100',
            'txt_slug'                 => 'required|max:100',
            'txt_banner_image'                 => 'mimes:jpeg,jpg,png|max:1020',
        ],[
            'txt_title.required' => 'กรุณากรอก ชื่อ class นักเตะ',
            'txt_title.max' => 'ชื่อ class นักเตะต้องไม่เกิน 100 ตัวอักษร',
            'txt_slug.required' => 'กรุณากรอก Slug',
            'txt_slug.max' => 'Slug ต้องไม่เกิน 100 ตัวอักษร',
            'txt_banner_image.mimes' => 'ต้องเป็นไฟล์ .jpg .jpeg .png เท่านั้น',
            'txt_banner_image.max' => 'ไฟล์ภาพต้องไม่เกิน 1 mb',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first()
            ]);
        }


        // $banner_image_large
        if($request->hasFile('txt_banner_image')) {
            $banner_image = $this->uploadImage(
                $request->file('txt_banner_image'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image = PlayerClass::where('id',$request->txt_id)->value('banner_image');

            }else{
                $banner_image = null;
            }
        }


        $saved = PlayerClass::savePlayerClass($request, $banner_image);


        if ($saved) {
            return response()->json([
                'status'=>true,
                'message'=>'บันทึกเรียบร้อย',
            ]);
        }

        return response()->json([
            'status'=>false,
            'message'=>'เกิดข้อปิดพลาด โปรดลองใหม่อีกครั้ง',
        ]);

    }


    public function dataTable()
    {
        $banners = PlayerClass::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            return '<a href="' . route('admin.fo4.class.manage', ['id' => $_model->id]) . '"><img src="' . $_model->banner_image . '" class="img-responsive" width="60px"></a>';
        });

        $datatables->editColumn('name', function ($_model) {
            return $_model->name;
        });


        $datatables->editColumn('slug', function ($_model) {
            return $_model->slug;
        });

        $datatables->editColumn('order', function ($_model) {
            return $_model->order;
        });


        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.fo4.class.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image','name', 'slug' , 'order' , 'status', 'action'])->make(true);
    }


    public function updateStatus(Request $request)
    {
        $playerClass = PlayerClass::where('id',$request->id)->first();

        if ($request->status == 'false') {
            $playerClass->status = 'inactive';
        }
        else{
            $playerClass->status = 'active';
        }


        try {
            $playerClass->save();
            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียบร้อยแล้ว'
            ]);
        }catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
    }


    public function edit($id)
    {
        $data = PlayerClass::where('id',$id)->first();

        return view('admin.fo4.class.create' ,['data' => $data]);
    }


    public function manage($id)
    {
        $data = ManagePlayerClass::where('player_class_id',$id)->first();
        return view('admin.fo4.class.manage',[
            'id' => $id ,
            'data' => $data,
        ]);
    }


    public function saveManage(Request $request)
    {


        $manage = ManagePlayerClass::where('player_class_id',$request->txt_id)->first();

        $validator = Validator::make(
            $request->all(),
            [
                'txt_background_class_player'          => 'max:2048',
                'txt_logo_class_player'          => 'max:2048',
                'txt_bg_list_class_player'          => 'mimes:jpeg,jpg,png|max:2048',
                'txt_list_class_player'          => 'max:2048',
                'txt_bg_detail'          => 'max:2048',

            ],
            [
                'txt_background_class_player.max'           => 'ขนาดรูป Background Logo ต้องไม่เกิน 2 mb',
                'txt_logo_class_player.max'           => 'ขนาดรูป Logo ต้องไม่เกิน 2 mb',
                'txt_bg_list_class_player.max'           => 'ขนาดรูป Background List ต้องไม่เกิน 2 mb',
                'txt_list_class_player.max'           => 'ขนาดรูป List ต้องไม่เกิน 2 mb',
                'txt_bg_detail.max'           => 'ขนาดรูป Background Detail ต้องไม่เกิน 2 mb',
            ]
        );


        if($validator->fails()) {
            $key_array_first = array_keys($validator->messages()->toArray())[0];
            return Response::json([
                'status'    => FALSE,
                'message'   => $validator->messages()->toArray()[$key_array_first][0]
            ]);
        }


        // txt_background_class_player
        if($request->hasFile('txt_background_class_player')) {
            $banner_bg_class_player = $this->uploadImage(
                $request->file('txt_background_class_player'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($manage->bg_logo_class)) {
                $banner_bg_class_player = ManagePlayerClass::where('id',$request->txt_id)->value('bg_logo_class');

            }else{
                $banner_bg_class_player = null;
            }
        }


        // txt_logo_class_player
        if($request->hasFile('txt_logo_class_player')) {
            $banner_class_player = $this->uploadImage(
                $request->file('txt_logo_class_player'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($manage->logo_class)) {
                $banner_class_player = ManagePlayerClass::where('id',$request->txt_id)->value('logo_class');

            }else{
                $banner_class_player = null;
            }
        }


        // txt_bg_list_class_player
        if($request->hasFile('txt_bg_list_class_player')) {
            $banner_bg_list_class_player = $this->uploadImage(
                $request->file('txt_bg_list_class_player'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($manage->bg_list_class)) {
                $banner_bg_list_class_player = ManagePlayerClass::where('id',$request->txt_id)->value('bg_list_class');

            }else{
                $banner_bg_list_class_player = null;
            }
        }


        // txt_list_class_player
        if($request->hasFile('txt_list_class_player')) {
            $banner_list_class_player = $this->uploadImage(
                $request->file('txt_list_class_player'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($manage->list_class)) {
                $banner_list_class_player = ManagePlayerClass::where('id',$request->txt_id)->value('list_class');

            }else{
                $banner_list_class_player = null;
            }
        }


        // txt_bg_detail
        if($request->hasFile('txt_bg_detail')) {
            $banner_bg_detail = $this->uploadImage(
                $request->file('txt_bg_detail'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($manage->detail_class)) {
                $banner_bg_detail = ManagePlayerClass::where('id',$request->txt_id)->value('detail_class');

            }else{
                $banner_bg_detail = null;
            }
        }



        $saved = ManagePlayerClass::saveManageClassPlayer($request, $banner_bg_class_player , $banner_class_player , $banner_bg_list_class_player , $banner_list_class_player , $banner_bg_detail);


        if ($saved) {
            return response()->json([
                'status'=>true,
                'message'=>'บันทึกเรียบร้อย',
            ]);
        }

        return response()->json([
            'status'=>false,
            'message'=>'เกิดข้อปิดพลาด โปรดลองใหม่อีกครั้ง',
        ]);
    }


    public function ordering()
    {

        $player_class = PlayerClass::where('status', 'active')->orderBy('order', 'asc')->get();
        return view('admin.fo4.class.order', [
            'player_class' => $player_class
        ]);
    }


    public function saveOrder(Request $request)
    {

        if (!$request->newSort) {
            return response()->json([
                'status' => false,
                'message' => "Something Wrong.",
            ]);
        }
        $news = explode(',', $request->newSort);


        try {
            for ($i=0 ; $i<sizeof($news) ; $i++) {

                $event = PlayerClass::where('id',$news[$i])->first();
                if ($event) {
                    $event->order = $i + 1;
                    $event->updated_at = Carbon::now();
                    $event->updated_by = Auth::user()->id;
                    $event->save();
                }


            }


        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'จัดเรียงเรียบร้อย',
        ]);
    }


    public function deleteEvent(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = PlayerClass::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found event.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }


        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }
}
