<?php

namespace App\Http\Controllers\Admin\fo4;

use App\Http\Controllers\ApiController;
use App\Models\fo4\DBManagerBook;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator ;
use Response ;
use Session;
use Yajra\DataTables\DataTables;

class ManagerBookController extends ApiController
{
    private $pagepath='admin.fo4.manager-book.';
    private $pageheader='Manager Book';
    private $uploadPath = 'fo4/mainsite/';

    function __construct()
    {
        $this->middleware('guest');
        $this->init();
    }

    private function init()
    {
        $this->viewData['pageheader']=$this->pageheader;
        $this->viewData['breadcrumbs'] = [
            ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.fo4.manager-book.index'),'active'=>''],
            ['text' => 'User','url'=>'','active'=>''],
            ['text' => 'Report','url'=>'','active'=>'active']
        ];
    }


    public function getIndex()
    {
//        dd('getIndex');

        return view('admin.fo4.manager-book.menu.index');
    }


    public function createMenu()
    {
        return view('admin.fo4.manager-book.menu.create');
    }


    public function saveMenu(Request $request)
    {


        $validator = Validator::make(
            $request->all(),
            [
                'txt_menu'     => 'required|max:100',
                'txt_url'       => 'required|max:1000',
                'txt_show_datetime' => 'required',

            ],
            [
                'txt_menu.required'    => 'กรุณากรอก ชื่อเมนู',
                'txt_menu.max'          => 'ชื่อเมนูต้องไม่เกิน 100 ตัวอักษร',
                'txt_url.required'      => 'กรุณากรอก URL',
                'txt_url.max'           => 'URL ไม่ควรยาวเกิน 1000 ตัวอักษร',
                'txt_show_datetime.required'           => 'กรุณาระบุวันเวลา'
            ]
        );


        if($validator->fails()) {
            $key_array_first = array_keys($validator->messages()->toArray())[0];
            return Response::json([
                'status'    => FALSE,
                'message'   => $validator->messages()->toArray()[$key_array_first][0]
            ]);
        }


        if ($request->txt_status) {
            $request->txt_status = 'active';
        }
        else{
            $request->txt_status = 'inactive';
        }

        $manager_book_menu = new DBManagerBook() ;
        $manager_book_menu->menu_name = $request->txt_menu;
        $manager_book_menu->url = $request->txt_url ;
        $manager_book_menu->show_datetime = $request->txt_show_datetime ;
        $manager_book_menu->status =  $request->txt_status ;
        $manager_book_menu->updated_at = Carbon::now();
        $manager_book_menu->updated_by = Session::get('userinfo')['data']['id'];

        try {
            $manager_book_menu->save();
            return response()->json([
               'status' => true,
               'message' => 'บันทึกเรียบร้อย'
            ]);
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด'
            ]);
        }

    }


    public function dataTable(Request $request)
    {
// $menu = DBMenu::where('parent_id','0')->get();
        $menu = DBManagerBook::get();
        // $menu = Auth::user();
        // dd($menu);

        $datatables = Datatables::of($menu);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('name', function($_model) {
            return '<a href="' . route('admin.fo4.manager-book.edit', ['id' => $_model->id]) . '" >' . $_model->menu_name . '</a>';
        });
        $datatables->editColumn('url', function($_model) {
            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';
        });

        $datatables->editColumn('show', function($_model){

            if($_model->status == 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Show" data-off-text="No Show" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Show" data-off-text="No Show" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.fo4.manager-book.edit', ['id' => $_model->id]) . '" ><i class="icon-pencil7"></i> Edit</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name','url','show','action'])->make(true);
    }


    public function getEdit($id)
    {

        $menu_manager = DBManagerBook::where('id',$id)->first();

        return view('admin.fo4.manager-book.menu.edit',['menu_manager' => $menu_manager]);


    }


    public function saveEdit(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'txt_menu'     => 'required|max:100',
                'txt_url'       => 'required|max:1000',
                'txt_show_datetime' => 'required',

            ],
            [
                'txt_menu.required'    => 'กรุณากรอก ชื่อเมนู',
                'txt_menu.max'          => 'ชื่อเมนูต้องไม่เกิน 100 ตัวอักษร',
                'txt_url.required'      => 'กรุณากรอก URL',
                'txt_url.max'           => 'URL ไม่ควรยาวเกิน 1000 ตัวอักษร',
                'txt_show_datetime.required'           => 'กรุณาระบุวันเวลา'
            ]
        );


        if($validator->fails()) {
            $key_array_first = array_keys($validator->messages()->toArray())[0];
            return Response::json([
                'status'    => FALSE,
                'message'   => $validator->messages()->toArray()[$key_array_first][0]
            ]);
        }


        if ($request->txt_status) {
            $request->txt_status = 'active';
        }
        else{
            $request->txt_status = 'inactive';
        }

        $manager_book_menu = DBManagerBook::where('id',$request->txt_id)->first() ;
        $manager_book_menu->menu_name = $request->txt_menu;
        $manager_book_menu->url = $request->txt_url ;
        $manager_book_menu->show_datetime = $request->txt_show_datetime ;
        $manager_book_menu->status =  $request->txt_status ;
        $manager_book_menu->updated_at = Carbon::now();
        $manager_book_menu->updated_by = Session::get('userinfo')['data']['id'];

        try {
            $manager_book_menu->save();
            return response()->json([
                'status' => true,
                'message' => 'แก้ไขเรียบร้อย'
            ]);
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด'
            ]);
        }
    }


    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        if ($status == 'true') {
            $status = 'active';
        }
        else {
            $status = 'inactive';
        }

        $updated = DBManagerBook::where('id',$id)->first();
        $updated->status = $status;

        try {
            $updated->save();
            return response()->json([
                'status' => true,
                'message' => 'อัพเดทสถานะเรียบร้อย'
            ]);
        }catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง'
            ]);
        }

    }
}

