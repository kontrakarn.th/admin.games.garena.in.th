<?php

namespace App\Http\Controllers\Admin\fo4;

use App\Http\Controllers\ApiController;
use App\Models\fo4\DBEventManagerBook;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator ;
use Yajra\DataTables\DataTables;

class ManagerBookEventController extends ApiController
{
    private $pagepath='admin.fo4.manager-book.';
    private $pageheader='Manager Book';
    private $uploadPath = 'fo4/mainsite/';

    function __construct()
    {
        $this->middleware('guest');
        $this->init();
    }

    private function init()
    {
        $this->viewData['pageheader']=$this->pageheader;
        $this->viewData['breadcrumbs'] = [
            ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.fo4.manager-book.index'),'active'=>''],
            ['text' => 'User','url'=>'','active'=>''],
            ['text' => 'Report','url'=>'','active'=>'active']
        ];
    }


    public function getIndex()
    {

        return view('admin.fo4.manager-book.event.index');
    }


    public function createEvent()
    {

        $data = [] ;
        return view('admin.fo4.manager-book.event.create',['data' => $data]);

    }


    public function saveEvent(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'txt_title'                 => 'required',
            'txt_link'                 => 'required',
            'txt_banner_image'          => 'mimes:jpeg,jpg,gif,png|dimensions:width=750,height=420',
        ],[
            'txt_title.required' => 'กรุณากรอก หัวข้อ',
            'txt_link.required' => 'กรุณากรอก Link',
            'txt_banner_image.dimensions'           => 'ขนาดรูปต้องเป็น 750 × 420 เท่านั้น',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first()
            ]);
        }


        if($request->hasFile('txt_banner_image')) {
            $banner_image = $this->uploadImage(
                $request->file('txt_banner_image'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image = DBEventManagerBook::where('id',$request->txt_id)->value('banner_image');

            }else{
                $banner_image = null;
            }
        }

        $saved = DBEventManagerBook::saveEventManagerBook($request, $banner_image);


        if ($saved) {
            return response()->json([
                'status'=>true,
                'message'=>'บันทึกเรียบร้อย',
            ]);
        }

        return response()->json([
            'status'=>false,
            'message'=>'เกิดข้อปิดพลาด โปรดลองใหม่อีกครั้ง',
        ]);

    }


    public function dataTable(Request $request)
    {

        $banners = DBEventManagerBook::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            return '<a href="' . route('admin.fo4.manager-book.event.edit', ['id' => $_model->id]) . '"><img src="' . $_model->banner_image . '" class="img-responsive" width="150px"></a>';
        });
        $datatables->editColumn('url', function ($_model) {
            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';
        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.fo4.manager-book.event.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image','url', 'title', 'status', 'order', 'action'])->make(true);
    }


    public function editEvent($id)
    {


        $data = DBEventManagerBook::where('id',$id)->first();

        return view('admin.fo4.manager-book.event.create',['data' => $data]);
    }


    public function updateStatus(Request $request)
    {


        $event = DBEventManagerBook::where('id',$request->id)->first();

        if ($request->status == 'false') {
            $event->status = 'inactive';
            $event->order = null;
        }
        else {
            $event->status = 'active';

            $find_last_order = DBEventManagerBook::orderBy('order','desc')->first();
            $event->order = ($find_last_order->order)+1 ;
        }


        try {
            $event->save();

            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียนร้อยแล้ว'
            ]);
        } catch (\Exception $x ) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด โปรดลองใหม่อีกครั้ง'
            ]);
        }
    }


    public function ordering()
    {

        $banners = DBEventManagerBook::where('status', 'active')->orderBy('order', 'asc')->get();
        return view('admin.fo4.manager-book.event.ordering', [
            'banners' => $banners
        ]);
    }


    public function saveOrder(Request $request)
    {

        if (!$request->newSort) {
            return response()->json([
                'status' => false,
                'message' => "Something Wrong.",
            ]);
        }
        $news = explode(',', $request->newSort);


        try {
            for ($i=0 ; $i<sizeof($news) ; $i++) {

                $event = DBEventManagerBook::where('id',$news[$i])->first();
                if ($event) {
                    $event->order = $i + 1;
                    $event->updated_at = Carbon::now();
                    $event->updated_by = Auth::user()->id;
                    $event->save();
                }


            }

        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'จัดเรียงเรียบร้อย',
        ]);



    }


    public function deleteEvent(Request $request)
    {

        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = DBEventManagerBook::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found event.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }


        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }
}
