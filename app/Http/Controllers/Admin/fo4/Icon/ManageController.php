<?php

namespace App\Http\Controllers\Admin\fo4\Icon;

use App\Http\Controllers\ApiController;
use App\Models\fo4\Icon\Profile;
use App\Models\fo4\Icon\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ManageController extends ApiController
{
    private $pagepath='admin.fo4.icon.';
    private $pageheader='Icon';
    private $uploadPath = 'fo4/mainsite/';

    function __construct()
    {

    }

    public function index()
    {
        $rooms = Room::where('status','active')->get();
        $icons = Profile::where('status','active')->get();

        return view('admin.fo4.icon.index',[
            'icons' => $icons ,
            'rooms' => $rooms
        ]);
    }


    public function manage($id)
    {


        $room = Room::where('id',$id)->first();
        $icons = Profile::where('status','active')->where('room_id',$room->id)->get();

        $icon_rooms = Profile::where('show','show')->orderBy('order','asc')->pluck('id')->toArray();

        $all_icons = Profile::where('room_id',$room->id)->where('status','active')->orWhere('room_id',null)->get();
        $all_icons_in_room = Profile::where('room_id',$room->id)->pluck('id')->toArray();

        return view('admin.fo4.icon.manage' ,[
            'room' => $room,
            'icons' => $icons,
            'icon_rooms' => $icon_rooms,
            'all_icons' => $all_icons,
            'all_icons_in_room' => $all_icons_in_room
        ]);
    }


    public function setting(Request $request)
    {

        $all_icon_in_room = Profile::where('room_id',$request->room)->pluck('id')->toArray();
//        dd($all_icon_in_room);
//        dd($request->icon_show);

        if ($request->icon_show){
            $show   = array_values($request->icon_show);
        }else{
            $show    = [];
        }


        $profiles = Profile::where('room_id',$request->room)->orWhere('room_id',null)->get();

        foreach ($profiles as $profile){

            $get_last_order = Profile::orderBy('order','desc')->first();
            if (in_array((string)$profile->id,$show,FALSE)){
                $profile->show = 'show';
                if ($get_last_order->order == null) {
                    $profile->order = 1;
                }
                else {
                    $profile->order = $get_last_order->order + 1;
                }
            }else{
                $profile->show = null;
                $profile->order = null;
            }


            try {
                $profile->save();
            }catch (\Exception $x) {
                return response()->json([
                    'status' => false,
                    'message' => $x->getMessage()
                ]);
            }
        }

        return response()->json([
            'status' => true,
            'message' => 'บันทึกสำเร็จ'
        ]);
    }


    public function datatable(Request $request , $room)
    {
        $icon      = Profile::where('room_id',$room)->get();
        $datatables = Datatables::of($icon);

        $datatables->editColumn('name', function ($_model) {
            return $_model->first_name." ".$_model->last_name;
        });


        $datatables->editColumn('image', function ($_model) {
            return "<img src='$_model->profile_small_image' width='70px'>";
        });

        $datatables->editColumn('nation', function ($_model) {
            return "<img src='$_model->flag_image' width='40px'>";;
        });

        $datatables->editColumn('feet', function ($_model) {
            return $_model->foot_left." - ".$_model->foot_right;
        });

        $datatables->editColumn('fp', function ($_model) {
            return $_model->FP;
        });
        $datatables->editColumn('ovr', function ($_model) {
            return $_model->OVR;
        });

        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="javascript:void(0)" onclick="leaveIcon(' . $_model->id . ')"><i class="icon-trash"></i> Leave</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name', 'image','nation','feet', 'fp' ,'ovr' , 'action'])->make(true);

    }


    public function addIcon(Request $request)
    {

        if ($request->icon_show_in_room){
            $show   = array_values($request->icon_show_in_room);
        }else{
            $show    = [];
        }


        $profiles = Profile::where('room_id',$request->room)->orWhere('room_id',null)->get();
//        $profiles = Profile::all();


        foreach ($profiles as $profile){

            if (in_array((string)$profile->id,$show,FALSE)){
                $profile->room_id = $request->room;
            }else{
                $profile->room_id = null;
                $profile->show = null;
            }


            try {
                $profile->save();
            }catch (\Exception $x) {
                return response()->json([
                    'status' => false,
                    'message' => $x->getMessage()
                ]);
            }
        }

        return response()->json([
            'status' => true,
            'message' => 'บันทึกสำเร็จ'
        ]);
    }


    public function order($room)
    {


        $icon_shows = Profile::where('room_id',$room)
            ->where('status','active')
            ->where('show','show')
            ->orderBy('order','asc')
            ->get();

        return view('admin.fo4.icon.order',[
            'icon_shows' => $icon_shows,
        ]);
    }


    public function saveOrder(Request $request)
    {

        if (!$request->newSort) {
            return response()->json([
                'status' => false,
                'message' => "Something Wrong.",
            ]);
        }
        $news = explode(',', $request->newSort);


        try {
            for ($i=0 ; $i<sizeof($news) ; $i++) {

                $event = Profile::where('id',$news[$i])->first();
                if ($event) {
                    $event->order = $i + 1;
                    $event->updated_at = Carbon::now();
                    $event->updated_by = Auth::user()->id;
                    $event->save();
                }


            }


        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาด',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'จัดเรียงเรียบร้อย',
        ]);


    }


    public function leave(Request $request)
    {

        $profile = Profile::where('id',$request->id)->first();


        if ($profile) {
            $profile->room_id = null;
            $profile->show = null;
            $profile->order = null;

            try {
                $profile->save();
                return response()->json([
                    'status' => true,
                    'message' => 'ออกจากห้องเรียบร้อย'
                ]);
            }catch (\Exception $x) {
                return response()->json([
                    'status' => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }
        else {
            return response()->json([
                'status' => false,
                'message' => 'เกิดข้อผิดพลาดโปรดลองใหม่อีกครั้ง'
            ]);
        }
    }

}
