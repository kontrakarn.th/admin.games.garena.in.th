<?php

namespace App\Http\Controllers\Admin\fo4\Icon;

use App\Http\Controllers\ApiController;
use App\Models\fo4\Icon\Profile;
use App\Models\fo4\Icon\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator ;
use Yajra\DataTables\DataTables;

class RoomController extends ApiController
{

    private $pagepath='admin.fo4.icon.';
    private $pageheader='Icon';
    private $uploadPath = 'fo4/mainsite/';


    function __construct()
    {

    }


    public function index()
    {

        return view('admin.fo4.icon.room.index');
    }

    public function create()
    {

        return view('admin.fo4.icon.room.create');
    }


    public function dataTable(Request $request)
    {
        $banners = Room::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            return '<a href="' . route('admin.fo4.icon.room.edit', ['id' => $_model->id]) . '"><img src="' . $_model->banner_image . '" class="img-responsive" width="100px"></a>';
        });
        $datatables->editColumn('name', function ($_model) {
            return '<a href="' . route('admin.fo4.icon.room.edit', ['id' => $_model->id]) . '">'.$_model->name.'</a>';
        });
//        $datatables->editColumn('url', function ($_model) {
//            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';
//        });
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.fo4.icon.room.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['name','image','status', 'action'])->make(true);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_title'                 => 'required|max:100',
            'txt_banner_image'                 => 'max:1048',
        ],[
            'txt_title.required' => 'กรุณากรอก ชื่อ',
            'txt_title.max' => 'ชื่อต้องไม่เกิน 100 ตัวอักษร',
            'txt_banner_image.max' => 'รูปต้องมีขนาดไม่เกิน 2 mb'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first()
            ]);
        }

        // txt_banner_image
        if($request->hasFile('txt_banner_image')) {
            $banner_image = $this->uploadImage(
                $request->file('txt_banner_image'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image = Room::where('id',$request->txt_id)->value('banner_image');

            }else{
                $banner_image = null;
            }
        }


        if ($request->txt_id){
            $room = Room::where('id',$request->txt_id)->first();
        }
        else {
            $room = new Room() ;
        }

        if ($request->txt_status) {
            $request->txt_status = 'active';
        }
        else{
            $request->txt_status = 'inactive' ;
        }

        $room->name = $request->txt_title ;
        $room->status = $request->txt_status ;
        $room->banner_image = $banner_image;

        try {
            $room->save();

            return response()->json([
               'status' => true,
               'message' => 'บันทึกเรียบร้อยแล้ว'
            ]);
        }
        catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }
    }

    public function updateStatus(Request $request)
    {


        $room = Room::where('id',$request->id)->first();

        if ($request->status == 'false') {
            $room->status = 'inactive';
        }
        else{
            $room->status = 'active';
        }


        try {
            $room->save();
            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียบร้อยแล้ว'
            ]);
        }catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
    }


    public function edit($id)
    {

        $data = Room::where('id',$id)->first();

        return view('admin.fo4.icon.room.edit' ,['data' => $data]);
    }


    public function deleteEvent(Request $request)
    {

        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Room::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found event.",
            ]);
        }

        try{

            $check->delete();

            $profiles = Profile::where('room_id',$id)->get();
            foreach ($profiles as $profile){
                $profile->room_id = null;
                $profile->show = null;
                $profile->order = null;
                $profile->save();
            }

        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }


        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }
}
