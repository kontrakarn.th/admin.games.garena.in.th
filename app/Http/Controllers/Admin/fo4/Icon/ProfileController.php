<?php

namespace App\Http\Controllers\Admin\fo4\Icon;

use App\Http\Controllers\ApiController;
use App\Models\fo4\Icon\Expertise;
use App\Models\fo4\Icon\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Yajra\DataTables\DataTables;

class ProfileController extends ApiController
{
    private $pagepath='admin.fo4.icon.';
    private $pageheader='Icon';
    private $uploadPath = 'fo4/mainsite/';


    function __construct()
    {

    }

    public function index()
    {
        return view('admin.fo4.icon.profile.index');
    }


    public function create()
    {
        $expertise = Expertise::get();
        return view('admin.fo4.icon.profile.create' ,[
            'expertise' => $expertise
        ]);
    }


    public function save(Request $request)
    {

//        dd($request->all());


        $validator = Validator::make($request->all(), [
            'txt_first_name'                 => 'required|max:100',
            'txt_middle_name'                 => 'required|max:100',
            'txt_last_name'                 => 'required|max:100',
            'txt_nick_name'                 => 'required|max:100',
            'txt_weight'                 => 'required|max:3',
            'txt_height'                 => 'required|max:3',
            'foot_left'                 => 'required|max:1|min:1',
            'foot_right'                 => 'required|max:1|min:1',
            'txt_fp'                 => 'required|max:3',
            'txt_ovr'                 => 'required|max:3',
            'expertise'                 => 'required',
//            'txt_banner_image_large'          => 'mimes:jpeg,jpg,png|dimensions:width=660,height=1000',
//            'txt_banner_image_small'          => 'mimes:jpeg,jpg,png|dimensions:width=200,height=200',
//            'txt_banner_image_icon'          => 'mimes:jpeg,jpg,png|dimensions:width=80,height=80',
//            'txt_banner_image_history'          => 'mimes:jpeg,jpg,png|dimensions:width=670,height=175',
//            'txt_banner_image_award'          => 'mimes:jpeg,jpg,png|dimensions:width=670,height=175',
//            'txt_banner_image_flag'          => 'mimes:jpeg,jpg,png|dimensions:width=50,height=30',
//            'txt_banner_image_all_stat'          => 'mimes:jpeg,jpg,png|dimensions:width=1265,height=800',
//            'txt_banner_image_logo_team'          => 'mimes:jpeg,jpg,png|dimensions:width=110,height=140',
//            'txt_banner_image_shirt'          => 'mimes:jpeg,jpg,png|dimensions:width=120,height=170',
        ],[
            'txt_first_name.required' => 'กรุณากรอก ชื่อ',
            'txt_middle_name.required' => 'กรุณากรอก ชื่อกลาง',
            'txt_last_name.required' => 'กรุณากรอก นามสกุล',
            'txt_nick_name.required' => 'กรุณากรอก ฉายา',
            'txt_weight.required' => 'กรุณากรอก น้ำหนัก',
            'txt_height.required' => 'กรุณากรอก ส่วนสูง',
            'foot_left.required' => 'กรุณากรอก เท้าซ้าย',
            'foot_right.required' => 'กรุณากรอก เท้าขวา',
            'txt_fp.required' => 'กรุณากรอกค่า FP',
            'txt_ovr.required' => 'กรุณากรอกค่า OVR',

            'txt_first_name.max' => 'ชื่อต้องไม่เกิน 100 ตัวอักษร',
            'txt_middle_name.max' => 'ชื่อกลางต้องไม่เกิน 100 ตัวอักษร',
            'txt_last_name.max' => 'นามสกุลต้องไม่เกิน 100 ตัวอักษร',
            'txt_nick_name.max' => 'ฉายาต้องไม่เกิน 100 ตัวอักษร',
            'txt_weight.max' => 'น้ำหนักต้องไม่เกิน 3 หลัก',
            'txt_height.max' => 'ส่วนสูงต้องไม่เกิน 3 หลัก',
            'foot_left.max' => 'เท้าซ้ายต้องไม่เกิน 1 หลัก',
            'foot_right.max' => 'เท้าขวาต้องไม่เกิน 1 หลัก',
            'txt_fp.max' => 'FP ต้องไม่เกิน 3 หลัก',
            'txt_ovr.max' => 'OVR ต้องไม่เกิน 3 หลัก',

            'txt_banner_image_large.dimensions'           => 'ขนาดรูป Profile Large ต้องเป็น 660 × 1000 เท่านั้น',
            'txt_banner_image_small.dimensions'           => 'ขนาดรูป Profile Small (Large) ต้องเป็น 200 × 200 เท่านั้น',
            'txt_banner_image_icon.dimensions'           => 'ขนาดรูป Profile Small (Icon) ต้องเป็น 80 × 80 เท่านั้น',
            'txt_banner_image_history.dimensions'           => 'ขนาดรูป History ต้องเป็น 670 × 175 เท่านั้น',
            'txt_banner_image_award.dimensions'           => 'ขนาดรูป Award ต้องเป็น 670 × 175 เท่านั้น',
            'txt_banner_image_flag.dimensions'           => 'ขนาดรูป Flag ต้องเป็น 50 × 30 เท่านั้น',
            'txt_banner_image_all_stat.dimensions'           => 'ขนาดรูป All Stat ต้องเป็น 1145 × 720 เท่านั้น',
            'txt_banner_image_logo_team.dimensions'           => 'ขนาดรูป Logo Team ต้องเป็น 110 × 140 เท่านั้น',
            'txt_banner_image_shirt.dimensions'           => 'ขนาดรูป Shirt ต้องเป็น 120 × 172 เท่านั้น',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first()
            ]);
        }


        // $banner_image_large
        if($request->hasFile('txt_banner_image_large')) {
            $banner_image_large = $this->uploadImage(
                $request->file('txt_banner_image_large'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_large = Profile::where('id',$request->txt_id)->value('profile_big_image');

            }else{
                $banner_image_large = null;
            }
        }


        // $banner_image_small
        if($request->hasFile('txt_banner_image_small')) {
            $banner_image_small = $this->uploadImage(
                $request->file('txt_banner_image_small'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_small = Profile::where('id',$request->txt_id)->value('profile_small_image');

            }else{
                $banner_image_small = null;
            }
        }


        // $banner_image_icon
        if($request->hasFile('txt_banner_image_icon')) {
            $banner_image_icon = $this->uploadImage(
                $request->file('txt_banner_image_icon'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_icon = Profile::where('id',$request->txt_id)->value('profile_image_icon');

            }else{
                $banner_image_icon = null;
            }
        }


        // $banner_image_history
        if($request->hasFile('txt_banner_image_history')) {
            $banner_image_history = $this->uploadImage(
                $request->file('txt_banner_image_history'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_history = Profile::where('id',$request->txt_id)->value('history_image');

            }else{
                $banner_image_history = null;
            }
        }


        // $banner_image_award
        if($request->hasFile('txt_banner_image_award')) {
            $banner_image_award = $this->uploadImage(
                $request->file('txt_banner_image_award'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_award = Profile::where('id',$request->txt_id)->value('award_image');

            }else{
                $banner_image_award = null;
            }
        }


        // $banner_image_flag
        if($request->hasFile('txt_banner_image_flag')) {
            $banner_image_flag = $this->uploadImage(
                $request->file('txt_banner_image_flag'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_flag = Profile::where('id',$request->txt_id)->value('flag_image');

            }else{
                $banner_image_flag = null;
            }
        }


        // $banner_image_all_stat
        if($request->hasFile('txt_banner_image_all_stat')) {
            $banner_image_all_stat= $this->uploadImage(
                $request->file('txt_banner_image_all_stat'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_all_stat = Profile::where('id',$request->txt_id)->value('profile_all_stat');

            }else{
                $banner_image_all_stat = null;
            }
        }

        // $banner_image_logo_team
        if($request->hasFile('txt_banner_image_logo_team')) {
            $banner_image_logo_team= $this->uploadImage(
                $request->file('txt_banner_image_logo_team'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_logo_team = Profile::where('id',$request->txt_id)->value('logo_team');

            }else{
                $banner_image_logo_team = null;
            }
        }

        // $banner_image_shirt
        if($request->hasFile('txt_banner_image_shirt')) {
            $banner_image_shirt= $this->uploadImage(
                $request->file('txt_banner_image_shirt'),
                $this->uploadPath.'banner/'.$request->txt_id,
                'upload',
                null
            );
        }else{
            if (isset($request->txt_id)) {
                $banner_image_shirt = Profile::where('id',$request->txt_id)->value('shirt_image');

            }else{
                $banner_image_shirt = null;
            }
        }


        $saved = Profile::saveProfileIcon($request, $banner_image_large , $banner_image_small , $banner_image_icon , $banner_image_history , $banner_image_award, $banner_image_flag , $banner_image_all_stat , $banner_image_logo_team,$banner_image_shirt);


        if ($saved) {
            return response()->json([
                'status'=>true,
                'message'=>'บันทึกเรียบร้อย',
            ]);
        }

        return response()->json([
            'status'=>false,
            'message'=>'เกิดข้อปิดพลาด โปรดลองใหม่อีกครั้ง',
        ]);

    }


    public function dataTable(Request $request)
    {
        $banners = Profile::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function ($_model) {
            return '<a href="' . route('admin.fo4.icon.profile.edit', ['id' => $_model->id]) . '"><img src="' . $_model->profile_small_image . '" class="img-responsive" width="100px"></a>';
        });

        $datatables->editColumn('name', function ($_model) {
            return $_model->first_name ." ".$_model->last_name;
        });

        $datatables->editColumn('nation', function ($_model) {
            return "<img src='$_model->flag_image' width='40px'>";;
        });

        $datatables->editColumn('feet', function ($_model) {
            return $_model->foot_left ." - ".$_model->foot_right;
        });

        $datatables->editColumn('fp', function ($_model) {
            return $_model->FP;
        });

        $datatables->editColumn('ovr', function ($_model) {
            return $_model->OVR;
        });

        $datatables->editColumn('status', function ($_model) {
            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.fo4.icon.profile.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image','name','nation' , 'feet' , 'fp' , 'ovr' , 'status', 'action'])->make(true);
    }


    public function edit($id)
    {

        $data = Profile::where('id',$id)->first();
        $expertise = Expertise::get();

        return view('admin.fo4.icon.profile.create',[
            'data' => $data,
            'expertise' => $expertise
        ]);
    }


    public function updateStatus(Request $request)
    {
        $profile = Profile::where('id',$request->id)->first();

        if ($request->status == 'false') {
            $profile->status = 'inactive';
        }
        else{
            $profile->status = 'active';
        }


        try {
            $profile->save();
            return response()->json([
                'status' => true,
                'message' => 'เปลี่ยนสถานะเรียบร้อยแล้ว'
            ]);
        }catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }
    }


    public function deleteIcon(Request $request)
    {

        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Profile::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found event.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }


        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }
}
