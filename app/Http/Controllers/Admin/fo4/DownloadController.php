<?php

namespace App\Http\Controllers\Admin\fo4;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Models\AuthUser;
use App\Models\fo4\DBMenu;
use App\Models\fo4\DBDownload;
use App\Models\fo4\DBSpec;

class DownloadController extends ApiController
{
    private $pagepath='admin.fo4.download.';
    private $pageheader='Download manager';
    private $uploadPath = 'fo4/mainsite/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.fo4.menu.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
    	
    }

    public function add()
    {
    	
    }

    public function edit(Request $request)
    {
        $this->viewData['specData'] = $specData = DBSpec::getSpecData();
    	$this->viewData['downloadData'] = $downloadData = DBDownload::getDownloadData();

        return view($this->pagepath.'edit',$this->viewData);
    }

    public function saveElement(Request $request)
    {
        $typeDownload = array('gpc','patch','download','driver');

        if (in_array($request->types, $typeDownload)) {
            $saved = DBDownload::saveElement($request);
        }else{
            $saved = DBSpec::saveElement($request);
        }

        if ($saved) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Saved'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Saved'
        ]);
    }

    public function addElement(Request $request)
    {
        $typeDownload = array('gpc','patch','download','driver');

        if (in_array($request->types, $typeDownload)) {
            $added = DBDownload::addElement($request);
        }else{
            $added = DBSpec::addElement($request);
        }

        if ($added) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Added'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Added'
        ]);
    }

    public function deleteElement(Request $request)
    {

    	$id = $request->id;


        $deleted = DBDownload::deleteElement($id);

        if ($deleted) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Deleted'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Deleted'
        ]);
    }


    public function deleteElementSpec(Request $request)
    {

        $updateData = [
            'status'        => 'inactive',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'deleted_by'    => Auth::user()->id,
            'deleted_at'    => Carbon::now(),
        ];
        try {
            DBSpec::where('id',$request->id)->update($updateData);
            return response()->json([
                'status' => true,
                'message' => 'Element Deleted'
            ]);
        }catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => 'Element Not Deleted'
            ]);
        }
    }
   
}