<?php

namespace App\Http\Controllers\Admin\fo4;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Models\AuthUser;
use App\Models\fo4\DBMenu;
use App\Models\fo4\DBPage;

class PageController extends ApiController
{
    private $pagepath='admin.fo4.page.';
    private $pageheader='Page manager';
    private $uploadPath = 'fo4/mainsite/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.fo4.menu.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
    	$this->viewData['data'] = 'test';
    	// dd(session()->get('user'));
    	// dd(auth()->user()->name);
    	// dd(Auth::user());

    	// print_r($this->viewData);
        return view($this->pagepath.'manage',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function add()
    {
    	$this->viewData['data'] = 'test';
    	// dd(session()->get('user'));
    	// dd(auth()->user()->name);
    	// dd(Auth::user());

    	// print_r($this->viewData);
        return view($this->pagepath.'add',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function edit(Request $request)
    {
    	$this->viewData['slug'] = $slug = $request->slug;
    	
    	$this->viewData['count_section'] = $count_section = DBPage::getPageSection($slug);
    	$this->viewData['max_section'] = count($count_section);
    	$this->viewData['datas'] = $datas = DBPage::getPageData($slug);

        /*foreach ($datas as $key => $data) {
            if (!empty($data->url)) {
                $videoType = $this->videoType($data->url);
                if ($videoType == 'unknown') {
                    $link = $this->youtubeLink($data->url);
                    print_r($link);
                    $data->url = $link;
                }
            }
        }*/

    	// print_r($this->viewData['slug']);
        return view($this->pagepath.'edit',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function saveElement(Request $request)
    {
    	$id = $request->id;
    	$field = $request->field;
    	$value = $request->value;

        if ($field == 'url') {
            $videoType = $this->videoType($value);
            if ($videoType == 'youtube') {
                $value = $this->youtubeID($value);
            }
        }

        $saved = DBPage::saveElement($id,$field,$value);

        if ($saved) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Saved'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Saved'
        ]);
    }

    public function addSection(Request $request)
    {
    	$slug 			= $request->slug;
    	$section_id 	= $request->section_id;
        $element_type 	= $request->element_type;

        $added = DBPage::addSection($slug, $section_id, $element_type);

        if ($added) {

	        return response()->json([
	            'status' => true,
	            'message' => 'Section '.$section_id.' Added'
	        ]);
        }
        return response()->json([
	            'status' => false,
	            'message' => 'Section '.$section_id.' Not Added'
	        ]);

    	
    }

    public function addElement(Request $request)
    {
    	$slug 			= $request->slug;
    	$section_id 	= $request->section_id;
        $element_type 	= $request->element_type;

        $added = DBPage::addElement($slug, $section_id, $element_type);

        if ($added) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Added'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Added'
        ]);
    }

    public function deleteSection(Request $request)
    {
    	$id = $request->id;

        $deleted = DBPage::deleteSection($id);

        if ($deleted) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Section Deleted'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Section Not Deleted'
        ]);
    }

    public function deleteElement(Request $request)
    {
    	$id = $request->id;

        $deleted = DBPage::deleteElement($id);

        if ($deleted) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Deleted'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Deleted'
        ]);
    }

    public function saveImage(Request $request)
    {
    	$id = $request->id;
    	$formData = $request->txt_image;
    	$slug = $request->txt_slug;

    	$model=DBPage::find($id);

        if($request->hasFile('txt_image')){
            $src = $model->src = $this->uploadImage(
                $request->file('txt_image'),
                $this->uploadPath.'page/'.$slug,
                'upload',
                null
            );
        }
        $model->created_by=Auth()->user()->id;
        $model->updated_by=Auth()->user()->id;
        $model->save();

        return response()->json([
            'status'=>true,
            'message'=>'Save Image Completed',
        ]);
    }

    public function saveImageEditor(Request $request)
    {
        $slug = $request->txt_slug;
        #upload image
        if($request->hasFile('file')) {
            $image = $this->uploadImage(
                $request->file('file'),
                $this->uploadPath.'page/'.$slug,
                'upload',
                null
            );
        }else{
            return response()->json([
                'status'=>false,
                'message'=>'No File',
            ]);
        }

        return response()->json([
            'status'=>true,
            'message'=>$image,
        ]);
    }

    public function getMenuDatatable()
    {
        // $menu = DBMenu::where('parent_id','0')->get();
        $menu = DBMenu::where('type','page')->get();
        // $menu = Auth::user();
        // dd($menu);

        $datatables = Datatables::of($menu);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('name', function($_model) {
            return '<a href="' . route('admin.fo4.page.edit', ['slug' => $_model->slug]) . '" target="_blank">' . $_model->name . '</a>';
        });
        $datatables->editColumn('slug', function($_model) {
            return $_model->slug;
        });
        $datatables->editColumn('url', function($_model) {
            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';;
        });
        $datatables->editColumn('status', function($_model){

            if($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
        $datatables->editColumn('parent_id', function($_model) {
            return $_model->parent_id;
        });

         // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name','url','status'])->make(true);
    }

    public function videoType($url) 
    {
        if (strpos($url, 'youtube') > 0) {
            return 'youtube';
        } elseif (strpos($url, 'cdngarenanow') > 0) {
            return 'cdn';
        } else {
            return 'unknown';
        }
    }

    public function youtubeID($url)
    {
        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
        $id = $matches[1];
        return $id;
    }

    public function youtubeLink($id)
    {
        $link = 'https://www.youtube.com/watch?v='.$id;
        return $link;
    }

   
}