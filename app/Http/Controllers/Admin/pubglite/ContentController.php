<?php

namespace App\Http\Controllers\Admin\pubglite;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Models\AuthUser;
use App\Models\pubglite\DBMenu;
use App\Models\pubglite\DBContent;

class ContentController extends ApiController
{
    private $pagepath='admin.pubglite.content.';
    private $pageheader='Content Manager';
    private $uploadPath = 'pubglite/mainsite/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.pubglite.menu.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
    	$this->viewData['data'] = 'test';
    	// dd(session()->get('user'));
    	// dd(auth()->user()->name);
    	// dd(Auth::user());

    	// print_r($this->viewData);
        $this->viewData['parentMenu'] = $parentMenu = DBMenu::getParentMenuData();
        // dd( DBMenu::getParentMenuData());
        return view($this->pagepath.'manage',$this->viewData);
        // return view('admin.pubglite.dashboard');
    }

    public function add()
    {
    	$this->viewData['data'] = 'test';
    	// dd(session()->get('user'));
    	// dd(auth()->user()->name);
    	// dd(Auth::user());

    	// print_r($this->viewData);
        return view($this->pagepath.'add',$this->viewData);
        // return view('admin.pubglite.dashboard');
    }

    public function edit(Request $request)
    {
    	$this->viewData['id'] = $id = $request->id;
    	
    	$this->viewData['data'] = $data = DBContent::getContentData($id);

    	// dd($data);
    	// print_r($data);
        return view($this->pagepath.'edit',$this->viewData);
        // return view('admin.pubglite.dashboard');
    }

    public function addContent(Request $request)
    {
        if (!empty($request->name)) {
            return response()->json([
                'status'=>false,
                'message'=>'No Menu Name',
            ]);
        }

        $slug = str_slug($request->txt_slug, '-');

        $checkduplicate = DBContent::where('slug',$slug)->count();

        if ($checkduplicate > 0) {  
            return response()->json([
                'status'=>false,
                'message'=>'Slug นี้ถูกใช้งานแล้ว',
            ]);
        }

        $added = DBContent::addContent($request, $slug);

        if ($added) {
            return response()->json([
                'status'=>true,
                'message'=>'added',
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'message'=>'Not add',
            ]);
        }
    }

    public function saveContent(Request $request)
    {
        $slug = str_slug($request->txt_slug, '-');

        $checkduplicate = DBContent::where('id','!=',$request->txt_id)->where('slug',$slug)->count();

        if ($checkduplicate > 0) {  
            return response()->json([
                'status'=>false,
                'message'=>'Slug นี้ถูกใช้งานแล้ว',
            ]);
        }

        if($request->hasFile('txt_cover_image')) {
            $cover_image = $this->uploadImage(
                $request->file('txt_cover_image'),
                $this->uploadPath.'content/'.$slug.'/cover',
                'upload',
                null
        );
        }else{
            $cover_image = DBContent::where('id',$request->txt_id)->value('cover_image');
        }

        if($request->hasFile('txt_share_image')) {
            $share_image = $this->uploadImage(
                $request->file('txt_share_image'),
                $this->uploadPath.'content/'.$slug.'/share',
                'upload',
                null
            );
        }else{
        	$share_image = DBContent::where('id',$request->txt_id)->value('share_image');
        }

        $saved = DBContent::saveContent($request, $cover_image, $share_image, $slug);

        if ($saved) {
        	return response()->json([
	            'status'=>true,
	            'message'=>'Saved',
	        ]);
        }

    	return response()->json([
            'status'=>false,
            'message'=>'Not Save',
        ]);
    }

    public function deleteContent(Request $request)
    {
        $id = $request->id;

        $deleted = DBContent::deleteContent($id);

        if ($deleted) {
            return response()->json([
                'status' => true,
                'message' => 'Content Deleted'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Content Not Deleted'
        ]);
    }

    public function saveImage(Request $request)
    {
    	$slug = $request->txt_slug;
        #upload image
        if($request->hasFile('file')) {
            $image = $this->uploadImage(
                $request->file('file'),
                $this->uploadPath.'content'.$slug,
                'upload',
                null
            );
        }else{
        	return response()->json([
	            'status'=>false,
	            'message'=>'No File',
	        ]);
        }

        return response()->json([
            'status'=>true,
            'message'=>$image,
        ]);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $updated = DBContent::updateStatus($id,$status);

        if ($updated) {
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Status Not Updated'
        ]);
    }

    public function getMenuDatatable()
    {
        // $menu = DBMenu::where('type','content')->get();
        $menu = DBContent::get();
        // $menu = Auth::user();
        // dd($menu);

        $datatables = Datatables::of($menu);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('name', function($_model) {
            return '<a href="' . route('admin.pubglite.content.edit', ['id' => $_model->id]) . '" target="_blank">' . $_model->title . '</a>';
        });
        $datatables->editColumn('slug', function($_model) {
            return $_model->slug;
        });
        $datatables->editColumn('draft', function($_model) {
            if ($_model->status == 'draft') {
                return $_model->status;
            }
            
        });
         $datatables->editColumn('view', function($_model) {
            return $_model->view;
        });
        $datatables->editColumn('status', function($_model){

            if($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.pubglite.content.edit', ['id' => $_model->id]) . '" target="_blank"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteContent(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

         // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name','status', 'action'])->make(true);
    }

   
}