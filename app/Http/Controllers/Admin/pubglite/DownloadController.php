<?php

namespace App\Http\Controllers\Admin\pubglite;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Models\AuthUser;
use App\Models\pubglite\DBMenu;
use App\Models\pubglite\DBDownload;
use App\Models\pubglite\DBSpec;

class DownloadController extends ApiController
{
    private $pagepath='admin.pubglite.download.';
    private $pageheader='Download manager';
    private $uploadPath = 'pubglite/mainsite/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.pubglite.menu.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
    	
    }

    public function add()
    {
    	
    }

    public function edit(Request $request)
    {
        $this->viewData['specData'] = $specData = DBSpec::getSpecData();
    	$this->viewData['downloadData'] = $downloadData = DBDownload::getDownloadData();

        return view($this->pagepath.'edit',$this->viewData);
    }

    public function saveElement(Request $request)
    {
        $typeDownload = array('gpc','patch','download','driver');

        if (in_array($request->types, $typeDownload)) {
            $saved = DBDownload::saveElement($request);
        }else{
            $saved = DBSpec::saveElement($request);
        }

        if ($saved) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Saved'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Saved'
        ]);
    }

    public function addElement(Request $request)
    {
        $typeDownload = array('gpc','patch','download','driver');

        if (in_array($request->types, $typeDownload)) {
            $added = DBDownload::addElement($request);
        }else{
            $added = DBSpec::addElement($request);
        }

        if ($added) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Added'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Added'
        ]);
    }

    public function deleteElement(Request $request)
    {
    	$id = $request->id;

        $deleted = DBDownload::deleteElement($id);

        if ($deleted) {
        	return response()->json([
	            'status' => true,
	            'message' => 'Element Deleted'
	        ]);
        }
    	return response()->json([
            'status' => false,
            'message' => 'Element Not Deleted'
        ]);
    }
   
}