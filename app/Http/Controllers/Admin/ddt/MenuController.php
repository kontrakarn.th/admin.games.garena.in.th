<?php

namespace App\Http\Controllers\Admin\ddt;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Models\AuthUser;
use App\Models\AuthMenu;
use App\Models\ddt\DBMenu;

class MenuController extends ApiController
{
    private $pagepath='admin.ddt.menu.';
    private $pageheader='Menu Manager';
    private $uploadPath = 'ddt/mainsite/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.ddt.menu.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
    	$this->viewData['data'] = 'test';
    	// dd(session()->get('user'));
    	// dd(auth()->user()->name);
    	// dd(Auth::user());

    	$this->viewData['parentMenu'] = $parentMenu = DBMenu::getParentMenuData();

    	// $this->viewData['menu'] = $menu = AuthMenu::renderAsHtml();

        // $this->viewData['menu'] = html_entity_decode($menu);
    	// dd($menu);
        // print_r($this->viewData);
        return view($this->pagepath.'manage',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function add()
    {
    	$this->viewData['data'] = 'test';
    	// dd(session()->get('user'));
    	// dd(auth()->user()->name);
    	// dd(Auth::user());

    	// print_r($this->viewData);
        return view($this->pagepath.'add',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function edit(Request $request)
    {
    	$this->viewData['id'] = $id = $request->id;
    	
    	$this->viewData['data'] = $data = DBMenu::getMenuData($id);

    	$this->viewData['parentMenuData'] = $parentMenuData = DBMenu::getParentMenuData($id);


        return view($this->pagepath.'edit',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function getMenuDatatable()
    {
    	// $menu = DBMenu::where('parent_id','0')->get();
    	$menu = DBMenu::get();
    	// $menu = Auth::user();
        // dd($menu);

        $datatables = Datatables::of($menu);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('name', function($_model) {
            return '<a href="' . route('admin.ddt.menu.edit', ['id' => $_model->id]) . '" target="_blank">' . $_model->name . '</a>';
        });
        $datatables->editColumn('slug', function($_model) {
            return $_model->slug;
        });
        $datatables->editColumn('url', function($_model) {
            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';;
        });

        $datatables->editColumn('type', function($_model) {
            return $_model->type;
        });
        $datatables->editColumn('show', function($_model){

            if($_model->show == true) {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Show" data-off-text="No Show" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Show" data-off-text="No Show" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
        $datatables->editColumn('parent_id', function($_model) {
            return $_model->parent_id;
        });
        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.ddt.menu.edit', ['id' => $_model->id]) . '" target="_blank"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteMenu(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

         // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name','url','show','action'])->make(true);
    }

    public function saveImage(Request $request)
    {
    	$slug = $request->txt_slug;
        #upload image
        if($request->hasFile('file')) {
            $image = $this->uploadImage(
                $request->file('file'),
                $this->uploadPath.'menu/'.$slug,
                'upload',
                null
            );
        }else{
        	return response()->json([
	            'status'=>false,
	            'message'=>'No File',
	        ]);
        }

        return response()->json([
            'status'=>true,
            'message'=>$image,
        ]);
    }

    public function addMenu(Request $request)
    {
    	if (!empty($request->name)) {
    		return response()->json([
	            'status'=>false,
	            'message'=>'No Menu Name',
	        ]);
    	}

        $slug = str_slug($request->txt_slug, '-');

        $checkduplicate = DBMenu::where('slug',$slug)->count();

        if ($checkduplicate > 0) {  
            return response()->json([
                'status'=>false,
                'message'=>'Slug นี้ถูกใช้งานแล้ว',
            ]);
        }

    	$added = DBMenu::addMenu($request,$slug);

    	if ($added) {
    		return response()->json([
	            'status'=>true,
	            'message'=>'added',
	        ]);
    	}else{
    		return response()->json([
	            'status'=>false,
	            'message'=>'Not add Menu',
	        ]);
    	}
    }

    public function saveMenu(Request $request)
    {
    	if($request->hasFile('txt_image')) {
            $image = $this->uploadImage(
                $request->file('txt_image'),
                $this->uploadPath.'menu/'.$request->txt_slug,
                'upload',
                null
            );
        }else{
        	$image = DBMenu::where('id',$request->id)->value('share_image');
        }

        $saved = DBMenu::saveMenu($request, $image);

        if ($saved) {
        	return response()->json([
	            'status'=>true,
	            'message'=>'Saved',
	        ]);
        }

    	return response()->json([
            'status'=>false,
            'message'=>'Not Save',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $updated = DBMenu::updateStatus($id,$status);

        if ($updated) {
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Status Not Updated'
        ]);
    }

    public function deleteMenu(Request $request)
    {
        $id = $request->id;

        $deleted = DBMenu::deleteMenu($id);

        if ($deleted) {
            return response()->json([
                'status' => true,
                'message' => 'Content Deleted'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Content Not Deleted'
        ]);
    }
}
