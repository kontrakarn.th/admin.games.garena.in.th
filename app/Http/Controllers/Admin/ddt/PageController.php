<?php

namespace App\Http\Controllers\Admin\ddt;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Models\AuthUser;
use App\Models\ddt\DBMenu;
use App\Models\ddt\DBPage;

class PageController extends ApiController {

    private $pagepath = 'admin.ddt.page.';
    private $pageheader = 'Page manager';
    private $uploadPath = 'ddt/mainsite/';

    function __construct() {
        $this->middleware('guest');
        $this->init();
    }

    private function init() {
        $this->viewData['pageheader'] = $this->pageheader;
        $this->viewData['breadcrumbs'] = [
            ['text' => '<i class="fa fa-list-alt"></i> Home', 'url' => route('admin.ddt.menu.index'), 'active' => ''],
            ['text' => 'User', 'url' => '', 'active' => ''],
            ['text' => 'Report', 'url' => '', 'active' => 'active']
        ];
    }

    public function index() {
        // dd("aaa");
        $this->viewData['data'] = 'test';
        // dd(session()->get('user'));
        // dd(auth()->user()->name);
        // dd(Auth::user());
        // print_r($this->viewData);
        return view($this->pagepath . 'manage', $this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function add() {
        $this->viewData['data'] = 'test';
        // dd(session()->get('user'));
        // dd(auth()->user()->name);
        // dd(Auth::user());
        // print_r($this->viewData);
        return view($this->pagepath . 'add', $this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function edit(Request $request) {

        $this->viewData['slug'] = $slug = $request->slug;

        $this->viewData['count_section'] = $count_section = DBPage::getPageSection($slug);
        $this->viewData['max_section'] = count($count_section);
        $rawdata = DBPage::getPageData($slug);

        $arr_data = $rawdata->groupBy('section_name')->toArray();
        
//            dd($arr_data);
        $this->viewData['data'] = ([

            'popup_img_image_src'           => $arr_data['popup'][0]['src'],
            'popup_link_goto_url'           => $arr_data['popup'][1]['url'],

            'download_bg_src'               => $arr_data['download'][0]['src'],
            'download_bg_alt'               => $arr_data['download'][0]['alt'],
            'download_img_logo_src'         => $arr_data['download'][1]['src'],
            'download_img_logo_alt'         => $arr_data['download'][1]['alt'],
            'download_img_qrcode_src'       => $arr_data['download'][2]['src'],
            'download_img_qrcode_alt'       => $arr_data['download'][2]['alt'],
            'download_btn_app_store_url'    => $arr_data['download'][3]['url'],
            'download_btn_google_play_url'  => $arr_data['download'][4]['url'],
            'download_btn_apk_url'          => $arr_data['download'][5]['url'],
                
                
            'rank_bg_src'                   => $arr_data['rank'][0]['src'],
            'rank_bg_alt'                   => $arr_data['rank'][0]['alt'],
            'rank_img_header_src'           => $arr_data['rank'][1]['src'],
            'rank_img_header_alt'           => $arr_data['rank'][1]['alt'],
            'rank_img_img_guide_src'        => $arr_data['rank'][2]['src'],
            'rank_img_img_guide_alt'        => $arr_data['rank'][2]['alt'],
            'rank_btn_beginner_url'         => $arr_data['rank'][3]['url'],
            'rank_btn_add_ticket_url'       => $arr_data['rank'][4]['url'],
            'rank_btn_add_ticket_content'   => $arr_data['rank'][4]['content'],
            'rank_btn_community_url'        => $arr_data['rank'][5]['url'],
            'rank_btn_community_content'    => $arr_data['rank'][5]['content'],
            'rank_btn_gallery_url'          => $arr_data['rank'][6]['url'],
            'rank_btn_gallery_content'      => $arr_data['rank'][6]['content'],
            'rank_btn_support_url'          => $arr_data['rank'][7]['url'],
            'rank_btn_support_content'      => $arr_data['rank'][7]['content'],
            'rank_btn_facebook_url'         => $arr_data['rank'][8]['url'],
            
            'news_bg_src'                   => $arr_data['news'][0]['src'],
            'news_bg_alt'                   => $arr_data['news'][0]['alt'],
            'news_img_src'                  => $arr_data['news'][1]['src'],
            'news_img_alt'                  => $arr_data['news'][1]['alt'],
            
            'item_guide_bg_src'             => $arr_data['item_guide'][0]['src'],
            'item_guide_bg_alt'             => $arr_data['item_guide'][0]['alt'],
            'item_guide_img_src'            => $arr_data['item_guide'][1]['src'],
            'item_guide_img_alt'            => $arr_data['item_guide'][1]['alt'],
            
            'banner_bg_src'                 => $arr_data['banner'][0]['src'],
            'banner_bg_alt'                 => $arr_data['banner'][0]['alt'],
            'banner_img_src'                => $arr_data['banner'][1]['src'],
            'banner_img_alt'                => $arr_data['banner'][1]['alt'],
            
            'footer_img_src'                => $arr_data['footer'][0]['src'],
            'footer_img_alt'                => $arr_data['footer'][0]['alt'],
        ]);


        return view($this->pagepath . 'edit', $this->viewData);
    }

    public function saveImg(Request $request){
//        dd($request->all());
        $cover_image = "";
            if($request->hasFile('txt_image')) {
                $cover_image = $this->uploadImage(
                    $request->file('txt_image'),
                    $this->uploadPath.'pagge',
                    'upload',
                    null
                );
                
                $data =  $this->insertDB($request->section_name,
                         $request->types,
                         $request->name,
                         $request->column,
                         $cover_image
                 );
        
                 return $data;
        
        
            } else {
                $data =  $this->insertDB($request->section_name,
                         $request->types,
                         $request->name,
                         $request->column,
                         $cover_image
                 );

                return $data;
            }
       
    }
    public function updatePage(Request $request){
        $data =  $this->insertDB($request->section_name,
                        $request->types,
                        $request->name,
                        $request->column,
                        $request->value
                );
        
        return $data;
                
    }
    
    public function insertDB($section_name,$types,$name,$column,$value){
        
        $model = DBPage::where(['section_name'  => $section_name , 
                                'types'         => $types , 
                                'name'          => $name 
                               ])->first();
        $model->{$column} = $value;
        $model->created_by = Auth()->user()->id;
        $model->updated_by = Auth()->user()->id;
        
        if($model->save()){
         return response()->json([
                        'status' => true,
                        'message' => 'Saved'
            ]);
        }else{
            return response()->json([
                    'status' => false,
                    'message' => 'Not Saved'
                ]);
        }
    }
    
//    
//    public function saveElement(Request $request) {
//            $id = $request->id;
//            $field = $request->field;
//            $value = $request->value;
//
//            $saved = DBPage::saveElement($id, $field, $value);
//
//        if ($saved) {
//            return response()->json([
//                        'status' => true,
//                        'message' => 'Element Saved'
//            ]);
//        }
//        return response()->json([
//                    'status' => false,
//                    'message' => 'Element Not Saved'
//        ]);
//    }
//
//    public function addSection(Request $request) {
//        $slug = $request->slug;
//        $section_id = $request->section_id;
//        $element_type = $request->element_type;
//
//        $added = DBPage::addSection($slug, $section_id, $element_type);
//
//        if ($added) {
//
//            return response()->json([
//                        'status' => true,
//                        'message' => 'Section ' . $section_id . ' Added'
//            ]);
//        }
//        return response()->json([
//                    'status' => false,
//                    'message' => 'Section ' . $section_id . ' Not Added'
//        ]);
//    }
//
//    public function addElement(Request $request) {
//        $slug = $request->slug;
//        $section_id = $request->section_id;
//        $element_type = $request->element_type;
//
//        $added = DBPage::addElement($slug, $section_id, $element_type);
//
//        if ($added) {
//            return response()->json([
//                        'status' => true,
//                        'message' => 'Element Added'
//            ]);
//        }
//        return response()->json([
//                    'status' => false,
//                    'message' => 'Element Not Added'
//        ]);
//    }
//
//    
//    public function saveImage(Request $request) {
//        $id = $request->id;
//        $formData = $request->txt_image;
//        $slug = $request->txt_slug;
//
//        $model = DBPage::find($id);
//
//        if ($request->hasFile('txt_image')) {
//            $src = $model->src = $this->uploadImage(
//                    $request->file('txt_image'), $this->uploadPath . 'page/' . $slug, 'upload', null
//            );
//        }
//        $model->created_by = Auth()->user()->id;
//        $model->updated_by = Auth()->user()->id;
//        $model->save();
//
//        return response()->json([
//                    'status' => true,
//                    'message' => 'Save Image Completed',
//        ]);
//    }
//
//    public function saveImageEditor(Request $request) {
//        $slug = $request->txt_slug;
//        #upload image
//        if ($request->hasFile('file')) {
//            $image = $this->uploadImage(
//                    $request->file('file'), $this->uploadPath . 'page/' . $slug, 'upload', null
//            );
//        } else {
//            return response()->json([
//                        'status' => false,
//                        'message' => 'No File',
//            ]);
//        }
//
//        return response()->json([
//                    'status' => true,
//                    'message' => $image,
//        ]);
//    }

    public function getMenuDatatable() {
        // $menu = DBMenu::where('parent_id','0')->get();
        $menu = DBMenu::where('manage_page', '1')->get();
        // $menu = Auth::user();
        // dd($menu);

        $datatables = Datatables::of($menu);
        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('name', function($_model) {
            return '<a href="' . route('admin.ddt.page.edit', ['slug' => $_model->slug]) . '" target="_blank">' . $_model->name . '</a>';
        });
        $datatables->editColumn('slug', function($_model) {
            return $_model->slug;
        });
        $datatables->editColumn('url', function($_model) {
            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';
            ;
        });
        $datatables->editColumn('status', function($_model) {

            if ($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        $datatables->editColumn('parent_id', function($_model) {
            return $_model->parent_id;
        });

        // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name', 'url', 'status'])->make(true);
    }

}
