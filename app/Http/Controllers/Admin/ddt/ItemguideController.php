<?php

namespace App\Http\Controllers\Admin\ddt;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Models\AuthUser;
use App\Models\ddt\DBMenu;
//use App\Models\ddt\DBBanner;
use App\Models\ddt\DBItemguide;

class ItemguideController extends ApiController
{
    private $pagepath='admin.ddt.itemguide.';
    private $pageheader='Item Guide Manager';
    private $uploadPath = 'ddt/mainsite/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.ddt.menu.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
    	// print_r($this->viewData);
        $this->viewData['parentMenu'] = $parentMenu = DBMenu::getParentMenuData();
//            dd($this->viewData['parentMenu']);
        return view($this->pagepath.'manage',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function add()
    {
      
        $this->viewData['page'] = 'add';

        return view($this->pagepath.'edit',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function edit(Request $request)
    {
       
    	$this->viewData['id'] = $id = $request->id;
        $this->viewData['data'] = $data = DBItemguide::getData($id);
        $this->viewData['page'] = 'edit';

        return view($this->pagepath.'edit',$this->viewData);
    }

    public function saveItemguide(Request $request)
    {
        
            $itemguide_image = null;
            $itemguide_thumbnail = null;
        if($request->hasFile('txt_image')) {
//            dd(1);
            $itemguide_image = $this->uploadImage(
                $request->file('txt_image'),
                $this->uploadPath.'itemguide/'.$request->txt_id,
                'upload',
                null
            );
            DBItemguide::where('id',$request->txt_id)->value('image');
        }
        
        if ($request->hasFile('txt_thumbnail')) {
          
            $itemguide_thumbnail = $this->uploadImage(
                $request->file('txt_thumbnail'),
                $this->uploadPath.'itemguide/'.$request->txt_id,
                'upload',
                null
            );
            
        }
        
        $saved = DBItemguide::saveItemguide($request, $itemguide_image,$itemguide_thumbnail);

        if ($saved) {
        	return response()->json([
	            'status'=>true,
	            'message'=>'Saved',
	        ]);
        }

    	return response()->json([
            'status'=>false,
            'message'=>'Not Save',
        ]);
    }

    public function deleteBanner(Request $request)
    {
        $id = $request->id;

        $deleted = DBBanner::deleteBanner($id);

        if ($deleted) {
            return response()->json([
                'status' => true,
                'message' => 'Content Deleted'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Content Not Deleted'
        ]);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $updated = DBBanner::updateStatus($id,$status);

        if ($updated) {
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Status Not Updated'
        ]);
    }

    public function getDatatable()
    {
        // $menu = DBMenu::where('type','content')->get();
        $menu = DBItemguide::get();
        // $menu = Auth::user();
//         dd($menu);

        $datatables = Datatables::of($menu);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('category', function($_model) {
            return $_model->category;
        });
        $datatables->editColumn('level', function($_model) {
            return $_model->level;
        });
        $datatables->editColumn('name', function($_model) {
            return $_model->name;
        });
         $datatables->editColumn('title', function($_model) {
            return '<a href="' . route('admin.ddt.itemguide.edit', ['id' => $_model->id]) . '" target="_blank">' . $_model->title . '</a>';
        });
        $datatables->editColumn('image', function($_model) {
            return '<img src="' . $_model->banner_image . '" class="img-responsive" alt="'.$_model->alt.'">';
        });
        $datatables->editColumn('status', function($_model){

            if($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
//        dd($datatables);
        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.ddt.itemguide.edit', ['id' => $_model->id]) . '" target="_blank"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

         // dd($menu->get()->toArray());
        return $datatables->rawColumns(['title','url','image','status', 'action'])->make(true);
    }

   
}