<?php

namespace App\Http\Controllers\Admin\main;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Models\AuthUser;
use App\Models\AuthMenu;
use App\Models\Games;
use App\Models\UserGame;
use App\Models\fo4\DBMenu;

class UserController extends ApiController
{
    private $pagepath='admin.main.user.';
    private $pageheader='User Manager';
    private $uploadPath = 'fo4/mainsite/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.main.user.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
        // $this->viewData['menu'] = html_entity_decode($menu);
    	// dd($menu);
    	// print_r($this->viewData);
        return view($this->pagepath.'manage',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function add()
    {
    	// print_r($this->viewData);
        $this->viewData['games'] = $games = Games::get();
        // dd($games);
        return view($this->pagepath.'edit',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function edit(Request $request)
    {
        $this->viewData['id'] = $id = $request->id;
        $this->viewData['games'] = $games = Games::get();
        $this->viewData['data'] = $data = AuthUser::getUserData($id);
    	$this->viewData['userGameRelation'] = $userGameRelation = UserGame::getUserGameData($id);
    	// print_r($slug);
        return view($this->pagepath.'edit',$this->viewData);
        // return view('admin.fo4.dashboard');
    }

    public function addUser(Request $request)
    {
        if (!empty($request->name)) {
            return response()->json([
                'status'=>false,
                'message'=>'No User Name',
            ]);
        }

        $added = AuthUser::saveUser($request);

        if ($added) {
            return response()->json([
                'status'=>true,
                'message'=>'updated',
            ]);
        }else{
            return response()->json([
                'status'=>false,
                'message'=>'Not updated',
            ]);
        }
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $updated = AuthUser::updateStatus($id,$status);

        if ($updated) {
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'Status Not Updated'
        ]);
    }

    public function getMenuDatatable()
    {
    	// $menu = DBMenu::where('parent_id','0')->get();
    	$menu = AuthUser::get();
    	// $menu = Auth::user();
        // dd($menu);

        $datatables = Datatables::of($menu);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('name', function($_model) {
            return '<a href="' . route('admin.main.user.edit', ['slug' => $_model->id]) . '" >' . $_model->name . '</a>';
        });
        $datatables->editColumn('email', function($_model) {
            return $_model->email;
        });
        $datatables->editColumn('status', function($_model){

            if($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
        $datatables->editColumn('parent_id', function($_model) {
            return $_model->parent_id;
        });
        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.main.user.edit', ['id' => $_model->id]) . '" ><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteUser(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

         // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name','url','status','action'])->make(true);
    }
}
