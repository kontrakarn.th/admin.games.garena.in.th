<?php

namespace App\Http\Controllers\Admin\main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AuthUser;
use Illuminate\Support\Facades\Auth;
// use Category;

class IndexController extends Controller
{
    private $pagepath='admin.main.';
    private $pageheader='Report Free Fire (Online Event)';
    private $uploadPath = 'garena_world/';

    function __construct()
    {
    	$this->middleware('guest');
        $this->init();
    }

    private function init()
    {
      $this->viewData['pageheader']=$this->pageheader;
      $this->viewData['breadcrumbs'] = [
          ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.main.index'),'active'=>''],
          ['text' => 'User','url'=>'','active'=>''],
          ['text' => 'Report','url'=>'','active'=>'active']
      ];
    }

    public function index()
    {
    	$this->viewData['data'] = 'test';
    	// dd(session()->get('user'));
    	// dd(auth()->user()->name);
    	// dd(Auth::user());
      // $categories = Category::nested()->get();
      // $this->viewData['categories'] = html_entity_decode(Category::renderAsHtml());
      // Menu::active(['games'])->renderAsHtml();

      // dd($categories);

    	// print_r($this->viewData);
        return view($this->pagepath.'dashboard',$this->viewData);
        // return view('admin.main.dashboard');
    }
}
