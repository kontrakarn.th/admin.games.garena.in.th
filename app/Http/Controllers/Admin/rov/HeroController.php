<?php

namespace App\Http\Controllers\Admin\rov;

use App\Http\Controllers\ApiController;
use App\Models\rov\DBEnchantment;
use App\Models\rov\DBGameInfo_Role;
use App\Models\rov\DBGameInfo_Spell;
use App\Models\rov\DBHero;
use App\Models\rov\DBItem;
use App\Models\rov\DBItemSet;
use App\Models\rov\DBRune;
use App\Models\rov\DBSkill;
use App\Models\rov\DBSkin;
use App\Models\rov\DBTournament;
use App\Models\rov\DBUser;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class HeroController extends HelperController
{
    private $pagepath = 'admin.rov.hero.';
    private $uploadPath = 'rov/hero/';

    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $heroes         = DBHero::orderBy('name')->get();
        $heroes_free    = DBHero::whereFlag('free')->pluck('id')->toArray();
        $heroes_new     = DBHero::whereFlag('new')->pluck('id')->toArray();

        return view('admin.rov.hero.index', [
            'heroes'        => $heroes,
            'heroes_free'   => $heroes_free,
            'heroes_new'    => $heroes_new,
        ]);
    }

    public function gameInfo()
    {
        $data = [
            'items'        => DBItem::get(),
            'runes'        => DBRune::get(),
            'spells'       => DBGameInfo_Spell::get(),
            'roles'        => DBGameInfo_Role::get(),
            'enchantments' => DBEnchantment::where('level', '>', 0)->get(),
        ];

        return view('admin.rov.hero.gameinfo', $data);
    }

    public function datatable(Request $request)
    {
        $heros      = DBHero::get();
        $datatables = Datatables::of($heros);

        $datatables->editColumn('role', function ($_model) {
            return $_model->role_text;
        });


        $datatables->editColumn('image', function ($_model) {
            return "<img src='$_model->image' width='70px'>";
        });

        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="https://rov.in.th/hero/'.$_model->slug.'" target="_blank"><i class="icon-eye"></i> View</a></li>
                        <li><a href="'.route('admin.rov.hero.edit', [$_model]).'"><i class="icon-pencil7"></i> Edit</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        // dd($menu->get()->toArray());
        return $datatables->rawColumns(['name', 'role', 'image', 'action'])->make(true);

    }

    public function edit(DBHero $hero)
    {
        $data = [
            'data'            => $hero,
            'spells'          => DBGameInfo_Spell::orderBy('name')->get(),
            'roles'           => DBGameInfo_Role::orderBy('name')->get(),
            'runes'           => DBRune::get(),
            'enchantments'    => DBEnchantment::where('level', '>', 0)->get(),
            'userRunes'       => $hero->getRunes(),
            'runesAmount'     => $hero->getRunesAmount(),
            'userEnchantment' => $hero->getEnchantment(),
        ];

        return view('admin.rov.hero.add', $data);
    }

    public function add()
    {

        $data = [
            'spells'          => DBGameInfo_Spell::orderBy('name')->get(),
            'roles'           => DBGameInfo_Role::orderBy('name')->get(),
            'runes'           => DBRune::get(),
            'enchantments'    => DBEnchantment::where('level', '>', 0)->get(),
            'userRunes'       => ['', '', '', '', '', ''],
            'runesAmount'     => [null, null, null, null, null, null,],
            'userEnchantment' => [null, null, null, null, null,],
        ];
        return view('admin.rov.hero.add', $data);
    }

    public function updateStatus(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $hero = DBHero::find($id);
        if ($hero) {


            $hero->status     = $status == "true" ? "active" : "inactive";
            $hero->updated_by = Auth::user()->id;
            $hero->save();

            return response()->json([
                'status'  => true,
                'message' => 'Status Updated',
            ]);
        }

        return response()->json([
            'status'  => false,
            'message' => 'Something went wrong.',
        ]);
    }

    public function saveData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'slogan'      => '',
            'description' => 'required|max:3000',
            'image'       => 'mimes:jpeg,jpg,gif,png',
            'image_bg'    => 'mimes:jpeg,jpg,gif,png',
            'flag'        => 'required',
            'spotlight'   => '',
            'suggestion'  => 'max:3000',
        ], [
            'content_image.dimensions' => 'ขนาดรูปต้องเป็น 1920 × 1080 เท่านั้น',
            'share_image.dimensions'   => 'ขนาดรูป Share ต้องเป็น 1200 × 630 เท่านั้น',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id) { // update
            $hero             = DBHero::find($request->id);
            $hero->updated_at = Carbon::now();
            $hero->updated_by = Auth::user()->id;

            if (!$hero) {
                return response()->json([
                    'status'  => false,
                    'message' => "Not found Hero",
                ]);
            }
        } else { // create new one

            $hero             = new DBHero();
            $hero->slug       = Str::slug($request->name,'-');
            $hero->created_by = Auth::user()->id;
        }


        // Rune
        $rune = $this->setRuneEnchantment($request);
        $hero->update($rune);

        // Upload image
        if ($request->hasFile('image')) {

            $content_image = $this->uploadImage(
                $request->file('image'),
                $this->uploadPath.'image',
                'upload',
                null
            );

            $hero->image = $content_image;
        }

        if ($request->hasFile('image_bg')) {

            $content_image_bg = $this->uploadImage(
                $request->file('image_bg'),
                $this->uploadPath.'bg',
                'upload',
                null
            );

            $hero->image_bg = $content_image_bg;
        }

        // check spotlight
        if ($request->spotlight){
            if(strpos($request->spotlight, 's://www.youtube.com/watch?v=') <= 0){
                return response()->json([
                    'status'  => false,
                    'message' => "Invalid Hero Spotlight YT",
                ]);
            }

        }



        $hero->name           = $request->name;
        $hero->slogan         = $request->slogan;
        $hero->flag           = $request->flag ?? "none";
        $hero->description    = $request->description;
        $hero->spotlight      = $request->spotlight;
        $hero->suggestion     = $request->suggestion;
        $hero->role_id        = $request->role_id;
        $hero->role_second_id = $request->role_second_id ? $request->role_second_id != 0 ? $request->role_second_id : null : null;
        $hero->spell          = $request->spell;

//        if ($request->skin_id) {
//            for ($i = 0; $i < sizeof($request->skin_id); $i++) {
//                if ($request->skin_name[$i] && $request->skin_name[$i] != null) {
//
//                    if ($request->skin_id[$i] && $request->skin_id[$i] != null) {
//                        $skin = DBSkin::find($request->skin_id[$i]);
//                    } else {
//                        $skin = new DBSkin();
//                    }
//
//
//                    $skin->hero_id      = $hero->id;
//                    $skin->name         = $request->skin_name[$i];
//                    $skin->slug         = Str::slug($hero->name.' '.$skin->name, '-');
//                    $skin->image        = $request->skin_images[$i];
//                    $skin->image_banner = $request->skin_banner_images[$i];
//s
//                    $skin->created_by = Auth::user()->id;
//                    $skin->save();
//                }
//            }
//        }


        try {
            $hero->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }

    }

    public function deletedSkin(Request $request)
    {
        $id = $request->id;
        if (!$id) {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid',
            ]);
        }

        $skin = DBSkin::find($id);
        if (!$skin) {
            return response()->json([
                'status'  => false,
                'message' => 'Not Found Skin',
            ]);
        }

        $skin->deleted_by = Auth::user()->id;
        $name             = $skin->name;
        $skin->delete();


        return response()->json([
            'status'  => true,
            'message' => 'ลบสกิน '.$name.' เรียบร้อย',
        ]);
    }

    public function addItemSet(DBHero $hero)
    {

        $data = [
            'items' => DBItem::get(),
            'hero'  => $hero,

        ];

        return view('admin.rov.hero.itemset', $data);
    }

    public function editItemSet(DBHero $hero, DBItemSet $itemset)
    {
        $data = [
            'items'   => DBItem::get(),
            'hero'    => $hero,
            'itemset' => $itemset,

        ];

        return view('admin.rov.hero.itemset', $data);
    }

    public function saveItemSet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required',
            'hero_id' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id) { // update
            $itemset             = DBItemSet::find($request->id);
            $itemset->updated_at = Carbon::now();
            $itemset->updated_by = Auth::user()->id;

            if (!$itemset) {
                return response()->json([
                    'status'  => false,
                    'message' => "Not found Hero",
                ]);
            }
        } else { // create new one

            $itemset             = new DBItemSet();
            $itemset->created_by = Auth::user()->id;
        }

        $itemset->name    = $request->name;
        $itemset->hero_id = $request->hero_id;
        $itemset->item1   = $request->item1;
        $itemset->item2   = $request->item2;
        $itemset->item3   = $request->item3;
        $itemset->item4   = $request->item4;
        $itemset->item5   = $request->item5;
        $itemset->item6   = $request->item6;

        try {
            $itemset->save();
            return response()->json([
                'status'  => true,
                'message' => "Successfully",
            ]);

        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }

    }

    public function saveSkill(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'        => 'required|max:255',
            'hero_id'     => 'required',
            'type'        => 'required',
            'description' => 'required|max:1000',
            'image'       => 'mimes:jpeg,jpg,gif,png',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id) { // update
            $skill             = DBSkill::find($request->id);
            $skill->updated_at = Carbon::now();
            $skill->updated_by = Auth::user()->id;

            if (!$skill) {
                return response()->json([
                    'status'  => false,
                    'message' => "Not found Hero",
                ]);
            }
        } else { // create new one

            $skill             = new DBSkill();
            $skill->created_by = Auth::user()->id;
        }

        $skill->name        = $request->name;
        $skill->type        = $request->type;
        $skill->hero_id     = $request->hero_id;
        $skill->description = $request->description;
        $skill->isEx        = (string)(isset($request->isEx) ? 1 : 0);

        if ($request->hasFile('image')) {

            $content_image = $this->uploadImage(
                $request->file('image'),
                $this->uploadPath.'image',
                'upload',
                null
            );

            $skill->image = $content_image;
        }

        try {
            $skill->save();
            return response()->json([
                'status'  => true,
                'message' => "Successfully",
            ]);

        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function deleteItemSet(DBHero $hero, DBItemSet $itemset)
    {
        $itemset->deleted_by = Auth::user()->id;
        $itemset->save();
        $itemset->delete();

        return redirect()->route('admin.rov.hero.edit', [$hero]);
    }

    public function addSkill(DBHero $hero)
    {

        $data = [
            'hero' => $hero,
        ];

        return view('admin.rov.hero.skill', $data);
    }

    public function editSkill(DBHero $hero, DBSkill $skill)
    {
        $data = [
            'items' => DBItem::get(),
            'hero'  => $hero,
            'skill' => $skill,

        ];

        return view('admin.rov.hero.skill', $data);
    }

    public function deleteSkill(DBHero $hero, DBSkill $skill)
    {
        $skill->deleted_by = Auth::user()->id;
        $skill->save();
        $skill->delete();

        return redirect()->route('admin.rov.hero.edit', [$hero]);
    }

    public function addSkin(DBHero $hero)
    {

        $data = [
            'hero' => $hero,
        ];

        return view('admin.rov.hero.skin', $data);
    }

    public function editSkin(DBHero $hero, DBSkin $skin)
    {
        $data = [
            'items' => DBItem::get(),
            'hero'  => $hero,
            'skin'  => $skin,

        ];

        return view('admin.rov.hero.skin', $data);
    }

    public function saveSkin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'         => 'required|max:255',
            'hero_id'      => 'required',
            'image'        => 'mimes:jpeg,jpg,gif,png',
            'image_banner' => 'mimes:jpeg,jpg,gif,png',

        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id) { // update
            $skin             = DBSkin::find($request->id);
            $skin->updated_at = Carbon::now();
            $skin->updated_by = Auth::user()->id;

            if (!$skin) {
                return response()->json([
                    'status'  => false,
                    'message' => "Not found skin",
                ]);
            }
        } else { // create new one

            $skin             = new DBSkin();
            $skin->created_by = Auth::user()->id;
            $skin->slug       = Str::slug($request->name,'_');
        }

        $skin->name        = $request->name;
        $skin->hero_id     = $request->hero_id;

        if ($request->hasFile('image')) {

            $content_image = $this->uploadImage(
                $request->file('image'),
                $this->uploadPath.'image',
                'upload',
                null
            );

            $skin->image = $content_image;
        }

        if ($request->hasFile('image_banner')) {

            $content_image_banner = $this->uploadImage(
                $request->file('image_banner'),
                $this->uploadPath.'image',
                'upload',
                null
            );

            $skin->image_banner = $content_image_banner;
        }

        try {
            $skin->save();
            return response()->json([
                'status'  => true,
                'message' => "Successfully",
            ]);

        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function deleteSkin(DBHero $hero, DBSkin $skin)
    {
        $skin->deleted_by = Auth::user()->id;
        $skin->save();
        $skin->delete();

        return redirect()->route('admin.rov.hero.edit', [$hero]);
    }


    public function setting(Request $request)
    {
        if ($request->heroes_free){
            $free   = array_values($request->heroes_free);
        }else{
            $free    = [];
        }
        if ($request->heroes_new){
            $new    = array_values($request->heroes_new);
        }else{
            $new    = [];
        }

        $heroes = DBHero::all();

        foreach ($heroes as $hero){

            if (in_array((string)$hero->id,$free,FALSE)){
                $hero->flag = "free";
            }else{
                $hero->flag = "none";
            }

            if ($new && $new[0]){
                if ((string)$hero->id == $new[0]){
                    $hero->flag = "new";
                }
            }

            $hero->save();
        }

        return response()->json([
            'status'  => true,
            'message' => "Successfully",
        ]);
    }

}
