<?php

namespace App\Http\Controllers\Admin\rov;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelperController extends ApiController
{
    public function setRuneEnchantment($request)
    {
        return [
            'rune1' => (int)$request->rune1 != 0 ? (int)$request->rune1 : null,
            'rune2' => (int)$request->rune2 != 0 ? (int)$request->rune2 : null,
            'rune3' => (int)$request->rune3 != 0 ? (int)$request->rune3 : null,
            'rune4' => (int)$request->rune4 != 0 ? (int)$request->rune4 : null,
            'rune5' => (int)$request->rune5 != 0 ? (int)$request->rune5 : null,
            'rune6' => (int)$request->rune6 != 0 ? (int)$request->rune6 : null,

            'amount_rune1' => (int)$request->rune1 != 0 ? (int)$request->amount_rune1 : null,
            'amount_rune2' => (int)$request->rune2 != 0 ? (int)$request->amount_rune2 : null,
            'amount_rune3' => (int)$request->rune3 != 0 ? (int)$request->amount_rune3 : null,
            'amount_rune4' => (int)$request->rune4 != 0 ? (int)$request->amount_rune4 : null,
            'amount_rune5' => (int)$request->rune5 != 0 ? (int)$request->amount_rune5 : null,
            'amount_rune6' => (int)$request->rune6 != 0 ? (int)$request->amount_rune6 : null,

            'enchantment1' => (int)$request->enchantment1 != 0 ? (int)$request->enchantment1 : null,
            'enchantment2' => (int)$request->enchantment2 != 0 ? (int)$request->enchantment2 : null,
            'enchantment3' => (int)$request->enchantment3 != 0 ? (int)$request->enchantment3 : null,
            'enchantment4' => (int)$request->enchantment4 != 0 ? (int)$request->enchantment4 : null,
            'enchantment5' => (int)$request->enchantment5 != 0 ? (int)$request->enchantment5 : null,
        ];
    }

}
