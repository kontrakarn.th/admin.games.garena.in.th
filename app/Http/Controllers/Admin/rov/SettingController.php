<?php

namespace App\Http\Controllers\Admin\rov;

use App\Http\Controllers\ApiController;
use App\Models\rov\DBSetting;
use Carbon\Carbon;
use Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class SettingController extends ApiController
{

    private $uploadPath = 'rov/mainsite/';

    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $setting = new DBSetting();
        $header_logo = $setting::where('option_name','header_logo')->first()->option_value;
        $header_links = $setting::where('option_name','header_link')->get();
        $ios_download_link = $setting::where('option_name','ios_download_link')->first()->option_value;
        $android_download_link = $setting::where('option_name','android_download_link')->first()->option_value;
        $qr_code = $setting::where('option_name','qr_code')->first()->option_value;
        $facebook_link = $setting::where('option_name','facebook_link')->first()->option_value;
        $instagram_link = $setting::where('option_name','instagram_link')->first()->option_value;
        $twitter_link = $setting::where('option_name','twitter_link')->first()->option_value;
        $footer_section1 = $setting::where('option_name','footer_link')->where('section',1)->get();
        $footer_section2 = $setting::where('option_name','footer_link')->where('section',2)->get();
        $footer_section3 = $setting::where('option_name','footer_link')->where('section',3)->get();

        $data = [
            'header_logo'           => $header_logo,
            'header_links'          => $header_links,
            'ios_download_link'     => $ios_download_link,
            'android_download_link' => $android_download_link,
            'qr_code'               => $qr_code,
            'facebook_link'         => $facebook_link,
            'instagram_link'        => $instagram_link,
            'twitter_link'          => $twitter_link,
            'footer_section1'       => $footer_section1,
            'footer_section2'       => $footer_section2,
            'footer_section3'       => $footer_section3,
        ];


        return view('admin.rov.setting.index',$data);
    }

    public function saveHeader(Request $request)
    {

        $status = false;
        $mesg = null;

        try{
            for ($i = 0;$i < sizeof($request->header_link_type);$i++){
                $header_link = DBSetting::where('option_name','header_link')->where('order',$i+1)->first();
                if ($header_link){
                    $header_link->type = $request->header_link_type[$i];
                    $header_link->option_value = $request->header_link_value[$i];
                    $header_link->section = $request->header_link_section[$i] ?? null;
                    $header_link->updated_at = Carbon::now();
                    $header_link->updated_by = Auth::user()->id;
                    $header_link->save();
                }

            }

            if($request->hasFile('header_logo')) {
                $header_logo = $this->uploadImage(
                    $request->file('header_logo'),
                    $this->uploadPath.'header',
                    'upload',
                    null
                );
                $setting = DBSetting::where('option_name','header_logo')->first();
                $setting->option_value = $header_logo;
                $setting->updated_at = Carbon::now();
                $setting->updated_by = Auth::user()->id;
                $setting->save();
            }

            $status = true;
            $mesg = "Updated Header Settings";
        }catch (\Exception $x){
            $status = false;
            $mesg = $x->getMessage();
        }

        Cache::pull('foothead');

        return response()->json([
            'status'=>$status,
            'message'=>$mesg,
        ]);
    }

    public function saveDownload(Request $request)
    {
        $status = false;
        $mesg = null;

        $validator = Validator::make($request->all(), [
            'ios_download_link' => 'required',
            'android_download_link' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first() ?? "Validate",
            ]);
        }

        $ios_download_link = DBSetting::where('option_name','ios_download_link')->first();
        $ios_download_link->option_value = $request->ios_download_link;
        $ios_download_link->updated_at = Carbon::now();
        $ios_download_link->updated_by = Auth::user()->id;
        $android_download_link = DBSetting::where('option_name','android_download_link')->first();
        $android_download_link->option_value = $request->android_download_link;
        $android_download_link->updated_at = Carbon::now();
        $android_download_link->updated_by = Auth::user()->id;

        if($request->hasFile('qr_code')) {
            $qr_code = $this->uploadImage(
                $request->file('qr_code'),
                $this->uploadPath.'qr_code',
                'upload',
                null
            );
            $setting = DBSetting::where('option_name','qr_code')->first();
            $setting->option_value = $qr_code;
            $setting->updated_at = Carbon::now();
            $setting->updated_by = Auth::user()->id;
            $setting->save();
        }

        try{
            $ios_download_link->save();
            $android_download_link->save();
            $status = true;

            Cache::pull('homepage.ios_download_link');
            Cache::pull('homepage.android_download_link');
            Cache::pull('homepage.qr_code');

            $mesg = "Updated Download Settings";
        }catch (\Exception $x){
            $status = false;
            $mesg = $x->getMessage();
        }

        return response()->json([
            'status'=>$status,
            'message'=>$mesg,
        ]);
    }

    public function saveFooter(Request $request)
    {
        $status = false;
        $mesg = null;

        $validator = Validator::make($request->all(), [
            'facebook_link' => 'required',
            'instagram_link' => 'required',
            'twitter_link' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first() ?? "Validate",
            ]);
        }

        $facebook_link = DBSetting::where('option_name','facebook_link')->first();
        $facebook_link->option_value = $request->facebook_link;
        $facebook_link->updated_at = Carbon::now();
        $facebook_link->updated_by = Auth::user()->id;

        $instagram_link = DBSetting::where('option_name','instagram_link')->first();
        $instagram_link->option_value = $request->instagram_link;
        $instagram_link->updated_at = Carbon::now();
        $instagram_link->updated_by = Auth::user()->id;

        $twitter_link = DBSetting::where('option_name','twitter_link')->first();
        $twitter_link->option_value = $request->twitter_link;
        $twitter_link->updated_at = Carbon::now();
        $twitter_link->updated_by = Auth::user()->id;

        try{
            $facebook_link->save();
            $instagram_link->save();
            $twitter_link->save();

            for ($i = 0;$i < sizeof($request->footer_link_type1);$i++){
                for ($j = 1; $j <= 3;$j++){
                    $footer_link = DBSetting::where('option_name','footer_link')->where('order',$i+1)->where('section',$j)->first();
                    if ($footer_link){
                        $footer_link->type = $request->{'footer_link_type'.$j}[$i];
                        $footer_link->option_value = $request->{'footer_link_value'.$j}[$i];
                        $footer_link->updated_at = Carbon::now();
                        $footer_link->updated_by = Auth::user()->id;
                        $footer_link->save();
                    }
                }
            }

            Cache::pull('foothead');

            $status = true;
            $mesg = "Updated Footer Settings";
        }catch (\Exception $x){
            $status = false;
            $mesg = $x->getMessage();
        }

        return response()->json([
            'status'=>$status,
            'message'=>$mesg,
        ]);
    }
}
