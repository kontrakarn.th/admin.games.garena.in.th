<?php

namespace App\Http\Controllers\Admin\rov;

use App\Http\Controllers\ApiController;
use App\Models\fo4\DBMenu;
use App\Models\rov\DBBanner;
use App\Models\rov\DBCategory;
use App\Models\rov\DBContent;
use App\Models\rov\DBContentTag;
use App\Models\rov\DBTag;
use Carbon\Carbon;
use Cache;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ContentController extends ApiController
{

    private $pagepath       ='admin.rov.content.';
    private $pageheader     ='Content Manager';
    private $uploadPath     = 'rov/content/';
    private $default_share  = 'https://static2.garena.in.th/data/rov/content/content/4d9f4761275c83ab15df75eca81d1765.png';


    function __construct()
    {
        $this->middleware('guest');
        $this->init();
    }

    private function init()
    {
        $this->viewData['pageheader']=$this->pagepath;
//        $this->viewData['breadcrumbs'] = [
//            ['text' => '<i class="fa fa-list-alt"></i> Home','url'=>route('admin.rov.menu.index'),'active'=>''],
//            ['text' => 'User','url'=>'','active'=>''],
//            ['text' => 'Report','url'=>'','active'=>'active']
//        ];
    }

    public function index()
    {
        $this->viewData['parentMenu'] = $parentMenu = DBMenu::getParentMenuData();
        return view($this->pagepath.'manage',$this->viewData);
    }

    public function add()
    {
        $categories = DBCategory::all();
        $tags = DBTag::all();

        $data = [
            'categories'       => $categories,
            'tags'             => $tags,
        ];
        return view($this->pagepath.'add',$data);
    }

    public function edit($content)
    {
        $id = $content;
        $data = DBContent::findOrFail($id);
        $categories = DBCategory::all();

        $data = [
            'categories'       => $categories,
            'data'       => $data,
        ];
        return view($this->pagepath.'add',$data);

    }

    public function saveContent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'slug'              => 'required|max:16',
            'category_id'       => 'required',
            'title'             => 'required',
            'description'       => 'required',
            'content_image'     => 'mimes:jpeg,jpg,gif,png|dimensions:width=1920,height=1080',
            'share_image'       => 'mimes:jpeg,jpg,gif,png|dimensions:width=1200,height=630',
            'show_datetime'     => 'required',
            'hilight'           => 'required',
            'detail'            => 'required',
        ], [
            'content_image.dimensions' => 'ขนาดรูปต้องเป็น 1920 × 1080 เท่านั้น',
            'share_image.dimensions' => 'ขนาดรูป Share ต้องเป็น 1200 × 630 เท่านั้น'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => false,
                'message'   => $validator->messages()->first()
            ]);
        }


        if ($request->id){ // update
            $content = DBContent::find($request->id);
            $content->updated_at = Carbon::now();
            $content->updated_by = Auth::user()->id;
            $content->slug = $request->slug;

            if (!$content){
                return response()->json([
                    'status'    => false,
                    'message'   => "Not found Content",
                ]);
            }
        }else { // create new one

            $content = new DBContent();
            $content->slug = $this->checkSlugExist($request->slug ?? "news-".time());
        }

        $content->title = $request->title ?? "-";
        $content->description = $request->description ?? null;
        $content->category_id = $request->category_id;
        $content->show_datetime = $request->show_datetime ?? Carbon::now();
        $content->hilight = $request->hilight;
        $content->created_by = Auth::user()->id;

        if($request->hasFile('content_image')) {

            $content_image = $this->uploadImage(
                $request->file('content_image'),
                $this->uploadPath.'image',
                'upload',
                null
            );

            $content->content_image = $content_image;
        }

        if($request->hasFile('share_image')) {

            $share_image = $this->uploadImage(
                $request->file('share_image'),
                $this->uploadPath.'image',
                'upload',
                null
            );

            $content->share_image = $share_image;
        }else {
            if (!$request->id) {
                $content->share_image = $this->default_share;
            }
        }

        $content->detail = $request->detail;

        try{
            $content->save();
            $status = true;

            if ($request->tags){
                $content_tags = DBContentTag::where('content_id',$content->id)->forceDelete();
                for ($i = 0; $i < sizeof($request->tags);$i++){
                    $tag = $request->tags[$i];
                    $check = DBTag::where('name',$tag)->first();
                    if (!$check){
                        $check = new DBTag();
                        $check->name = $tag;
                        $check->slug = DBTag::translate($tag);
                        $check->created_by = Auth::user()->id;
                        $check->save();
                    }

                    $content_tag = new DBContentTag();
                    $content_tag->content_id = $content->id;
                    $content_tag->tag_id = $check->id;
                    $content_tag->created_by = Auth::user()->id;
                    $content_tag->save();
                }
            }


            if ($request->id) {
                $mesg = "Updated Content";
            }else{
                $mesg = "Created Content";
            }
        }catch (\Exception $x){
            $status = false;
            $mesg = $x->getMessage();
        }

        $this->clearCache();

        return response()->json([
            'status'    => $status,
            'message'   => $mesg,
        ]);
    }


    public function datatable()
    {
        $contents = DBContent::get();
        $datatables = Datatables::of($contents);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('image', function($_model) {
            return '<a href="'.route('admin.rov.content.edit', ['id' => $_model->id]) . '"><img src="' . $_model->content_image . '" class="img-responsive" width="150px"></a>';
        });
        $datatables->editColumn('title', function($_model) {
            return '<h6><b>'.$_model->title.'</b><br><small>'.$_model->description.'</small></h6>';
        });
        $datatables->editColumn('category', function($_model) {
            return '<p>'.$_model->category_name.'</p>';
        });
        $datatables->editColumn('show_datetime', function($_model) {
            return Carbon::parse($_model->show_datetime)->format("d M Y");
        });
        $datatables->editColumn('view', function($_model) {
            return '<i class="icon-eye"></i> '.$_model->view;
        });
        $datatables->editColumn('status', function($_model){

            if($_model->status === 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.url('https://rov.in.th'.$_model->url).'" target="_blank"><i class="icon-link"></i> View</a></li>
                        <li><a href="' . route('admin.rov.content.edit', ['id' => $_model->id]) . '"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        // dd($menu->get()->toArray());
        return $datatables->rawColumns(['id','image','title','category','status','hilight','view','action'])->make(true);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $updated = DBContent::updateStatus($id,$status);

        $this->clearCache();

        if ($updated) {
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => 'Status Not Updated'
        ]);
    }

    public function saveImage(Request $request)
    {
        $slug = $request->txt_slug;
        #upload image
        if($request->hasFile('file')) {
            $image = $this->uploadImage(
                $request->file('file'),
                $this->uploadPath.'content'.$slug,
                'upload',
                null
            );
        }else{
            return response()->json([
                'status'=>false,
                'message'=>'No File',
            ]);
        }

        return response()->json([
            'status'=>true,
            'message'=>$image,
        ]);
    }

    public function checkSlugExist($slug)
    {
        $check = DBContent::whereSlug($slug)->first();
        if ($check){
            return substr($slug."-".time(),0,16);
        }
        return $slug;
    }

    public function deleteContent(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = DBContent::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found content.",
            ]);
        }

        $contenttags = DBContentTag::where('content_id',$check->id)->delete();


        try{
            $check->delete();
            $this->clearCache();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }

//    public function tran()
//    {
//        return DBTag::translate("ตัวแทน");
//    }

    public function clearCache()
    {
//        Cache::put('test', 'value', 1440);
        Cache::tags('news')->flush();
    }

}
