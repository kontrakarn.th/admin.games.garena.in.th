<?php

namespace App\Http\Controllers\Admin\rov;

use App\Http\Controllers\ApiController;
use App\Models\rov\DBTournament;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Cache;
use Validator;
use Illuminate\Http\Request;

class TourController extends ApiController
{
    private $pagepath='admin.rov.tournament.';
    private $uploadPath = 'rov/tour/';

    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $actives = DBTournament::where('status','active')->take(4)->get();
        $inactives = DBTournament::where('status','inactive')->get();
        return view('admin.rov.tournament.index',[
            'actives'       => $actives,
            'inactives'     => $inactives,
        ]);
    }

    public function add()
    {
        return view('admin.rov.tournament.add');
    }

    public function saveTour(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'             => 'required',
            'logo_image'        => 'required',
            'tour_image'        => 'required',
            'status_tour'       => 'required',
        ]);

        if ($request->id) {
            $tour = DBTournament::find($request->id);
            if (!$tour){
                return response()->json([
                    'status'    => false,
                    'message'   => $validator->messages()->first()
                ]);
            }
            $tour->updated_by   = Auth::user()->id;
            $tour->updated_at   = Carbon::now();

        }else{
            if ($validator->fails()) {
                return response()->json([
                    'status'    => false,
                    'message'   => $validator->messages()->first()
                ]);
            }
            $tour = new DBTournament();
            $tour->created_by   = Auth::user()->id;
            $tour->created_at   = Carbon::now();
            $tour->status       = "inactive";
        }
        $tour->title            = $request->title;
        $tour->status_tour      = $request->status_tour ?? "comingsoon";
        $tour->link             = $request->link ?? null;

        if ($request->hasFile('logo_image')) {
            $logo_image = $this->uploadImage(
                $request->file('logo_image'),
                $this->uploadPath . 'logo',
                'upload',
                null
            );
            $tour->logo_image   = $logo_image;
        }

        if ($request->hasFile('tour_image')) {
            $tour_image = $this->uploadImage(
                $request->file('tour_image'),
                $this->uploadPath . 'bg',
                'upload',
                null
            );
            $tour->tour_image   = $tour_image;
        }

        try {
            $tour->save();
            $this->clearCache();

            return response()->json([
                'status'        => true,
                'message'       => "Updated",
            ]);
        }
        catch (\Exception $x){
            return response()->json([
                'status'        => false,
                'message'       => $x->getMessage(),
            ]);
        }
    }

    public function edit(DBTournament $tour)
    {
        return view('admin.rov.tournament.add',[
            'data'      => $tour,
        ]);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $updated = DBTournament::updateStatus($id, $status);

        if ($updated) {
            $this->clearCache();
            return response()->json([
                'status' => true,
                'message' => 'Status Updated'
            ]);
        }
        return response()->json([
            'status' => false,
            'message' => 'There four tournaments that is already active.'
        ]);
    }

    public function clearCache()
    {
        Cache::pull('homepage.tours');
    }
}
