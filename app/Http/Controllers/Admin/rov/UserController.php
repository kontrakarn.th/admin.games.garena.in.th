<?php

namespace App\Http\Controllers\Admin\rov;

use App\Models\rov\DBContent;
use App\Models\rov\DBHero;
use App\Models\rov\DBUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{

    private $pagepath='admin.rov.user.';
    private $pageheader='User Registration';
    private $uploadPath = 'rov/user/';


    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $user               = new DBUser();
        $all_count          = $user->count();
        $regis_count        = $user->where('email','!=',null)->count();
        $verify_count       = $user->where('verified',true)->count();
        $received_count     = $user->where('received_register','!=',null)->count();


        if (\request()->has('search')){
            $users          = $user->where('open_id',\request()->get('search'))->paginate(50);
        }else{
            $users          = $user->orderBy('created_at','desc')->paginate(50);
        }



        return view('admin.rov.user.index',[
            'all_count'         => $all_count,
            'regis_count'       => $regis_count,
            'verify_count'      => $verify_count,
            'received_count'    => $received_count,
            'users'             => $users,
        ]);
    }

    public function datatable(Request $request)
    {
        $users = DBUser::orderBy('created_at','desc')->take(5000)->get();
        $datatables = Datatables::of($users);

        $datatables->editColumn('created_at', function($_model) {
            return "<small>".Carbon::parse($_model->created_at)->setTimezone('Asia/Bangkok')."</small>";
        });

        $datatables->editColumn('profile_image', function($_model) {
            return "<img src='$_model->profile_image' width='50px' class='img-circle'>";
        });

        $datatables->editColumn('game_name', function($_model) {
            return "<p><b>Game name : $_model->game_name_decode</b>
                    <br><small class='text-muted'>Open ID : $_model->open_id</small>
                    <br><small class='text-muted'>Tencent ID : $_model->t_open_id</small>
                    </p>";
        });

        $datatables->editColumn('verified', function($_model) {
            if ($_model->verified === true){
                return "<p class='text-success'><i class='icon-check'></i> Verified</p>";
            }
            else{
                return "<p class='text-danger'>Not Verify</p>";
            }
        });

        $datatables->editColumn('firstname', function($_model) {

            if (!$_model->firstname){
                return "<small class='text-muted'>ยังไม่ลงทะเบียน</small>";
            }
            return "$_model->firstname $_model->lastname";
        });

        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route("admin.rov.user.userDetail",[$_model]).'"><i class="icon-pencil7"></i> Detail</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });


        // dd($menu->get()->toArray());
        return $datatables->rawColumns(['created_at','profile_image','game_name','verified','firstname','action'])->make(true);

    }

    public function userDetail(DBUser $user)
    {
        return view('admin.rov.user.detail',[
            'user'  => $user,
        ]);
    }



}
