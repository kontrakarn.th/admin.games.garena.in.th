<?php

namespace App\Http\Controllers\Admin\codm;

use App\Http\Controllers\ApiController;
use App\Models\codm\Setting;
use App\Models\rov\DBSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SettingController extends ApiController
{
    function __construct()
    {
        $this->middleware('guest');
    }


    public function index()
    {

        $data = [
            'logo_image'            => Setting::whereOptionName('logo_image')->select('option_value')->first()->option_value,
            'logo_image_mobile'     => Setting::whereOptionName('logo_image_mobile')->select('option_value')->first()->option_value,
            'header_links'          => Setting::whereOptionName('header_links')->select('option_type', 'option_value')->get(),
            'ios_download_link'     => Setting::whereOptionName('ios_download_link')->select('option_value')->first()->option_value,
            'android_download_link' => Setting::whereOptionName('android_download_link')->select('option_value')->first()->option_value,
            'qr_code'               => Setting::whereOptionName('qr_code')->select('option_value')->first()->option_value,
            'fb_link'               => Setting::whereOptionName('fb_link')->select('option_value')->first()->option_value,
            'yt_link'               => Setting::whereOptionName('yt_link')->select('option_value')->first()->option_value,
            'ig_link'               => Setting::whereOptionName('ig_link')->select('option_value')->first()->option_value,
            'footer_section1'       => Setting::whereOptionName('footer_links')->whereOptionSection(1)->select('option_type', 'option_value')->get(),
            'footer_section2'       => Setting::whereOptionName('footer_links')->whereOptionSection(2)->select('option_type', 'option_value')->get(),
        ];


        return view('admin.codm.setting.index', $data);
    }

    public function saveHeader(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'logo_image' => 'mimes:jpeg,jpg,gif,png|max:1024',
        ], [
            'logo_image.mimes' => 'ต้องเป็นรูปภาพเท่านั้น',
            'logo_image.size'  => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        $setting = Setting::whereOptionName('logo_image')->first();

        if ($request->hasFile('logo_image')) {
            $header_logo           = $this->uploadImage(
                $request->file('logo_image'),
                'codmth/main',
                'upload',
                null
            );
            $setting->option_value = $header_logo;
        }

        $setting_mobile = Setting::whereOptionName('logo_image_mobile')->first();

        if ($request->hasFile('logo_image_mobile')) {
            $header_logo           = $this->uploadImage(
                $request->file('logo_image_mobile'),
                'codmth/main',
                'upload',
                null
            );
            $setting_mobile->option_value = $header_logo;
        }

        for ($i = 0; $i < sizeof($request->header_link_type); $i++) {
            $header_link = Setting::whereOptionName('header_links')
                ->whereOptionOrder($i + 1)
                ->first();

            if ($header_link) {
                $header_link->option_type  = $request->header_link_type[$i];
                $header_link->option_value = $request->header_link_value[$i];
                $header_link->save();
            }

        }

        try {
            $setting->save();
            $setting_mobile->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }


    }


    public function saveDownload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ios_download_link'         => 'required|max:255',
            'android_download_link'     => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        $setting_ios = Setting::whereOptionName('ios_download_link')->first();
        $setting_ios->option_value = $request->ios_download_link;
        $setting_and = Setting::whereOptionName('android_download_link')->first();
        $setting_and->option_value = $request->android_download_link;
        $setting_qrc = Setting::whereOptionName('qr_code')->first();

        if ($request->hasFile('qr_code')) {
            $qr_code = $this->uploadImage(
                $request->file('qr_code'),
                'codmth/main',
                'upload',
                null
            );
            $setting_qrc->option_value = $qr_code;
        }

        try {
            $setting_ios->save();
            $setting_and->save();
            $setting_qrc->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }
    public function saveFooter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fb_link'         => 'required|max:255',
            'yt_link'         => 'required|max:255',
            'ig_link'         => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        $setting_fb = Setting::whereOptionName('fb_link')->first();
        $setting_fb->option_value = $request->fb_link;
        $setting_yt = Setting::whereOptionName('yt_link')->first();
        $setting_yt->option_value = $request->yt_link;
        $setting_ig = Setting::whereOptionName('ig_link')->first();
        $setting_ig->option_value = $request->ig_link;

        for ($i = 0; $i < sizeof($request->footer_link_type1); $i++) {
            $footer_link = Setting::whereOptionName('footer_links')
                ->whereOptionOrder($i+1)
                ->whereOptionSection(1)
                ->first();

            if ($footer_link) {
                $footer_link->option_type  = $request->footer_link_type1[$i];
                $footer_link->option_value = $request->footer_link_value1[$i];
                $footer_link->save();
            }

        }

        for ($i = 0; $i < sizeof($request->footer_link_type2); $i++) {
            $footer_link = Setting::whereOptionName('footer_links')
                ->whereOptionOrder($i+1)
                ->whereOptionSection(2)
                ->first();

            if ($footer_link) {
                $footer_link->option_type  = $request->footer_link_type2[$i];
                $footer_link->option_value = $request->footer_link_value2[$i];
                $footer_link->save();
            }

        }

        try {
            $setting_fb->save();
            $setting_yt->save();
            $setting_ig->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }


}
