<?php

namespace App\Http\Controllers\Admin\codm;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\codm\Playlist;
use App\Models\codm\Slug;
use App\Models\codm\Video;
use Exception;
use Illuminate\Support\Facades\Validator;
use PDOException;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class TVController extends ApiController
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $slugs = Slug::all();
            return view('admin.codm.tv.index', compact('slugs'));
        } catch (PDOException $e) {
            return response()->json($e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.codm.tv.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'slug' => 'required|unique:codmtv.codmtv_slugs,slug,NULL,id,deleted_at,NULL|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        try {
            DB::beginTransaction();
            $slug = new Slug();
            $slug->slug = $request->slug;
            $slug->is_default = 0;
            $slug->status = 'inactive';
            $slug->save();
            DB::commit();

            return redirect()->route('admin.codm.tv.index');
        } catch (PDOException $e) {
            DB::rollBack();
            return redirect()->back()
            ->withErrors($e->getMessage())
            ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slug  =  Slug::find($id);
        if ($slug->hasMain()->get()->count() < 1) {
            return view('admin.codm.tv.show', compact('slug'));
        } else {
            $main = $slug->hasMain()->first();
           
            return view('admin.codm.tv.show', compact(['slug', 'main']));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slug = Slug::find($id);
        return view('admin.codm.tv.edit', compact('slug'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'slug' => 'required|unique:codmtv.codmtv_slugs,slug,NULL,id,deleted_at,NULL|max:255',
            ]);
    
            if ($validator->fails()) {
                return response()->json([
                        'status' => false,
                        'message' => 'Slug is already used.'
                ]);
            }
    
            DB::beginTransaction();
                    
            $slug = Slug::find($request->slug_id);
            $slug->slug = $request->slug;
            $slug->save();

         
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'success'
            ]);
        } catch (PDOException $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $slug = Slug::find($request->id);
            if ($slug->is_default == 1) {
                return response()->json([
                    'status' => false,
                    'message' => 'this is default slug.'
                ]);
            }
            $slug->delete();
            
            return response()->json([
                    'status' => true,
                    'message' => 'success'
                ]);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function setDefault(Request $request)
    {
        try {
            $slug = Slug::find($request->id);
            $default = Slug::where('is_default', 1)->first();
            if ($default) {
                $default->is_default = 0;
                $default->save();
            }

            $slug->is_default = 1;
            $slug->save();
            
            return response()->json(['status'=>true, 'message' => 'success']);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function active(Request $request)
    {
        try {
            $slug = Slug::find($request->id);

            if (count($slug->videos) < 1 || count($slug->playlists) < 1) {
                return response()->json([
                    'status' => false,
                    'message' => 'This slug dont\'t have vdo or playlist.'
                ]);
            }

            if ($slug->hasMain()->get()->count() < 1) {
                return response()->json([
                    'status' => false,
                    'message' => 'This slug dont\'t have main vdo.'
                ]);
            }
            
            if ($request->status == "true") {
                $slug->status = "active";
            } else {
                if ($slug->is_default == 1) {
                    return response()->json([
                        'status' => false,
                        'message' => 'This is default slug.'
                    ]);
                }
                $slug->status = "inactive";
            }
    
            $slug->save();
            
            return response()->json([
                    'status' => true,
                    'message' => 'Success'
                ]);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function activeVdo(Request $request)
    {
        try {
            $vdo = Video::find($request->id);

          
            if ($request->status == "true") {
                $vdo->status = 1;
            } else {
                if ($vdo->is_main == 1) {
                    return response()->json([
                        'status' => false,
                        'message' => 'This is main vdo.'
                    ]);
                }
                $vdo->status = 0;
            }
    
            $vdo->save();
            
            return response()->json([
                    'status' => true,
                    'message' => 'success'
                ]);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function activePlaylist(Request $request)
    {
        try {
            $playlist = Playlist::find($request->id);
            
            if ($request->status == "true") {
                $playlist->status = 1;
            } else {
                if ($playlist->is_default == 1) {
                    return response()->json([
                        'status' => false,
                        'message' => 'this is default slug.'
                    ]);
                }
                $playlist->status = 0;
            }
    
            $playlist->save();
            
            return response()->json([
                    'status' => true,
                    'message' => 'success'
                ]);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
    
    public function datatable()
    {
        $slugs = Slug::all();

        $datatables = Datatables::of($slugs);

        $datatables->editColumn('id', function ($_model) {
            return $_model->id;
        });
        $datatables->editColumn('slug', function ($_model) {
            return $_model->slug;
        });

        $datatables->editColumn('is_default', function ($_model) {
          
            if ($_model->is_default == 1) {
                $_action = '<button class="btn btn-success">default</button>';
            } elseif ($_model->status == "active") {
                $_action = '<button class="btn btn-primary" onclick="setDefault('.$_model->id.')">set as default</button>';
            } else {
                $_action = '';
            }
            return $_action;
        });

        $datatables->editColumn('status', function ($_model) {
            if ($_model->status == 'active') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.codm.tv.show', ['id' => $_model->id]) . '" target="_self"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="del(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        return $datatables->rawColumns(["is_default", "slug", "status", "action"])->make(true);
    }

    public function deleteVdo(Request $request)
    {
        try {
            $vdo = Video::find($request->id);
            if ($vdo->is_main == 1) {
                return response()->json([
                    'status' => false,
                    'message' => 'this is main vdo.'
                ]);
            }
            $vdo->delete();
            
            return response()->json([
                    'status' => true,
                    'message' => 'success'
                ]);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function deletePlaylist(Request $request)
    {
        try {
            $playlist = Playlist::find($request->id);
            $playlist->delete();
            if ($playlist->is_main == 1) {
                return response()->json([
                    'status' => false,
                    'message' => 'this is main vdo.'
                ]);
            }
            
            return response()->json([
                    'status' => true,
                    'message' => 'success'
                ]);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function datatableVdo(Request $request)
    {

        $vdos = Video::where('slug_id', $request->slug_id)
                ->where('is_main', '<>', 1)
                ->OrderBy('created_at', 'desc')->get();

        $datatables = Datatables::of($vdos);

        $datatables->editColumn('slug', function ($_model) {
            return $_model->slug->slug;
        });
        $datatables->editColumn('title', function ($_model) {
            return $_model->title;
        });

    
        $datatables->editColumn('status', function ($_model) {
            if ($_model->status == 1) {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });
        
        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.codm.tv.vdo.edit', ['id' => $_model->id]) . '" target="_self"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteVdo(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        return $datatables->rawColumns(["slug", "title", "status", "action"])->make(true);
    }


    public function datatablePlaylist(Request $request)
    {

        $playlists = Playlist::where('slug_id', $request->slug_id)
        ->OrderBy('created_at', 'desc')->get();

        $datatables = Datatables::of($playlists);

        $datatables->editColumn('slug', function ($_model) {
            return $_model->slug->slug;
        });
        $datatables->editColumn('title', function ($_model) {
            return $_model->title;
        });

   
        $datatables->editColumn('status', function ($_model) {

            if ($_model->status == 1) {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.codm.tv.playlist.edit', ['id' => $_model->id]) . '" target="_self"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deletePlaylist(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        return $datatables->rawColumns(["slug", "title", "is_main", "status", "action"])->make(true);
    }

    public function setMainVDO(Request $request)
    {
        try {
            $vdo = Video::find($request->id);
            $default = Video::where('is_main', 1)
                        ->where('slug_id', $vdo->slug_id)
                        ->first();
            if ($default) {
                $default->is_main = 0;
                $default->save();
            }

            $vdo->is_main = 1;
            $vdo->save();
            
            return response()->json(['status'=>true, 'message' => 'success']);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function setMainPlaylist(Request $request)
    {
        try {
            $playlist = Playlist::find($request->id);
            $default = Playlist::where('is_main', 1)
                        ->where('slug_id', $playlist->slug_id)
                        ->first();
            if ($default) {
                $default->is_main = 0;
                $default->save();
            }

            $playlist->is_main = 1;
            $playlist->save();
            
            return response()->json(['status'=>true, 'message' => 'success']);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function storePlaylist(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'slug_id' => 'required',
                'playlist_url' => 'required',
                'playlist_title' => 'required|max:255',
                'playlist_thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                'playlist_status' => 'required'
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            if ($request->hasFile('playlist_thumbnail')) {
                $playlist = new Playlist();
                $playlist->title = $request->playlist_title;
                $playlist->url =  $request->playlist_url;
                $playlist->status = $request->playlist_status;
                //$vdo->thumbnail = 'https://static2.garena.in.th/data/codmth/tv/3feca50141f8981ced287bfa3f645a6cpng';
                $playlist->thumbnail = $this->uploadImage(
                    $request->playlist_thumbnail,
                    'codmth/tv',
                    'upload',
                    null
                );
                $playlist->slug_id = $request->slug_id;
                $playlist->save();
            
                return redirect()->route('admin.codm.tv.show', ['id' => $request->slug_id]);
            }
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function storeVdo(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'slug_id' => 'required',
                'vdo_url' => 'required',
                'vdo_title' => 'required|max:255',
                'vdo_thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                'vdo_status' => 'required'
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            
            if ($request->hasFile('vdo_thumbnail')) {
                $vdo = new Video();
                $vdo->title = $request->vdo_title;
                $vdo->url =  $request->vdo_url;
                $vdo->status = $request->vdo_status;
                $vdo->is_main = 0;
                //$vdo->thumbnail = 'https://static2.garena.in.th/data/codmth/tv/3feca50141f8981ced287bfa3f645a6cpng';
                $vdo->thumbnail = $this->uploadImage(
                    $request->vdo_thumbnail,
                    'codmth/tv',
                    'upload',
                    null
                );
                $vdo->slug_id = $request->slug_id;
                $vdo->save();
            
                return redirect()->route('admin.codm.tv.show', ['id' => $request->slug_id]);
            }
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function editVdo(Request $request)
    {
        $vdo = Video::find($request->id);
        return view('admin.codm.tv.edit-vdo', compact('vdo'));
    }

    public function editPlaylist(Request $request)
    {
        $playlist = Playlist::find($request->id);
        return view('admin.codm.tv.edit-playlist', compact('playlist'));
    }

    public function updateVdo(Request $request)
    {
        try {
            $vdo = Video::find($request->id);
            $vdo->title = $request->vdo_title;
            $vdo->url = $request->vdo_url;
            $vdo->status = $request->vdo_status;

            if ($request->hasFile('vdo_thumbnail')) {
                $vdo->thumbnail = $this->uploadImage(
                    $request->vdo_thumbnail,
                    'codmth/tv',
                    'upload',
                    null
                );
            }

            $vdo->save();

            return redirect()->route('admin.codm.tv.show', ['id' => $vdo->slug_id]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updatePlaylist(Request $request)
    {
        try {
           // return $request;
            $playlist = Playlist::find($request->id);
            $playlist->title = $request->playlist_title;
            $playlist->url = $request->playlist_url;
            $playlist->status = $request->playlist_status;

            if ($request->hasFile('playlist_thumbnail')) {
                $playlist->thumbnail = $this->uploadImage(
                    $request->playlist_thumbnail,
                    'codmth/tv',
                    'upload',
                    null
                );
            }
           
            $playlist->save();

            return redirect()->route('admin.codm.tv.show', ['id' => $playlist->slug_id]);
        } catch (Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function saveMainVdo(Request $request)
    {
    
        try {
            if (!isset($request->main_id)) {
                $validator = Validator::make($request->all(), [
                    'slug_id' => 'required',
                    'main_url' => 'required',
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'slug_id' => 'required',
                    'main_url' => 'required',
                ]);
            }
            if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
           
            if (isset($request->main_id)) {
                $vdo = Video::find($request->main_id);
                if (!is_null($request->main_url)) {
                    $vdo->url = $request->main_url;
                }
                $vdo->save();

                return redirect()->route('admin.codm.tv.show', ['id' => $request->slug_id, 'main' => $vdo]);
            }

                $vdo = new Video();
                $vdo->url =  $request->main_url;
                $vdo->title = 'Main';
                $vdo->status = 1;
                $vdo->is_main = 1;
             
                $vdo->slug_id = $request->slug_id;
                $vdo->save();
            
                return redirect()->route('admin.codm.tv.show', ['id' => $request->slug_id, 'main' => $vdo]);
        } catch (PDOException $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
