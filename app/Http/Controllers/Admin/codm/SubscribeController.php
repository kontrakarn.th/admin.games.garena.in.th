<?php

namespace App\Http\Controllers\Admin\codm;

use App\Http\Controllers\ApiController;
use App\Models\codm\Subscribe;
use App\Models\rov\DBSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class SubscribeController extends ApiController
{
    function __construct()
    {
        $this->middleware('guest');
    }


    public function index()
    {
        $subscribe = Subscribe::orderBy('id', 'desc')->first();

        $data;

        if (isset($subscribe) === false) {
            $data = [
                'main_title' => '',
                'main_description' => '',
                'main_link_youtube' => '',

                'video1_image' => '',
                'video1_promote' => '',
                'video1_title' => '',
                'video1_description' => '',
                'video1_youtube' => '',

                'video2_image' => '',
                'video2_promote' => '',
                'video2_title' => '',
                'video2_description' => '',
                'video2_youtube' => '',

                'video3_image' => '',
                'video3_promote' => '',
                'video3_title' => '',
                'video3_description' => '',
                'video3_youtube' => '',
            ];
        } else {
            $data = [
                'main_title' => $subscribe->main_title,
                'main_description' => $subscribe->main_description,
                'main_link_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->main_link_youtube),

                'video1_image' => $subscribe->video1_image,
                'video1_promote' => $subscribe->video1_promote,
                'video1_title' => $subscribe->video1_title,
                'video1_description' => $subscribe->video1_description,
                'video1_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->video1_youtube),

                'video2_image' => $subscribe->video2_image,
                'video2_promote' => $subscribe->video2_promote,
                'video2_title' => $subscribe->video2_title,
                'video2_description' => $subscribe->video2_description,
                'video2_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->video2_youtube),

                'video3_image' => $subscribe->video3_image,
                'video3_promote' => $subscribe->video3_promote,
                'video3_title' => $subscribe->video3_title,
                'video3_description' => $subscribe->video3_description,
                'video3_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->video3_youtube),
            ];
        }

        return view('admin.codm.subscribe.index', $data);
    }

    public function saveHeader(Request $request)
    {



        $validator = Validator::make($request->all(), [
            'slug' => 'required|max:255',
            'main_title'         => 'required|max:255',
            'main_description'     => 'required|max:500',
            'main_link_youtube'     => 'required|max:255',

            'video1_promote'     => 'required|max:100',
            'video1_title'     => 'required|max:100',
            'video1_description'     => 'required|max:255',
            'video1_youtube'     => 'required|max:255',

            'video2_promote'     => 'required|max:100',
            'video2_title'     => 'required|max:100',
            'video2_description'     => 'required|max:255',
            'video2_youtube'     => 'required|max:255',

            'video3_promote'     => 'required|max:100',
            'video3_title'     => 'required|max:100',
            'video3_description'     => 'required|max:255',
            'video3_youtube'     => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id_subscribe) {
            $newSubscribe = Subscribe::where('id',$request->id_subscribe)->first();
        } else {
            $newSubscribe = new Subscribe;
            $newSubscribe->status = "INACTIVE";

            $validator = Validator::make($request->all(), [
                'logo_image_mobile1' => 'required',
                'logo_image_mobile2' => 'required',
                'logo_image_mobile3' => 'required',
            ], [
                'logo_image_mobile1.required' => 'กรุณาอัพโหลด Thumbnail Video 1',
                'logo_image_mobile2.required' => 'กรุณาอัพโหลด Thumbnail Video 2',
                'logo_image_mobile3.required' => 'กรุณาอัพโหลด Thumbnail Video 3',
            ]);


            if ($validator->fails()) {
                return response()->json([
                    'status'  => false,
                    'message' => $validator->messages()->first(),
                ]);
            }
        }

        $newSubscribe->main_title = $request->main_title;
        $newSubscribe->main_description = $request->main_description;
        $newSubscribe->main_link_youtube = "https://www.youtube.com/embed/" . $request->main_link_youtube;
        $newSubscribe->slug = $request->slug;

        if (isset($subscribe->video1_image) === true) {
            $newSubscribe->video1_image = $request->video1_image;
        }

        $newSubscribe->video1_promote = $request->video1_promote;
        $newSubscribe->video1_title = $request->video1_title;
        $newSubscribe->video1_description = $request->video1_description;
        $newSubscribe->video1_youtube = "https://www.youtube.com/embed/" . $request->video1_youtube;


        if (isset($subscribe->video2_image) === true) {
            $newSubscribe->video2_image = $request->video2_image;
        }

        $newSubscribe->video2_promote = $request->video2_promote;
        $newSubscribe->video2_title = $request->video2_title;
        $newSubscribe->video2_description = $request->video2_description;
        $newSubscribe->video2_youtube = "https://www.youtube.com/embed/" . $request->video2_youtube;


        if (isset($subscribe->video3_image) === true) {
            $newSubscribe->video3_image = $request->video3_image;
        }

        $newSubscribe->video3_promote = $request->video3_promote;
        $newSubscribe->video3_title = $request->video3_title;
        $newSubscribe->video3_description = $request->video3_description;
        $newSubscribe->video3_youtube = "https://www.youtube.com/embed/" . $request->video3_youtube;

        if ($request->hasFile('logo_image_mobile1')) {
            $validator = Validator::make($request->all(), [
                'logo_image_mobile1' => 'mimes:jpeg,jpg,gif,png|max:1024',
            ], [
                'logo_image_mobile1.mimes' => 'ต้องเป็นรูปภาพเท่านั้น',
                'logo_image_mobile1.size'  => 'ขนาดรูปห้ามเกิน 1 MB',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'  => false,
                    'message' => $validator->messages()->first(),
                ]);
            }

            $image = $this->uploadImage(
                $request->file('logo_image_mobile1'),
                'codmth/subscribe',
                'upload',
                null
            );

            $newSubscribe->video1_image = $image;
        }

        if ($request->hasFile('logo_image_mobile2')) {
            $validator = Validator::make($request->all(), [
                'logo_image_mobile2' => 'mimes:jpeg,jpg,gif,png|max:1024',
            ], [
                'logo_image_mobile2.mimes' => 'ต้องเป็นรูปภาพเท่านั้น',
                'logo_image_mobile2.size'  => 'ขนาดรูปห้ามเกิน 1 MB',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'  => false,
                    'message' => $validator->messages()->first(),
                ]);
            }

            $image = $this->uploadImage(
                $request->file('logo_image_mobile2'),
                'codmth/subscribe',
                'upload',
                null
            );

            $newSubscribe->video2_image = $image;
        }

        if ($request->hasFile('logo_image_mobile3')) {
            $validator = Validator::make($request->all(), [
                'logo_image_mobile3' => 'mimes:jpeg,jpg,gif,png|max:1024',
            ], [
                'logo_image_mobile3.mimes' => 'ต้องเป็นรูปภาพเท่านั้น',
                'logo_image_mobile3.size'  => 'ขนาดรูปห้ามเกิน 1 MB',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'  => false,
                    'message' => $validator->messages()->first(),
                ]);
            }

            $image = $this->uploadImage(
                $request->file('logo_image_mobile3'),
                'codmth/subscribe',
                'upload',
                null
            );

            $newSubscribe->video3_image = $image;
        }

        try {
            $newSubscribe->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }


    }


    public function saveDownload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ios_download_link'         => 'required|max:255',
            'android_download_link'     => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        $setting_ios = Setting::whereOptionName('ios_download_link')->first();
        $setting_ios->option_value = $request->ios_download_link;
        $setting_and = Setting::whereOptionName('android_download_link')->first();
        $setting_and->option_value = $request->android_download_link;
        $setting_qrc = Setting::whereOptionName('qr_code')->first();

        if ($request->hasFile('qr_code')) {
            $qr_code = $this->uploadImage(
                $request->file('qr_code'),
                'codmth/main',
                'upload',
                null
            );
            $setting_qrc->option_value = $qr_code;
        }

        try {
            $setting_ios->save();
            $setting_and->save();
            $setting_qrc->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }
    public function saveFooter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fb_link'         => 'required|max:255',
            'yt_link'         => 'required|max:255',
            'ig_link'         => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        $setting_fb = Setting::whereOptionName('fb_link')->first();
        $setting_fb->option_value = $request->fb_link;
        $setting_yt = Setting::whereOptionName('yt_link')->first();
        $setting_yt->option_value = $request->yt_link;
        $setting_ig = Setting::whereOptionName('ig_link')->first();
        $setting_ig->option_value = $request->ig_link;

        for ($i = 0; $i < sizeof($request->footer_link_type1); $i++) {
            $footer_link = Setting::whereOptionName('footer_links')
                ->whereOptionOrder($i+1)
                ->whereOptionSection(1)
                ->first();

            if ($footer_link) {
                $footer_link->option_type  = $request->footer_link_type1[$i];
                $footer_link->option_value = $request->footer_link_value1[$i];
                $footer_link->save();
            }

        }

        for ($i = 0; $i < sizeof($request->footer_link_type2); $i++) {
            $footer_link = Setting::whereOptionName('footer_links')
                ->whereOptionOrder($i+1)
                ->whereOptionSection(2)
                ->first();

            if ($footer_link) {
                $footer_link->option_type  = $request->footer_link_type2[$i];
                $footer_link->option_value = $request->footer_link_value2[$i];
                $footer_link->save();
            }

        }

        try {
            $setting_fb->save();
            $setting_yt->save();
            $setting_ig->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }


    public function create()
    {

        return view("admin.codm.subscribe.manage");
    }


    public function datatable(Request $request)
    {
        // $menu = DBMenu::where('type','content')->get();
        $subscribe = Subscribe::get();
        // $menu = Auth::user();
        // dd($menu);

        $datatables = Datatables::of($subscribe);

        $datatables->editColumn('id', function($_model) {
            return $_model->id;
        });
        $datatables->editColumn('slug', function($_model) {
            return $_model->slug;
        });
        $datatables->editColumn('main_title', function($_model) {
            return $_model->main_title;
        });
        $datatables->editColumn('video1_title', function($_model) {
//            return '<a href="' . route('admin.fo4.banner.edit', ['id' => $_model->id]) . '" target="_blank">' . $_model->alt . '</a>';
            return $_model->video1_title;
        });
        $datatables->editColumn('video2_title', function($_model) {
//            return '<a href="' . $_model->url . '" target="_blank">' . $_model->url . '</a>';
            return $_model->video2_title;
        });
        $datatables->editColumn('video3_title', function($_model) {
//            return '<img src="' . $_model->banner_image . '" class="img-responsive" alt="'.$_model->alt.'">';
            return $_model->video3_title;
        });
        $datatables->editColumn('set_default', function($_model) {
            if ($_model->status_default === "default") {
                $_action = "<button class='btn btn-success'>default</button>";
            } elseif ($_model->status == "ACTIVE"){
                $_action = "<button class='btn btn-primary' onclick='setDefault($_model->id)'>set as default</button>";
            } else {
                $_action = "";
            }

            return $_action;

        });
        $datatables->editColumn('status', function($_model){

            if($_model->status == 'ACTIVE') {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="' . $_model->id . '" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;

        });
        $datatables->editColumn('action', function($_model) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="' . route('admin.codm.subscribe.edit', ['id' => $_model->id]) . '" target="_blank"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner(' . $_model->id . ')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });

        // dd($menu->get()->toArray());
        return $datatables->rawColumns(["main_title","set_default", "slug", "video1_title", "video2_title", "video3_title","status", "action"])->make(true);

    }


    public function updateStatus(Request $request)
    {

        $subscribe = Subscribe::where('id' , $request->id)->first();
        if ($request->status == "true") {
            $subscribe->status = "ACTIVE";
        } else {
            $subscribe->status = "INACTIVE";
        }

        try {
            $subscribe->save();
            return response()->json([
                'status' => true,
                'message' => 'สำเร็จ'
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }
    }


    public function deleteBanner(Request $request)
    {
        try {
            $subscribe = Subscribe::where('id',$request->id)->delete();
            return response()->json([
                'status' => true,
                'message' => 'ลบสำเร็จ'
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }

    }


    public function edit($id)
    {
        $subscribe = Subscribe::where('id',$id)->first();

        $data;

        if (isset($subscribe) === false) {
            $data = [
                'main_title' => '',
                'main_description' => '',
                'main_link_youtube' => '',

                'video1_image' => '',
                'video1_promote' => '',
                'video1_title' => '',
                'video1_description' => '',
                'video1_youtube' => '',

                'video2_image' => '',
                'video2_promote' => '',
                'video2_title' => '',
                'video2_description' => '',
                'video2_youtube' => '',

                'video3_image' => '',
                'video3_promote' => '',
                'video3_title' => '',
                'video3_description' => '',
                'video3_youtube' => '',
                'id' => $id ,
                'slug' => ''
            ];
        } else {
            $data = [
                'main_title' => $subscribe->main_title,
                'main_description' => $subscribe->main_description,
                'main_link_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->main_link_youtube),

                'video1_image' => $subscribe->video1_image,
                'video1_promote' => $subscribe->video1_promote,
                'video1_title' => $subscribe->video1_title,
                'video1_description' => $subscribe->video1_description,
                'video1_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->video1_youtube),

                'video2_image' => $subscribe->video2_image,
                'video2_promote' => $subscribe->video2_promote,
                'video2_title' => $subscribe->video2_title,
                'video2_description' => $subscribe->video2_description,
                'video2_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->video2_youtube),

                'video3_image' => $subscribe->video3_image,
                'video3_promote' => $subscribe->video3_promote,
                'video3_title' => $subscribe->video3_title,
                'video3_description' => $subscribe->video3_description,
                'video3_youtube' => str_replace("https://www.youtube.com/embed/", "", $subscribe->video3_youtube),

                'id' => $id ,
                'slug' => $subscribe->slug
            ];
        }

        return view('admin.codm.subscribe.edit', $data);
    }


    public function createPage()
    {
        return view("admin.codm.subscribe.create");
    }


    public function setDefault(Request $request)
    {
        $find_default = Subscribe::where('status_default','default')->first();
        $find_default->status_default = null;

        try {
            $find_default->save();
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }


        $subscribe = Subscribe::where('id',$request->id)->first();
        $subscribe->status_default = 'default';


        try {
            $subscribe->save();
            return response()->json([
                'status' => true,
                'message' => 'สำเร็จ'
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status' => false,
                'message' => $x->getMessage()
            ]);
        }


    }
}
