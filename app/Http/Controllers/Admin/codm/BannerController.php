<?php

namespace App\Http\Controllers\Admin\codm;

use App\Http\Controllers\ApiController;
use App\Models\codm\Banner;
use App\Models\codm\Setting;
use App\Models\rov\DBBanner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class BannerController extends ApiController
{
    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $data = [];

        return view('admin.codm.banner.index', $data);
    }

    public function getDatatable()
    {
        $banners    = Banner::get();
        $datatables = Datatables::of($banners);
        $datatables->editColumn('id', function ($item) {
            return $item->id;
        });
        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.banner.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="400px"></a>';
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.banner.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['image', 'status', 'order', 'action'])->make(true);
    }

    public function add()
    {
        $data = [];

        return view('admin.codm.banner.add', $data);
    }

    public function edit(Banner $banner)
    {
        $data = [
            'data' => $banner,
        ];

        return view('admin.codm.banner.add', $data);
    }

    public function saveBanner(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'image'        => 'mimes:jpeg,jpg,gif,png|dimensions:width=1920,height=970|max:1024'.($request->id ? '' : '|required'),
            'image_mobile' => 'mimes:jpeg,jpg,gif,png|dimensions:width=640,height=700|max:1024'.($request->id ? '' : '|required'),
        ], [
            'image.dimensions'        => 'ขนาดรูป 1920 x 970 เท่านั้น',
            'image.size'              => 'ขนาดรูปห้ามเกิน 1 MB',
            'image_mobile.dimensions' => 'ขนาดรูป 640 x 700 เท่านั้น',
            'image_mobile.size'       => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }


        if ($request->id && $request->id != null) {
            $banner = Banner::find($request->id);
        } else {
            $banner = new Banner();
        }

        if ($request->hasFile('image')) {
            $image         = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $banner->image = $image;
        }

        if ($request->hasFile('image_mobile')) {
            $image_mobile         = $this->uploadImage(
                $request->file('image_mobile'),
                'codmth/main',
                'upload',
                null
            );
            $banner->image_mobile = $image_mobile;
        }

        $banner->link = $request->link ?? null;

        try {
            $banner->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }

    }

    public function updateStatus(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $banner = Banner::find($id);

        if ($banner) {

            if ($status == 'true'){
                $order = Banner::whereStatus('active')->orderBy('order','desc')->first();
                $banner->order  = $order ? $order->order + 1 : 1;
                $banner->status = 'active';
            }else{
                $banner->order = null;
                $banner->status = 'inactive';
            }


            try {
                $banner->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }

    public function deleteBanner(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Banner::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found banner.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }


}
