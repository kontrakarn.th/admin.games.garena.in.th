<?php

namespace App\Http\Controllers\Admin\codm;

use App\Http\Controllers\ApiController;
use App\Models\codm\Attachment;
use App\Models\codm\Banner;
use App\Models\codm\Character;
use App\Models\codm\Game_Beginners;
use App\Models\codm\Game_OperatorSkills;
use App\Models\codm\News;
use App\Models\codm\Perk;
use App\Models\codm\Weapon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class GameGuideController extends ApiController
{
    function __construct()
    {
        $this->middleware('guest');
    }


    public function operatorSkills()
    {
        $data = [
            'items' => Game_OperatorSkills::orderBy('id','asc')->get(),
        ];

        return view('admin.codm.game_guides.operator_skills.index',$data);
    }

    public function editOperatorSkills(Game_OperatorSkills $opera)
    {
        return view('admin.codm.game_guides.operator_skills.add',[
            'data'  => $opera,
        ]);
    }


    public function createOperatorSkills()
    {
        return view('admin.codm.game_guides.operator_skills.add');
    }

    public function createOperatorSkillsSaveData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image'         => 'mimes:jpeg,jpg,gif,png|max:1024'.($request->id ? '' : '|required'),
            'preview_video' => 'mimes:jpeg,jpg,gif,png|max:1024',
            'yt'            => '',
            'name'          => 'required|max:255',
            'description'   => 'required|max:255',
        ], [
            'image.dimensions'        => 'ขนาดรูป 1920 x 970 เท่านั้น',
            'image.size'              => 'ขนาดรูปห้ามเกิน 1 MB',
            'image_mobile.dimensions' => 'ขนาดรูป 640 x 700 เท่านั้น',
            'image_mobile.size'       => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }


        if ($request->id && $request->id != null) {
            $opera = Game_OperatorSkills::find($request->id);
        } else {
            $opera = new Game_OperatorSkills();
        }

        if ($request->hasFile('image')) {
            $image      = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $opera->image = $image;
        }

        if ($request->hasFile('preview_video')) {
            $image_pr   = $this->uploadImage(
                $request->file('preview_video'),
                'codmth/main',
                'upload',
                null
            );
            $opera->preview_video = $image_pr;
        }

        $opera->name            = $request->name;
        $opera->description     = $request->description;
        $opera->yt              = $request->yt;

        try {
            $opera->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function updateStatusOperatorSkills(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $opera = Game_OperatorSkills::find($id);

        if ($opera) {

            if ($status == 'true'){
                $opera->status = 'active';
            }else{
                $opera->status = 'inactive';
            }


            try {
                $opera->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }


    public function deleteOperatorSkills(Game_OperatorSkills $opera)
    {
        $opera->delete();

        return redirect()->route('admin.codm.gameguides.operator_skills.index');
    }

    public function beginners()
    {

        return view('admin.codm.game_guides.beginners.index');

    }

    public function createBeginners()
    {
        return view('admin.codm.game_guides.beginners.add');
    }

    public function createBeginnersSaveData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required|max:255',
            'short_desc'    => 'required|max:255',
            'description'   => 'required|max:8000',
            'image'         => 'mimes:jpeg,jpg,gif,png|dimensions:width=1200,height=630|max:1024'.($request->id ? '' : '|required'),
            'image_share'   => 'mimes:jpeg,jpg,gif,png|dimensions:width=1200,height=630|max:1024',
        ], [
            'image.dimensions'       => 'ขนาดรูป 1200 × 630 เท่านั้น',
            'image.size'             => 'ขนาดรูปห้ามเกิน 1 MB',
            'image_share.dimensions' => 'ขนาดรูป 1200 x 630 เท่านั้น',
            'image_share.size'       => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id && $request->id != null) {
            $news = Game_Beginners::find($request->id);
        } else {
            $news   = new Game_Beginners();
            $category = "beginner";
            $slug   = Str::slug($category.' '.$request->title."-".rand(1000, 9999), '-');

            if (!$request->hasFile('image_share')) {
                $news->image_share = "https://cdngarenanow-a.akamaihd.net/webth/codm/mainsite/share.jpg";

            }
            $news->slug             = $slug;
            $news->category         = $category;
        }

        if ($request->hasFile('image')) {
            $image       = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $news->image = $image;
        }

        if ($request->hasFile('image_share')) {
            $image_share       = $this->uploadImage(
                $request->file('image_share'),
                'codmth/main',
                'upload',
                null
            );
            $news->image_share = $image_share;
        }

        $news->title            = $request->title;
        $news->short_desc       = $request->short_desc;
        $news->description      = $request->description;

        try {
            $news->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }


    public function getDatatableBeginners()
    {
        $banners    = Game_Beginners::whereCategory('beginner')->get();
        $datatables = Datatables::of($banners);

        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.gameguides.beginners.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="200px"></a>';
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.gameguides.beginners.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['id','image', 'title', 'status', 'action'])->make(true);

    }

    public function editBeginners(Game_Beginners $beginner)
    {
        return view('admin.codm.game_guides.beginners.add',[
            'data'  => $beginner,
        ]);
    }


    public function updateStatusBeginners(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $opera = Game_Beginners::find($id);

        if ($opera) {

            if ($status == 'true'){
                $opera->status = 'active';
            }else{
                $opera->status = 'inactive';
            }


            try {
                $opera->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }

    public function deleteBeginners(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Game_Beginners::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found news.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }


    public function modes()
    {

        return view('admin.codm.game_guides.modes.index');

    }

    public function createModes()
    {
        return view('admin.codm.game_guides.modes.add');
    }

    public function getDatatableModes()
    {
        $banners    = Game_Beginners::whereCategory('mode')->get();
        $datatables = Datatables::of($banners);

        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.gameguides.modes.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="200px"></a>';
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.gameguides.modes.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['id','image', 'title','short_desc', 'status', 'action'])->make(true);

    }


    public function createModesSaveData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required|max:255',
            'short_desc'    => 'required|max:255',
            'image'         => 'mimes:jpeg,jpg,gif,png|dimensions:width=1155,height=287|max:1024'.($request->id ? '' : '|required'),
        ], [
            'image.dimensions'       => 'ขนาดรูป 1155 × 287 เท่านั้น',
            'image.size'             => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id && $request->id != null) {
            $news = Game_Beginners::find($request->id);
        } else {
            $news   = new Game_Beginners();
            $category = "mode";
            $slug   = Str::slug($category.' '.$request->title."-".rand(1000, 9999), '-');
            $news->slug             = $slug;
            $news->category         = $category;
        }

        if ($request->hasFile('image')) {
            $image       = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $news->image = $image;
        }


        $news->title            = $request->title;
        $news->short_desc       = $request->short_desc;
        $news->description      = null;

        try {
            $news->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function editModes(Game_Beginners $beginner)
    {
        return view('admin.codm.game_guides.modes.add',[
            'data'  => $beginner,
        ]);
    }

    public function attachments()
    {
        return view('admin.codm.game_guides.attachments.index');
    }

    public function createAttachments()
    {
        return view('admin.codm.game_guides.attachments.add');
    }

    public function creatAttachmentsSaveData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required|max:255',
            'short_desc'    => 'required|max:255',
            'type'          => 'required',
            'for'           => 'required',
            'image'         => 'mimes:jpeg,jpg,gif,png|dimensions:width=130,height=130|max:1024'.($request->id ? '' : '|required'),
        ], [
            'image.dimensions'       => 'ขนาดรูป 130 × 130 เท่านั้น',
            'image.size'             => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id && $request->id != null) {
            $attachment = Attachment::find($request->id);
        } else {
            $attachment   = new Attachment();
        }

        if ($request->hasFile('image')) {
            $image       = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $attachment->image = $image;
        }


        $attachment->name               = $request->title;
        $attachment->description        = $request->short_desc;
        $attachment->type               = $request->type;
        $attachment->for                = $request->for;

        try {
            $attachment->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }

    }

    public function getDatatableAttachments()
    {
        $banners    = Attachment::get();
        $datatables = Datatables::of($banners);

        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.gameguides.attachments.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="200px"></a>';
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.gameguides.attachments.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['id','image', 'name','type','for', 'status', 'action'])->make(true);

    }


    public function editAttachments(Attachment $attachment)
    {
        return view('admin.codm.game_guides.attachments.add',
        [
            'data'  => $attachment,
        ]);
    }

    public function updateStatusAttachments(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $att = Attachment::find($id);

        if ($att) {

            if ($status == 'true'){
                $att->status = 'active';
            }else{
                $att->status = 'inactive';
            }


            try {
                $att->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }

    public function deleteAttachments(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Attachment::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found news.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }


    public function weapons()
    {
        return view('admin.codm.game_guides.weapons.index');
    }


    public function createWeapons()
    {
        $data = [
            'scopes'        => Attachment::whereType('scope')->get(),
            'attachments'   => Attachment::where('type','!=','scope')->get()
        ];
        return view('admin.codm.game_guides.weapons.add',$data);
    }

    public function editWeapons(Weapon $weapon)
    {
        $data = [
            'data'          => $weapon,
            'scopes'        => Attachment::whereType('scope')->get(),
            'attachments'   => Attachment::where('type','!=','scope')->get()
        ];
        return view('admin.codm.game_guides.weapons.add',$data);
    }


    public function creatWeaponsSaveData(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'series'            => 'required|max:255',
            'name'              => 'required|max:255',
            'description'       => 'required|max:255',
            'type'              => 'required',
            'class'             => 'required',
            'image'             => 'mimes:jpeg,jpg,gif,png|max:1024'.($request->id ? '' : '|required'),
        ], [
            'image.size'             => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id && $request->id != null) {
            $weapon = Weapon::find($request->id);
        } else {
            $weapon   = new Weapon();
        }

        if ($request->hasFile('image')) {
            $image       = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $weapon->image = $image;
        }


        $weapon->series      = $request->series;
        $weapon->name        = $request->name;
        $weapon->description = $request->description;
        $weapon->type        = $request->type;
        $weapon->class       = $request->class;

        $weapon->percent_damage   = $request->percent_damage;
        $weapon->percent_rof      = $request->percent_rof;
        $weapon->percent_accuracy = $request->percent_accuracy;
        $weapon->percent_mobility = $request->percent_mobility;
        $weapon->percent_range    = $request->percent_range;

        $weapon->scope    = $request->scope;
        $weapon->attachment1    = $request->attachment1;
        $weapon->attachment2    = $request->attachment2;
        $weapon->attachment3    = $request->attachment3;

        try {
            $weapon->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function getDatatableWeapons()
    {
        $weapons    = Weapon::get();
        $datatables = Datatables::of($weapons);

        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.gameguides.weapons.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="200px"></a>';
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.gameguides.weapons.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['id','image', 'name','class', 'status', 'action'])->make(true);

    }

    public function updateStatusWeapons(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $att = Weapon::find($id);

        if ($att) {

            if ($status == 'true'){
                $att->status = 'active';
            }else{
                $att->status = 'inactive';
            }


            try {
                $att->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }

    public function deleteWeapons(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Weapon::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found news.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }

    public function perks()
    {
        return view('admin.codm.game_guides.perks.index');
    }


    public function createPerks()
    {
        $data = [
        ];
        return view('admin.codm.game_guides.perks.add',$data);
    }

    public function editPerks(Perk $perk)
    {
        $data = [
            'data'          => $perk,
        ];
        return view('admin.codm.game_guides.perks.add',$data);
    }

    public function getDatatablePerks()
    {
        $banners    = Perk::get();
        $datatables = Datatables::of($banners);

        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.gameguides.perks.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="200px"></a>';
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.gameguides.perks.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['id','image', 'name','type', 'status', 'action'])->make(true);

    }


    public function creatPerksSaveData(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name'              => 'required|max:255',
            'description'       => 'required|max:255',
            'type'              => 'required',
            'image'             => 'mimes:jpeg,jpg,gif,png|max:1024'.($request->id ? '' : '|required'),
        ], [
            'image.size'             => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id && $request->id != null) {
            $perk = Perk::find($request->id);
        } else {
            $perk   = new Perk();
        }

        if ($request->hasFile('image')) {
            $image       = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $perk->image = $image;
        }


        $perk->name        = $request->name;
        $perk->description = $request->description;
        $perk->type        = $request->type;

        try {
            $perk->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function updateStatusPerks(Request $request)
    {

        $id     = $request->id;
        $status = $request->status;

        $att = Perk::find($id);

        if ($att) {

            if ($status == 'true'){
                $att->status = 'active';
            }else{
                $att->status = 'inactive';
            }


            try {
                $att->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }

    public function deletePerks(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Perk::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found news.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }



    public function characters()
    {
        return view('admin.codm.game_guides.characters.index');
    }


    public function createCharacters()
    {
        $data = [

        ];
        return view('admin.codm.game_guides.characters.add',$data);
    }

    public function editCharacters(Character $character)
    {
        $data = [
            'data'          => $character,
        ];
        return view('admin.codm.game_guides.characters.add',$data);
    }

    public function getDatatableCharacters()
    {
        $banners    = Character::get();
        $datatables = Datatables::of($banners);

        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.gameguides.characters.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="200px"></a>';
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.gameguides.characters.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['id','image', 'name', 'status', 'action'])->make(true);

    }


    public function creatCharactersSaveData(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name'              => 'required|max:255',
            'description'       => 'required|max:255',
            'image'             => 'mimes:jpeg,jpg,gif,png|max:1024'.($request->id ? '' : '|required'),
        ], [
            'image.size'             => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id && $request->id != null) {
            $char = Character::find($request->id);
        } else {
            $char   = new Character();
            $last = Character::orderBy('id','desc')->first();
            if (!$last){
                $last = 1;
            }else{
                $last = $last->order + 1;
            }
            $char->order       = $last;
        }

        if ($request->hasFile('image')) {
            $image       = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $char->image = $image;
        }


        $char->name        = $request->name;
        $char->description = $request->description;

        try {
            $char->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }
    }

    public function updateStatusCharacters(Request $request)
    {

        $id     = $request->id;
        $status = $request->status;

        $att = Character::find($id);

        if ($att) {

            if ($status == 'true'){
                $att->status = 'active';
            }else{
                $att->status = 'inactive';
            }


            try {
                $att->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }

    public function deleteCharacters(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = Character::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found news.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }








}
