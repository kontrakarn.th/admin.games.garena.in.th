<?php

namespace App\Http\Controllers\Admin\codm;

use App\Http\Controllers\ApiController;
use App\Models\codm\Banner;
use App\Models\codm\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class NewsController extends ApiController
{
    function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $data = [];

        return view('admin.codm.news.index', $data);
    }

    public function add()
    {
        $data = [];

        return view('admin.codm.news.add', $data);
    }

    public function edit(News $news)
    {
        $data = [
            'data' => $news,
        ];

        return view('admin.codm.news.add', $data);
    }


    public function saveBanner(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title'         => 'required|max:255',
            'short_desc'    => 'required|max:255',
            'category'      => 'required',
            'show_datetime' => 'required',
            'detail'        => 'required',
            'image'         => 'mimes:jpeg,jpg,gif,png|dimensions:width=500,height=640|max:1024'.($request->id ? '' : '|required'),
            'image_share'   => 'mimes:jpeg,jpg,gif,png|dimensions:width=1200,height=630|max:1024',
        ], [
            'image.dimensions'       => 'ขนาดรูป 500 × 640 เท่านั้น',
            'image.size'             => 'ขนาดรูปห้ามเกิน 1 MB',
            'image_share.dimensions' => 'ขนาดรูป 1200 x 630 เท่านั้น',
            'image_share.size'       => 'ขนาดรูปห้ามเกิน 1 MB',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => false,
                'message' => $validator->messages()->first(),
            ]);
        }

        if ($request->id && $request->id != null) {
            $news = News::find($request->id);
        } else {
            $news   = new News();
            $category = $request->category;
            if ($category == "system"){
                $category = "sys";
            }
            $slug   = Str::slug($category.' '.$request->title."-".rand(1000, 9999), '-');
            $check  = News::whereSlug($slug)->first();
            if ($check) {
                $slug = Str::slug($request->category.' '.$request->title."-".rand(10000, 99999), '-');
            }

            if (!$request->hasFile('image_share')) {
                $news->image_share = "https://cdngarenanow-a.akamaihd.net/webth/codm/mainsite/share.jpg";

            }
            $news->slug          = $slug;
        }

        if ($request->hasFile('image')) {
            $image       = $this->uploadImage(
                $request->file('image'),
                'codmth/main',
                'upload',
                null
            );
            $news->image = $image;
        }

        if ($request->hasFile('image_share')) {
            $image_share       = $this->uploadImage(
                $request->file('image_share'),
                'codmth/main',
                'upload',
                null
            );
            $news->image_share = $image_share;
        }

        $news->title         = $request->title;
        $news->short_desc    = $request->short_desc;
        $news->show_datetime = $request->show_datetime;
        $news->category      = $request->category;
        $news->description   = $request->detail;

        try {
            $news->save();
            return response()->json([
                'status'  => true,
                'message' => 'Successfully',
            ]);
        } catch (\Exception $x) {
            return response()->json([
                'status'  => false,
                'message' => $x->getMessage(),
            ]);
        }

    }


    public function getDatatable()
    {
        $banners    = News::get();
        $datatables = Datatables::of($banners);

        $datatables->editColumn('image', function ($item) {

            return '<a href="'.route('admin.codm.news.edit',
                    [$item]).'"><img src="'.$item->image.'" class="img-responsive" width="200px"></a>';
        });

        $datatables->editColumn('category', function ($item) {

            if ($item->isHilight == "yes"){
                return Str::title($item->category)."<br><b class='text-blue'>Highlight *</b>";
            }


            return Str::title($item->category);
        });

        $datatables->editColumn('status', function ($item) {
            if ($item->status === 'active') {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" checked="checked">';
            } else {
                $_action = '<input type="checkbox" data-id="'.$item->id.'" class="switch" data-on-text="Active" data-off-text="Inactive" data-on-color="success" data-off-color="default" >';
            }
            return $_action;
        });

        $datatables->editColumn('action', function ($item) {
            $_actionDropdown = '<ul class="icons-list">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="'.route('admin.codm.news.edit', [$item]).'"><i class="icon-pencil7"></i> Edit</a></li>
                        <li><a href="javascript:void(0)" onclick="deleteBanner('.$item->id.')"><i class="icon-cross2"></i> Remove</a></li>
                    </ul>
                </li>
            </ul>';
            return $_actionDropdown;
        });
        return $datatables->rawColumns(['id','image', 'title', 'category', 'show_datetime', 'status', 'action'])->make(true);
    }


    public function saveImage(Request $request)
    {
        #upload image
        if ($request->hasFile('file')) {
            $image = $this->uploadImage(
                $request->file('file'),
                'codmth/main',
                'upload',
                null
            );
        } else {
            return response()->json([
                'status'  => false,
                'message' => 'No File',
            ]);
        }

        return response()->json([
            'status'  => true,
            'message' => $image,
        ]);
    }


    public function updateStatus(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $news = News::find($id);

        if ($news) {

            $news->status = $status == 'true' ? 'active' : 'inactive';


            try {
                $news->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Status Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }


    public function updateHighlight(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        $news = News::find($id);

        $highlight = News::whereCategory($news->category)->where('isHilight','yes')->first();


        if ($news) {

            if ($status == 'true'){

                if ($highlight){
                    $highlight->isHilight = 'no';
                    $highlight->save();
                }

                $news->isHilight = 'yes';
            }else{
                $news->isHilight = 'no';
            }

            try {
                $news->save();
                return response()->json([
                    'status'  => true,
                    'message' => 'Highlight Updated',
                ]);
            } catch (\Exception $x) {
                return response()->json([
                    'status'  => false,
                    'message' => $x->getMessage(),
                ]);
            }
        }

        return response()->json([
            'status'  => false,
            'message' => 'Error',
        ]);
    }

    public function deleteBanner(Request $request)
    {
        $id = $request->id;
        if (!$id){
            return response()->json([
                'status' => false,
                'message' => "The id field is required.",
            ]);
        }

        $check = News::find($id);
        if (!$check){
            return response()->json([
                'status' => false,
                'message' => "Not found news.",
            ]);
        }

        try{
            $check->delete();
        }catch (\Exception $x){
            return response()->json([
                'status' => false,
                'message' => $x->getMessage(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => "Deleted",
        ]);
    }




}
