<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use Illuminate\Support\Facades\Session;
use App\Models\AuthUser;
use Illuminate\Support\Facades\Auth;

class GoogleLoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider() {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback() {
        $user = Socialite::driver('google')->user();
        // dd($user);
        if (strpos($user->email, '@garena.co.th') > 0 || strpos($user->email, '@seagroup.com') > 0) {

            $haveUser = AuthUser::where('email', $user->email)->where('status', 'active')->count();
            // dd($haveUser);
            if ($haveUser) {
                Session::put('user', $user); //setsession
                $this->updateToken($user)->updateAvartar($user)->updateIP($user);
            } else {
                return redirect()->route('auth.login')->with('msg', 'ไม่พบชื่อผู้ใช้งานนี้ในระบบ กรุณาติดต่อผู้ดูแลระบบ');
            }

            $userdata = [
                'email' => $user->email,
                'password' => $user->token
            ];

            if (Auth::guard()->attempt($userdata)) {
               
                return redirect()->to('/');
            } else {
                
                return redirect()->route('auth.logout');
            }
        } else {
           //dd($user);
            return redirect()->route('auth.login')->with('msg', 'ไม่สามารถเข้าใช้งานระบบได้ต้องเป็น User Garena เท่านั้น');
        }
    }

    function getIP(): string {
        // check ip
        $ip = request()->getClientIp();
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = array_shift($ips);
        }
        return $ip;
    }

    public function index(Request $request) {
        if (auth()->check()) {
            return redirect()->route('admin.main.index');
        } else {
            return view('auth.login');
        }
    }

    protected function updateIP($user) {
        AuthUser::where('email', $user->email)->update(['last_ip' => $this->getIP()]);
        return $this;
    }

    protected function updateToken($user) {
        AuthUser::where('email', $user->email)->update(['password' => bcrypt($user->token)]);
        return $this;
    }

    protected function updateAvartar($user) {
        AuthUser::where('email', $user->email)->update(['profile_picture' => $user->avatar_original]);
        return $this;
    }

    public function logout() {
        auth()->logout();
        Session::flush();
        return redirect()->route('auth.login');
    }
}
