<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    protected $apiImgHost = 'http://static.api.cmsgarena.com';

    function __construct()
    {
        parent::__construct();
    }

    protected function uploadImage($file, $path, $uploadType = 'upload', $resize)
    {
		$new_name = md5(time().$this->generateRandomString(8));
        $new_name = $new_name.'.'.$file->getClientOriginalExtension();

        $uploadJson = $this->static_api(
            $file->getPathName(),
            $new_name,
            $path,
            $uploadType,
            $resize
        );

        $uploadDecode = json_decode($uploadJson['exec']);

        if($uploadDecode->status === TRUE){
            return $uploadDecode->result;
        } else {
            return false;
        }
    }

    protected function generateRandomString($length = 10)
	{
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function deleteImage($fileName = null, $path = null)
    {
        if(is_null($fileName) || is_null($path))
            return false;

        $tempName = tempnam(sys_get_temp_dir(), 'TMP_');
        @file_put_contents($tempName, @file_get_contents($fileName));

        $delete = $this->static_api(
            $tempName,
            $fileName,
            $path,
            'delete'
            ,NULL
        );

        return $delete;
    }

    protected function static_api($file_tmp_path, $file_name, $file_path, $api_type='upload', $resize=NULL)
    {
        $data = array(
            'api_type'  => $api_type,
            'resize'    => $resize,
            'save_path' => $file_path
        );
        return $this->curl_upload_file($file_tmp_path,$file_name,$data,true);
    }

    protected function curl_upload_file($path_img, $file_name, $data, $debug=false)
    {
        $path = $path_img.';filename='.$file_name;

        // For php version 5.4+
        $cfile = new \CURLFile($path_img, mime_content_type($path_img), $file_name);
        $post_data = array_merge(array('file' => $cfile), $data);

        // use this instead of two line above if php version is old
        // $post_data = array_merge(array('file' =>'@'.$path),$data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiImgHost);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Connection: Keep-Alive',
            'Keep-Alive: 300'
        ));
        $result = curl_exec($ch);
        $status = curl_getinfo($ch);   //get status
        curl_close ($ch);
        if($debug){
            return array(
                'exec' => $result,
                'info' => $status,
                'path' => $path
            );
        }else{
            return $result;
        }
    }

    /**
     * POST request
     */
    protected function postApi($url,array $params)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);

        $params_merge = '';
        $startflag = true;
        foreach($params as $key=>$x)
        {
            $params_merge .= (($startflag)?'':'&').$key.'='.$x;

            if($startflag == true)
                $startflag = false;
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params_merge);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        //Try to decode JSON if not throw exception
        if( json_encode($server_output) === false ) {
            throw new Exception( json_last_error() );
        }

        return json_decode($server_output);
    }

    /**
     * GET request
     */
    protected function getApi($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $resp = curl_exec($ch);
        curl_close($ch);
        return $resp;
    }
}
