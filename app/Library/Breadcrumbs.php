<?php
namespace App\Library;

use Route;

class Breadcrumbs
{
    public static function render($breadcrumbs = [], $home = 'admin.index')
    {
        $html = '<ul class="breadcrumb">';
        // if(Route::currentRouteName() === $home){
        //     $html .= '<li class="active"><i class="fa fa-home"></i> Home</li>';
        // }else{
        //     if(strpos("/",$home)) $url = $home;
        //     else  $url = route($home);
        //     $html .= '<li><a href="' . $url . '"><i class="fa fa-home"></i> Home</a></li>';
        // }
        if (strpos("/", $home)) $url = $home;
        else  $url = route($home);
        $html .= '<li><a href="' . $url . '"><i class="fa fa-home"></i> Home</a></li>';
        if (is_array($breadcrumbs) && count($breadcrumbs) > 0) {
            foreach ($breadcrumbs as $kBreadcrumb => $vBreadcrumb) {
                if (isset($vBreadcrumb['text'])) {
                    if (isset($vBreadcrumb['url'])) {
                        if (strpos("/", $vBreadcrumb['url'])) {
                            $url = $vBreadcrumb['url'];
                        } else if (isset($vBreadcrumb['params'])) {
                            $url = route($vBreadcrumb['url'], $vBreadcrumb['params']);
                        } else {
                            $url = route($vBreadcrumb['url']);
                        }
                        $html .= '<li><a href="' . $url . '"> ' . $vBreadcrumb['text'] . ' </a></li>';
                    } else {
                        if ($vBreadcrumb == end($breadcrumbs)) {
                            $html .= '<li class="active"> ' . $vBreadcrumb['text'] . ' </li>';
                        } else {
                            $html .= '<li> ' . $vBreadcrumb['text'] . ' </li>';
                        }
                    }
                }
            }
        }

        $html .= '</ul>';

        return $html;
    }
}
