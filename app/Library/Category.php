<?php

namespace App\Library;

use Nestable\NestableTrait;
use App\Models\AuthMenu;

class Category extends \Eloquent {

    use NestableTrait;

    protected $parent = 'parent_id';
    protected $connection = 'fo4';
    protected $table='menu';

    public static function generateMenu(){
        // $categories = self::nested()->get();
        $categories = AuthMenu::getMenuAll();

        return $categories;
    }

}