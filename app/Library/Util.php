<?php
namespace App\Library;

class Util {
    
    public function static_api($file_tmp_path,$file_name,$file_path,$api_type='upload',$resize=NULL){
        $data = array(
            'api_type'  =>  $api_type,
            'resize'    =>  $resize,
            'save_path' =>  $file_path
        );
        return $this->curl_upload_file($file_tmp_path,$file_name,$data,true);
    }
    
    public function curl_upload_file($path_img,$file_name,$data,$debug=false){
        $host_api = 'http://static.api.cmsgarena.com/';
        $path = $path_img.';filename='.$file_name;


        $cfile = new \CURLFile($path_img, mime_content_type($path_img), $file_name);

        $post_data = array_merge(array('file' => $cfile), $data);
        //print_r($post_data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$host_api);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Connection: Keep-Alive',
            'Keep-Alive: 300'
        ));
        $result=curl_exec ($ch);
        $status = curl_getinfo($ch);   //get status
        if($debug){
            return array('exec'=>$result,'info'=>$status,'path'=>$path);
        }else{
            return $result;
        }
        curl_close ($ch);
    }
}
