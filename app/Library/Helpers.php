<?php
namespace App\Library;

use Route;
use App\Models\AuthMenu;
use App\Models\MenuUserRelation;

class Helpers
{

    public static function getMenuTop($rolemenu){
        $menu_collection=collect();
        foreach($rolemenu as $value){
            $model=AuthMenu::find($value);
            $chk=AuthMenu::get_Parent_Like_active($model);
            $menu_collection= $menu_collection->merge($chk);
        }

        return $menu_collection->unique();
    }
    public static function generateMenu()
    {
        $rolemenu=MenuUserRelation::getMenu(auth()->user()->id)->pluck("m_id")->toArray();

        $allmenu=self::getMenuTop($rolemenu);

        $menu=$allmenu->where('parent_id',0);
        $showmenu='';
        foreach($menu as $key=>$value){
            $numparentmenu=$allmenu->where('parent_id',$value->id)->count();
            // dd($numparentmenu);
            if($numparentmenu>0){

                $m_active='';
                if(in_array(Route::currentRouteName(),explode(",",$value->active_menu))){
                    $m_active='active';
                }

                $showmenu.='<li class="treeview '.$m_active.'">
                    <a href="#">
                        <i class="fa '.$value->icon.'"></i>
                        <span>'.$value->menu_name.'</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">';
                $showmenu.=self::generateParentMenu($value->id,$rolemenu,$allmenu);
                $showmenu.='</ul></li>';

            }else{

                $m_active_2='';
                if(Route::currentRouteName()==$value->url){
                    $m_active_2='active';
                }
                if(auth()->user()->access_level!='admin'){
                    if(in_array($value->id,$rolemenu)){
                        $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                        $showmenu.='</a></li>';
                    }
                }else{
                    $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                    $showmenu.='</a></li>';
                }

            }
        }
        echo $showmenu;
    }

    private static function generateParentMenu($id,$rolemenu,$allmenu){

        $menu=$allmenu->where('parent_id',$id);
        $showmenu='';
        foreach ($menu as $key=>$value){
            $numparentmenu=$allmenu->where('parent_id',$value->id)->count();

            if($numparentmenu>0){
                $m_active='';

                if(in_array(Route::currentRouteName(),explode(",",$value->active_menu))){
                    $m_active='active';
                }
                // dd(Route::currentRouteName());
                $showmenu.='
                <li class="treeview '.$m_active.'">
                    <a href="#">
                        <i class="fa '.$value->icon.'"></i>
                        '.$value->menu_name.'
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>';

                $showmenu.='</a><ul class="treeview-menu">';
                $showmenu.=self::generateParentMenu($value->id,$rolemenu,$allmenu);
                $showmenu.='</ul></li>';

            }else{

                $m_active_2='';
                if(Route::currentRouteName()==$value->url){
                    $m_active_2='active';
                }
                if(auth()->user()->access_level!='admin'){
                    if(in_array($value->id,$rolemenu)){
                        if(Route::has($value->url)){
                            $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                            $showmenu.='</a></li>';
                        }else{
                            $showmenu.='<li class="'.$m_active_2.'"><a href="#"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                            $showmenu.='</a></li>';
                        }

                    }
                }else{
                    if(Route::has($value->url)){
                        $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                        $showmenu.='</a></li>';
                    }else{
                        $showmenu.='<li class="'.$m_active_2.'"><a href="#"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                        $showmenu.='</a></li>';
                    }

                }
            }
        }
        return $showmenu;

    }

    public static function generateMenuAdmin()
    {
        // dd(Route::getRoutes());
        $rolemenu=MenuUserRelation::getMenu(auth()->user()->id)->pluck("m_id")->toArray();
        $menu=AuthMenu::get_Parent_Menu(0);
        $showmenu='';
        foreach($menu as $key=>$value){
            $numparentmenu=AuthMenu::get_Count_Parent_Menu($value->id);
            if($numparentmenu>0){

                $m_active='';
                if(in_array(Route::currentRouteName(),explode(",",$value->active_menu))){
                    $m_active='active';
                }

                $showmenu.='<li class="treeview '.$m_active.'">
                    <a href="#">
                        <i class="fa '.$value->icon.'"></i>
                        <span>'.$value->menu_name.'</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">';
                $showmenu.=self::generateParentMenuAdmin($value->id,$rolemenu,'');
                $showmenu.='</ul></li>';

            }else{

                $m_active_2='';
                if(Route::currentRouteName()==$value->url){
                    $m_active_2='active';
                }
                if(auth()->user()->access_level!='admin'){
                    if(in_array($value->id,$rolemenu)){
                        if(Route::has($value->url)){
                            $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                            $showmenu.='</a></li>';
                        }else{
                            $showmenu.='<li class="'.$m_active_2.'"><a href="#"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                            $showmenu.='</a></li>';
                        }
                    }
                }else{
                    if(Route::has($value->url)){
                        $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                        $showmenu.='</a></li>';
                    }else{
                        $showmenu.='<li class="'.$m_active_2.'"><a href="#"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                        $showmenu.='</a></li>';
                    }
                }

            }
        }
        echo $showmenu;
    }

    private static function generateParentMenuAdmin($id,$rolemenu,$active){

        $menu=AuthMenu::get_Parent_Menu($id);
        $showmenu='';
        foreach ($menu as $key=>$value){
            $numparentmenu=$numparentmenu=AuthMenu::get_Count_Parent_Menu($value->id);

            if($numparentmenu>0){
                $m_active='';

                if(in_array(Route::currentRouteName(),explode(",",$value->active_menu))){
                    $m_active='active';
                }
                // dd(Route::currentRouteName());
                $showmenu.='
                <li class="treeview '.$m_active.'">
                    <a href="#">
                        <i class="fa '.$value->icon.'"></i>
                        '.$value->menu_name.'
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>';

                $showmenu.='</a><ul class="treeview-menu">';
                $showmenu.=self::generateParentMenuAdmin($value->id,$rolemenu,'');
                $showmenu.='</ul></li>';

            }else{

                $m_active_2='';
                if(Route::currentRouteName()==$value->url){
                    $m_active_2='active';
                }
                if(auth()->user()->access_level!='admin'){
                    if(in_array($value->id,$rolemenu)){
                        if(Route::has($value->url)){
                            $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                            $showmenu.='</a></li>';
                        }else{
                            $showmenu.='<li class="'.$m_active_2.'"><a href="#"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                            $showmenu.='</a></li>';
                        }

                    }
                }else{
                    if(Route::has($value->url)){
                        $showmenu.='<li class="'.$m_active_2.'"><a href="'.route($value->url).'"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                        $showmenu.='</a></li>';
                    }else{
                        $showmenu.='<li class="'.$m_active_2.'"><a href="#"><i class="fa '.$value->icon.'"></i> <span>'.$value->menu_name.'</span>';
                        $showmenu.='</a></li>';
                    }
                }
            }
        }
        return $showmenu;

    }

    public static function generateRole($currole=[])
    {
        $menu=AuthMenu::get_Parent_Menu(0);
        $showmenu='';
        foreach($menu as $key=>$value){
            $numparentmenu=AuthMenu::get_Count_Parent_Menu($value->id);
            if($numparentmenu>0){
                $showmenu.='<li class="list-group-item" data-top_menu="'.$value->id.'"><label>'.$value->menu_name.'</label></li>';
                $showmenu.=self::generateParentRole(1,$value->id,$currole);
            }else{
                $showmenu.='<li class="list-group-item">
                     <label>
                         <input type="checkbox" name="menu[]" id="menu'.$value->id.'" data-parent_menu="'.$value->parent_id.'" style="margin-right:15px;" value="'.$value->id.'"';
                if(in_array($value->id,$currole)){
                    $showmenu.=' checked ';
                }
                $showmenu.='>'.$value->menu_name.'
                     </label>
                 </li>';
            }
        }
        echo $showmenu;
    }

    private static function generateParentRole($round,$id,$active){

        $menu=AuthMenu::get_Parent_Menu($id);
        $showmenu='';
        $stylepad='style="padding-left:'.($round*15+20).'px"' ;
        foreach ($menu as $key=>$value){
            $numparentmenu=$numparentmenu=AuthMenu::get_Count_Parent_Menu($value->id);

            if($numparentmenu>0){

                $showmenu.='<li class="list-group-item" '.$stylepad.' data-top_menu="'.$value->id.'"><label>'.$value->menu_name.'</label></li>';
                $showmenu.=self::generateParentRole(1,$value->id,$active);
            }else{
                $showmenu.='<li class="list-group-item" '.$stylepad.'>
                     <label>
                         <input type="checkbox" name="menu[]" id="menu'.$value->id.'" data-parent_menu="'.$value->parent_id.'" style="margin-right:15px;" value="'.$value->id.'"';
                 if(in_array($value->id,$active)){
                     $showmenu.=' checked ';
                 }
                $showmenu.='>'.$value->menu_name.'
                     </label>
                 </li>';
            }
        }
        return $showmenu;

    }
}
