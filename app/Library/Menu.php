<?php
namespace App\Library;

use Route;

class Menu 
{
    public static function active($active = [], $open = false)
    {
        $class = '';
        if(is_array($active) && count($active) > 0)
        {
            if(in_array(Route::currentRouteName(), $active) !== false)//array_search
            {
                if($open === false)
                {
                    $class = 'active';
                }
                else
                {
                    $class = 'active open';
                }
            }
        }

        return $class;
    }
}
