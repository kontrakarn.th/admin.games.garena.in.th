<?php

namespace App\Observers\codm;

use Illuminate\Support\Facades\Cache;

class SettingObserve
{
    private $userID = '';

    public function __construct(){
        $this->userID = auth()->user()->id;
    }

    public function saving($model)
    {
        $model->updated_by = $this->userID;
    }

    public function saved($model)
    {
        $model->updated_by = $this->userID;
    }


    public function updating($model)
    {
        // flush
        Cache::setPrefix(config('cache.codm_prefix'));
        Cache::flush();
        $model->updated_by = $this->userID;
    }

    public function updated($model)
    {
        $model->updated_by = $this->userID;
    }


    public function creating($model)
    {
        $model->created_by = $this->userID;
    }

    public function created($model)
    {
        $model->created_by = $this->userID;
    }


    public function removing($model)
    {
        $model->deleted_by = $this->userID;
    }

    public function removed($model)
    {
        $model->deleted_by = $this->userID;
    }
}
