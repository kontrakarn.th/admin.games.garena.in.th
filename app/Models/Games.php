<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class Games extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $dates = ['deleted_at'];
    protected $table='games';

    public static function getGameData($id)
    {
        $data = self::where('id',$id)->first();

        // dd($menu);

        if ($data != null) {
            return $data;
        }else{
            return '';
            // self::addContent($slug);
        }
        
    }

    public static function addGame($request)
    {
        $data = [
            'name'       	=> $request->txt_name,
            'status'        => 'active',
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false; 
        }

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }
}
