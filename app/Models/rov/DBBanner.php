<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DBBanner extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_banners';

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false;
        }

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }

    public static function saveBanner($request, $banner_image,$banner_image_mobile)
    {
        $updateData = [
            'title'                 => $request->txt_title,
            'head_title'            => $request->txt_head_title ?? null,
            'left_text'             => $request->txt_left_text ?? null,
            'right_text'            => $request->txt_right_text ?? null,
            'description'           => $request->txt_description ?? null,
            'link'                  => $request->txt_link ?? null,
            'video_path'            => $request->video_path ?? null,
            'banner_image'          => $banner_image,
            'banner_image_mobile'   => $banner_image_mobile,
            'updated_by'            => Auth::user()->id,
            'updated_at'            => Carbon::now()
        ];

        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }

    public function getFileFormatAttribute()
    {
        if($this->banner_image){
            return pathinfo($this->banner_image, PATHINFO_EXTENSION);
        }

        return null;
    }


}
