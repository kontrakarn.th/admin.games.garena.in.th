<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DBHero extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $table = 'mainsite_heros';
    protected $fillable = [
        'rune1','rune2','rune3','rune4','rune5','rune6',
        'amount_rune1','amount_rune2','amount_rune3','amount_rune4','amount_rune5','amount_rune6',
        'enchantment1','enchantment2','enchantment3','enchantment4','enchantment5','enchantment6',
        ];

    public function getRouteKeyName()
    {
        return "slug";
    }

    public function getRoleTextAttribute()
    {
        $role_array = [
            '',
            'Carry',
            'Tank',
            'Fighter',
            'Assassin',
            'Mage',
            'Support',
        ];

        $role_text = "";

        if ($this->role_id) {
            $role_text = $role_array[$this->role_id];
        }

        if ($this->role_second_id) {
            $role_text .= ' / '.$role_array[$this->role_second_id];
        }

        return $role_text;
    }

    public function skins()
    {
        return $this->hasMany('App\Models\rov\DBSkin', 'hero_id');
    }

    public function itemsets()
    {
        return $this->hasMany('App\Models\rov\DBItemSet', 'hero_id');
    }

    public function skills()
    {
        return $this->hasMany('App\Models\rov\DBSkill', 'hero_id');
    }

    public function getRunes()
    {
        return [$this->rune1,$this->rune2,$this->rune3,$this->rune4,$this->rune5,$this->rune6];
    }

    public function getRunesAmount()
    {
        return [$this->amount_rune1,$this->amount_rune2,$this->amount_rune3,$this->amount_rune4,$this->amount_rune5,$this->amount_rune6];
    }

    public function getEnchantment()
    {
        return [$this->enchantment1,$this->enchantment2,$this->enchantment3,$this->enchantment4,$this->enchantment5,$this->enchantment6];

    }
}
