<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DBContentTag extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_content_tags';

    public function content()
    {
        return $this->belongsTo('App\Models\rov\DBContent','content_id');
    }

    public function getTagNameAttribute()
    {
        $tag = DBTag::find($this->tag_id);
        if ($tag){
            return "{$tag->name}";
        }

        return null;
    }
}
