<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DBTag extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_tags';

    public function content_tag()
    {
        return $this->belongsTo('App\Models\rov\DBContentTag','tag_id');
    }

    public static function translate($key)
    {

        $from   = "th";
        $to     = "en";
        $api    = 'trnsl.1.1.20190704T082247Z.216aae97640c484a.128fa34b67e3fa97c467088ed73afb3e33a30577';

        if (preg_match('/[^A-Za-z0-9]/', $key)){
            try{
                $url    = file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate?key=' . $api . '&lang=' . $from . '-' . $to . '&text=' . urlencode($key));
                $json   = json_decode($url);
                $get    = preg_replace('/\s+/', '', strtolower($json->text[0]));

                return $get;
            }catch (\Exception $x){
                return $key;
            }
        }
        return $key;

    }

}
