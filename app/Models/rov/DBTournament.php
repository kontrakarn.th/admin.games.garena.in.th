<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DBTournament extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_tournaments';

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false;
        }

        if ($status == 'true') {
            $check = self::where('status', 'active')->count();
            if ($check >= 4 ){
                return false;
            }
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }
}
