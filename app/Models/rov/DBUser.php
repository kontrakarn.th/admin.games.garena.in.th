<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DBUser extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'users';

    public function getRouteKeyName()
    {
        return "open_id";
    }

    public function getGameNameDecodeAttribute()
    {
        return json_decode($this->game_name);
    }

}
