<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DBSkill extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_gameinfo_skills';

    public function hero()
    {
        return $this->belongsTo('App\Models\rov\DBHero','hero_id');
    }
}
