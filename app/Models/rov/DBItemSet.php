<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DBItemSet extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_gameinfo_itemsets';

    public function hero()
    {
        return $this->belongsTo('App\Models\rov\DBHero','hero_id');
    }

    public function getPreviewAttribute()
    {

        $preview = "";

        if ($this->item1 != 0){
            $item = DBItem::find($this->item1);
            $preview .= "<img src='$item->image' alt='' height='40px'>";
        }

        if ($this->item2 != 0){
            $item = DBItem::find($this->item2);
            $preview .= "<img src='$item->image' alt='' height='40px'>";
        }

        if ($this->item3 != 0){
            $item = DBItem::find($this->item3);
            $preview .= "<img src='$item->image' alt='' height='40px'>";
        }

        if ($this->item4 != 0){
            $item = DBItem::find($this->item4);
            $preview .= "<img src='$item->image' alt='' height='40px'>";
        }

        if ($this->item5 != 0){
            $item = DBItem::find($this->item5);
            $preview .= "<img src='$item->image' alt='' height='40px'>";
        }

        if ($this->item6 != 0){
            $item = DBItem::find($this->item6);
            $preview .= "<img src='$item->image' alt='' height='40px'>";
        }
        return $preview ;
    }

}
