<?php

namespace App\Models\rov;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DBContent extends Model
{
    use SoftDeletes;
    protected $connection = 'rov';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_contents';

    public function getCategoryNameAttribute()
    {
        $cat = DBCategory::find($this->category_id);
        if ($cat){
            return "{$cat->name}";
        }

        return null;
    }

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false;
        }

        $content = self::where('id', $id);
        if (!$content){
            return false;
        }

        $show_datetime = $content->first()->show_datetime;

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }

        if ($content->first()->content_tags->count() > 0){
            foreach ($content->first()->content_tags as $content_tag){
                $content_tag->status = $txt_status;
                $content_tag->save();
            }
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'show_datetime' => $show_datetime,
        ];

        $content->update($updateData);

        return true;
    }

    public function content_tags()
    {
        return $this->hasMany('App\Models\rov\DBContentTag','content_id');
    }

    public function getUrlAttribute()
    {
        $cat = DBCategory::find($this->category_id);
        if ($cat){
            return '/'.$cat->slug.'/'.$this->slug;
        }
        return null;
    }



}
