<?php

namespace App\Models\contra;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\contra\DBMenu;
//use App\Models\contra\DBSpec;

class DBItemguide extends Model
{
    use SoftDeletes;
    protected $connection = 'contra';
    protected $dates = ['deleted_at'];
    protected $table='item_guide';

    public static function getData($id)
    {
    	$data = self::where('id',$id)->first();

    	if ($data != null) {
    		return $data;
    	}else{
    		return 'Content not found';
    	}
    	
    }

    public static function addBanner($request)
    {
        $order = self::max('order');

        $data = [
            'order'         => ($order+1),
            'status'        => 'active',
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function saveItemguide($request, $itemguide_image,$itemguide_thumbnail)
    {
       
        $updateData = [
            'title'         => $request->txt_title,
            'name'          => $request->txt_name,
            'category'      => $request->txt_category,
            'detail'        => $request->txt_detail,
            'level'        => $request->txt_level,
            'status'        => 'active',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];
        
         if($itemguide_image != null){
           $updateData['image'] = $itemguide_image;
        }
        if($itemguide_thumbnail != null){
           $updateData['thumbnail'] = $itemguide_thumbnail;
        }
//        dd($updateData);
        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false; 
        }

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }

    public static function deleteBanner($id)
    {
        if (empty($id)) {
            return false; 
        }

        $updateData = [
            'status'        => 'inactive',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'deleted_by'    => Auth::user()->id,
            'deleted_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }
}
