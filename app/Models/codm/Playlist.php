<?php

namespace App\Models\codm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Playlist extends Model
{
    use SoftDeletes;
    protected $connection = 'codmtv';
    protected $table = 'codmtv_playlists';

    public function slug()
    {
        return $this->belongsTo('App\Models\codm\Slug');
    }
    
}
