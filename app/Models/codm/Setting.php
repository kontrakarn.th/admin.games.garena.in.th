<?php

namespace App\Models\codm;

use App\Observers\codm\SettingObserve;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;
    protected $connection       = 'codm';
    protected $dates            = ['deleted_at','created_at','updated_at'];
    protected $table            = 'mainsite_settings';

    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new SettingObserve());
        parent::boot();

    }
}
