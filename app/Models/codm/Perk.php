<?php

namespace App\Models\codm;

use App\Observers\codm\GameGuideObserve;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perk extends Model
{
    use SoftDeletes;
    protected $connection       = 'codm';
    protected $dates            = ['deleted_at','created_at','updated_at'];
    protected $table            = 'mainsite_game_perks';

    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new GameGuideObserve());
        parent::boot();

    }
}
