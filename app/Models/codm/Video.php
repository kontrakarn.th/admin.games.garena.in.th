<?php

namespace App\Models\codm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;
    protected $connection = 'codmtv';
    protected $table = 'codmtv_videos';

    public function slug()
    {
        return $this->belongsTo('App\Models\codm\Slug');
    }
}
