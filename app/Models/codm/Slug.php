<?php

namespace App\Models\codm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slug extends Model
{
    use SoftDeletes;
    protected $connection = 'codmtv';
    protected $table = 'codmtv_slugs';

    public function videos()
    {
        return $this->hasMany('App\Models\codm\Video', 'slug_id', 'id');
    }

    public function playlists()
    {
        return $this->hasMany('App\Models\codm\Playlist', 'slug_id', 'id');
    }

    public function hasMain()
    {
        return $this->videos()->where('is_main', 1);
    }
}
