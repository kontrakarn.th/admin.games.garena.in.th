<?php

namespace App\Models\codm;

use App\Observers\codm\GameGuideObserve;
use App\Observers\codm\NewsObserve;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game_OperatorSkills extends Model
{
    use SoftDeletes;
    protected $connection       = 'codm';
    protected $dates            = ['deleted_at','created_at','updated_at'];
    protected $table            = 'mainsite_game_operator_skills';

    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new GameGuideObserve());
        parent::boot();

    }
}
