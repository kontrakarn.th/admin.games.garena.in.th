<?php

namespace App\Models\codm;

use App\Observers\codm\BannerObserve;
use App\Observers\codm\NewsObserve;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    protected $connection       = 'codm';
    protected $dates            = ['deleted_at','created_at','updated_at'];
    protected $table            = 'mainsite_news';

    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new NewsObserve());
        parent::boot();

    }


    public function getRouteKeyName()
    {
        return "slug";
    }
}
