<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\UserGame;

class AuthUser extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    protected $dates = ['deleted_at'];
    protected $table='users';

    public static function getUserData($id)
    {
        $data = self::where('id',$id)->first();

        // dd($menu);

        if ($data != null) {
            return $data;
        }else{
            return '';
            // self::addContent($slug);
        }
        
    }

    public static function saveUser($request)
    {
//        dd($request->txt_game);
        $updateData = [
            'name'       	=> $request->txt_name,
            'email'         => $request->txt_email,
            'access_level'  => $request->txt_type,
            'status'        => 'active',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];

        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            $user_id = $request->txt_id;
            // return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            $user_id = self::insertGetId($updateData);
            // return true;
        }

        if ($request->txt_type == 'agent') {
        	if (!empty($request->txt_game)) {
                UserGame::saveUserGame($user_id, $request->txt_game);
                return true;
//	        	foreach ($request->txt_game as $key => $value) {
//		        	UserGame::saveUserGame($user_id, $value);
////		        	return true;
//		        }
	        }
        }else{
        	UserGame::saveUserGameAdmin($user_id);
        	return true;
        }

        return false;
    }

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false; 
        }

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'suspended';
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }


    public function getGame()
    {
        return $this->hasMany(UserGame::class,'id');
    }
}
