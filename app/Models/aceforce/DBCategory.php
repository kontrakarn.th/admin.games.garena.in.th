<?php

namespace App\Models\aceforce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class DBCategory extends Model
{
    use SoftDeletes;

    protected $connection = 'aceforce';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_categories';


}
