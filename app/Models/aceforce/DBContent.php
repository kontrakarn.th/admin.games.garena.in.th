<?php

namespace App\Models\aceforce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class DBContent extends Model
{
    use SoftDeletes;

    protected $connection = 'aceforce';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_contents';


    public function getCategoryNameAttribute()
    {
        $cat = DBCategory::find($this->category_id);
        if ($cat){
            return "{$cat->name}";
        }

        return null;
    }

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false;
        }

        $content = self::where('id', $id);
        if (!$content){
            return false;
        }

        $show_datetime = $content->first()->show_datetime;

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }


        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'show_datetime' => $show_datetime,
        ];

        $content->update($updateData);

        return true;
    }


    public function getUrlAttribute()
    {
        $cat = DBCategory::find($this->category_id);
        if ($cat){
            return '/'.$cat->slug.'/'.$this->slug;
        }
        return null;
    }

    public function category_name()
    {
        return $this->belongsTo(DBCategory::class,'category_id');
    }

}
