<?php

namespace App\Models\aceforce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class DBFeature extends Model
{
    use SoftDeletes;

    protected $connection = 'aceforce';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_features';


    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false;
        }

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }

    public static function saveFeature($request, $banner_image)
    {

        if ($request->txt_status == "on") {
            $request->txt_status = "active" ;
        } else {
            $request->txt_status = "inactive" ;
        }

        $updateData = [
            'title'                 => $request->txt_title,
            'link'                  => $request->txt_link ?? null,
            'banner_image'          => $banner_image,
            'status'                => $request->txt_status,
            'updated_by'            => Auth::user()->id,
            'updated_at'            => Carbon::now()
        ];

        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }

    public function getFileFormatAttribute()
    {
        if($this->banner_image){
            return pathinfo($this->banner_image, PATHINFO_EXTENSION);
        }

        return null;
    }


}
