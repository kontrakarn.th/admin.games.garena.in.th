<?php

namespace App\Models\aceforce;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class DBFeatureBg extends Model
{
    use SoftDeletes;

    protected $connection = 'aceforce';
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table='mainsite_features_bg';



    public static function saveBg($request, $bg_image,$image_header,$key)
    {
        $updateData = [
            'bg_image'          => $bg_image,
            'image_header'   => $image_header,
            'updated_by'            => Auth::user()->id,
            'updated_at'            => Carbon::now()
        ];

        self::where("key", $key)->update($updateData);
        return true;

    }

    public function getFileFormatAttribute()
    {
        if($this->banner_image){
            return pathinfo($this->banner_image, PATHINFO_EXTENSION);
        }

        return null;
    }


}
