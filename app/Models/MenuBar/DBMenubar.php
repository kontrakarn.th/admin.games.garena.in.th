<?php

namespace App\Models\MenuBar;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DBMenubar extends Model
{
    use SoftDeletes;

    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='menubar';
}
