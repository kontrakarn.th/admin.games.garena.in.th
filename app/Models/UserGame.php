<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class UserGame extends Model
{
    // use SoftDeletes;
    protected $connection = 'mysql';
    // protected $dates = ['deleted_at'];
    protected $table='user_game';

    public static function getUserGameData($id)
    {
        $data = self::where('user_id',$id)->where('status','active')->pluck('game_id');

        // dd($data);

        if ($data != null) {
            return $data;
        }else{
            return false;
        }
    }

    public static function saveUserGame($user_id, $data_game_id)
    {

        $role_games = self::where('user_id',$user_id)->where('status','active')->select('game_id')->get();
        $roles = array();
        foreach ($role_games as $game_id) {
            array_push($roles, $game_id->game_id);
        }
//        dd(self::where('user_id',$user_id)->where('status','active')->select('game_id')->get()->toArray());

//        dd($roles);
        if (sizeof($roles) <= sizeof($data_game_id)) {

            foreach ($data_game_id as $game_id){
                if (!in_array($game_id,$roles)) {
                    $findUserRoles = self::where('user_id',$user_id)->where('game_id',$game_id)->first();
                    if ($findUserRoles) {
                        $findUserRoles->status = 'active';
                        $findUserRoles->save();
                    } else {
                        $updateData = [
                            'user_id'       => $user_id,
                            'game_id'       => $game_id,
                            'status'        => 'active',
                        ];
                        self::insert($updateData);
                    }
                }
            }
        } else {
            foreach ($roles as $game_id) {
                if (!in_array($game_id,$data_game_id)) {
                    $roles_status = self::where('user_id',$user_id)->where('game_id',$game_id)->first();
                    $roles_status->status = 'inactive';
                    $roles_status->save();
                }
            }
        }


        return true;

//        dd('break');
//        $count = self::where('user_id',$user_id)->where('game_id',$game_id)->count();
//
//        if ($count > 0) {
//
//            $data = [
//                'status'        => 'inactive',
//            ];
//            self::where('user_id',$user_id)->update($data);
//
//            $updateData = [
//                'status'        => 'active',
//            ];
//            self::where('user_id',$user_id)->where('game_id',$game_id)->update($updateData);
//        }else{
//            $updateData = [
//                'user_id'       => $user_id,
//                'game_id'       => $game_id,
//                'status'        => 'active',
//            ];
//            self::insert($updateData);
//        }
//
//        return true;
    }

    public static function saveUserGameAdmin($user_id)
    {
        $count = self::where('user_id',$user_id)->count();

        if ($count > 0) {

            $data = [
                'status'        => 'inactive',
            ];
            self::where('user_id',$user_id)->update($data);
        }
        
        return true;
    }

    public function getGameName()
    {
        return $this->belongsTo(Games::class,'game_id');
    }
}
