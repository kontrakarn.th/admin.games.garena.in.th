<?php

namespace App\Models\fo4;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\fo4\DBMenu;

class DBContent extends Model
{
    use SoftDeletes;
    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='content';

    public static function getContentData($id)
    {
    	$data = self::where('id',$id)->first();

    	// dd($menu);

    	if ($data != null) {
    		return $data;
    	}else{
    		return '';
            // self::addContent($slug);
    	}
    	
    }

    public static function saveContent($request, $cover_image, $share_image, $slug)
    {
    	if (empty($request->txt_slug)) {
            return false; 
        }

        if ($request->txt_pin == 'on') {
            $txt_status = true;
        }else{
            $txt_status = false;
        }

        $updateData = [
            'slug'          => $slug,
            'title'         => $request->txt_title,
            'description'   => $request->txt_description,
            'show_datetime' => $request->txt_show_datetime,
            'detail'        => $request->txt_detail,
            'pin'           => $txt_status,
            'cover_image'   => $cover_image,
            'share_image'   => $share_image,
            'category'      => $request->txt_category,
            'types'         => $request->txt_types,
            'status'        => $request->status,
            'keyword'       => $request->txt_keyword,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];
        self::where('id', $request->txt_id)->update($updateData);

        return true;
    }

    public static function addContent($request, $slug)
    {
        $data = [
            'menu_id'       => $request->txt_menu_id,
            'slug'          => $slug,
            'title'         => $request->txt_title,
            'status'        => 'inactive',
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function deleteContent($id)
    {
        if (empty($id)) {
            return false; 
        }

        $updateData = [
            'status'        => 'inactive',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'deleted_by'    => Auth::user()->id,
            'deleted_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }

    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false; 
        }

        if ($status == 'true') {
            $txt_status = 'active';
        }else{
            $txt_status = 'inactive';
        }

        $updateData = [
            'status'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }
}
