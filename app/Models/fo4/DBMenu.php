<?php

namespace App\Models\fo4;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Nestable\NestableTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\fo4\DBSpec;


class DBMenu extends Model
{
    use SoftDeletes;
    // use NestableTrait;
    protected $parent = 'parent_id';
    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='menu';

    public static function getMenuData($id)
    {
    	$data = self::where('id',$id)->first();

    	// dd($menu);

    	if ($data != null) {
    		return $data;
    	}else{
    		return 'Content not found';
    	}
    	
    }

    public static function getParentMenuData()
    {
    	$data = self::where('status','active')->get();

    	// dd($menu);

    	/*$data = self::active(function($select ,$li, $href, $label) {

		    $select->addAttr('class', 'form-control');

		})->renderAsDropdown();*/

    	if ($data != null) {
    		return $data;
    	}else{
    		return 'Content not found';
    	}
    	
    }

    public static function addMenu($request, $slug)
    {
    	$order = self::where('parent_id', $request->txt_parent_id)->max('order');

        $data = [
            'parent_id'     => $request->txt_parent_id,
            'name'    		=> $request->txt_name,
            'slug'    		=> $slug,
            'type'    		=> $request->txt_type,
            'order'         => ($order+1),
            'status'        => 'active',
            'show'        	=> true,
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function saveMenu($request, $image)
    {
    	if (empty($request->id)) {
            return false; 
        }

        if ($request->txt_show == 'on') {
            $txt_status = true;
        }else{
            $txt_status = false;
        }

        if (empty($request->txt_parent_id)) {
            $request->txt_parent_id = '0';
        }

        $updateData = [
            'parent_id'		=> $request->txt_parent_id,
            'slug'          => $request->txt_slug,
            'name'         	=> $request->txt_name,
            'url'         	=> $request->txt_url,
            'share_title'   => $request->txt_share_title,
            'share_description'   => $request->txt_share_description,
            'keyword'       => $request->txt_keyword,
            'share_image' 	=> $image,
            'show'        	=> $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];
        self::where('id', $request->id)->update($updateData);

        return true;
    }
    public static function updateStatus($id,$status)
    {
        if (empty($id)) {
            return false; 
        }

        if ($status == 'true') {
            $txt_status = true;
        }else{
            $txt_status = false;
        }

        $updateData = [
            'show'        => $txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }

    public static function deleteMenu($id)
    {
        if (empty($id)) {
            return false; 
        }

        $updateData = [
            'status'        => 'inactive',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'deleted_by'    => Auth::user()->id,
            'deleted_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }
}
