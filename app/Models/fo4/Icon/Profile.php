<?php

namespace App\Models\fo4\Icon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Profile extends Model
{
    use SoftDeletes;

    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='icon_profile';


    public static function saveProfileIcon($request, $banner_image_large , $banner_image_small , $banner_image_icon , $banner_image_history , $banner_image_award, $banner_image_flag , $banner_image_all_stat, $banner_image_logo_team,$banner_image_shirt)
    {
        if ($request->txt_status) {
            $request->txt_status = 'active';

        }
        else {
            $request->txt_status = 'inactive' ;
        }




//        if ($request->txt_id) {
//            if ($request->txt_banner_image_large) {
//                if ($banner_image_large == null) {
//                    $img = Profile::where('id',$request->txt_id)->first();
//                    $banner_image_large = $img->profile_big_image ;
//                }
//            }
//        }
//

        // check order


        $updateData = [
            'first_name'    => $request->txt_first_name,
            'middle_name'    => $request->txt_middle_name,
            'last_name'    => $request->txt_last_name,
            'nick_name'    => $request->txt_nick_name,
            'body'    => $request->body,
            'weight'    => $request->txt_weight,
            'height'    => $request->txt_height,
            'foot_left'    => $request->foot_left,
            'foot_right'    => $request->foot_right,
            'FP'    => $request->txt_fp,
            'OVR'    => $request->txt_ovr,
            'position'    => implode(',',$request->expertise),
            'profile_big_image'  => $banner_image_large,
            'profile_small_image'  => $banner_image_small,
            'profile_image_icon'  => $banner_image_icon,
            'history_image'  => $banner_image_history,
            'award_image'  => $banner_image_award,
            'flag_image'  => $banner_image_flag,
            'profile_all_stat'  => $banner_image_all_stat,
            'logo_team'  => $banner_image_logo_team,
            'shirt_image'  => $banner_image_shirt,
            'status'        => $request->txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];

        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }
}
