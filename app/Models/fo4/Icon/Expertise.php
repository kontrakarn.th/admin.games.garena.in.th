<?php

namespace App\Models\fo4\Icon;

use Illuminate\Database\Eloquent\Model;

class Expertise extends Model
{
    protected $connection = 'fo4';
    protected $table='icon_expertise';
}
