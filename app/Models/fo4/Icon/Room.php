<?php

namespace App\Models\fo4\Icon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;

    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='icon_room';
}
