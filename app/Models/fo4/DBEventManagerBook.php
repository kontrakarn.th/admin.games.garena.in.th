<?php

namespace App\Models\fo4;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DBEventManagerBook extends Model
{
    use SoftDeletes;

    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='manager_book_event';

    public static function saveEventManagerBook($request, $banner_image)
    {


        if ($request->txt_status) {
            $request->txt_status = 'active';
            $order = DBEventManagerBook::orderBy('order','desc')->first();
            $count_order = ($order->order) + 1  ;

        }
        else {
            $request->txt_status = 'inactive' ;
            $count_order = null ;
        }

        if ($request->txt_id) {
            if ($request->txt_banner_image) {
                if ($banner_image == null) {
                    $img = DBEventManagerBook::where('id',$request->txt_id)->first();
                    $banner_image = $img->banner_image ;
                }
            }
        }


        // check order


        $updateData = [
            'name'           => $request->txt_title,
            'banner_image'  => $banner_image,
            'url'           => $request->txt_link,
            'status'        => $request->txt_status,
            'show_datetime' => $request->txt_show_datetime,
            'start_date' => $request->txt_start_datetime,
            'end_date' => $request->txt_end_datetime,
            'order' => $count_order,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];

        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }

}
