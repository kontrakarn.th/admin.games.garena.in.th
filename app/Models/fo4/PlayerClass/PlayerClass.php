<?php

namespace App\Models\fo4\PlayerClass;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PlayerClass extends Model
{
    use SoftDeletes;

    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='class_player';



    public static function savePlayerClass($request, $banner_image)
    {
        if ($request->txt_status) {
            $request->txt_status = 'active';

        }
        else {
            $request->txt_status = 'inactive' ;
        }

        $last_class = PlayerClass::orderBy('order','desc')->first();

        if ($last_class) {
            $last_class->order = ($last_class->order) + 1;
        }else {
            $last_class->order = 1 ;
        }

        $updateData = [
            'name'    => $request->txt_title,
            'slug'    => $request->txt_slug,
            'banner_image'    => $banner_image,
            'order'    => $last_class->order,
            'show_datetime'    => Carbon::parse($request->txt_show_datetime),
            'status'        => $request->txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];

        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }
}
