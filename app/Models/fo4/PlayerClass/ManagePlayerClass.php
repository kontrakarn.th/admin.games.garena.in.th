<?php

namespace App\Models\fo4\PlayerClass;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class ManagePlayerClass extends Model
{
    use SoftDeletes;

    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='class_player_detail';


    public static function saveManageClassPlayer($request, $banner_bg_class_player , $banner_class_player , $banner_bg_list_class_player , $banner_list_class_player , $banner_bg_detail)
    {
        $updateData = [
            'player_class_id'    => $request->txt_id,
            'bg_logo_class'    => $banner_bg_class_player,
            'logo_class'    => $banner_class_player,
            'bg_list_class'    => $banner_bg_list_class_player,
            'list_class'    => $banner_list_class_player,
            'detail_class'    => $banner_bg_detail,
            'txt_detail'    => $request->txt_details,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];

        if (isset($request->old_manage_class)) {
            self::where('player_class_id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }

}
