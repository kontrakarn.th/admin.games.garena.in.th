<?php

namespace App\Models\fo4;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\fo4\DBMenu;

class DBSpec extends Model
{
    use SoftDeletes;
    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='spec';

    public static function getSpecData()
    {
    	$data = self::where('status','active')->get();

    	if ($data != null) {
    		return $data;
    	}else{
    		return 'Content not found';
    	}
    	
    }

    public static function addElement($request)
    {
        $order = self::max('order');

        $data = [
            'order'         => ($order+1),
            'status'        => 'active',
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function saveElement($request)
    {
        if (empty($request->id)) {
            return false; 
        }

        $updateData = [
            'item'          => $request->item,
            'minimum'       => $request->minimum,
            'recommended'   => $request->recommended,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];
        self::where('id', $request->id)->update($updateData);

        return true;
    }
}
