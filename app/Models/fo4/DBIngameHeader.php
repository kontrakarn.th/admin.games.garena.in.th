<?php

namespace App\Models\fo4;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DBIngameHeader extends Model
{
    use SoftDeletes;
    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='in_game_header';


    public static function saveEventManagerBook($request, $banner_image)
    {


        if ($request->txt_status) {
            $request->txt_status = 'active';
        }
        else {
            $request->txt_status = 'inactive' ;
        }

        if ($request->txt_id) {
            if ($request->txt_banner_image) {
                if ($banner_image == null) {
                    $img = DBIngameHeader::where('id',$request->txt_id)->first();
                    $banner_image = $img->banner_image ;
                }
            }
        }


        // check order


        $updateData = [
            'name'           => $request->txt_title,
            'banner_image'  => $banner_image,
            'status'        => $request->txt_status,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];

        if (isset($request->txt_id)) {
            self::where('id', $request->txt_id)->update($updateData);
            return true;
        }else{
            $updateData['created_by'] = Auth::user()->id;
            $updateData['created_at'] = Carbon::now();
            self::insert($updateData);
            return true;
        }
        return false;
    }

}
