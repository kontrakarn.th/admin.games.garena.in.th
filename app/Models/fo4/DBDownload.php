<?php

namespace App\Models\fo4;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\fo4\DBMenu;
use App\Models\fo4\DBSpec;

class DBDownload extends Model
{
    use SoftDeletes;
    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='download';

    public static function getDownloadData()
    {
    	$data = self::where('status','active')->get();

    	if ($data != null) {
    		return $data;
    	}else{
    		return 'Content not found';
    	}
    	
    }

    public static function addElement($request)
    {
        $order = self::max('order');

        $data = [
            'types'       	=> $request->types,
            'order'         => ($order+1),
            'status'        => 'active',
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function saveElement($request)
    {
        if (empty($request->id)) {
            return false; 
        }

        $updateData = [
            'txt'          	=> $request->txt,
            'types'         => $request->types,
            'url'           => $request->url,
            'title'         => $request->title,
            'description'   => $request->description,
            'detail'        => $request->detail,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];
        self::where('id', $request->id)->update($updateData);

        return true;
    }

    public static function deleteElement($id)
    {
        if (empty($id)) {
            return false; 
        }
        $updateData = [
            'status'        => 'inactive',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'deleted_by'    => Auth::user()->id,
            'deleted_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }
}
