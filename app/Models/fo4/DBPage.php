<?php

namespace App\Models\fo4;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\fo4\DBMenu;

class DBPage extends Model
{
    use SoftDeletes;
    protected $connection = 'fo4';
    protected $dates = ['deleted_at'];
    protected $table='page';

    public static function getPageData($slug)
    {
    	$menu = DBMenu::where('slug', $slug)->first();

    	$data = self::where('menu_id',$menu->id)->where('status','active')->get();

    	// dd($menu);

    	if ($data != null) {
    		return $data;
    	}else{
    		return 'Page not found';
    	}
    	
    }

    public static function getPageSection($slug)
    {
    	$menu = DBMenu::where('slug', $slug)->first();

    	$data = self::where('menu_id',$menu->id)->where('status','active')->groupBy('section_id')->pluck('section_id');

    	// dd($data);

    	if ($data != null) {
    		return $data;
    	}else{
    		return 'Page not found';
    	}
    	
    }

    public static function addSection($slug, $section_id, $element_type)
    {
        $menu_id = DBMenu::where('slug', $slug)->value('id');

        $data = [
            'menu_id'       => $menu_id,
            'section_id'    => $section_id,
            'types'         => $element_type,
            'order'         => '1',
            'status'        => 'active',
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function addElement($slug, $section_id, $element_type)
    {
        $menu_id = DBMenu::where('slug', $slug)->value('id');

        $order = self::where('section_id', $section_id)->max('order');

        $data = [
            'menu_id'       => $menu_id,
            'section_id'    => $section_id,
            'types'         => $element_type,
            'order'         => ($order+1),
            'status'        => 'active',
            'created_by'    => Auth::user()->id,
            'created_at'    => Carbon::now(),
        ];
        $id = self::insertGetId($data);

        if ($id) {
            return true;
        }
        return false; 
    }

    public static function deleteSection($id)
    {
        if (empty($id)) {
            return false; 
        }

        $updateData = [
            'status'        => 'inactive',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'deleted_by'    => Auth::user()->id,
            'deleted_at'    => Carbon::now(),
        ];
        self::where('section_id', $id)->update($updateData);

        return true;
    }

    public static function deleteElement($id)
    {
        if (empty($id)) {
            return false; 
        }

        $updateData = [
            'status'        => 'inactive',
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now(),
            'deleted_by'    => Auth::user()->id,
            'deleted_at'    => Carbon::now(),
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }

    public static function saveElement($id,$field,$value)
    {
        if (empty($id)) {
            return false; 
        }

        $updateData = [
            $field          => $value,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()
        ];
        self::where('id', $id)->update($updateData);

        return true;
    }
}
