<?php

namespace App\Models;

use Nestable\NestableTrait;

class AuthMenu extends \Eloquent {

    use NestableTrait;

    protected $parent = 'parent_id';
    protected $connection = 'fo4';
    protected $table='menu';

    public static function getMenuAll()
    {
    	return self::nested()->get();
    }

}