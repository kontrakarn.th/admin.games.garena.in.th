<?php 

// Menu
Route::group(['prefix' => 'menu', 'as' => 'menu.'], function () {
    Route::get('/manage', 'MenuController@index')->name('index');
	Route::get('/datatable', 'MenuController@getMenuDatatable')->name('datatable');
	Route::get('/add', 'MenuController@add')->name('add');
	Route::get('/edit/{id}', 'MenuController@edit')->name('edit');
	Route::post('/save-image', 'MenuController@saveImage')->name('saveImage');
	Route::post('/save-menu', 'MenuController@saveMenu')->name('saveMenu');
	Route::post('/add-menu', 'MenuController@addMenu')->name('addMenu');
	Route::post('/update-status', 'MenuController@updateStatus')->name('updateStatus');
	Route::post('/delete-menu', 'MenuController@deleteBanner')->name('deleteMenu');
});

// Page
Route::group(['prefix' => 'page', 'as' => 'page.'], function () {
	Route::get('/manage', 'PageController@index')->name('index');
	Route::get('/datatable', 'PageController@getMenuDatatable')->name('datatable');
	Route::get('/add', 'PageController@add')->name('add');
	Route::get('/edit/{slug}', 'PageController@edit')->name('edit');
	Route::post('/save-element', 'PageController@saveElement')->name('saveElement');
	Route::post('/save-image', 'PageController@saveImage')->name('saveImage');
	Route::post('/save-image-editor', 'PageController@saveImageEditor')->name('saveImageEditor');
	Route::post('/add-section', 'PageController@addSection')->name('addSection');
	Route::post('/add-element', 'PageController@addElement')->name('addElement');
	Route::post('/delete-section', 'PageController@deleteSection')->name('deleteSection');
	Route::post('/delete-element', 'PageController@deleteElement')->name('deleteElement');
});

// Content
Route::group(['prefix' => 'content', 'as' => 'content.'], function () {
	Route::get('/manage', 'ContentController@index')->name('index');
	Route::get('/datatable', 'ContentController@getMenuDatatable')->name('datatable');
	Route::get('/add', 'ContentController@edit')->name('add');
	Route::get('/edit/{id}', 'ContentController@edit')->name('edit');
	Route::post('/save-image', 'ContentController@saveImage')->name('saveImage');
	Route::post('/save-content', 'ContentController@saveContent')->name('saveContent');
	Route::post('/add-content', 'ContentController@addContent')->name('addContent');
	Route::post('/delete-content', 'ContentController@deleteContent')->name('deleteContent');
	Route::post('/update-status', 'ContentController@updateStatus')->name('updateStatus');
});

// Download
Route::group(['prefix' => 'download', 'as' => 'download.'], function () {
	Route::get('/edit', 'DownloadController@edit')->name('edit');
	Route::post('/add-element', 'DownloadController@addElement')->name('addElement');
	Route::post('/delete-element', 'DownloadController@deleteElement')->name('deleteElement');
	Route::post('/delete-element-spec', 'DownloadController@deleteElementSpec')->name('deleteElementSpec');
	Route::post('/save-element', 'DownloadController@saveElement')->name('saveElement');
});

// Banner
Route::group(['prefix' => 'banner', 'as' => 'banner.'], function () {
	Route::get('/manage', 'BannerController@index')->name('index');
	Route::get('/datatable', 'BannerController@getDatatable')->name('datatable');
	Route::get('/add', 'BannerController@add')->name('add');
	Route::get('/edit/{id}', 'BannerController@edit')->name('edit');
	Route::post('/save-banner', 'BannerController@saveBanner')->name('saveBanner');
	Route::post('/update-status', 'BannerController@updateStatus')->name('updateStatus');
	Route::post('/delete-banner', 'BannerController@deleteBanner')->name('deleteBanner');
});

Route::group(['prefix' => 'event', 'as' => 'event.'], function () {
	Route::get('/manage', 'EventController@index')->name('index');
	Route::get('/datatable', 'EventController@getDatatable')->name('datatable');
	Route::get('/add', 'EventController@add')->name('add');
	Route::get('/edit/{id}', 'EventController@edit')->name('edit');
	Route::post('/save-event', 'EventController@saveEvent')->name('saveEvent');
	Route::post('/update-status', 'EventController@updateStatus')->name('updateStatus');
	Route::post('/delete-event', 'EventController@deleteEvent')->name('deleteEvent');
});





// Manager Book
Route::group(['prefix' => 'manager-book', 'as' => 'manager-book.'], function () {

    // Menu
    Route::get('/menu', 'ManagerBookController@getIndex')->name('index');
    Route::get('/edit/{id}', 'ManagerBookController@getEdit')->name('edit');
    Route::get('/dataTable', 'ManagerBookController@dataTable')->name('dataTable');
    Route::get('/create/menu', 'ManagerBookController@createMenu')->name('create.menu');
    Route::post('/save/menu', 'ManagerBookController@saveMenu')->name('save.menu');
    Route::post('/edit/menu', 'ManagerBookController@saveEdit')->name('save.edit');
    Route::post('/updateStatus', 'ManagerBookController@updateStatus')->name('updateStatus');



    // Event
    Route::get('/event', 'ManagerBookEventController@getIndex')->name('event.index');
    Route::get('/event/create', 'ManagerBookEventController@createEvent')->name('event.create');
    Route::get('/event/edit/{id}', 'ManagerBookEventController@editEvent')->name('event.edit');
    Route::get('/event/ordering', 'ManagerBookEventController@ordering')->name('event.ordering');
    Route::post('/event/save', 'ManagerBookEventController@saveEvent')->name('event.save');
    Route::get('/event/dataTable', 'ManagerBookEventController@dataTable')->name('event.dataTable');
    Route::post('/event/updateStatus', 'ManagerBookEventController@updateStatus')->name('event.updateStatus');
    Route::post('/event/saveOrder', 'ManagerBookEventController@saveOrder')->name('event.save.order');
    Route::post('/event/deleteEvent', 'ManagerBookEventController@deleteEvent')->name('event.deleteEvent');

});


// In Game
Route::group(['prefix' => 'in_game', 'as' => 'in_game.'], function () {

    // Menu
    Route::get('/index', 'MenuIngameController@getIndex')->name('index');
    Route::get('/create', 'MenuIngameController@create')->name('create');
    Route::get('/dataTable', 'MenuIngameController@dataTable')->name('dataTable');
    Route::get('/edit/{id}', 'MenuIngameController@edit')->name('edit');
    Route::get('/ordering', 'MenuIngameController@ordering')->name('ordering');
    Route::post('/save', 'MenuIngameController@save')->name('save');
    Route::post('/updateStatus', 'MenuIngameController@updateStatus')->name('updateStatus');
    Route::post('/saveOrder', 'MenuIngameController@saveOrder')->name('save.order');
    Route::post('/deleteEvent', 'MenuIngameController@deleteEvent')->name('deleteEvent');



    Route::get('/header', 'MenuIngameController@header')->name('header');
    Route::get('/header/create', 'MenuIngameController@headerCreate')->name('header.create');
    Route::get('/header/edit/{id}', 'MenuIngameController@headerEdit')->name('header.edit');
    Route::get('/header/dataTable', 'MenuIngameController@headerDataTable')->name('header.dataTable');
    Route::post('/header/save', 'MenuIngameController@headerSave')->name('header.save');
    Route::post('/header/updateStatus', 'MenuIngameController@headerUpdateStatus')->name('header.updateStatus');
    Route::post('/header/deleteEvent', 'MenuIngameController@headerdDleteEvent')->name('header.deleteEvent');

});


// Icon
Route::group(['prefix' => 'icon', 'as' => 'icon.' , 'namespace' => 'Icon'], function () {

    // Room
    Route::group(['prefix' => 'room', 'as' => 'room.'], function () {
        Route::get('/index', 'RoomController@index')->name('index');
        Route::get('/create', 'RoomController@create')->name('create');
        Route::get('/edit/{id}', 'RoomController@edit')->name('edit');
        Route::get('/dataTable', 'RoomController@dataTable')->name('dataTable');
        Route::post('/save', 'RoomController@save')->name('save');
        Route::post('/updateStatus', 'RoomController@updateStatus')->name('updateStatus');
        Route::post('/deleteEvent', 'RoomController@deleteEvent')->name('deleteEvent');

    });

    // Profile
    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
        Route::get('/index', 'ProfileController@index')->name('index');
        Route::get('/create', 'ProfileController@create')->name('create');
        Route::get('/edit/{id}', 'ProfileController@edit')->name('edit');
        Route::get('/dataTable', 'ProfileController@dataTable')->name('dataTable');
        Route::post('/save', 'ProfileController@save')->name('save');
        Route::post('/updateStatus', 'ProfileController@updateStatus')->name('updateStatus');
        Route::post('/deleteIcon', 'ProfileController@deleteIcon')->name('deleteIcon');
    });



    // Manage
    Route::get('/index', 'ManageController@index')->name('index');
    Route::get('/manage/{id}', 'ManageController@manage')->name('manage');
    Route::post('/setting', 'ManageController@setting')->name('setting');
    Route::post('/addIcon', 'ManageController@addIcon')->name('addIcon');
    Route::get('/datatable/{room}', 'ManageController@datatable')->name('datatable');
    Route::post('/leave', 'ManageController@leave')->name('leave');
    Route::get('/order/{room}', 'ManageController@order')->name('order');
    Route::post('/saveOrder', 'ManageController@saveOrder')->name('save.order');

});


// Class
Route::group(['prefix' => 'class', 'as' => 'class.' , 'namespace' => 'classIcon'], function () {

    Route::get('/index', 'IconClassController@index')->name('index');
    Route::get('/create', 'IconClassController@create')->name('create');
    Route::get('/edit/{id}', 'IconClassController@edit')->name('edit');
    Route::get('/dataTable', 'IconClassController@dataTable')->name('dataTable');
    Route::get('/manage/{id}', 'IconClassController@manage')->name('manage');
    Route::get('/ordering', 'IconClassController@ordering')->name('ordering');
    Route::post('/save', 'IconClassController@save')->name('save');
    Route::post('/updateStatus', 'IconClassController@updateStatus')->name('updateStatus');
    Route::post('/save/manage', 'IconClassController@saveManage')->name('save.manage');
    Route::post('/save/order', 'IconClassController@saveOrder')->name('save.order');
    Route::post('/deleteEvent', 'IconClassController@deleteEvent')->name('deleteEvent');


});



// Menu bar
Route::group(['prefix' => 'menubar', 'as' => 'menubar.' , 'namespace' => 'Menubar'], function () {

    Route::get('/index', 'MenuBarController@index')->name('index');
    Route::get('/edit/{id}', 'MenuBarController@edit')->name('edit');
    Route::get('/dataTable', 'MenuBarController@dataTable')->name('dataTable');
    Route::get('/submenu/{id}', 'MenuBarController@submenu')->name('submenu');
    Route::get('/create/parent', 'MenuBarController@createParent')->name('create.parent');
    Route::get('/ordering', 'MenuBarController@ordering')->name('ordering');
    Route::post('/save', 'MenuBarController@save')->name('save');
    Route::post('/updateStatus', 'MenuBarController@updateStatus')->name('updateStatus');
    Route::post('/save/order', 'MenuBarController@saveOrder')->name('save.order');
    Route::post('/deleteEvent', 'MenuBarController@deleteEvent')->name('deleteEvent');



    # sub menu bar
    Route::get('/submenu/dataTable/{parent}', 'MenuBarController@submenuDataTable')->name('submenu.dataTable');
    Route::get('/submenu/create/{parent}', 'MenuBarController@createSubmenu')->name('createSubmenu');
    Route::get('/edit/submenu/{id}', 'MenuBarController@editSubmenu')->name('edit.submenu');
    Route::post('/save/submenu', 'MenuBarController@saveSubmenu')->name('save.submenu');
    Route::post('/updateStatusSubmenu', 'MenuBarController@updateStatusSubmenu')->name('updateStatusSubmenu');
    Route::post('/sub/deleteEvent', 'MenuBarController@subDeleteEvent')->name('sub.deleteEvent');


});
