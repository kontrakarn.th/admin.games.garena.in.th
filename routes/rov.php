<?php

// save image
Route::post('/save-image', 'ContentController@saveImage')->name('saveImage');


// banner
Route::group(['prefix' => 'banner', 'as' => 'banner.'], function () {
    Route::get('/manage', 'BannerController@index')->name('index');
    Route::get('/ordering', 'BannerController@ordering')->name('ordering');
    Route::get('/add', 'BannerController@add')->name('add');
    Route::get('/{id}/edit', 'BannerController@edit')->name('edit');
    Route::get('/datatable', 'BannerController@getDatatable')->name('datatable');
    Route::post('/save-banner', 'BannerController@saveBanner')->name('saveBanner');
    Route::post('/update-status', 'BannerController@updateStatus')->name('updateStatus');
    Route::post('/save-order', 'BannerController@saveOrder')->name('saveOrder');
    Route::post('/delete-banner', 'BannerController@deleteBanner')->name('deleteBanner');

});

// contents/news
Route::group(['prefix' => 'content', 'as' => 'content.'], function () {
    Route::get('/manage', 'ContentController@index')->name('index');
    Route::get('/add', 'ContentController@add')->name('add');
    Route::post('/save-content', 'ContentController@saveContent')->name('saveContent');
    Route::get('/datatable', 'ContentController@datatable')->name('datatable');
    Route::get('/{id}/edit', 'ContentController@edit')->name('edit');
    Route::post('/update-status', 'ContentController@updateStatus')->name('updateStatus');
    Route::post('/delete-content', 'ContentController@deleteContent')->name('deleteContent');

});

// categories
Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
    Route::get('/index', 'ContentController@index')->name('index');
});

// setting
Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {
    Route::get('/', 'SettingController@index')->name('index');
    Route::post('/save-header', 'SettingController@saveHeader')->name('saveHeader');
    Route::post('/save-download', 'SettingController@saveDownload')->name('saveDownload');
    Route::post('/save-footer', 'SettingController@saveFooter')->name('saveFooter');
});

// categories
Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('/index'             , 'UserController@index')->name('index');
    Route::get('/datatable'         , 'UserController@datatable')->name('datatable');
    Route::get('/{user}/detail'     , 'UserController@userDetail')->name('userDetail');
});

// heros
Route::group(['prefix' => 'hero', 'as' => 'hero.'], function () {
    Route::get('/index'                         , 'HeroController@index')->name('index');
    Route::get('/{hero}/edit'                   , 'HeroController@edit')->name('edit');
    Route::get('/add'                           , 'HeroController@add')->name('add');
    Route::get('/gameinfo'                      , 'HeroController@gameInfo')->name('gameinfo');


    // item set
    Route::get('/{hero}/item/add'               , 'HeroController@addItemSet')->name('addItemSet');
    Route::get('/{hero}/item/{itemset}'         , 'HeroController@editItemSet')->name('editItemSet');
    Route::get('/{hero}/item/{itemset}/delete'  , 'HeroController@deleteItemSet')->name('deleteItemSet');

    // skill
    Route::get('/{hero}/skill/add'              , 'HeroController@addSkill')->name('addSkill');
    Route::get('/{hero}/skill/{skill}'          , 'HeroController@editSkill')->name('editSkill');
    Route::get('/{hero}/skill/{skill}/delete'   , 'HeroController@deleteSkill')->name('deleteSkill');

    // skin
    Route::get('/{hero}/skin/add'               , 'HeroController@addSkin')->name('addSkin');
    Route::get('/{hero}/skin/{skin}'            , 'HeroController@editSkin')->name('editSkin');
    Route::get('/{hero}/skin/{skin}/delete'     , 'HeroController@deleteSkin')->name('deleteSkin');


    // apis
    Route::get('/datatable'                     , 'HeroController@datatable')->name('datatable');
    Route::post('/update-status'                , 'HeroController@updateStatus')->name('updateStatus');
    Route::post('/save'                         , 'HeroController@saveData')->name('saveData');
    Route::post('/saveItemSet'                  , 'HeroController@saveItemSet')->name('saveItemSet');
    Route::post('/saveSkill'                    , 'HeroController@saveSkill')->name('saveSkill');
    Route::post('/saveSkin'                     , 'HeroController@saveSkin')->name('saveSkin');
    Route::post('/delete-skin'                  , 'HeroController@deletedSkin')->name('deletedSkin');
    Route::post('/setting'                      , 'HeroController@setting')->name('setting');

});

// tournament
Route::group(['prefix' => 'tournament', 'as' => 'tournament.'], function () {
    Route::get('/index'             , 'TourController@index')->name('index');
    Route::get('/add'               , 'TourController@add')->name('add');
    Route::post('/save-tour'        , 'TourController@saveTour')->name('saveTour');
    Route::get('/{tour}/edit'       , 'TourController@edit')->name('edit');
    Route::post('/update-status'    , 'TourController@updateStatus')->name('updateStatus');
});