<?php 

// Menu
Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('/manage', 'UserController@index')->name('index');
    Route::get('/user-datatable', 'UserController@getMenuDatatable')->name('userdatatable');
    Route::get('/add', 'UserController@add')->name('add');
    Route::get('/edit/{id}', 'UserController@edit')->name('edit');
    Route::post('/add-user', 'UserController@addUser')->name('addUser');
    Route::post('/update-status', 'UserController@updateStatus')->name('updateStatus');
});