<?php // Menu

    Route::group(['prefix' => 'menu', 'as' => 'menu.'], function () {
        Route::get('/manage', 'MenuController@index')->name('index');
        Route::get('/datatable', 'MenuController@getMenuDatatable')->name('datatable');
        Route::get('/add', 'MenuController@add')->name('add');
        Route::get('/edit/{id}', 'MenuController@edit')->name('edit');
        Route::post('/save-image', 'MenuController@saveImage')->name('saveImage');
        Route::post('/save-menu', 'MenuController@saveMenu')->name('saveMenu');
        Route::post('/add-menu', 'MenuController@addMenu')->name('addMenu');
        Route::post('/update-status', 'MenuController@updateStatus')->name('updateStatus');
        Route::post('/delete-menu', 'MenuController@deleteMenu')->name('deleteMenu');
    });

    // Page
Route::group(['prefix' => 'page', 'as' => 'page.'], function () {
	Route::get('/manage', 'PageController@index')->name('index');
	Route::get('/datatable', 'PageController@getMenuDatatable')->name('datatable');
	Route::get('/add', 'PageController@add')->name('add');
	Route::get('/edit/{slug}', 'PageController@edit')->name('edit');
	Route::post('/save-element', 'PageController@saveElement')->name('saveElement');
	Route::post('/save-image', 'PageController@saveImage')->name('saveImage');
	Route::post('/save-image-editor', 'PageController@saveImageEditor')->name('saveImageEditor');
	Route::post('/add-section', 'PageController@addSection')->name('addSection');
	Route::post('/add-element', 'PageController@addElement')->name('addElement');
	Route::post('/delete-section', 'PageController@deleteSection')->name('deleteSection');
	Route::post('/delete-element', 'PageController@deleteElement')->name('deleteElement');
        
	Route::post('/update-page', 'PageController@updatePage')->name('updatePage');
        Route::post('/save-img', 'PageController@saveImg')->name('saveImg');
});

// Content
Route::group(['prefix' => 'content', 'as' => 'content.'], function () {
	Route::get('/manage', 'ContentController@index')->name('index');
	Route::get('/datatable', 'ContentController@getMenuDatatable')->name('datatable');
	Route::get('/add', 'ContentController@edit')->name('add');
	Route::get('/edit/{id}', 'ContentController@edit')->name('edit');
	Route::post('/save-image', 'ContentController@saveImage')->name('saveImage');
	Route::post('/save-content', 'ContentController@saveContent')->name('saveContent');
	Route::post('/add-content', 'ContentController@addContent')->name('addContent');
	Route::post('/delete-content', 'ContentController@deleteContent')->name('deleteContent');
	Route::post('/update-status', 'ContentController@updateStatus')->name('updateStatus');
});


// Banner
Route::group(['prefix' => 'banner', 'as' => 'banner.'], function () {
	Route::get('/manage', 'BannerController@index')->name('index');
	Route::get('/datatable', 'BannerController@getDatatable')->name('datatable');
	Route::get('/add', 'BannerController@add')->name('add');
	Route::get('/edit/{id}', 'BannerController@edit')->name('edit');
	Route::post('/save-banner', 'BannerController@saveBanner')->name('saveBanner');
	Route::post('/update-status', 'BannerController@updateStatus')->name('updateStatus');
	Route::post('/delete-banner', 'BannerController@deleteBanner')->name('deleteBanner');
});

// item guide
Route::group(['prefix' => 'itemguide', 'as' => 'itemguide.'], function () {
	Route::get('/manage', 'ItemguideController@index')->name('index');
	Route::get('/datatable', 'ItemguideController@getDatatable')->name('datatable');
	Route::get('/add', 'ItemguideController@add')->name('add');
	Route::get('/edit/{id}', 'ItemguideController@edit')->name('edit');
	Route::post('/save-itemguide', 'ItemguideController@saveItemguide')->name('saveItemguide');
	Route::post('/update-status', 'ItemguideController@updateStatus')->name('updateStatus');
	Route::post('/delete-banner', 'ItemguideController@deleteBanner')->name('deleteBanner');
});