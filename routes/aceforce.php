<?php

// save image
Route::post('/save-image', 'ContentController@saveImage')->name('saveImage');



// banner
Route::group(['prefix' => 'banner', 'as' => 'banner.'], function () {
    Route::get('/manage', 'BannerController@index')->name('index');
    Route::get('/ordering', 'BannerController@ordering')->name('ordering');
    Route::get('/add', 'BannerController@add')->name('add');
    Route::get('/{id}/edit', 'BannerController@edit')->name('edit');
    Route::get('/datatable', 'BannerController@getDatatable')->name('datatable');
    Route::post('/save-banner', 'BannerController@saveBanner')->name('saveBanner');
    Route::post('/update-status', 'BannerController@updateStatus')->name('updateStatus');
    Route::post('/save-order', 'BannerController@saveOrder')->name('saveOrder');
    Route::post('/delete-banner', 'BannerController@deleteBanner')->name('deleteBanner');

});


// setting
Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {
    Route::get('/', 'SettingController@index')->name('index');
    Route::post('/save-header', 'SettingController@saveHeader')->name('saveHeader');
    Route::post('/save-download', 'SettingController@saveDownload')->name('saveDownload');
    Route::post('/save-footer', 'SettingController@saveFooter')->name('saveFooter');
});


// feature
Route::group(['prefix' => 'feature', 'as' => 'feature.'], function () {
    Route::get('/', 'FeatureController@index')->name('index');
    Route::get('/ordering', 'FeatureController@ordering')->name('ordering');
    Route::get('/add', 'FeatureController@add')->name('add');
    Route::get('/datatable', 'FeatureController@getDatatable')->name('datatable');
    Route::post('/save-banner', 'FeatureController@saveBanner')->name('saveBanner');
    Route::post('/save-bg-feature', 'FeatureController@saveBgFeature')->name('saveBgFeature');
    Route::get('/{id}/edit', 'FeatureController@edit')->name('edit');
    Route::post('/update-status', 'FeatureController@updateStatus')->name('updateStatus');
    Route::post('/save-order', 'FeatureController@saveOrder')->name('saveOrder');
    Route::post('/delete-banner', 'FeatureController@deleteBanner')->name('deleteBanner');


});


// news
Route::group(['prefix' => 'content', 'as' => 'content.'], function () {
    Route::get('/manage', 'ContentController@index')->name('index');
    Route::get('/add', 'ContentController@add')->name('add');
    Route::post('/save-content', 'ContentController@saveContent')->name('saveContent');
    Route::get('/datatable', 'ContentController@datatable')->name('datatable');
    Route::get('/{id}/edit', 'ContentController@edit')->name('edit');
    Route::post('/update-status', 'ContentController@updateStatus')->name('updateStatus');
    Route::post('/delete-content', 'ContentController@deleteContent')->name('deleteContent');
    Route::post('/save-bg-content', 'ContentController@saveBgFeature')->name('saveBgFeature');
});


// highlight
Route::group(['prefix' => 'highlight', 'as' => 'highlight.'], function () {
    Route::get('/manage', 'HighlightController@index')->name('index');
    Route::get('/ordering', 'HighlightController@ordering')->name('ordering');
    Route::get('/add', 'HighlightController@add')->name('add');
    Route::get('/{id}/edit', 'HighlightController@edit')->name('edit');
    Route::get('/datatable', 'HighlightController@getDatatable')->name('datatable');
    Route::post('/save-banner', 'HighlightController@saveBanner')->name('saveBanner');
    Route::post('/update-status', 'HighlightController@updateStatus')->name('updateStatus');
    Route::post('/save-order', 'HighlightController@saveOrder')->name('saveOrder');
    Route::post('/delete-banner', 'HighlightController@deleteBanner')->name('deleteBanner');
});



// menu
Route::group(['prefix' => 'menubar', 'as' => 'menubar.'], function () {

    Route::get('/index', 'MenuController@index')->name('index');
    Route::get('/edit/{id}', 'MenuController@edit')->name('edit');
    Route::get('/dataTable', 'MenuController@dataTable')->name('dataTable');
    Route::get('/submenu/{id}', 'MenuController@submenu')->name('submenu');
    Route::get('/create/parent', 'MenuController@createParent')->name('create.parent');
    Route::get('/ordering', 'MenuController@ordering')->name('ordering');
    Route::post('/save', 'MenuController@save')->name('save');
    Route::post('/updateStatus', 'MenuController@updateStatus')->name('updateStatus');
    Route::post('/save/order', 'MenuController@saveOrder')->name('save.order');
    Route::post('/deleteEvent', 'MenuController@deleteEvent')->name('deleteEvent');



    # sub menu bar
    Route::get('/submenu/dataTable/{parent}', 'MenuController@submenuDataTable')->name('submenu.dataTable');
    Route::get('/submenu/create/{parent}', 'MenuController@createSubmenu')->name('createSubmenu');
    Route::get('/edit/submenu/{id}', 'MenuController@editSubmenu')->name('edit.submenu');
    Route::post('/save/submenu', 'MenuController@saveSubmenu')->name('save.submenu');
    Route::post('/updateStatusSubmenu', 'MenuController@updateStatusSubmenu')->name('updateStatusSubmenu');
    Route::post('/sub/deleteEvent', 'MenuController@subDeleteEvent')->name('sub.deleteEvent');


});