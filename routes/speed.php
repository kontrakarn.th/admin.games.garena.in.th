<?php 

// Content
Route::group(['prefix' => 'content', 'as' => 'content.'], function () {
	Route::get('/manage', 'ContentController@index')->name('index');
	Route::get('/datatable', 'ContentController@getMenuDatatable')->name('datatable');
	Route::get('/add', 'ContentController@edit')->name('add');
	Route::get('/edit/{id}', 'ContentController@edit')->name('edit');
	Route::post('/save-image', 'ContentController@saveImage')->name('saveImage');
	Route::post('/save-content', 'ContentController@saveContent')->name('saveContent');
	Route::post('/add-content', 'ContentController@addContent')->name('addContent');
	Route::post('/delete-content', 'ContentController@deleteContent')->name('deleteContent');
	Route::post('/update-status', 'ContentController@updateStatus')->name('updateStatus');
});

