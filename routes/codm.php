<?php

// save image
//Route::post('/save-image', 'ContentController@saveImage')->name('saveImage');

// setting
Route::group(['prefix' => 'setting', 'as' => 'setting.'], function () {
    Route::get('/'                  , 'SettingController@index')->name('index');
    Route::post('/save-header'      , 'SettingController@saveHeader')->name('saveHeader');
    Route::post('/save-download'    , 'SettingController@saveDownload')->name('saveDownload');
    Route::post('/save-footer'      , 'SettingController@saveFooter')->name('saveFooter');
});


// banner
Route::group(['prefix' => 'banner', 'as' => 'banner.'], function () {
    Route::get('/'                  , 'BannerController@index')->name('index');
    Route::get('/add'               , 'BannerController@add')->name('add');
    Route::get('/edit/{banner}'     , 'BannerController@edit')->name('edit');

    // api
    Route::get('/datatable'         , 'BannerController@getDatatable')->name('datatable');
    Route::post('/saveBanner'       , 'BannerController@saveBanner')->name('saveBanner');
    Route::post('/updateStatus'     , 'BannerController@updateStatus')->name('updateStatus');
    Route::post('/deleteBanner'     , 'BannerController@deleteBanner')->name('deleteBanner');
});


// news
Route::group(['prefix' => 'news', 'as' => 'news.'], function () {
    Route::get('/'                  , 'NewsController@index')->name('index');
    Route::get('/add'               , 'NewsController@add')->name('add');
    Route::get('/edit/{news}'       , 'NewsController@edit')->name('edit');
//
    // api
    Route::get('/datatable'         , 'NewsController@getDatatable')->name('datatable');
    Route::post('/saveBanner'       , 'NewsController@saveBanner')->name('saveBanner');
    Route::post('/saveImage'        , 'NewsController@saveImage')->name('saveImage');
    Route::post('/updateStatus'     , 'NewsController@updateStatus')->name('updateStatus');
    Route::post('/updateHighlight'  , 'NewsController@updateHighlight')->name('updateHighlight');
    Route::post('/deleteBanner'     , 'NewsController@deleteBanner')->name('deleteBanner');
});

// Subscribe
Route::group(['prefix' => 'subscribe', 'as' => 'subscribe.'], function () {
    Route::get('/'                  , 'SubscribeController@index')->name('index');
    Route::get('/create'                  , 'SubscribeController@create')->name('create');
    Route::get('/create/page'                  , 'SubscribeController@createPage')->name('create.page');
    Route::get('/edit/{id}'                  , 'SubscribeController@edit')->name('edit');
    Route::get('/datatable'                  , 'SubscribeController@datatable')->name('datatable');
    Route::post('/save-header'      , 'SubscribeController@saveHeader')->name('saveHeader');
    Route::post('/updateStatus'      , 'SubscribeController@updateStatus')->name('updateStatus');
    Route::post('/deleteBanner'      , 'SubscribeController@deleteBanner')->name('deleteBanner');
    Route::post('/setDefault'      , 'SubscribeController@setDefault')->name('setDefault');
    // Route::get('/add'               , 'BannerController@add')->name('add');
    // Route::get('/edit/{subscribe}'     , 'BannerController@edit')->name('edit');
    //
    // // api
    // Route::get('/datatable'         , 'BannerController@getDatatable')->name('datatable');
    // Route::post('/saveBanner'       , 'BannerController@saveBanner')->name('saveBanner');
    // Route::post('/updateStatus'     , 'BannerController@updateStatus')->name('updateStatus');
    // Route::post('/deleteBanner'     , 'BannerController@deleteBanner')->name('deleteBanner');
});


//TV
Route::group(['prefix' => 'tv', 'as' => 'tv.'], function () {
    Route::get('/'                  , 'TVController@index')->name('index');
    Route::get('/create'                  , 'TVController@create')->name('create');
    Route::post('/', 'TVController@store')->name('store');
    Route::post('/vdo/store', 'TVController@storeVdo')->name('vdo.store');
    Route::post('/playlist/store', 'TVController@storePlaylist')->name('playlist.store');
    Route::get('/vdo/edit', 'TVController@editVdo')->name('vdo.edit');
    Route::post('/vdo/update', 'TVController@updateVdo')->name('vdo.update');
    Route::post('/playlist/update', 'TVController@updatePlaylist')->name('playlist.update');
    Route::get('/playlist/edit', 'TVController@editPlaylist')->name('playlist.edit');
    Route::post('/update', 'TVController@update')->name('update');
    Route::post('/setDefault', 'TVController@setDefault')->name('setDefault');
    Route::post('/setMainVDO', 'TVController@setMainVDO')->name('setMainVDO');
    Route::post('/setMainPlaylist', 'TVController@setMainPlaylist')->name('setMainPlaylist');
    Route::get('/datatable', 'TVController@datatable')->name('datatable');
    Route::POST('/datatableVdo', 'TVController@datatableVdo')->name('datatableVdo');
    Route::POST('/datatablePlaylist', 'TVController@datatablePlaylist')->name('datatablePlaylist');
    Route::post('/active', 'TVController@active')->name('active');
    Route::post('/activeVDO', 'TVController@activeVdo')->name('activeVdo');
    Route::post('/activePlaylist', 'TVController@activePlaylist')->name('activePlaylist');
    Route::get('/edit/{id}', 'TVController@edit')->name('edit');
    Route::get('/{id}', 'TVController@show')->name('show');
    Route::post('/delete', 'TVController@destroy')->name('delete');
    Route::post('/vdoList', 'TVController@vdoList')->name('vdoList');
    Route::post('/delete-vdo', 'TVController@deleteVdo')->name('deleteVdo');
    Route::post('/delete-playlist', 'TVController@deletePlaylist')->name('deletePlaylist');
    Route::post('/saveMainVdo', 'TVController@saveMainVdo')->name('saveMainVdo');
    


});
// GAME GUIDES
Route::group(['prefix' => 'gameguides', 'as' => 'gameguides.'], function () {

    Route::group(['prefix' => 'beginners', 'as' => 'beginners.'], function () {
        Route::get('/'                      , 'GameGuideController@beginners')->name('index');
        Route::get('/create'                , 'GameGuideController@createBeginners')->name('create');
        Route::get('/{beginner}/edit'       , 'GameGuideController@editBeginners')->name('edit');
        Route::get('/{beginner}/delete'     , 'GameGuideController@deleteBeginners')->name('delete');
//
        Route::get('/datatable'             , 'GameGuideController@getDatatableBeginners')->name('datatable');
        Route::post('/saveData'             , 'GameGuideController@createBeginnersSaveData')->name('saveData');
        Route::post('/updateStatus'         , 'GameGuideController@updateStatusBeginners')->name('updateStatus');
        Route::post('/delete'               , 'GameGuideController@deleteBeginners')->name('delete');

    });
    Route::group(['prefix' => 'modes', 'as' => 'modes.'], function () {
        Route::get('/'                      , 'GameGuideController@modes')->name('index');
        Route::get('/create'                , 'GameGuideController@createModes')->name('create');
        Route::get('/{beginner}/edit'       , 'GameGuideController@editModes')->name('edit');
        Route::get('/{beginner}/delete'     , 'GameGuideController@deleteModes')->name('delete');
////
        Route::get('/datatable'             , 'GameGuideController@getDatatableModes')->name('datatable');
        Route::post('/saveData'             , 'GameGuideController@createModesSaveData')->name('saveData');
        Route::post('/delete'               , 'GameGuideController@deleteBeginners')->name('delete');

    });

    Route::group(['prefix' => 'operator_skills', 'as' => 'operator_skills.'], function () {
        Route::get('/'                      , 'GameGuideController@operatorSkills')->name('index');
        Route::get('/create'                , 'GameGuideController@createOperatorSkills')->name('create');
        Route::get('/{opera}/edit'          , 'GameGuideController@editOperatorSkills')->name('edit');
        Route::get('/{opera}/delete'        , 'GameGuideController@deleteOperatorSkills')->name('delete');

        Route::post('/saveData'             , 'GameGuideController@createOperatorSkillsSaveData')->name('saveData');
        Route::post('/updateStatus'         , 'GameGuideController@updateStatusOperatorSkills')->name('updateStatus');
    });

    Route::group(['prefix' => 'attachments', 'as' => 'attachments.'], function () {
        Route::get('/'                      , 'GameGuideController@attachments')->name('index');
        Route::get('/create'                , 'GameGuideController@createAttachments')->name('create');
        Route::get('/{attachment}/edit'     , 'GameGuideController@editAttachments')->name('edit');

        Route::get('/datatable'             , 'GameGuideController@getDatatableAttachments')->name('datatable');
        Route::post('/saveData'             , 'GameGuideController@creatAttachmentsSaveData')->name('saveData');
        Route::post('/updateStatus'         , 'GameGuideController@updateStatusAttachments')->name('updateStatus');
        Route::post('/delete'               , 'GameGuideController@deleteAttachments')->name('delete');

    });

    Route::group(['prefix' => 'weapons', 'as' => 'weapons.'], function () {
        Route::get('/'                      , 'GameGuideController@weapons')->name('index');
        Route::get('/create'                , 'GameGuideController@createWeapons')->name('create');
        Route::get('/{weapon}/edit'         , 'GameGuideController@editWeapons')->name('edit');
//
        Route::get('/datatable'             , 'GameGuideController@getDatatableWeapons')->name('datatable');
        Route::post('/saveData'             , 'GameGuideController@creatWeaponsSaveData')->name('saveData');
        Route::post('/updateStatus'         , 'GameGuideController@updateStatusWeapons')->name('updateStatus');
        Route::post('/delete'               , 'GameGuideController@deleteWeapons')->name('delete');

    });

    Route::group(['prefix' => 'perks', 'as' => 'perks.'], function () {
        Route::get('/'                      , 'GameGuideController@perks')->name('index');
        Route::get('/create'                , 'GameGuideController@createPerks')->name('create');
        Route::get('/{perk}/edit'           , 'GameGuideController@editPerks')->name('edit');

        Route::get('/datatable'             , 'GameGuideController@getDatatablePerks')->name('datatable');
        Route::post('/saveData'             , 'GameGuideController@creatPerksSaveData')->name('saveData');
        Route::post('/updateStatus'         , 'GameGuideController@updateStatusPerks')->name('updateStatus');
        Route::post('/delete'               , 'GameGuideController@deletePerks')->name('delete');

    });

    Route::group(['prefix' => 'characters', 'as' => 'characters.'], function () {
        Route::get('/'                      , 'GameGuideController@characters')->name('index');
        Route::get('/create'                , 'GameGuideController@createCharacters')->name('create');
        Route::get('/{character}/edit'      , 'GameGuideController@editCharacters')->name('edit');

        Route::get('/datatable'             , 'GameGuideController@getDatatableCharacters')->name('datatable');
        Route::post('/saveData'             , 'GameGuideController@creatCharactersSaveData')->name('saveData');
        Route::post('/updateStatus'         , 'GameGuideController@updateStatusCharacters')->name('updateStatus');
        Route::post('/delete'               , 'GameGuideController@deleteCharacters')->name('delete');

    });

});
