<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function() {
    return redirect()->to('/auth/login');
});

Route::get('/login', function() {
    return redirect()->to('/auth/login');
})->name('login');

Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function() {
  	Route::get('/login', 'GoogleLoginController@index')->name('login');
    Route::get('/login/google', 'GoogleLoginController@redirectToProvider')->name('login.google');
    Route::get('/callback', 'GoogleLoginController@handleProviderCallback')->name('login.google.callback');

    Route::any('/logout', 'GoogleLoginController@logout')->name('logout');
});

Route::group(['namespace' => 'Admin', 'as' => 'admin.'], function () {

	// Main
	Route::group(['namespace' => 'main' , 'as' => 'main.'], function () {
	    Route::get('/dashboard', 'IndexController@index')->name('index');
	    include('user.php');
	});

	// FIFA Online 4
	Route::group(['prefix' => 'fo4', 'namespace' => 'fo4' , 'as' => 'fo4.'], function () {
    	include('fo4.php');
	});
	
	// DD Tank
	Route::group(['prefix' => 'ddt', 'namespace' => 'ddt' , 'as' => 'ddt.'], function () {
			include('ddt.php');
	});

	// Contra
	Route::group(['prefix' => 'contra', 'namespace' => 'contra' , 'as' => 'contra.'], function () {
		include('contra.php');
	});

	// Speed
	Route::group(['prefix' => 'speed', 'namespace' => 'speed' , 'as' => 'speed.'], function () {
		include('speed.php');
	});

	// Pubg Lite
	Route::group(['prefix' => 'pubglite', 'namespace' => 'pubglite' , 'as' => 'pubglite.'], function () {
		include('pubglite.php');
	});

	// ROV
	Route::group(['prefix' => 'rov', 'namespace' => 'rov' , 'as' => 'rov.'], function () {
		include('rov.php');
	});

	// CODM
	Route::group(['prefix' => 'codm', 'namespace' => 'codm' , 'as' => 'codm.'], function () {
		include('codm.php');
	});

	// Ace Force
	Route::group(['prefix' => 'aceforce', 'namespace' => 'aceforce' , 'as' => 'aceforce.'], function () {
		include('aceforce.php');
	});
	
});


