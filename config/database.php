<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),
    // 'default' => 'fo4',

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'fo4' => [
            'driver' => 'mysql',
            'host' => env('DB_FO4_HOST', '127.0.0.1'),
            'port' => env('DB_FO4_PORT', '3306'),
            'database' => env('DB_FO4_DATABASE', 'forge'),
            'username' => env('DB_FO4_USERNAME', 'forge'),
            'password' => env('DB_FO4_PASSWORD', ''),
            'unix_socket' => env('DB_FO4_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'ddt' => [
            'driver' => 'mysql',
            'host' => env('DB_DDT_HOST', '127.0.0.1'),
            'port' => env('DB_DDT_PORT', '3306'),
            'database' => env('DB_DDT_DATABASE', 'forge'),
            'username' => env('DB_DDT_USERNAME', 'forge'),
            'password' => env('DB_DDT_PASSWORD', ''),
            'unix_socket' => env('DB_DDT_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'contra' => [
            'driver' => 'mysql',
            'host' => env('DB_CONTRA_HOST', '127.0.0.1'),
            'port' => env('DB_CONTRA_PORT', '3306'),
            'database' => env('DB_CONTRA_DATABASE', 'forge'),
            'username' => env('DB_CONTRA_USERNAME', 'forge'),
            'password' => env('DB_CONTRA_PASSWORD', ''),
            'unix_socket' => env('DB_CONTRA_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pubglite' => [
            'driver' => 'mysql',
            'host' => env('DB_PUBGLITE_HOST', '127.0.0.1'),
            'port' => env('DB_PUBGLITE_PORT', '3306'),
            'database' => env('DB_PUBGLITE_DATABASE', 'forge'),
            'username' => env('DB_PUBGLITE_USERNAME', 'forge'),
            'password' => env('DB_PUBGLITE_PASSWORD', ''),
            'unix_socket' => env('DB_PUBGLITE_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'speed' => [
            'driver' => 'mysql',
            'host' => env('DB_SPEED_HOST', '127.0.0.1'),
            'port' => env('DB_SPEED_PORT', '3306'),
            'database' => env('DB_SPEED_DATABASE', 'forge'),
            'username' => env('DB_SPEED_USERNAME', 'forge'),
            'password' => env('DB_SPEED_PASSWORD', ''),
            'unix_socket' => env('DB_SPEED_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'rov' => [
            'driver' => 'mysql',
            'host' => env('DB_ROV_MAINSITE_HOST', '127.0.0.1'),
            'port' => env('DB_ROV_MAINSITE_PORT', '3306'),
            'database' => env('DB_ROV_MAINSITE_DATABASE', 'forge'),
            'username' => env('DB_ROV_MAINSITE_USERNAME', '
            \
            forge'),
            'password' => env('DB_ROV_MAINSITE_PASSWORD', ''),
            'unix_socket' => env('DB_ROV_MAINSITE_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'codm' => [
            'driver' => 'mysql',
            'host' => env('DB_CODM_MAINSITE_HOST', '127.0.0.1'),
            'port' => env('DB_CODM_MAINSITE_PORT', '3306'),
            'database' => env('DB_CODM_MAINSITE_DATABASE', 'forge'),
            'username' => env('DB_CODM_MAINSITE_USERNAME', 'forge'),
            'password' => env('DB_CODM_MAINSITE_PASSWORD', ''),
            'unix_socket' => env('DB_CODM_MAINSITE_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'aceforce' => [
            'driver' => 'mysql',
            'host' => env('DB_ACEFORCE_MAINSITE_HOST', '127.0.0.1'),
            'port' => env('DB_ACEFORCE_MAINSITE_PORT', '3306'),
            'database' => env('DB_ACEFORCE_MAINSITE_DATABASE', 'forge'),
            'username' => env('DB_ACEFORCE_MAINSITE_USERNAME', 'forge'),
            'password' => env('DB_ACEFORCE_MAINSITE_PASSWORD', ''),
            'unix_socket' => env('DB_ACEFORCE_MAINSITE_SOCKET', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'mongodb' => [
            'driver'        => 'mongodb',
            'host'          => env('DB_MONGO_HOST'),
            'port'          => env('DB_MONGO_PORT', '27017'),
            'database'      => env('DB_MONGO_DATABASE', 'forge'),
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],
        
        'codmtv' => [
            'driver' => 'mysql',
            'host' => env('CODMTV_DBHOST', ''),
            'port' => env('CODMTV_DBPORT', ''),
            'database' => env('CODMTV_DBNAME', ''),
            'username' => env('CODMTV_DBUSER', ''),
            'password' => env('CODMTV_DBPASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DATABASE', 0)
        ],

    ],

];
