<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRovBannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rov')->create('mainsite_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->string('head_title',255)->nullable();
            $table->mediumText('description')->nullable();
            $table->string('right_text',255)->nullable();
            $table->string('left_text',255)->nullable();
            $table->string('banner_image',255);
            $table->string('banner_image_mobile',255);
            $table->string('link',255)->nullable();
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->integer('order')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainsite_banners');
    }
}
