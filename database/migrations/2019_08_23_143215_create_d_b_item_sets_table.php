<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBItemSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::connection('rov')->create('mainsite_gameinfo_itemsets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hero_id');
            $table->string('name');
            $table->string('description');
            $table->integer('item1')->nullable();
            $table->integer('item2')->nullable();
            $table->integer('item3')->nullable();
            $table->integer('item4')->nullable();
            $table->integer('item5')->nullable();
            $table->integer('item6')->nullable();

            //
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainsite_gameinfo_itemsets');
    }
}
