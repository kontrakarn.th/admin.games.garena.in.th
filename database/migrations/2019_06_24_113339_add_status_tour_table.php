<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rov')->table('mainsite_tournaments', function (Blueprint $table) {
            $table->enum('status_tour',['comingsoon','registration','expired'])->default('comingsoon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mainsite_tournaments', function (Blueprint $table) {
            //
        });
    }
}
