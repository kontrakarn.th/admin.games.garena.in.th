<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameOperatorSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('codm')->create('mainsite_game_operator_skills', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 100);
            $table->string('description', 255)->nullable();
            $table->string('image', 255);
            $table->string('preview_video', 255)->nullable();
            $table->string('yt', 255)->nullable();
            $table->enum('status',['active','inactive'])->default('inactive');

            //
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainsite_game_operator_skills');
    }
}
