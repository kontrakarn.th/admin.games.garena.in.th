<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameBeginnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('codm')->create('mainsite_game_beginners', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->enum('category',['beginner','mode'])->default('beginner');
            $table->string('slug');

            $table->text('description')->nullable();

            $table->string('image');
            $table->string('image_share')->nullable();

            $table->enum('status',['inactive','active'])->default('inactive');

            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->index('id');
            $table->index('slug');
            $table->index('status');
            $table->index('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainsite_game_beginners');
    }
}
