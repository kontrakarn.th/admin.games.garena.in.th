<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBHeroesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rov')->create('mainsite_heros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug',16)->unique();
            $table->string('image')->nullable();
            $table->string('image_bg')->nullable();
            $table->string('slogan')->nullable();
            $table->string('description')->nullable();
            $table->enum('status',['active','inactive'])->default('active');
            $table->enum('flag',['new','free','none'])->default('none');

            //
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_b_heroes');
    }
}
