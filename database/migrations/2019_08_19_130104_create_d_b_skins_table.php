<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBSkinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rov')->create('mainsite_gameinfo_skins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hero_id');
            $table->string('name');
            $table->string('slug');
            $table->string('image')->nullable();
            $table->string('image_banner')->nullable();
            $table->enum('status',['active','inactive'])->default('active');
            //
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_b_skins');
    }
}
