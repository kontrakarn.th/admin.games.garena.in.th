<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('codm')->create('mainsite_game_attachments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('image');
            $table->string('description')->nullable();;

            $table->enum('type',['scope','magazine','grip','muzzle','laser sight','stock','other']);
            $table->enum('for',['sr','ar','lmg','smg','shotgun']);

            $table->enum('status',['inactive','active'])->default('inactive');

            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->index('id');
            $table->index('status');
            $table->index('type');
            $table->index('for');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainsite_game_attachments');
    }
}
