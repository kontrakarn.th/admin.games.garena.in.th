<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::connection('rov')->create('mainsite_gameinfo_skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hero_id');
            $table->enum('type',[0,1,2,3])->default(0);
            $table->enum('isEx',[0,1])->default(0);
            $table->string('name');
            $table->string('description');
            $table->integer('image');

            //
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainsite_gameinfo_skills');
    }
}
