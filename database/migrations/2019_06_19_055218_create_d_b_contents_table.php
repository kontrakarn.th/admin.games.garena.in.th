<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rov')->create('mainsite_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->string('slug',255);
            $table->mediumText('description');
            $table->mediumText('detail');
            $table->string('content_image',255);
            $table->string('share_image',255)->nullable();
            $table->integer('view')->default('0');
            $table->enum('hilight',['yes','no'])->default('no');
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->unsignedInteger('category_id');

            //
            $table->foreign('category_id')->references('id')->on('mainsite_categories');

            //
            $table->timestamp('show_datetime');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_b_contents');
    }
}
