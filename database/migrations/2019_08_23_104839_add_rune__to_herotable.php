<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRuneToHerotable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rov')->table('mainsite_heros', function (Blueprint $table) {

            $table->integer('rune1')->nullable();
            $table->integer('amount_rune1')->nullable();
            $table->integer('rune2')->nullable();
            $table->integer('amount_rune2')->nullable();
            $table->integer('rune3')->nullable();
            $table->integer('amount_rune3')->nullable();
            $table->integer('rune4')->nullable();
            $table->integer('amount_rune4')->nullable();
            $table->integer('rune5')->nullable();
            $table->integer('amount_rune5')->nullable();
            $table->integer('rune6')->nullable();
            $table->integer('amount_rune6')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mainsite_heros', function (Blueprint $table) {
            //
        });
    }
}
