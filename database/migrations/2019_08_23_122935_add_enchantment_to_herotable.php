<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnchantmentToHerotable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('rov')->table('mainsite_heros', function (Blueprint $table) {


            $table->integer('enchantment1')->nullable()->after('amount_rune6');
            $table->integer('enchantment2')->nullable()->after('enchantment1');
            $table->integer('enchantment3')->nullable()->after('enchantment2');
            $table->integer('enchantment4')->nullable()->after('enchantment3');
            $table->integer('enchantment5')->nullable()->after('enchantment4');
            $table->integer('enchantment6')->nullable()->after('enchantment5');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mainsite_heros', function (Blueprint $table) {
            //
        });
    }
}
