<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeaponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('codm')->create('mainsite_game_weapons', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('series');
            $table->string('image');
            $table->string('description')->nullable();

            $table->enum('type',['sr','ar','lmg','smg','shotgun']);

            $table->integer('percent_damage');
            $table->integer('percent_rof');
            $table->integer('percent_accuracy');
            $table->integer('percent_mobility');
            $table->integer('percent_range');

            $table->integer('camos')->nullable();
            $table->integer('scope')->nullable();
            $table->integer('attachment1')->nullable();
            $table->integer('attachment2')->nullable();
            $table->integer('attachment3')->nullable();

            $table->enum('status',['inactive','active'])->default('inactive');

            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->index('id');
            $table->index('name');
            $table->index('status');
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainsite_game_weapons');
    }
}
